function Nursery:HostChild(kid)
    family_log(kid:GetRenameInitText(), "stays in nursery while parents work")
    self.colonists[#self.colonists + 1] = kid
    kid.nursery = self
end

function Nursery:EvictChild(kid)
    family_log(kid:GetRenameInitText(), "returns home from nursery")
    local idx = table.find(self.colonists, kid)
    if idx then
        table.remove(self.colonists, idx)
    end
    kid.nursery = false
end

local function FindNursery(dome, kid, reserved)
    for _, nursery in pairs(dome and dome.labels.Nursery or empty_table) do
        if nursery:GetFreeSpace() > reserved then
            return nursery
        end
    end
end

local CanWork_Colonist = Colonist.CanWork
function Colonist:CanWork(...)
    if self.stay_with_kids == UIColony.day then
        family_log(self:GetRenameInitText(), "can't work today, stay with kids")
    end
    return self.stay_with_kids ~= UIColony.day and CanWork_Colonist(self, ...)
end

local Work_Colonist = Colonist.Work
function Colonist:Work(...)
    local to_host = {}
    local not_possible = false
    local partner = self.partner
    if not partner or partner.workplace_shift == self.workplace_shift then
        if not partner or self.specialist == "none" and (partner.specialist ~= "none" or self.stat_affection > partner.stat_affection)
                or self.specialist ~= "none" and partner.specialist ~= "none" and self.stat_affection > partner.stat_affection then
            for _, kid in pairs(self.children or empty_table) do
                if kid.traits.Child and (kid.parents[1] == self or not kid.parents[1]) then
                    if kid.workplace and kid.workplace_shift == self.workplace_shift then
                        --in school maybe or other accepting workplace which is ok
                    else
                        local nursery = FindNursery(self.dome, kid, #to_host)
                        if nursery then
                            to_host[#to_host + 1] = { kid, nursery }
                        else
                            not_possible = true
                        end
                    end
                end
            end
        end
    end
    if not_possible then
        family_log(self:GetRenameInitText() .. " must stay with kids")
        if self.traits.Workaholic then
            self:ChangeAffection(-5 * const.Scale.Stat, "Workaholic colonist must stay home with children")
        end
        self.stay_with_kids = UIColony.day
        self:GetFired()
        return false
    else
        for _, v in ipairs(to_host) do
            v[2]:HostChild(v[1])
        end
    end
    return Work_Colonist(self, ...)
end

local StopWorkCycle_Workplace = Workplace.StopWorkCycle
function Workplace:StopWorkCycle(unit)
    StopWorkCycle_Workplace(self, unit)
    for _, kid in pairs(unit.children or empty_table) do
        if kid.nursery then
            kid.nursery:EvictChild(kid)
        end
    end
end