local function showBabyIcon(parent)
    local kids = parent.children
    return kids and #kids > 0 and kids[#kids].traits.ChildBaby
end

function ToggleColonistsView(dialog, button)
    local context = dialog.context
    local interests = (button == "interests")
    local family = (button == "family")
    local traits = (button == "traits")
    dialog.idInterestsTitle:SetVisible(interests)
    dialog.idFamilyTitle:SetVisible(family)
    dialog.idTraitsTitle:SetVisible(traits)
    context.interests = interests
    context.family = family
    local list = dialog.idList
    for _, item in ipairs(list) do
        if #item > 0 then
            local child = item[1] -- item is an XVirtualContent control so we get its child
            child.idInterests:SetVisible(interests)
            child.idFamilyInfo:SetVisible(family)
            child.idTraits:SetVisible(traits)
        end
    end
end

local UpdateUICommandCenterRow_orig = UpdateUICommandCenterRow
function UpdateUICommandCenterRow(self, context, row_type)
    UpdateUICommandCenterRow_orig(self, context, row_type)
    if row_type == "colonist" then
        local root_context = GetDialog(self).context
        local interests = root_context.interests
        local family = root_context.family
        self.idTraits:SetVisible(not interests and not family)
        self.idInterests:SetVisible(interests)
        self.idFamilyInfo:SetVisible(family)
    end
end

function OnMsg.ClassesPostprocess()
    local idxSpecialization, parent = lh.deep_search(XTemplates.InfopanelPerson, "Id", "idSpecialization")
    if idxSpecialization then
        parent.ContextUpdateOnOpen = true
        parent.OnContextUpdate = function(self, context, ...)
            self.idSpecialization:SetVisible(context)
            self.idMarried:SetVisible(context and context.partner)
            self.idBaby:SetVisible(context and (context.traits.ChildBaby or showBabyIcon(context)))
            self.idKids:SetVisible(context and context.traits.ChildParent and not showBabyIcon(context))
            if context then
                self:SetIcon(context:GetInfopanelIcon())
                self:SetRolloverText(T("<FamilyInfo>"))
                self.idSpecialization:SetImage(context.ip_specialization_icon)
            elseif string.find(self.Id, "^idFamily") then
                self:SetIcon("UI/Infopanel/colonist_empty.tga")
                self:SetRolloverText(T("N/A"))
            end
        end
        if not table.find(parent, "Id", "idKids") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
            "__class", "XImage",
            "Id", "idKids",
            "Dock", "box",
            "Image", CurrentModPath .. "UI/Infopanel/colonist_child.tga",
            }), idxSpecialization + 1)
        end
        if not table.find(parent, "Id", "idBaby") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
            "__class", "XImage",
            "Id", "idBaby",
            "Dock", "box",
            "Image", CurrentModPath .. "UI/Infopanel/colonist_baby.tga",
            }), idxSpecialization + 1)
        end
        if not table.find(parent, "Id", "idMarried") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idMarried",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/colonist_married.tga",
            }), idxSpecialization + 1)
        end
    end

    idxSpecialization, parent = lh.deep_search(XTemplates.CommandCenterRow, "Id", "idSpecialization")
    if idxSpecialization then
        if not table.find(parent, "Id", "idKids") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idKids",
                '__context_of_kind', "Colonist",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/pin_child.tga",
                'ImageFit', "smallest",
                'Columns', 2,
                "ContextUpdateOnOpen", true,
                "OnContextUpdate", function(self, context, ...)
                    self:SetVisible(context and context.traits.ChildParent and not showBabyIcon(context))
                end,
            }), idxSpecialization + 1)
        end
        if not table.find(parent, "Id", "idBaby") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idBaby",
                '__context_of_kind', "Colonist",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/pin_baby.tga",
                'ImageFit', "smallest",
                'Columns', 2,
                "ContextUpdateOnOpen", true,
                "OnContextUpdate", function(self, context, ...)
                    self:SetVisible(context and (context.traits.ChildBaby or showBabyIcon(context)))
                end,
            }), idxSpecialization + 1)
        end
        if not table.find(parent, "Id", "idMarried") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idMarried",
                '__context_of_kind', "Colonist",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/pin_married.tga",
                'ImageFit', "smallest",
                'Columns', 2,
                "ContextUpdateOnOpen", true,
                "OnContextUpdate", function(self, context, ...)
                    self:SetVisible(context and context.partner)
                end,
            }), idxSpecialization + 1)
        end
    end

    idxSpecialization, parent = lh.deep_search(XTemplates.PinButton, "Id", "idSpecialization")
    if idxSpecialization then
        if not table.find(parent, "Id", "idKids") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idKids",
                '__context_of_kind', "Colonist",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/pin_child.tga",
                'ZOrder', 2,
                'ImageFit', "stretch",
                'Columns', 2,
            }), idxSpecialization + 1)
        end
        if not table.find(parent, "Id", "idBaby") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idBaby",
                '__context_of_kind', "Colonist",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/pin_baby.tga",
                'ZOrder', 2,
                'ImageFit', "stretch",
                'Columns', 2,
            }), idxSpecialization + 1)
        end
        if not table.find(parent, "Id", "idMarried") then
            lh.safe_insert(parent, PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idMarried",
                '__context_of_kind', "Colonist",
                "Dock", "box",
                "Image", CurrentModPath .. "UI/Infopanel/pin_married.tga",
                'ZOrder', 2,
                'ImageFit', "stretch",
                'Columns', 2,
            }), idxSpecialization + 1)
        end
    end
end

function OnMsg.ClassesBuilt()
    local idxFamilyButton, buttonsParent = lh.deep_search(XTemplates.ColonistsOverview, "ActionId", "family")
    if idxFamilyButton then
        table.remove(buttonsParent, idxFamilyButton)
    end

    local idxHealth, statsParent = lh.deep_search(XTemplates.ColonistsOverview, "comment", "stats: health")
    if not table.find(statsParent, "comment", "stats: affection") then
        lh.safe_insert(statsParent, PlaceObj('XTemplateWindow', {
            'comment', "stats: affection",
            '__class', "XTextButton",
            'Margins', box(25, 0, 0, 0),
            'MinWidth', 36, 'MinHeight', 31,
            'MaxWidth', 36, 'MaxHeight', 31,
            'MouseCursor', "UI/Cursors/Rollover.tga",
            'OnPress', function(self, gamepad)
                SetColonistsSorting(self, "stat_affection")
            end,
            'Image', CurrentModPath .. "UI/Icons/Sections/family_section.tga",
        }), idxHealth + 1)
    end

    local traits, traitsParent = lh.deep_find(XTemplates.ColonistsOverview, "Id", "idTraitsTitle")
    traits.OnContextUpdate = function(self, context, ...)
        self:SetVisible(not context.interests and not context.family)
        XText.OnContextUpdate(self, context, ...)
    end

    if not table.find(traitsParent, "Id", "idFamilyTitle") then
        lh.safe_insert(traitsParent, PlaceObj("XTemplateWindow", {
            "comment", "interests",
            "__class", "XText",
            "Id", "idFamilyTitle",
            "Margins", box(30, 0, 0, 0),
            "Padding", box(0, 0, 0, 0),
            "HAlign", "left",
            "VAlign", "top",
            "MinWidth", 370,
            "MaxWidth", 370,
            "FoldWhenHidden", true,
            "HandleMouse", false,
            "TextStyle", "OverviewItemSection",
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context, ...)
                self:SetVisible(context.family)
                XText.OnContextUpdate(self, context, ...)
            end,
            "Translate", true,
            "Text", T("Family"),
            "TextHAlign", "center",
            "TextVAlign", "center"
        }), #traitsParent + 1)
    end

    local interestsAction = lh.deep_find(XTemplates.ColonistsOverview, "ActionId", "interests")
    interestsAction.OnAction = function(self, host, source)
        ToggleColonistsView(host, "interests")
        host:UpdateActionViews(host.idActionBar)
    end
    local idxTraitsAction, actionsParent = lh.deep_search(XTemplates.ColonistsOverview, "ActionId", "traits")
    actionsParent[idxTraitsAction].OnAction = function(self, host, source)
        ToggleColonistsView(host, "traits")
        host:UpdateActionViews(host.idActionBar)
    end
    actionsParent[idxTraitsAction].ActionState = function(self, host)
        return not host.context.interests and not host.context.family and "hidden"
    end

    if not table.find(actionsParent, "ActionId", "family") then
        lh.safe_insert(actionsParent, PlaceObj('XTemplateAction', {
            'ActionId', "family",
            'ActionName', T("FAMILY"),
            'ActionToolbar', "ActionBar",
            'ActionGamepad', "ButtonY",
            'ActionState', function(self, host)
                return host.context.family and "hidden"
            end,
            'OnAction', function(self, host, source)
                ToggleColonistsView(host, "family")
                host:UpdateActionViews(host.idActionBar)
            end,
        }), idxTraitsAction + 1)
    end

    local row = lh.deep_find(XTemplates.ColonistsOverview, function(o) return o.MinWidth and o.MinWidth > 1000 end)
    if row then --to fit all children
        row.MinWidth = 1100
        row.MaxWidth = 1100
    end

    local idxInterests, parent = lh.deep_search(XTemplates.ColonistOverviewRow, "Id", "idInterests")
    if not table.find(parent, "Id", "idFamilyInfo") then
        local partnerInfo = function()
            return PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelPerson",
                'RolloverTitle', T("Partner"),
                'RolloverHint', T("<left_click> Select  <right_click> Separate"),
                'RolloverHintGamepad', T("<ButtonA> Select  <ButtonX> Separate"),
                'Id', "idFamilyPartner",
                'OnPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Select()
                    end
                end,
                'OnAltPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Separate()
                    end
                end,
            })
        end
        local parentInfo = function(i)
            return PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelPerson",
                'RolloverTitle', T("Parent"),
                'RolloverHint', T("<left_click> Select  <right_click> Remove kid"),
                'RolloverHintGamepad', T("<ButtonA> Select  <ButtonX> Remove kid"),
                'Id', "idFamilyParent" .. i,
                'OnPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Select()
                    end
                end,
                'OnAltPress', function(self, _)
                    if IsValid(self.context) then
                        self.parent.context:RemoveParent(self.context)
                    end
                end,
            })
        end
        local childInfo = function(i)
            return PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelPerson",
                'RolloverTitle', T("Child"),
                'RolloverHint', T("<left_click> Select"),
                'RolloverHintGamepad', T("<ButtonA> Select"),
                'Id', "idFamilyKid" .. i,
                'OnPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Select()
                    end
                end,
            })
        end
        local familyInfo = PlaceObj('XTemplateWindow', {
            '__class', "XContextControl",
            'Id', "idFamilyInfo",
            'HAlign', "center",
            'LayoutMethod', "HWrap",
            "ContextUpdateOnOpen", true,
            'OnContextUpdate', function(self, context, ...)
                for i, win in ipairs(self) do
                    if i == 2 then
                        win:SetContext(context.partner)
                        XRecreateRolloverWindow(win)
                    elseif i == 4 then
                        win:SetContext(context.parents and context.parents[1])
                        XRecreateRolloverWindow(win)
                    elseif i == 5 then
                        win:SetContext(context.parents and context.parents[2])
                        XRecreateRolloverWindow(win)
                    end
                    if i > 6 then
                        local kid = context.children and (i - 6 <= #context.children) and context.children[i - 6]
                        if not kid and i > 7 then
                            win:SetVisible(false)
                        else
                            win:SetContext(kid)
                            XRecreateRolloverWindow(win)
                        end
                    end
                end
            end,
        }, {
            PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelText",
                'TextStyle', "SectionTitle",
                'Text', T("Partner "),
            }),
            partnerInfo(),
            PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelText",
                'TextStyle', "SectionTitle",
                'Text', T("     Parents "),
            }),
            parentInfo(1), parentInfo(2),
            PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelText",
                'TextStyle', "SectionTitle",
                'Text', T("     Children "),
            }),
            childInfo(1), childInfo(2), childInfo(3), childInfo(4), childInfo(5),
        })
        lh.safe_insert(parent, familyInfo, idxInterests + 1)
    end

    local idxAvgHealth, domeStatsParent = lh.deep_search(XTemplates.DomesOverview, "comment", "stats: health")
    if not table.find(domeStatsParent, "comment", "stats: affection") then
        lh.safe_insert(domeStatsParent, PlaceObj('XTemplateWindow', {
            'comment', "stats: affection",
            '__class', "XImage",
            'MinWidth', 65, 'MinHeight', 34,
            'MaxWidth', 65, 'MaxHeight', 34,
            'Image', CurrentModPath .. "UI/Icons/Sections/family_section.tga",
            'ImageFit', "smallest",
        }), idxAvgHealth)
    end

    local idxCitizensInfo, domeParent = lh.deep_search(XTemplates.sectionDome, function(o)
        return o.RolloverText and string.match(TDevModeGetEnglishText(o.RolloverText), "<UISectionCitizensRollover>") end)
    if not table.find(domeParent, "Id", "affectionAverage") then
        lh.safe_insert(domeParent, PlaceObj('XTemplateTemplate', {
            '__template', "InfopanelSection",
            'Id', "affectionAverage",
            'RolloverText', T("The average <em>Affection</em> of all Colonists living in this Dome."),
            'RolloverTitle', T("Average Affection <Stat(AverageAffection)>"),
            'Icon', CurrentModPath .. "UI/Icons/Sections/family_section.tga",
        }, {
            PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelStat",
                'BindTo', "AverageAffection",
            }),
        }), idxCitizensInfo + 1)
    end

    local idxInfo, infoParent = lh.deep_search(XTemplates.ipColonist, function(o)
        return o.RolloverText and string.match(TDevModeGetEnglishText(o.RolloverText), "<UIInfo>") end)
    if not table.find(infoParent, "Id", "family") then
        local partnerInfo = function()
            return PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelPerson",
                'RolloverTitle', T("Partner"),
                'RolloverHint', T("<left_click> Select  <right_click> Separate"),
                'RolloverHintGamepad', T("<ButtonA> Select  <ButtonX> Separate"),
                'Id', "idFamilyPartner",
                'OnPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Select()
                    end
                end,
                'OnAltPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Separate()
                    end
                end,
            })
        end
        local parentInfo = function(i)
            return PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelPerson",
                'RolloverTitle', T("Parent"),
                'RolloverHint', T("<left_click> Select  <right_click> Remove kid"),
                'RolloverHintGamepad', T("<ButtonA> Select  <ButtonX> Remove kid"),
                'Id', "idFamilyParent" .. i,
                'OnPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Select()
                    end
                end,
                'OnAltPress', function(self, _)
                    if IsValid(self.context) then
                        self.parent.context:RemoveParent(self.context)
                    end
                end,
            })
        end
        local childInfo = function(i)
            return PlaceObj('XTemplateTemplate', {
                '__template', "InfopanelPerson",
                'RolloverTitle', T("Child"),
                'RolloverHint', T("<left_click> Select"),
                'RolloverHintGamepad', T("<ButtonA> Select"),
                'Id', "idFamilyKid" .. i,
                'OnPress', function(self, _)
                    if IsValid(self.context) then
                        self.context:Select()
                    end
                end,
            })
        end
        local familyInfo = PlaceObj('XTemplateTemplate', {
            '__template', "InfopanelSection",
            'RolloverTitle', T("Affection <Affection>"),
            'RolloverText', T("<UIFamilyInfo>"),
            'Id', "family",
            'Icon', CurrentModPath .. "UI/Icons/Sections/family_section.tga",
        }, {
            PlaceObj("XTemplateWindow", {
                "__class", "XFrameProgress",
                "Id", "idFamily",
                "Margins", box(10, 15, 10, 14),
                "FoldWhenHidden", true,
                "Image", "UI/Infopanel/progress_bar.tga",
                "FrameBox", box(5, 0, 5, 0),
                "MinProgressSize", 8,
                "BindTo", "Affection",
                "OnContextUpdate", function(self, context, ...)
                    XFrameProgress.OnContextUpdate(self, context, ...)
                    local image = "UI/Infopanel/progress_bar_green.tga"
                    if context.stat_affection < g_Consts.LowStatLevel then
                        image = "UI/Infopanel/progress_bar_red.tga"
                    elseif context.stat_affection < g_Consts.HighStatLevel then
                        image = "UI/Infopanel/progress_bar_orange.tga"
                    end
                    self:SetProgressImage(image)
                end,
                "ProgressImage", "UI/Infopanel/progress_bar_green.tga",
                "ProgressFrameBox", box(4, 0, 4, 0),
            }),
            PlaceObj('XTemplateWindow', {
                '__class', "XContextControl",
                'HAlign', "center",
                'LayoutMethod', "HWrap",
                'OnContextUpdate', function(self, context, ...)
                    for i, win in ipairs(self) do
                        if i == 2 then
                            win:SetContext(context.partner)
                            XRecreateRolloverWindow(win)
                        elseif i == 4 then
                            win:SetContext(context.parents and context.parents[1])
                            XRecreateRolloverWindow(win)
                        elseif i == 5 then
                            win:SetContext(context.parents and context.parents[2])
                            XRecreateRolloverWindow(win)
                        end
                    end
                end,
            }, {
                PlaceObj('XTemplateTemplate', {
                    '__template', "InfopanelText",
                    'TextStyle', "SectionTitle",
                    'Text', T("Partner "),
                }),
                partnerInfo(),
                PlaceObj('XTemplateTemplate', {
                    '__template', "InfopanelText",
                    'TextStyle', "SectionTitle",
                    'Text', T("     Parents "),
                }),
                parentInfo(1), parentInfo(2),
            }),
            PlaceObj('XTemplateWindow', {
                '__class', "XContextControl",
                group = "Infopanel Sections",
                id = "sectionChildrenList",
                'HAlign', "center",
                'LayoutMethod', "HWrap",
                'OnContextUpdate', function(self, context, ...)
                    for i, win in ipairs(self) do
                        if i > 1 then
                            local kid = context.children and (i - 1 <= #context.children) and context.children[i - 1]
                            if not kid and i > 2 then
                                win:SetVisible(false)
                            else
                                win:SetContext(kid)
                                XRecreateRolloverWindow(win)
                            end
                        end
                    end
                end,
            }, {
                PlaceObj('XTemplateTemplate', {
                    '__template', "InfopanelText",
                    'TextStyle', "SectionTitle",
                    'Text', T("Children "),
                }),
                childInfo(1), childInfo(2), childInfo(3), childInfo(4), childInfo(5),
            }),
        })
        lh.safe_insert(infoParent, familyInfo, idxInfo + 1)
    end
end

local function GetAffectionLog(colonist)
    local t = {}
    local stat_scale = const.Scale.Stat
    local log = colonist.log_affection or empty_table
    local title = false
    local time = colonist.city.day - 5
    for i = 1, #log, 3 do
        if log[i] > time then
            local sol, amount, reason = log[i], log[i + 1], log[i + 2]
            if reason then
                local color = amount > 0 and TLookupTag("<green>") or TLookupTag("<red>")
                local amount_text = amount > 0 and Untranslated("+" .. (amount/stat_scale)) or (amount/stat_scale)
                local reason_text = T { "Sol <sol>: <clr><u(reason)> <amount></color>", amount = amount_text, clr = color, reason = reason, sol = sol }
                if not title then
                    t[#t + 1] = T(" ")
                    t[#t + 1] = T(4382, "Log of recent changes:")
                    title = true
                end
                t[#t + 1] = reason_text
            end
        end
    end
    return #t > 0 and table.concat(t, "<newline><left>") or false
end

function Colonist:GetFamilyInfo()
    local t = {
        T { "Name<right><RenameInitText><newline>", self },
        T { "Sols on Mars<right><sols>", self },
        T { "Sex<right><Gender>", self },
        T { "Birthplace<right><UIBirthplace>", self }
    }
    local text = table.concat(t, "<newline><left>")
    local log = GetAffectionLog(self)
    if log then
        text = text .. "<newline><left>" .. log
    end
    return text
end

function Colonist:GetUIFamilyInfo()
    local t = {}
    if self.partner then
        t[1] = T { "In a relationship with <partner>.", partner = self.partner:GetRenameInitText() }
    elseif self:GetAffection() < 60 and not self.traits.Child then
        t[1] = T("Living alone for now.")
    elseif not self.traits.Child then
        t[1] = T("Looking for a partner.")
    elseif self:GetAffection() < 60 then
        t[1] = T("A sad kid neglected by his family.")
    else
        t[1] = T("A happy kid looked after by his family.")
    end
    if self.traits.Child then
    elseif self.children and #self.children > 1 then
        t[2] = T { "Has <kids> children.", kids = #self.children }
    elseif self.children and #self.children == 1 then
        t[2] = T("Has one child.")
    else
        t[2] = T("Has no children.")
    end
    local text = table.concat(t, "<newline><left>")
    local log = GetAffectionLog(self)
    if log then
        text = text .. "<newline><left>" .. log
    end
    return text
end

local UIStatUpdate_Colonist = Colonist.UIStatUpdate
function Colonist:UIStatUpdate(win, stat)
    UIStatUpdate_Colonist(self, win, stat)
    if stat == "Affection" then --hide it in the array, is shown custom
        win:SetVisible(false)
    end
end