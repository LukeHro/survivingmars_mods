local stat_scale = const.Scale.Stat
local max_stat = stat_scale * 100
local stat_modifier = 5 * stat_scale

local function InitFamilies()
    fam_FormCouples = true
    fam_AssignChildToFamily = true
    fam_ChangeNameOnMarriage = true
    fam_SameSexMarriageChance = 20
end
OnMsg.CityStart = InitFamilies
OnMsg.LoadGame = InitFamilies

local vanilla_GenerateColonistData = GenerateColonistData
function GenerateColonistData(...)
    local colonist = vanilla_GenerateColonistData(...)
    if colonist.age_trait == "Child" then
        colonist.age_trait = "ChildBaby"
        colonist.traits.ChildBaby = true
    end
    return colonist
end

local UpdateHomelessLabels_Colonist = Colonist.UpdateHomelessLabels
function Colonist:UpdateHomelessLabels(...)
    if self.traits.ChildBaby and not self:IsOrphan() then
        return
    end
    UpdateHomelessLabels_Colonist(self, ...)
end

local OnDisappear_Colonist = Colonist.OnDisappear
function Colonist:OnDisappear(...)
    if self.age_trait == "ChildBaby" then
        self:SetResidence(false)
        self:ClearTransportRequest()
        UpdateAttachedSign(self)
        self:SetOutside(false)
        --removed clear dome
    else
        OnDisappear_Colonist(self)
    end
end

local UpdateAgeTrait_Colonist = Colonist.UpdateAgeTrait
function Colonist:UpdateAgeTrait(...)
    local age = self.age_trait
    UpdateAgeTrait_Colonist(self, ...)
    if age == "ChildBaby" and age ~= self.age_trait then
        if self.dome and self:GetMapID() ~= self.dome:GetMapID() then
            self:TransferToMap(self.dome:GetMapID())
        end
        self:Appear("RandomDome")
        self:UpdateResidence()
        self:AddToLabels()
        Msg("BabyBecameChild", self)
    end
end

function OnMsg.ClassesBuilt()
    table.insert(ColonistStatList, 1, "Affection")
    ColonistStat.Affection = { value = "Affection", text = T("Affection"), log = "log_affection",
                               description = T("Represents how does a colonist care about his relatives and is involved in their lives. Lowered by living separated or when they detach themselves because unhappy events. Increases when they come close together."),
                               icon = CurrentModPath .. "UI/Icons/Sections/family_section.tga" }
    ColonistStatReasons["+Affection"] = T("Happy family life <amount> (Affection)")
    ColonistStatReasons["-Affection"] = T("Bad family life <amount> (Affection)")

    if const.ColonistAges[1] ~= "ChildBaby" then
        table.insert(const.ColonistAges, 1, "ChildBaby")
    end
    const.ColonistAgeGroups.ChildBaby = {
        min = 0,
        next_agegroup = "Child",
        display_name = T("Baby"),
        description = T("A small baby under care of its parents")
    }
    const.ColonistAgeGroups.Child.min = 2
    const.ColonistAgeGroups.Youth.min = 8
    const.ColonistAgeGroups.Adult.min = 16
    const.ColonistAgeGroups["Middle Aged"].min = 36
    const.ColonistAgeGroups.Senior.min = 61

    for name, data in sorted_pairs(const.ColonistAgeGroups) do
        if not table.find(Colonist.properties, "id", "MinAge_" .. name) then
            table.insert(Colonist.properties, {
                category = "Trait",
                id = "MinAge_" .. name,
                name = T { 4299, "Min age <display_name>", data },
                default = data.min, editor = "number",
                help = T { 4300, "Min age for <name>", data },
                modifiable = true }
            )
        end
        Colonist:SetProperty("MinAge_" .. name, data.min)
    end
end

function OnMsg.ChildBorn(child, parent1, parent2)
    child.age_trait = "ChildBaby"
    child:AddTrait(child.age_trait)
    if not parent1 and not parent2 then
        family_log(child:GetRenameInitText() .. " found without a known parent")
        child:SetCommand("Idle")
    else
        if parent1 then
            local children = #parent1.children
            parent1:ChangeAffection(3 * stat_modifier - children * stat_modifier, children <= 1 and "Has a child" or "Has another child")
        end
        if parent2 then
            local children = #parent1.children
            parent1:ChangeAffection(3 * stat_modifier - children * stat_modifier, children <= 1 and "Has a child" or "Has another child")
        end
        family_log((parent1 and parent1:GetRenameInitText() or "") .. (parent1 and parent2 and " and " or "")
                .. (parent2 and parent2:GetRenameInitText() or "") .. " got a child: " .. child:GetRenameInitText())
        --why SetCommand not working here, also disappear while baby regardless?
        CreateGameTimeThread(function()
            child:Disappear()
        end)
    end
end

function OnMsg.CoupleFormed(colonist, partner)
    colonist:ChangeAffection(2 * stat_modifier, "Found partner")
    partner:ChangeAffection(2 * stat_modifier, "Found partner")
    family_log(colonist:GetRenameInitText() .. " and " .. partner:GetRenameInitText() .. " formed a couple.")
end

function OnMsg.ColonistDied(colonist, reason)
    local old_age = (reason == "Old age")
    local partner = colonist.partner
    if partner then
        if old_age then
            colonist.partner:ChangeAffection(-6 * stat_modifier, "Partner died of old age")
        else
            colonist.partner:ChangeAffection(-10 * stat_modifier, "Partner died of unnatural cause")
        end
    end
    for _, parent in pairs(colonist.parents or empty_table) do
        if old_age then
            parent:ChangeAffection((colonist.traits.Child and -8 or -4) * stat_modifier, "Child died of old age")
        else
            parent:ChangeAffection((colonist.traits.Child and -10 or -4) * stat_modifier, "Child died of unnatural cause")
        end
    end
    for _, kid in ipairs(colonist.children or empty_table) do
        if old_age then
            kid:ChangeAffection((kid.traits.Child and -6 or -2) * stat_modifier, "Parent died of old age")
        else
            kid:ChangeAffection((kid.traits.Child and -10 or -4) * stat_modifier, "Parent died of unnatural cause")
        end
        if kid.traits.ChildBaby and kid:IsOrphan() then
            family_log(kid:GetRenameInitText(), "became orphan")
            if kid.dome and kid:GetMapID() ~= kid.dome:GetMapID() then
                kid:TransferToMap(kid.dome:GetMapID())
            end
            kid:Appear("RandomDome")
            kid:UpdateResidence()
            kid:AddToLabels()
        end
    end
end

function Dome:GetAverageAffection()
    return GetAverageStat(self.labels.Colonist, "Affection")
end

local GetStat_Colonist = Colonist.GetStat
function Colonist:GetStat(stat)
    if stat == "Affection" then
        return self.stat_affection
    end
    return GetStat_Colonist(self, stat)
end

local ChangeStat_Colonist = Colonist.ChangeStat
function Colonist:ChangeStat(stat, amount, reason, time)
    if stat ~= "Affection" then
        return ChangeStat_Colonist(self, amount, reason, time)
    end
    return self:ChangeAffection(amount, reason)
end

function Colonist:GetAffection()
    return self.stat_affection and self.stat_affection / stat_scale or 60
end

function Colonist:ChangeAffection(delta, reason)
    self.stat_affection = self.stat_affection + delta
    self.stat_affection = Clamp(self.stat_affection, 0, max_stat)
    self.log_affection = self.log_affection or {}
    self:LogStatClear(self.log_affection, self.city.day - 5)
    if reason then
        self:AddToLog(self.log_affection, delta, reason)
    end
end

local UpdateMorale_Colonist = Colonist.UpdateMorale
function Colonist:UpdateMorale(...)
    local base = self.base_morale
    local stat_affection = self:GetAffection()
    if stat_affection >= g_Consts.HighStatLevel then
        self.base_morale = self.base_morale + g_Consts.HighStatMoraleEffect
    elseif stat_affection < g_Consts.LowStatLevel then
        self.base_morale = self.base_morale - g_Consts.LowStatMoraleEffect
    end
    UpdateMorale_Colonist(self, ...)
    self.base_morale = base
end

local DailyUpdaye_Colonist = Colonist.DailyUpdate
function Colonist:DailyUpdate(...)
    DailyUpdaye_Colonist(self, ...)
    if self.traits.Child then
        if self.stat_affection <= 0 then
            self:ChangeHealth(-stat_modifier, "<red>All alone on this planet </red>")
        end
        if self.stat_affection <= 20 * stat_scale then
            self:ChangeSanity(-stat_modifier, "<red>Becoming an adult with issues </red>")
        end
        if self.stat_affection <= 40 * stat_scale then
            self:ChangeComfort(-stat_modifier, "<red>Wishing a happy family </red>")
        end
        if self.stat_affection >= 60 * stat_scale then
            self:ChangeComfort(stat_modifier, "<green>Living in a happy family </green>")
        end
        if self.stat_affection >= 80 * stat_scale then
            self:ChangeSanity(stat_modifier, "<green>Becoming a fine adult </green>")
        end
    end
    if self.partner and self.stat_affection < g_Consts.LowStatLevel then
        self:ChangeAffection(-stat_modifier, "Partner is unhappy in the relation")
    elseif self.stat_affection >= g_Consts.HighStatLevel and self.partner then
        self:ChangeAffection(stat_modifier, "Partner is happy in the relation")
    end

    local family_morale_delta = 0
    local child_affection_delta = 0
    for _, kid in ipairs(self.children or empty_table) do
        if kid.traits.Child and not self.traits.ChildBaby and self.residence ~= kid.residence then
            child_affection_delta = child_affection_delta - 5
        end
        if kid.stat_affection < g_Consts.LowStatLevel then
            family_morale_delta = family_morale_delta - 1
        elseif kid.stat_affection > g_Consts.HighStatLevel then
            family_morale_delta = family_morale_delta + 1
        end
    end
    local parents = self.parents or empty_table
    for _, parent in pairs(parents) do
        if not Colonist.IsDead(parent) then
            if parent.stat_morale < g_Consts.LowStatLevel then
                family_morale_delta = family_morale_delta - 1
            elseif parent.stat_morale > g_Consts.HighStatLevel then
                family_morale_delta = family_morale_delta + 1
            end
        end
    end
    if self.traits.Child and not self.traits.ChildBaby and not self:IsOrphan() then
        if parents[1] and parents[1].residence == self.residence and parents[2] and parents[2].residence == self.residence then
            self:ChangeAffection(stat_modifier, "Lives with both parents")
        elseif (not parents[1] or parents[1].residence ~= self.residence) and (not parents[2] or parents[2].residence ~= self.residence) then
            self:ChangeAffection(-2 * stat_modifier, "Lives without the parents")
        end
    elseif self.residence and not self.partner and (not self.traits.Child or self:IsOrphan()) then
        if self.stat_affection < g_Consts.HighStatLevel then
            self:ChangeAffection(stat_modifier, "Getting back to a normal life")
        end
    elseif self.partner and not self.partner.disappeared then
        if self.partner.stat_morale < g_Consts.LowStatLevel then
            family_morale_delta = family_morale_delta - 1
        elseif self.partner.stat_morale > g_Consts.HighStatLevel then
            family_morale_delta = family_morale_delta + 1
        end
        if self.partner.residence ~= self.residence then
            self:ChangeAffection(-2 * stat_modifier, "Living separated from the partner")
        elseif child_affection_delta == 0 then
            self:ChangeAffection(stat_modifier, "Living with the whole family")
        end
    end
    if child_affection_delta ~= 0 then
        self:ChangeAffection(child_affection_delta * stat_scale,"Has children living in different place")
    end
    if family_morale_delta ~= 0 then
        self:ChangeAffection(family_morale_delta * stat_scale,"Family influence")
    end

    if self.stat_affection == 0 then
        if self.partner and self.partner.stat_affection < g_Consts.LowStatLevel then
            self:Separate()
        end
        for _, kid in ipairs(self.children or empty_table) do
            if kid.stat_affection < g_Consts.LowStatLevel then
                kid:RemoveParent(self)
            end
        end
    end
end

function Colonist:Separate()
    local partner = self.partner
    if partner then
        self.partner = false
        partner.partner = false
        if partner.stat_affection > g_Consts.LowStatLevel then
            partner:ChangeAffection(-6 * stat_modifier, "Separated")
        end
        if self.stat_affection > g_Consts.LowStatLevel then
            self:ChangeAffection(-6 * stat_modifier, "Separated")
        end
        family_log(self:GetRenameInitText() .. " and " .. partner:GetRenameInitText() .. " got separated.")
    end
end

function Colonist:RemoveParent(parent)
    if not self.parents then
        return
    end
    local idx = table.find(self.parents, parent)
    if idx then
        self.parents[idx] = false
    end
    idx = table.find(parent.children or empty_table, self)
    if idx then
        table.remove(parent.children, idx)
    end
    if self.stat_affection > g_Consts.LowStatLevel then
        self:ChangeAffection(-8 * stat_modifier, "Taken away from parents")
    end
    if parent.stat_affection > g_Consts.LowStatLevel then
        parent:ChangeAffection(-8 * stat_modifier, "Abandoned by parent")
    end
    family_log(self:GetRenameInitText() .. " was abandoned by " .. parent:GetRenameInitText())
end