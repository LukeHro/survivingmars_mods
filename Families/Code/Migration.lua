function MustStayWithKids(colonist)
    if not colonist.residence or not colonist.children then
        return false
    end
    if colonist.partner and colonist.residence == colonist.partner.residence then
        return false
    end
    for _, kid in pairs(colonist.children) do
        if not kid.traits.ChildBaby and colonist.residence == kid.residence then
            return true
        end
    end
end

local UpdateResidence_Colonist = Colonist.UpdateResidence
function Colonist:UpdateResidence(...)
    if self.traits.Child and not self.residence then
        if self.traits.ChildBaby and not self:IsOrphan() then
            return
        end
        local residence = self:CheckForcedResidence()
        if residence and not residence:CanReserveResidence(self) then
            local youngest residence:KickYoungestSingleResident()
            if youngest then
                family_log(self:GetRenameInitText(), "takes residence from ", youngest:GetRenameInitText())
            else
                family_log(self:GetRenameInitText(), "can't replace someone in family residence")
            end
        end
    end
    UpdateResidence_Colonist(self, ...)
end

local SetDome_Colonist = Colonist.SetDome
function Colonist:SetDome(dome, ...)
    SetDome_Colonist(self, dome, ...)
    for _, kid in pairs(self.children or empty_table) do
        if kid.traits.ChildBaby and (kid.parents[1] == self or kid.parents[2] == self) then
            kid.dome = self.dome
        end
    end
end

local FindEmigrationCommunities_Colonist = Colonist.FindEmigrationCommunities
function Colonist:FindEmigrationCommunities(pos, need_work, eval_threshold, work_threshold, home_threshold, ignore_community)
    if (self.traits.Child or self.traits.ChildBaby) and self:IsOrphan() then
        local dome = ignore_community or self.dome
        local has_free = dome and dome.free_spaces and dome.free_spaces.traits and dome.free_spaces.traits.Child and dome.free_spaces.traits.Child > 0
        return FindEmigrationCommunities_Colonist(self, pos, need_work, eval_threshold, work_threshold, has_free, ignore_community)
    end
    return FindEmigrationCommunities_Colonist(self, pos, need_work, eval_threshold, work_threshold, home_threshold, ignore_community)
end

function Colonist:GetFamilyResidence()
    local residence = false
    local parents = self.parents
    if parents and self.traits.Child then
        if not Colonist.IsDead(parents[1]) and IsValid(parents[1].residence) and self:GetMapID() == parents[1].residence:GetMapID() then
            residence = parents[1].residence
        end
        if not Colonist.IsDead(parents[2]) and IsValid(parents[2].residence) and self:GetMapID() == parents[2].residence:GetMapID() then
            if not residence or not residence:CanReserveResidence() or parents[2].residence.service_comfort > residence.service_comfort then
                residence = residence and residence == self.residence and residence
                        or parents[2].residence:CanReserveResidence() and parents[2].residence or residence
            end
        end
    end
    local partner = self.partner
    if partner and partner.residence and partner.residence ~= self.residence then
        residence = (MustStayWithKids(self) or not partner.residence.parent_dome:ChooseWorkplace(self)) and self.residence
                or (not residence or partner.residence.service_comfort >= residence.service_comfort) and partner.residence
    end
    local dome = IsKindOf(residence,"Community") and residence or residence and residence.parent_dome
    --residence with no dome is best guess for outside residences, if any added
    if dome and dome:GetScoreFor(self.traits) >= 0 or residence and not dome then
        return residence, dome or residence
    end
    family_log(self:GetRenameInitText(), " not allowed in family residence's dome")
end

--TODO only if colonist.affection > g_Consts.HighStatLevel follow if there's no work, stuff like that

local CheckForcedResidence_Colonist = Colonist.CheckForcedResidence
function Colonist:CheckForcedResidence()
    local forced = CheckForcedResidence_Colonist(self)
    if forced then
        return forced
    end
    local residence = self:GetFamilyResidence()
    if residence and residence ~= self.residence then
        if residence:CanReserveResidence(self) or residence:KickYoungestSingleResident() then
            family_log(self:GetRenameInitText(), "moving to family residence")
            return residence
        end
        family_log(self:GetRenameInitText(), "has no room in family residence")
    end
    return false
end

local CheckForcedDome_Colonist = Colonist.CheckForcedDome
function Colonist:CheckForcedDome()
    local forced = CheckForcedDome_Colonist(self)
    if forced then
        return forced
    end
    local residence, dome = self:GetFamilyResidence()
    if residence then
        if residence:CanReserveResidence(self) or residence:KickYoungestSingleResident() or dome:HasFreeLivingSpace() or self.traits.Child then
            family_log(self:GetRenameInitText(), "moving to family dome")
            return dome
        end
        family_log(self:GetRenameInitText(), "has no room in family dome")
    end
    return false
end

local IsSuitable_Residence = Residence.IsSuitable
function Residence:IsSuitable(colonist)
    if colonist.traits.ChildBaby and not self.children_only then
        return false
    end
    if not colonist.traits.Child then
        if MustStayWithKids(colonist) then
            return self == colonist.residence
        end
        return IsSuitable_Residence(self, colonist)
    end
    --child
    if self == colonist.residence then
        return true
    end
    for _, parent in pairs(colonist.parents or empty_table) do
        if not Colonist.IsDead(parent) and parent.residence and parent.residence == self then
            return true
        end
    end
    return colonist:IsOrphan() and self.children_only and IsSuitable_Residence(self, colonist)
end

local TraitFilterColonist_vanilla = TraitFilterColonist
function TraitFilterColonist(trait_filter, unit_traits)
    if unit_traits.ChildBaby then
        return 100000
    end
    return TraitFilterColonist_vanilla(trait_filter, unit_traits)
end

local KickOldestResident_Residence = Residence.KickOldestResident
function Residence:KickOldestResident()
    return self:KickOldestSingleResident() or KickOldestResident_Residence(self)
end

function Residence:KickOldestSingleResident()
    local oldest_resident = false
    for _, resident in pairs(self.colonists) do
        if not resident.partner and not resident.traits.Child and (not lh.safe_notEmpty(resident.children))
                and (not oldest_resident or oldest_resident.age < resident.age) then
            oldest_resident = resident
        end
    end
    if oldest_resident then
        self:KickResident(oldest_resident)
    end
    return oldest_resident
end

function Residence:KickYoungestSingleResident()
    --maybe better to kick youngest to give him a better chance to find a mate elsewhere
    local youngest_resident = false
    for _, resident in pairs(self.colonists) do
        if not resident.partner and not resident.traits.Child and (not lh.safe_notEmpty(resident.children))
                and (not youngest_resident or youngest_resident.age > resident.age and not resident.traits.Child) then
            youngest_resident = resident
        end
    end
    if youngest_resident then
        self:KickResident(youngest_resident)
    end
    return youngest_resident
end

local GatherAvailableColonists_CargoTransporter = CargoTransporter.GatherAvailableColonists
function CargoTransporter:GatherAvailableColonists(count, specialization, quick_load, transfer_available)
    local list = GatherAvailableColonists_CargoTransporter(self, 4 * count, specialization, quick_load, true)
    family_log("need", count, specialization, "found", #list, "transfer_available=", transfer_available)
    local crew = {}
    for _, unit in pairs(list) do
        if not unit.traits.Child and not unit.traits.ChildParent and Colonist.IsDead(unit.partner) then
            table.insert(crew, unit)
            if #crew >= count then
                family_log("returning singles")
                return crew
            end
        end
    end
    family_log("found", #crew, "singles")
    for _, unit in pairs(list) do
        if not unit.traits.Child and not unit.traits.ChildParent and unit.partner
                and not unit.partner.traits.ChildParent and table.find(list and unit.partner) then
            table.insert_unique(crew, unit)
            if #crew < count then
                table.insert_unique(unit.partner)
            end
            if #crew >= count then
                family_log("returning with pair")
                return crew
            end
        end
    end
    family_log("found", #crew, "with pair")
    if transfer_available then
        return crew
    end
    for _, unit in pairs(list) do
        if not unit.traits.Child and not unit.traits.ChildParent and not table.find(crew, unit) then
            table.insert(crew, unit)
            if #crew >= count then
                family_log("returning not parents")
                return crew
            end
        end
    end
    family_log("found", #crew, "not parents")
    for _, unit in pairs(list) do
        if not unit.traits.Child and not table.find(crew, unit) then
            table.insert(crew, unit)
            if #crew >= count then
                family_log("returning not children")
                return crew
            end
        end
    end
    family_log("found", #crew, "not children")
    for _, unit in pairs(list) do
        if not unit.traits.ChildBaby and not table.find(crew, unit) then
            table.insert(crew, unit)
            if #crew >= count then
                family_log("returning not babies")
                return crew
            end
        end
    end
    family_log("found", #crew, "not babies")
    for _, unit in pairs(list) do
        if not table.find(crew, unit) and IsKindOf(self, "LanderRocketBase") and MainCity.map_id ~= self:GetMapID() then
            --to return from asteroids
            table.insert(crew, unit)
            if #crew >= count then
                family_log("returning needed")
                return crew
            end
        end
    end
    family_log("found only", #crew)
    return #crew >= count and crew or {}
end