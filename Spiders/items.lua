return {
PlaceObj('ModItemAnimal', {
	PastureClass = "InsidePasture",
	air_consumption = 300,
	comment = "Spider...",
	description = T(701567391829, --[[ModItemAnimal LH_Spider description]] "Looks like there is a spider infestation in this Dome.\n\nComfort of colonists is lowered and they can even get infected.\nSecurity Stations personnel or any Officers can kill them."),
	display_icon = "UI/Icons/Buildings/animal_chicken.tga",
	display_name = T(727594953305, --[[ModItemAnimal LH_Spider display_name]] "Spider"),
	entities = {
		"SpiderBlue",
		"SpiderGreen",
		"SpiderRed",
		"SpiderYellow",
	},
	food = 200,
	grazing_spot = "Chicken",
	group = "Pest",
	health = 20,
	herd_size = 25,
	id = "LH_Spider",
	lifetime = 720000,
	water_consumption = 300,
}),
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemEntity', {
	'name', "SpiderBlue",
	'entity_name', "SpiderBlue",
	'import', "Entities/SpiderBlue.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "SpiderGreen",
	'entity_name', "SpiderGreen",
	'import', "Entities/SpiderGreen.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "SpiderRed",
	'entity_name', "SpiderRed",
	'import', "Entities/SpiderRed.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "SpiderYellow",
	'entity_name', "SpiderYellow",
	'import', "Entities/SpiderYellow.ent",
}),
PlaceObj('ModItemTraitPreset', {
	auto = false,
	category = "Negative",
	daily_update_func = function (colonist, trait)
        colonist:ChangeHealth(-trait.param * const.Scale.Stat, trait.id)
end,
	description = T(973012538220, --[[ModItemTraitPreset LH_Poisoned description]] "Loses Health each day. Can be cured in medical buildings."),
	display_name = T(403771989521, --[[ModItemTraitPreset LH_Poisoned display_name]] "Poisoned"),
	dome_filter_only = true,
	group = "Negative",
	hidden_on_start = true,
	id = "LH_Poisoned",
	incompatible = {},
	initial_filter = true,
	name = "LH_Poisoned",
	param = 10,
}),
}
