DefineClass.Pest = {
    __parents = { "BasePet" },
}

function Pest:GameInit()
    self.dome:AddToLabel("Pest", self)
    self.dome:RemoveFromLabel("Pet", self)
    self.dome.dome_comfort = self.dome.dome_comfort - 500
    self.display_name = self:GetPreset().display_name
    self.description = self:GetPreset().description
    self.health = self:GetPreset().health
    self:SetScale(100 - self.dome:Random(5) * 10)
end

function Pest:Done()
    self.dome:RemoveFromLabel("Pest", self)
    self.dome.dome_comfort = self.dome.dome_comfort + 500
end

function Pest:Roam(duration)
    local tEnd = GameTime() + duration
    while tEnd - GameTime() > 0 do
        if not IsValid(self.dome) then
            self:SetCommand("Die")
        end
        self:GoToRandomPosInDome(self.dome)
        self:PlayState("idle", -1000)
    end
end

local last_colonist_target --workaround for game freeze
function Pest:Idle()
    local dome = self.dome
    if dome and dome.labels.Colonist and (#dome.labels.Colonist > 0) then
        local colonist = FindNearestObject(dome.labels.Colonist, self:GetPos(), function(obj)
            return IsValid(obj) and not obj:IsDying() and not obj:IsDead() and not obj:IsVisitingBuilding()
        end)
        if colonist then
            if last_colonist_target ~= colonist.handle then
                last_colonist_target = colonist.handle
                return self:SetCommand("InterceptColonist", colonist, dome)
            end
        end
    end
    self:Roam(const.HourDuration)
end
function Pest:Spawn()
    self:SetPos(self.dome:GetSpotPos(self.dome:GetRandomSpot("Entrance")))
end

function Pest:Die()
    self:PlayState("die")
    DoneObject(self)
end

function Pest:CanInfect(colonist, ignore_command)
    if not ignore_command and self.command == "InterceptColonist" then
        return false
    end
    if self:GetVisualDist2D(colonist) < (10 * guim) then
        if colonist.specialist and (colonist.specialist == "security") then
            colonist:SetCommand("InterceptPest", self, self.dome)
        else
            self.dome:AddPanicMarker(colonist:GetPos(), guim, const.HourDuration / 60)
            local panic_pos, panic_radius = colonist:ShouldPanic(self.dome)
            if panic_pos then
                colonist:SetCommand("Panic", panic_pos, panic_radius)
            end
        end
    end
    if GetDomeAtPoint(GetObjectHexGrid(self), self) ~= GetDomeAtPoint(GetObjectHexGrid(colonist), colonist)
            or self:IsVisitingBuilding() or self:GetVisualDist2D(colonist) > (2 * guim) then
        return false
    end
    return IsObjectVisibleFromPoint(colonist, self:GetVisualPos() + point(0, 0, 180 * guic))
end

function Pest:InterceptColonistMoveSleep(...)
    Unit.MoveSleep(self, ...)
    if not self.intercept_params then
        return
    end
    local colonist = self.intercept_params.colonist
    if not IsValid(colonist) or colonist:IsDead() then
        self:SetCommand("Idle")
    end
    if self:CanInfect(colonist, true) and self:AttackColonist(colonist) then
        self:SetCommand("Idle")
    end
end

function Pest:InterceptColonist(colonist, dome, dist)
    self:PushDestructor(function(self)
        if dome and IsValid(colonist) and colonist.intercepted then
            colonist.intercepted[dome] = nil
        end
        self.intercept_params = nil
        self:SetMoveAnim("walk")
        self.MoveSleep = nil
    end)
    self.intercept_params = {
        colonist = colonist,
        dome = dome,
        dist = dist
    }
    self.MoveSleep = self.InterceptColonistMoveSleep
    self:SetMoveAnim("run")
    if dome then
        colonist.intercepted = colonist.intercepted or {}
        colonist.intercepted[dome] = true
    end
    self:ExitHolder(colonist)
    self:Goto(colonist, 2 * guim, 0)
    self:PopAndCallDestructor()
end

function Pest:AttackColonist(colonist)
    self:Face(colonist)
    self:PlayState("attack")
    self:SetState("idle")
    local infected
    if not colonist:IsDead() then
        if colonist.specialist and colonist.specialist == "security" and (self.dome:Random(100) > 70) then
            return false
        end
        if not colonist.traits.LH_Poisoned then
            colonist:AddTrait("LH_Poisoned")
            infected = true
        else
            colonist:ChangeHealth(-5 * const.Scale.Stat)
            infected = true
        end
        colonist:SetMoveAnim("moveRun")
    end
    return infected
end

function OnMsg.ClassesPostprocess()
    PlaceObj("OnScreenNotificationPreset", {
        SortKey = 1006500,
        expiration = 25000,
        priority = "Important",
        fx_action = "UINotificationResource",
        group = "Default",
        id = "DomeInfested",
        text = T { "Dome <dome> became infested with spiders.\nSecurity officers can help eradicate the pest." },
        title = T { "Dome infested!" },
        voiced_text = T { "Dome infested!" }
    })
end

function OnMsg.ModsReloaded()
    table.insert_unique(TraitsWithDailyUpdates, "LH_Poisoned")
end

local Service_MedicalBuilding = MedicalBuilding.Service
function MedicalBuilding:Service(unit, ...)
    Service_MedicalBuilding(self, unit, ...)
    if unit.traits.LH_Poisoned then
        unit:RemoveTrait("LH_Poisoned")
    end
end

local BuildingUpdate_Dome = Dome.BuildingUpdate
function Dome:BuildingUpdate(...)
    BuildingUpdate_Dome(self, ...)
    local now = GameTime()
    if not self.next_pest_check then
        self.next_pest_check = now
    end
    local threshold = BreathableAtmosphere and 200 or 240
    if self.labels.Pest and self.connected_domes and (#self.labels.Pest > 0) and (#self.connected_domes > 0) then
        if now + threshold * const.HourDuration >= self.next_pest_check then
            local dome = table.rand(connected_domes)
            if ((not dome.labels.Pest) or (#dome.labels.Pest == 0)) then
                SpawnPest(table.rand(connected_domes), self:Random(30), table.rand(self.labels.Pest).animal_type)
            end
        end
    end
    if now >= self.next_pest_check then
        if self:Random(250) > threshold then
            SpawnPest(self, self:Random(30))
        end
        self.next_pest_check = now + (threshold + self:Random(threshold)) * const.HourDuration
    end
    for _, pest in pairs(self.labels.Pest or {}) do
        if not BreathableAtmosphere and not self:HasAir() then
            if self:Random(100) < 80 then
                pest.health = pest.health - 1
                if pest.health <= 0 then
                    pest:SetCommand("Die")
                end
            end
        else
            if pest.health < pest:GetPreset().health then
                pest.health = pest.health + 1
            end
        end
    end
end

function SpawnPest(dome, num, type)
    if not dome or not dome:IsKindOf("Dome") then
        return
    end
    type = type or table.rand(Presets.Animal.Pest).id
    num = num or 1
    for _ = 1, num do
        local pest = g_Classes["Pest"]:new({
            dome = dome,
            animal_type = type,
            animal_group = "Pest"
        })
        pest:Spawn()
    end
    AddOnScreenNotification("DomeInfested", nil, {
        dome = dome:GetDisplayName()
    }, { dome }, dome.city.map_id)
end

local BuildingUpdate_SecurityStation = SecurityStation.BuildingUpdate
function SecurityStation:BuildingUpdate(...)
    local dome = self.parent_dome
    if dome and dome.labels.Pest and (#dome.labels.Pest > 0) then
        local pest = table.rand(dome.labels.Pest)
        if pest then
            if self.workers and self.current_shift and (#self.workers[self.current_shift] > 0) then
                local officer = FindNearestObject(self.workers[self.current_shift], self:GetPos(), function(obj)
                    return IsValid(obj) and not obj:IsDying() and not obj:IsDead()
                end)
                if officer then
                    officer:SetCommand("InterceptPest", pest, dome)
                end
            end
        end
    end
    if BuildingUpdate_SecurityStation then
        BuildingUpdate_SecurityStation(self, ...)
    end
end

local Idle_Colonist = Colonist.Idle
function Colonist:Idle(...)
    if self.specialist and (self.specialist == "security") then
        local dome = self.dome
        if dome and dome.labels.Pest and (#dome.labels.Pest > 0) then
            local pest = FindNearestObject(dome.labels.Pest, self:GetPos(), function(obj)
                return IsValid(obj) and not obj:IsDead()
            end)
            if pest and IsObjectVisibleFromPoint(pest, self:GetVisualPos() + point(0, 0, 180 * guic))
                    and self:GetVisualDist2D(pest) < (20 * guim) then
                if pest ~= self.last_pest_target then
                    self.last_pest_target = pest
                    return self:SetCommand("InterceptPest", pest, dome)
                end
            end
        end
    end
    return Idle_Colonist(self, ...)
end

function Pest:CanBeShoot(unit, ignore_command)
    if unit:IsDying() or not ignore_command and unit.command == "InterceptPest" then
        return false
    end
    if GetDomeAtPoint(GetObjectHexGrid(unit), unit) ~= GetDomeAtPoint(GetObjectHexGrid(self), self)
            or unit:IsVisitingBuilding() or unit:GetVisualDist2D(self) > (10 * guim) then
        return false
    end
    return IsObjectVisibleFromPoint(self, unit:GetVisualPos() + point(0, 0, 180 * guic))
end

function Colonist:InterceptPestMoveSleep(...)
    Unit.MoveSleep(self, ...)
    if not self.intercept_params then
        return
    end
    local target = self.intercept_params.target
    if not IsValid(target) or target:IsDead() then
        self:SetCommand("Idle")
    end
    if target:CanBeShoot(self, true) and self:ShootPest(target) then
        self:SetCommand("Idle")
    end
end

function Colonist:InterceptPest(target, dome, dist)
    self:PushDestructor(function(self)
        if dome and IsValid(target) and target.intercepted then
            target.intercepted[dome] = nil
        end
        self.intercept_params = nil
        self:SetMoveAnim("moveWalk")
        self.MoveSleep = nil
    end)
    self.intercept_params = {
        target = target,
        dome = dome,
        dist = dist
    }
    self.MoveSleep = self.InterceptPestMoveSleep
    self:SetMoveAnim("moveRun")
    if dome then
        target.intercepted = target.intercepted or {}
        target.intercepted[dome] = true
    end
    self:ExitHolder(target)
    self:Goto(target, 2 * guim, 0)
    self:PopAndCallDestructor()
end

function Colonist:ShootPest(pest)
    self:Face(pest)
    pest:Face(self)
    self:SetAnim(1, "attackStart")
    Sleep(self:TimeToAnimEnd())
    self:SetAnim(1, "attackIdle")
    PlayFX("Shoot", "start", self, pest)
    Sleep(self:TimeToAnimEnd())
    local dead
    if not pest:IsDead() then
        if (pest.dome:Random(100) < 50) then
            pest:SetCommand("Die")
            PlayFX("GetShot", "start", pest)
            dead = true
        else
            pest:SetCommand("InterceptColonist", self, dome)
        end
    end
    self:SetAnim(1, "attackEnd")
    Sleep(self:TimeToAnimEnd())
    return dead
end
