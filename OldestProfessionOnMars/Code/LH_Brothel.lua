DefineClass.LH_Brothel = {
    __parents = { "ElectricityConsumer", "ServiceWorkplace" },
	fx_actor_class = false,

}

DefineClass.LH_Brothel_Screen = { 
	__parents ={ "BuildingEntityClass", "AnimatedTextureObject" },
	entity = "LH_Brothel_Screen",
}

DefineClass.LH_Brothel_Sign = { 
	__parents ={ "BuildingEntityClass" },
	entity = "LH_Brothel_Sign",
}

local isMatron

function OnMsg.NewMapLoaded()
	local comm = g_CurrentMissionParams and g_CurrentMissionParams.idCommanderProfile
	isMatron = (comm == "LH_Matron")
end

function OnMsg.LoadGame()
	local comm = g_CurrentMissionParams and g_CurrentMissionParams.idCommanderProfile
	isMatron = (comm == "LH_Matron")	
end


function LH_Brothel:GameInit()
	-- Change fx_actor_class by another one. (Casino, Sapcebar..). It's used to play SoundFX & ParticleFX
	self.fx_actor_class = "Spacebar"
end

-- Turn ON Emissive texture for the attaches
function LH_Brothel:WorkLightsOn()
	self:SetSIModulation(200)
	
	local attaches = self:GetAttaches({"LH_Brothel_Screen", "LH_Brothel_Sign"})
	
	for k, att in ipairs(attaches) do
		att:SetSIModulation(200)
	end
end

function LH_Brothel:BuildingUpdate(time, ...)
	--[[local gain_point = Presets.TraitPreset.Negative.LH_Promiscuous.param + MulDivRound(self.performance, Presets.TraitPreset.Negative.LH_Promiscuous.param, 100)
	gain_point = MulDivRound(gain_point, time, g_Consts.WorkingHours * const.HourDuration)
	for _, unit in ipairs(self.visitors) do		
		if IsGameRuleActive("LH_DirtyFarmers") and unit.specialist and unit.specialist=="botanist" then
		elseif IsGameRuleActive("LH_DirtyMiners") and unit.specialist and unit.specialist=="geologist" then
	    elseif not unit.traits.LH_Promiscuous then
	    	unit.promiscuous = (unit.promiscuous or 0) + gain_point
	    	print(unit.promiscuous)
	    	if unit.promiscuous > 100 then
	    		unit:AddTrait("LH_Promiscuous")	    		
	    		unit.promiscuous = 0
	    	end
	    end
	end]]
	local gain_escort = Presets.TraitPreset.Specialization.LH_escort.param + MulDivRound(self.performance, Presets.TraitPreset.Specialization.LH_escort.param, 100)
	gain_escort = MulDivRound(gain_escort, time, g_Consts.WorkingHours * const.HourDuration)
	for _, worker in ipairs(self:GetWorkingWorkers()) do		
		if not worker.specialist or worker.specialist=="none" then
	    	worker.escort_training = (worker.escort_training or 0) + gain_escort
	    	if worker.escort_training > 100 then
	    		worker:AddTrait("LH_escort")
  				Msg("TrainingComplete", self, worker)		
				worker:ChangeWorkplacePerformance()
				RebuildInfopanel(worker)
	    	end
	    end
	end
    ServiceWorkplace.BuildingUpdate(self, time, ...)
    RebuildInfopanel(self)
end

function LH_Brothel:Service(unit, duration)
    if self:Random(100) <  Presets.TraitPreset.Negative.LH_Promiscuous.param then
    	if IsGameRuleActive("LH_DirtyFarmers") and unit.specialist and unit.specialist=="botanist" then
    	elseif IsGameRuleActive("LH_DirtyMiners") and unit.specialist and unit.specialist=="geologist" then	
        else unit:AddTrait("LH_Promiscuous")
        end
    end
    ServiceWorkplace.Service(self, unit, duration)
end

function LH_Brothel:OnChangeWorkshift(old, new)	
	if isMatron then
		local tmp = g_Consts.WorkDarkHoursSanityDecrease
		g_Consts.WorkDarkHoursSanityDecrease = 0
		Workplace.OnChangeWorkshift(self, old, new)
		g_Consts.WorkDarkHoursSanityDecrease = tmp
	else
		Workplace.OnChangeWorkshift(self, old, new)
	end	
end

function LH_Brothel:CanService(unit)
	local interest = unit.daily_interest
	local service_chance = Presets.TechPreset.Breakthroughs.LH_RedPlanetNightlife.param1
	if not interest then
		return false
	end
	if interest == "LH_interestPleasure" or self:HasUpgrade("LH_Brothel_StripBar") and self:IsUpgradeOn("LH_Brothel_StripBar") then
		return Service.CanService(self, unit)
	elseif unit.traits["LH_Promiscuous"]
			or IsGameRuleActive("LH_DirtyFarmers") and unit.specialist and unit.specialist=="botanist"
			or IsGameRuleActive("LH_DirtyMiners") and unit.specialist and unit.specialist=="geologist" then
		if self:Random(100) > service_chance*2 then
			return false
		end
	elseif self:Random(100) > service_chance then
		return false
	end
	return Service.CanService(self, unit)
end

function LH_Brothel:GetUpgradeIcon(upgrade_tier)
	return CurrentModPath..UpgradableBuilding.GetUpgradeIcon(self, upgrade_tier)
end

function LH_Brothel:OpenShift(shift)
	if shift ~=3 and not isMatron then
		return
	else
		Workplace.OpenShift(self,shift)
	end
end

function LH_Brothel:CloseShift(shift)
	if shift ~=3 and not isMatron then
		return
	else
        Workplace.CloseShift(self,shift)
	end
end

function LH_Brothel:ToggleShift(shift)
	if shift ~=3 and not isMatron then
		return
	else
		ShiftsBuilding.ToggleShift(self,shift)
	end
end

DefineStoryBitTrigger("BrothelVisited", "BrothelVisiteded")

function LH_Brothel:Assign(unit)
	Service.Assign(self, unit)
	Msg("BrothelVisited")
end

