local translationIdBase = 1107020100

function OnMsg.ClassesBuilt() 
	table.insert(ServiceInterestsList, "LH_interestPleasure")
	Interests.LH_interestPleasure = {
		display_name = T{translationIdBase+1, "Pleasure"},
	}
	const.ColonistSpecialization.LH_escort = {
		display_name = T{translationIdBase+2, "Escort"},
        display_name_plural = T{translationIdBase+3, "Escorts"},
        display_icon = "UI/Icons/Colonists/Femalescort.tga",
		description = T{translationIdBase+4, "A professional escort, looking for opportunities on the red planet."},
        sort_key = 20000
	}
	ColonistClasses.LH_escort = "Escort"
	local content_path = CurrentModPath
	local orig_Colonist_ChooseEntity = Colonist.ChooseEntity
	function Colonist:ChooseEntity()
		orig_Colonist_ChooseEntity(self)
		if self.specialist and self.specialist=="LH_escort" then
			self.infopanel_icon = content_path..self.infopanel_icon
			self.pin_icon = content_path..self.pin_icon
			self.ip_specialization_icon = content_path..self.ip_specialization_icon
			self.pin_specialization_icon = content_path..self.pin_specialization_icon
		end
	end	
	const.SanatoriumTraits[#const.SanatoriumTraits+1]="LH_Promiscuous"	
end

function OnMsg.ConstructionComplete(building)
	if building and IsKindOf(building, "LH_Brothel") then
		local comm = g_CurrentMissionParams and g_CurrentMissionParams.idCommanderProfile
    	if comm == "LH_Matron" then
    		building.active_shift = 0
    	end	
	end
end

function ReplaceInterests()
	if IsGameRuleActive("LH_DirtyFarmers") then
		local trait = table.find_value(Presets.TraitPreset.Specialization, 'id', "botanist")
		trait.add_interest = "LH_interestPleasure"
		trait.description = T{translationIdBase+5, "A botany specialist trained to work on farms. Interests: +Pleasure, - Social"}
		trait = table.find_value(Presets.TraitPreset.Specialization, 'id', "geologist")
		trait.add_interest = "interestDrinking"
		trait.description = T{6684, "default"}
	elseif IsGameRuleActive("LH_DirtyMiners") then
		local trait = table.find_value(Presets.TraitPreset.Specialization, 'id', "geologist")
		trait.add_interest = "LH_interestPleasure"
		trait.description = T{translationIdBase+6, "A geology specialist trained to work in extractor buildings. Interests: +Pleasure, - Relaxation"}
		trait = table.find_value(Presets.TraitPreset.Specialization, 'id', "botanist")
		trait.add_interest = "interestDrinking"
		trait.description = T{translationIdBase+7, "A botany specialist trained to work on farms. Interests: +Drinking, - Social"}
	else
		local trait = table.find_value(Presets.TraitPreset.Specialization, 'id', "botanist")
		trait.add_interest = "interestLuxury"
		trait.description = T{6685, "default"}
		trait = table.find_value(Presets.TraitPreset.Specialization, 'id', "geologist")
		trait.add_interest = "interestDrinking"
		trait.description = T{6684, "default"}
	end
end

function OnMsg.NewMapLoaded()
	ReplaceInterests()
	if UIColony and UIColony:IsTechResearched("LH_RedPlanetNightlife") then
		Presets.TraitPreset.Negative.LH_Promiscuous.modify_amount = Presets.TechPreset.Breakthroughs.LH_RedPlanetNightlife.param3
	else
		Presets.TraitPreset.Negative.LH_Promiscuous.modify_amount = Presets.TechPreset.Breakthroughs.LH_RedPlanetNightlife.param2
	end
end

function OnMsg.LoadGame()
	ReplaceInterests()
	table.insert_unique(g_SanatoriumTraits, "LH_Promiscuous")
	if UIColony and UIColony:IsTechResearched("LH_RedPlanetNightlife") then
		Presets.TraitPreset.Negative.LH_Promiscuous.modify_amount = Presets.TechPreset.Breakthroughs.LH_RedPlanetNightlife.param3
	else
		Presets.TraitPreset.Negative.LH_Promiscuous.modify_amount = Presets.TechPreset.Breakthroughs.LH_RedPlanetNightlife.param2
	end
end

function OnMsg.TechResearched(tech_id, city, first_time)
	if tech_id == "LH_RedPlanetNightlife" then
		Presets.TraitPreset.Negative.LH_Promiscuous.modify_amount = Presets.TechPreset.Breakthroughs.LH_RedPlanetNightlife.param3
	end
end

local GetNeededSpecialistAround_vanilla = GetNeededSpecialistAround
function GetNeededSpecialistAround(dome, for_specialization)
	local needed = GetNeededSpecialistAround_vanilla(dome, for_specialization) or empty_table	
	if dome and not for_specialization then
		needed.LH_escort = nil
	end
	return needed
end