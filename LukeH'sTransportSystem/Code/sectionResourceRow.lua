function OnMsg.ClassesPostprocess()
    PlaceObj("XTemplate", {
        __is_kind_of = "XSection",
        group = "Infopanel Sections",
        id = "sectionLHResourceRow",
        PlaceObj("XTemplateTemplate", {
            "__template", "InfopanelActiveSection",
            "RolloverHint", T("<left_click> Toggle"),
            "RolloverHintGamepad", T("<ButtonA> Toggle"),
            "TitleHAlign", "stretch",
            "OnContextUpdate", function(self, context, ...)
                LHResourceUpdate(self, ResolvePropObj(context), context.resource)
            end,
        }, {
            PlaceObj("XTemplateWindow", {
                "__class", "XTextButton",
                "__condition",
                function(parent, context)
                    return IsKindOf(ResolvePropObj(context), "TransportHub")
                end,
                "RolloverTemplate", "Rollover",
                "RolloverText", T("If enabled drones here will fetch the resource from other Transport Hubs"),
                "RolloverTitle", T("Transport policy"),
                "RolloverHint",
                T("<left_click> Change transport policy"),
                "RolloverHintGamepad",
                T("<ButtonA> Change transport policy"),
                "Id", "idTransportPolicy",
                "Dock", "right",
                "VAlign", "center",
                "MouseCursor", "UI/Cursors/Rollover.tga",
                "FocusOrder", point(1000, 1),
                "OnPress", function(self, gamepad)
                    local building = ResolvePropObj(self.context)
                    local res_id = building.storable_resources[self.context.resource]
                    building.transport_policy[res_id] = not building.transport_policy[res_id]
                end,
                "OnContextUpdate", function(self, context, ...)
                    local building = ResolvePropObj(context)
                    local res_id = building.storable_resources[context.resource]
                    self:SetImage(CurrentModPath .. "UI/Infopanel/transport_" .. (building.transport_policy[res_id] and "get" or "not") .. ".tga")
                end,
                "Image", CurrentModPath .. "UI/Infopanel/transport_not.tga",
                "ImageScale", point(800, 800),
                "Rows", 2,
                "ColumnsUse", "abbbb"
            })
        })
    })
end