DefineClass.HubDepot = {
    __parents = {"BuildingEntityClass"},
    entity = "StorageDepot",
}

DefineClass.TransportHub = {
    __parents = {
        "DroneHub",
        "LH_DynamicStorageDepot"
    },
    accumulate_dust = true,
    accumulate_maintenance_points = true,
    building_update_time = 10000,
    work_radius = 0,
    prio_button = true,
    on_off_button = true,
    charging_stations = false,
    auto_connect_requesters_at_start = true,
    accept_requester_connects = true,
    play_working_anim_on_this_attach = "DroneHubAntenna",
    total_requested_drones = 0,
    max_storage_per_resource = 10000,
    user_include_in_lrt = false,
    transport_policy = false,
    switch_fill_order = true,
    max_x = 18,
    max_y = 5,
    max_z = 2
}

function TransportHub:GameInit(...)
    self.transport_policy = {}
    LH_DynamicStorageDepot.GameInit(self, ...)
    DroneHub.GameInit(self, ...)
end

function TransportHub:GetDefaultPlacementOffset()
    return point30
end

function TransportHub:BuildingUpdate(...)
    if not self.working then
        return
    end
    local resources = {}
    for k, v in pairs(self.transport_policy) do
        if v then
            table.insert(resources, k)
        end
    end
    local resources_count = #resources
    local charging = false
    for _, drone in ipairs(self.drones) do
        if drone.command == "Idle" then
            if drone.battery < MulDivRound(drone.battery_max, 8, 10) then
                local charge_station = self:GetAttaches("NotBuildingRechargeStation")[1]
                if not charging and not charge_station.drone_charged then
                    charging = true
                    drone:SetCommand("Charge", charge_station)
                end
            else
                local realm = GetRealm(self)
                local offset = self:Random(resources_count)
                for i = 1, resources_count do
                    local idx = i + offset
                    if idx > resources_count then
                        idx = idx - resources_count
                    end
                    local resource = resources[idx]
                    local demand_request = self.demand[resource]
                    if demand_request:GetTargetAmount() > 0 then
                        local hub = realm:MapFindNearest(self, "map", "TransportHub", function(h, home)
                            return h~=home and not h.transport_policy[resource] and h.stockpiled_amount[resource] and h.stockpiled_amount[resource] > 0
                        end, self)
                        if hub then
                            --local supply_request, r_amount = hub:FindSupplyRequest(drone, resource, UIColony:IsTechResearched("ArtificialMuscles") and 2000 or 1000)
                            local supply_request = hub.supply[resource]
                            if supply_request then
                                drone.transport_hub = hub
                                drone:SetCommand("PickUp", supply_request, demand_request, resource,
                                        UIColony:IsTechResearched("ArtificialMuscles") and 2000 or 1000)
                                break
                            end
                        else
                            local elevator = realm:MapFindNearest(self, "map", "Elevator", function(e)
                                return e.working
                            end)
                            if elevator then
                                hub = GetRealm(elevator.other):MapFindNearest(self, "map", "TransportHub", function(h)
                                    return not h.transport_policy[resource] and h.stockpiled_amount[resource] and h.stockpiled_amount[resource] > 0
                                end)
                            end
                            if hub then
                                --local supply_request, r_amount = hub:FindSupplyRequest(self, resource, UIColony:IsTechResearched("ArtificialMuscles") and 2000 or 1000)
                                local supply_request = hub.supply[resource]
                                if supply_request then
                                    drone.transport_hub = hub
                                    drone:SetCommand("PickOnOtherSide", elevator, supply_request, demand_request, resource,
                                            UIColony:IsTechResearched("ArtificialMuscles") and 2000 or 1000)
                                    break
                                end
                            end
                        end
                    end
                end
            end
        end
    end
end

function Drone:PickOnOtherSide(elevator, ...)
    local entrance, entrance_point = elevator:GetEntrance(self)
    if self:Goto_NoDestlock(entrance_point) then
        self.transport_elevator = elevator
        elevator:LeadIn(self, entrance)
        self:TransferToMap(elevator.other:GetMapID())
        Sleep(5000)
        self:Disembark(elevator.other, "tunnel_entrance")
        Drone.PickUp(self, ...)
    end
end

local GoHome_Drone = Drone.GoHome
function Drone:GoHome(...)
    --if they are lost ie. when they go on the other map for a resource that's not there anymore to pick
    if self.transport_elevator and self.command_center:GetMapID() ~= self:GetMapID() then
        local elevator = self.transport_elevator.other
        local entrance, entrance_point = elevator:GetEntrance(self)
        if self:Goto_NoDestlock(entrance_point) then
            elevator:LeadIn(self, entrance)
            self:TransferToMap(elevator.other:GetMapID())
            Sleep(5000)
            self:Disembark(elevator.other, "tunnel_entrance")
            if self.command_center:GetMapID() == self:GetMapID() then
                self.transport_elevator = nil
            end
        end
    end
    GoHome_Drone(self, ...)
end

local Deliver_Drone = Drone.Deliver
function Drone:Deliver(d_request, do_not_improve)
    if self.transport_hub then
        if d_request then
            if self.battery < MulDivRound(self.battery_max, 8, 10) then
                local charge_station = self.transport_hub:GetAttaches("NotBuildingRechargeStation")[1]
                while charge_station.drone_charged do
                    Sleep(1000)
                end
                self:Charge(charge_station)
            end
        end
        if self.transport_elevator then
            local elevator = self.transport_elevator.other
            local entrance, entrance_point = elevator:GetEntrance(self)
            if self:Goto_NoDestlock(entrance_point) then
                elevator:LeadIn(self, entrance)
                self:TransferToMap(elevator.other:GetMapID())
                Sleep(5000)
                self:Disembark(elevator.other, "tunnel_entrance")
                self.transport_elevator = nil
            end
        end
        Deliver_Drone(self, d_request, true)
        self.transport_hub = nil
    else
        Deliver_Drone(self, d_request, do_not_improve)
    end
end

local AddCommandCenter_TaskRequester = TaskRequester.AddCommandCenter
function TaskRequester:AddCommandCenter(center, ...)
    if not center:IsKindOf("TransportHub") or self:IsKindOf("TransportHub") then
        return AddCommandCenter_TaskRequester(self, center, ...)
    end
end

function OnMsg.ClassesBuilt()
    local vanilla_condition = XTemplates.sectionServiceArea[1].__condition
    XTemplates.sectionServiceArea[1].__condition = function(parent, context)
        return vanilla_condition(parent, context) and not context:IsKindOf("TransportHub")
    end
end

function OnMsg.ModsReloaded()
    table.insert_unique(Presets.TechPreset["Robotics"].DroneHub, PlaceObj("Effect_TechUnlockBuilding", { Building = "TransportHub" }))
    table.insert_unique(Presets.TechPreset["Breakthroughs"].AutonomousHubs,
            PlaceObj("Effect_ModifyLabel", { Amount = 1, Label = "TransportHub", Prop = "disable_electricity_consumption" }))
    table.insert_unique(Presets.TechPreset["Breakthroughs"].AutonomousHubs,
            PlaceObj("Effect_ModifyLabel", { Amount = 1, Label = "TransportHub", Prop = "disable_maintenance" }))
    BuildingTechRequirements["TransportHub"] = { { tech = "DroneHub", hide = false } }
    if IsDlcAvailable("armstrong") then
        table.insert_unique(BuildingTemplates.TransportHub.storable_resources, "Seeds")
    end
    if IsDlcAvailable("picard") then
        table.insert_unique(BuildingTemplates.TransportHub.storable_resources, "PreciousMinerals")
    end
    if table.find(ModsLoaded, "id", "LH_Resources") then
        table.insert_unique(BuildingTemplates.TransportHub.storable_resources, "Crystals")
        table.insert_unique(BuildingTemplates.TransportHub.storable_resources, "Radioactive")
        table.insert_unique(BuildingTemplates.TransportHub.storable_resources, "Meat")
        table.insert_unique(BuildingTemplates.TransportHub.storable_resources, "Booze")
    end
    BuildingTemplates.DroneHub.entity2 = nil
    BuildingTemplates.DroneHub.entitydlc2 = nil
end

function OnMsg.TechResearched(tech_id, city)
    if tech_id == "WirelessPower" then
        g_WirelessPower = true
        local container_hubs = UICity.labels.TransportHub
        for i = 1, #(container_hubs or empty_table) do
            local hub = container_hubs[i]
            for j = 1, #(hub.charging_stations or empty_table) do
                hub.charging_stations[j]:StartAoePulse()
            end
        end
    end
end

function OnMsg.ClassesPostprocess()
    PlaceObj("BuildingTemplate", {
        "Group",
        "Storages",
        "Id",
        "LH_DynamicStorageDepot",
        "template_class",
        "LH_DynamicStorageDepot",
        "instant_build",
        true,
        "dome_forbidden",
        true,
        "can_refab",
        false,
        "display_name",
        T("Dynamic Depot"),
        "display_name_pl",
        T("Dynamic Depots"),
        "description",
        T("Stores a total of <resource(max_storage_per_resource)> units of transportable resources. Some resources will be transported to other depots within Drone range."),
        "build_category",
        "Storages",
        "display_icon",
        "UI/Icons/Buildings/universal_storage.tga",
        "entity",
        "StorageDepot",
        "encyclopedia_id",
        "UniversalStorageDepot",
        "encyclopedia_image",
        "UI/Encyclopedia/UniversalDepot.tga",
        "build_shortcut1",
        "U",
        "on_off_button",
        false,
        "prio_button",
        false,
        "count_as_building",
        false,
        "disabled_in_environment1",
        "",
        "disabled_in_environment2",
        "",
        "desired_amount",
        30000,
    })
end