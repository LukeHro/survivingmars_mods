DefineClass.LH_DynamicStorageDepot = {
    __parents = {
        "UniversalStorageDepot"
    },
    max_storage_per_resource = 180000,
    desire_slider_max = 180,
    switch_fill_order = false,
    max_x = 5,
    max_y = 18,
    max_z = 2
}

function _ENV:LHResourceUpdate(building, resource_pos)
    local resource = building.storable_resources[resource_pos]
    if building:IsResourceEnabled(resource) then
        self:SetIcon("UI/Icons/Sections/resource_accept.tga")
    else
        self:SetIcon("UI/Icons/Sections/resource_no_accept.tga")
    end
    self:SetTitle(T({615073837286, "<left><resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>",res = resource}))
    self:SetRolloverText(T({376908863267, [[
Toggle storage of this resource.

Status: <on_off(IsResourceEnabled(res))>]], res = resource}))
    self:SetRolloverTitle(T({883661504598, "<resource(res)>", res = resource}))
    function self:OnActivate(context, gamepad)
        building:ToggleAcceptResource(resource)
    end
end

local Init_UniversalStorageDepot = UniversalStorageDepot.Init
function LH_DynamicStorageDepot:Init()
    if IsKindOf(self, "LH_DynamicStorageDepot") then
        --table.insert_unique(BuildingTemplates.DynamicStorageDepot.storable_resources,"WasteRock")
        if IsDlcAvailable("armstrong") then
            table.insert_unique(self.storable_resources,"Seeds")
        end
        if IsDlcAvailable("picard") then
            table.insert_unique(self.storable_resources,"PreciousMinerals")
        end
        return
    end
    Init_UniversalStorageDepot(self)
end

local GameInit_UniversalStorageDepot = UniversalStorageDepot.GameInit
function UniversalStorageDepot:GameInit()
    if IsKindOf(self, "LH_DynamicStorageDepot") then
        --avoid error logs, call only DynamicStorageDepot's
        return
    end
    GameInit_UniversalStorageDepot(self)
end

function LH_DynamicStorageDepot:Init()

end

local default_offset = point(-460, 0, 0)
function LH_DynamicStorageDepot:GetDefaultPlacementOffset()
    return default_offset
end

function LH_DynamicStorageDepot:Init()
    self.placement_offset = self:GetDefaultPlacementOffset()
end

function LH_DynamicStorageDepot:ReInitBoxSpots()
    if self:HasSpot("Box") then
        self.cube_attach_spot_idx = self:GetSpotBeginIndex("Box")
    elseif self:HasSpot("Box1") then
        self.cube_attach_spot_idx = self:GetSpotBeginIndex("Box1")
    end
    self.placement_offset = self:GetDefaultPlacementOffset()
end

function LH_DynamicStorageDepot:GameInit()
    self.resource = self.storable_resources
    self:ReInitBoxSpots()
    local len = #self.storable_resources
    for i = 1, len do
        local resource_name = self.storable_resources[i]
        local amount = self.stockpiled_amount[resource_name] or 0
        self:SetCount(amount, resource_name)
    end
end

function LH_DynamicStorageDepot:GetMaxStorage(resource)
    return self.max_storage_per_resource
end

function LH_DynamicStorageDepot:IsFull(include_inbound)
    return self:GetStoredAmount(include_inbound) >= self:GetMaxStorage()
end

function LH_DynamicStorageDepot:SetCount(new_count, resource)
    self:ReInitBoxSpots()
    SharedStorageBaseVisualOnly.SetCount(self, new_count, resource)
end

function LH_DynamicStorageDepot:OnResourceCubePlaced(cube, resource)
    if resource == "WasteRock" then
        cube:SetScale(140)
        cube:SetAngle(SessionRandom:Random(21600))
    end
    --TODO move waste rock on top?
end

function LH_DynamicStorageDepot:RearrangeCubes(removed_cube_idx)
    SharedStorageBaseVisualOnly.RearrangeCubes(self, removed_cube_idx)
end

function LH_DynamicStorageDepot:RefreshDemand()
    local storable_resources = self.storable_resources
    local resource_count = #storable_resources
    local is_full = self:IsFull()
    if (is_full or self.was_full) then
        self.was_full = is_full
        for i = 1, resource_count do
            local resource_name = storable_resources[i]
            local demand = self.demand[resource_name]
            self.demand[resource_name]:AddAmount(is_full and -demand:GetActualAmount() or (self:GetMaxStorage() - self:GetStoredAmount(resource_name)))
        end
    end
end

function LH_DynamicStorageDepot:AddResource(...)
    UniversalStorageDepot.AddResource(self, ...)
    self:RefreshDemand()
end
LH_DynamicStorageDepot.AddDepotResource = LH_DynamicStorageDepot.AddResource
LH_DynamicStorageDepot.AddResourceAmount = LH_DynamicStorageDepot.AddResource
LH_DynamicStorageDepot.SetResourceAmount = false
LH_DynamicStorageDepot.SetCountFromRequest = false

function LH_DynamicStorageDepot:DroneLoadResource(...)
    UniversalStorageDepot.DroneLoadResource(self, ...)
    self:RefreshDemand()
end

function LH_DynamicStorageDepot:DroneUnloadResource(...)
    UniversalStorageDepot.DroneUnloadResource(self, ...)
    self:RefreshDemand()
end

function LH_DynamicStorageDepot:CheatFill()
    local storable_resources = self.storable_resources
    local resource_count = #storable_resources
    self:InterruptDrones(nil, function(drone)
        local r = drone.d_request
        if r and self.demand[r:GetResource()] == r then
            return drone
        end
    end)
    local to_add = DivRound((self.max_storage_per_resource - self:GetStoredAmount()) / const.ResourceScale, #storable_resources) * const.ResourceScale
    for i = 1, resource_count do
        local resource_name = storable_resources[i]
        if self.supply[resource_name] then
            self:AddResource(to_add, resource_name)
        end
    end
end
LH_DynamicStorageDepot.Fill = LH_DynamicStorageDepot.CheatFill
