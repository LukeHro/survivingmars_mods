function OnMsg.ClassesPostprocess()
    PlaceObj("XTemplate", {
        group = "Infopanel Sections",
        id = "sectionLHDynamicStorage",
        PlaceObj("XTemplateTemplate", {
            "__condition",
            function(parent, context)
                return IsKindOf(context, "LH_DynamicStorageDepot")
            end,
            "__template",
            "InfopanelSection",
            "RolloverText",
            T(10461, "Drones and Shuttles will attempt to stockpile at least <DesiredAmountUI> of each resources stored here."),
            "RolloverHint",
            T(116367034467, "<left_click> Set Desired Amount <newline><em>Ctrl + <left_click></em> Set Desired Amount in all <display_name_pl>"),
            "RolloverHintGamepad",
            T(10462, "<LB> / <RB>    change desired amount"),
            "Title",
            T(10463, "Desired Amount <DesiredAmountUI>"),
            "Icon",
            "UI/Icons/Sections/facility.tga"
        }, {
            PlaceObj("XTemplateTemplate", {
                "__template",
                "InfopanelSlider",
                "BindTo",
                "DesiredAmountSlider"
            })
        }),PlaceObj("XTemplateGroup", {
            "__context_of_kind", "LH_DynamicStorageDepot",
            "comment", "dynamic storage"
        }, {
            PlaceObj("XTemplateForEach", {
                "array", function(parent, context)
                    return nil, 1, #context.storable_resources
                end,
                "map", function(parent, context, array, i)
                    return i
                end,
                "item_in_context", "resource"
            }, {
                PlaceObj("XTemplateTemplate", {
                    "__template", "sectionLHResourceRow"
                })
            })
        })
    })
    local idx = table.find(XTemplates.ipBuilding[1][1], "__template", "sectionStorage")
    XTemplates.ipBuilding[1][1][idx]["__condition"] = function(parent, context)
        return not IsKindOf(context, "LH_DynamicStorageDepot") and not IsKindOf(context, "DynamicStorageDepot")
    end
    if (XTemplates.ipBuilding[1][1][idx+1]["__template"] ~= "sectionLHDynamicStorage") then
        table.insert(XTemplates.ipBuilding[1][1], idx+1, PlaceObj("XTemplateTemplate", {"__template", "sectionLHDynamicStorage"}))
    end
end