-- The main building that we're adding.
-- When a colonist dies, he will be instantly transported and buried in the closest cemetery.
-- This building must decrease the 'See dead sanity' effect.
-- When a new colonist is born on Mars he will recieve a positive trait from someone in on of the cemeteries.
DefineClass.LH_Cemetery = {
	__parents = { "Building" },
	graves = false, -- List of grave objects.
	free_spots = false, -- List of spots that can be used to place new graves.
	max_graves = 0, -- Graves capacity of this cemetery (see Cemetery:GameInit()).
}

-- This function will implement and apply the modification to the 'See dead sanity' effect
local function UpdateSeeDeadSanityEffect(change)
	-- Count the number of cemeteries built around the map
	local cemeteries = UICity.labels.LH_Cemetery or empty_table

	-- We cannot just take all building in the label, because it contains destroyed cemeteries as well.
	local old_count = 0
	for i,cemetery in ipairs(cemeteries) do
		if not cemetery.destroyed then
			old_count = old_count + 1
		end
	end
	local new_count = old_count + change

	if old_count == 0 and new_count > 0 then
		-- If there were no cemeteries and one was just added - add a modifier that decreases the effect by 50%
		CreateLabelModifier("CemeteryEffect", "Consts", "SeeDeadSanity", 0, -50)
	elseif old_count > 0 and new_count == 0 then
		-- If there were some cemeteries built, but the last one was just destroyed - remove the modifier added above
		RemoveLabelModifier("CemeteryEffect", "Consts", "SeeDeadSanity")
	end
end

function LH_Cemetery:Init()
	-- Try to decrease the effect of 'See dead sanity'.
	-- This must happen in Init, before the building is added to a label.
	UpdateSeeDeadSanityEffect(1)
end

function LH_Cemetery:GameInit()
	self.graves = { }

	-- The graves capacity is not hardcoded, but instead is taken from the cemetery entity as the number of 'Grave' spots.
	local first, last = self:GetSpotRange("Grave")
	self.max_graves = last - first + 1

	-- Fill the list of grave spots that are free (all of them at the moment)
	self.free_spots = { }
	for i=first, last do
		table.insert(self.free_spots, i)
	end
	--Shuffle the spot indices, so graves appear at random spots (see Cemetery:AddGrave())
	table.shuffle(self.free_spots)
end

function LH_Cemetery:OnDemolish()
	-- Try to revert the changes to the 'See dead sanity' effect (see Cemetery:Init()).
	-- This happens before the building is removed from the label.
	UpdateSeeDeadSanityEffect(-1)

	return Building.OnDemolish(self)
end

function LH_Cemetery:Done()
	-- Destroy all graves in this cemetery (see Cemetery:AddGrave()).
	for i,grave in ipairs(self.graves) do
		LH_SpawnZombie(grave, self.parent_dome)
		DoneObject(grave)
	end
end

function LH_Cemetery:AddGrave(colonist)
	-- Choose a free spot - the last in the list (for efficiency)
	local grave_spot, idx = self.free_spots[#self.free_spots] -- This is random because they were shuffled beforehand (see Cemetery:GameInit())
	table.remove(self.free_spots, #self.free_spots) -- Remove it (marking it as 'used')
	-- Create grave.
	local grave = PlaceObj("LH_Grave", { "name", colonist.name, "sols", colonist.sols, "decay", GameTime() + 5 * const.DayDuration, "spot", grave_spot })
	table.insert(self.graves, grave)


	-- Move the grave over to that position and rotate it appropriately
	-- Note: We're not attaching the grave to the cemetery's spot (using cemetery:Attach()),
	-- becuase that would make the graves not selectable, as it will be a child object of the cemetery object.
	grave:SetPos(self:GetSpotPos(grave_spot))
	local angle, axis = self:GetSpotRotation(grave_spot)
	grave:SetAngle(angle)
	grave:SetAxis(axis)
	-- Trigger FXs, associated with the grave placement
	PlayFX("GraveAdded", "start", grave)
	
	return grave
end

function LH_Cemetery:SpawnZombie(grave)
	local idx = table.find(self.graves, "handle", grave.handle)
	if idx then
		LH_SpawnZombie(grave, self.parent_dome)
		self.free_spots[#self.free_spots+1] = grave.spot
		table.remove(self.graves, idx)
	end
	DoneObject(grave)
end

function LH_Cemetery:BuildingUpdate()
	for i,grave in ipairs(self.graves or {}) do
		if GameTime() >= grave.decay then
			self.free_spots[#self.free_spots+1] = grave.spot
			table.remove(self.graves, i)
			DoneObject(grave)
			return
		end
	end
end

-- Quick check if there is free space for a new grave to be added to this cemetery.
function LH_Cemetery:HasFreeGraves()
	return #self.graves < self.max_graves
end

-- A grave can be selected to show who is buried there.
DefineClass.LH_Grave = {
	__parents = { "Object", "FXObject", "InfopanelObj" },

	enum_flags = { efSelectable = true }, -- Makes this object selectable.
	ip_template = "Infopanel", -- This shows which infopanel template will be used when selecting this object.
	-- This will make the graves seem like 'UnitSmall' instead of 'Grave' for the purposes of the FX system (helps with the selection particles on the ground).
	-- We could use 'UnitMedium' or 'UnitLarge' or just inherit 'SelectableWithParticles' (for building-like particles), but these options do not fit our graves.
	fx_actor_class = "UnitSmall",

	-- A list of all entities that can be used for a grave (random one is picked - see Grave:GameInit()).
	entities = { "CemeteryGrave_01", "CemeteryGrave_02", "CemeteryGrave_03" },

	name = "", -- Name of the colonist buried.
	sols = 0, -- Number of sols the colonist lived on Mars.
	decay = 0,
	spot = false
}

function LH_Grave:GameInit()
	-- Choose a random entity (visual model) for the grave
	local entity = table.rand(self.entities)
	self:ChangeEntity(entity)
end

-- The display name of this object (Used as title in the infopanel).
function LH_Grave:GetDisplayName()
	return T{917892953985, "Grave"}
end

-- The infopanel description of this object.
function LH_Grave:GetIPDescription()
	return T{917892953986, "Here lies <name> who lived on Mars for <sols> Sols"}
end

-- Allocation optimization.
local cemetery_search_query = {
	class = "LH_Cemetery",
	filter = function(cemetery)
		-- Destroyed cemeteries will be enumerated so we must filter them out
		return not cemetery.destroyed and cemetery:HasFreeGraves()
	end,
}
-- Finds a suitable cemetery for the colonist to be buried in. Only consider cemeteries, which have free spots for new graves
local function FindCemetery(colonist)
	return MapFindNearest(colonist, "map", "LH_Cemetery", function(o) return next(o.free_spots or empty_table) end)
end

-- A notification should be shown if a colonist dies but cannot be buried.
-- This notification will show itself again if a new cemetery was built, but was also filled.
local noCemeteriesNotification = false

-- React when a colonist has died.
function OnMsg.ColonistDied(colonist, reason)
	if reason == "undead" then
		LH_SpawnZombie(colonist, colonist.dome)
		colonist:SetVisible(false)
		if colonist:IsPinned() then
			colonist:TogglePin()
		end
		return
	end
	-- Find a cemetery.
	local cemetery = FindCemetery(colonist)
	
	if cemetery then
		-- Add a new grave.
		local grave = cemetery:AddGrave(colonist)
		grave.gender = colonist.entity_gender 
		grave.icon = colonist.ip_specialization_icon
		grave.cemetery = cemetery
		-- Instantly move the colonist body to it (just in case the camera or someone else wants to find the colonist).
		colonist:SetPos(grave:GetPos())

		-- Destroying the colonist object is not something that should be done at this point,
		-- so - hide the colonist body, unpin it (if pinned) and deselect (if selected).
		colonist:SetVisible(false)
		if colonist:IsPinned() then
			colonist:TogglePin()
		end
		if SelectedObj == colonist then
			if UIColony.hour < 5 or UIColony.hour >= 21 then
                SelectObj(false)
            else
                SelectObj(grave)
            end
		end

		-- Marks that next time there is no space for a grave the notification should be shown.
		noCemeteriesNotification = false
	elseif not noCemeteriesNotification then
		-- Show the 'no grave spaces' notifications.
		CreateRealTimeThread(WaitCustomPopupNotification,
			T{917892953987, "No Cemeteries to Bury Dead Colonist!"},
			T{917892953988, "Yet another colonist has died but no cemetery has place to store the body.\nIt shall be burned instead."},
			{ T{917892953989, "A new Cemetery shall be built for the unfortunate souls to come."} }
		)
		-- Mark that the notification has already been shown.
		noCemeteriesNotification = true
	end
end
