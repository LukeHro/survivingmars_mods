local translationIdBase = 1107060100

DefineClass.Undead = {
  __parents = {"BasePet"},
}

function Undead:GameInit()
  self.dome:AddToLabel("Undead", self)
  self.dome:RemoveFromLabel("Pet", self)
  self.dome.dome_comfort = self.dome.dome_comfort - 500
  self.display_name = self:GetPreset().display_name
  self.description = self:GetPreset().description
  self.spawn_time = GameTime()
  self.life_time = self:GetPreset().lifetime
end

function Undead:Done()
  self.dome:RemoveFromLabel("Undead", self)
  self.dome.dome_comfort = self.dome.dome_comfort + 500
end

function Undead:Roam(duration)
  local tEnd = GameTime() + duration
  while tEnd - GameTime() > 0 do
    if not IsValid(self.dome) then
      self:SetCommand("Die")
    end
    self:GoToRandomPosInDome(self.dome)
    self:PlayState("idle", -1000)
  end
end

local last_colonist_target --workaround for game freeze
function Undead:Idle()
  if (self.spawn_time + self.life_time <= GameTime()) then
    self:SetCommand("Die")
  else
    local dome = self.dome
    if dome and dome.labels.Colonist and (#dome.labels.Colonist > 0) then
      local colonist = FindNearestObject(dome.labels.Colonist, self:GetPos(), function (obj)
        return IsValid(obj) and not obj:IsDying() and not obj:IsDead() and not obj:IsVisitingBuilding()
      end)      
      if colonist then
        if last_colonist_target ~= colonist.handle then
          last_colonist_target = colonist.handle
          return self:SetCommand("InterceptColonist", colonist, dome)
        end
      else
      end    
    end
    self:Roam(const.HourDuration)
  end
end
function Undead:Spawn(source)
  self:SetPos(source:GetPos())
  self:SetAnim(1, "rise")
end

function Undead:Die()
  self:SetAnim(1, "die")
  Sleep(self:TimeToAnimEnd())
  DoneObject(self)
end

function LH_SpawnZombie(source, spawn_dome)
  source.gender = source.gender or Random(1, 100) <= 50 and "Male" or "Female"
  local type = (source.gender and source.gender=="Male") and "MaleZombie" or "FemaleZombie"
  local undead = g_Classes["Undead"]:new({
      dome = spawn_dome,
      animal_type = type,
      animal_group = "Undead"
    })
    undead:Spawn(source)
end

function OnMsg.ClassesBuilt()  
  PlaceObj('XTemplate', {
    __is_kind_of = "XContextControl",
    group = "Infopanel Sections",
    id = "sectionGravesList",
    PlaceObj('XTemplateWindow', {
      '__class', "XContextControl",
      'HAlign', "center",
      'LayoutMethod', "HWrap",
      'OnContextUpdate', function (self, context, ...)
    for i, win in ipairs(self) do
      local grave = context.graves[i]
      if grave then
        win:SetIcon(grave.icon)
      else
        win:SetIcon("UI/Infopanel/colonist_empty.tga")
      end
      win:SetContext(grave)
      XRecreateRolloverWindow(win)
    end
  end,
    }, {
      PlaceObj('XTemplateForEach', {
        'array', function (parent, context) return context.graves, 1, context.max_graves end,
        '__context', function (parent, context, item, i, n)  end,
      }, {
        PlaceObj('XTemplateTemplate', {
          '__template', "InfopanelPerson",
          'OnPress', function (self, gamepad)  
            if self.context then
              SelectObj(self.context)
            end
          end,
          'OnAltPress', function (self, gamepad)
            if self.context then
              local grave = self.context
              grave.cemetery:SpawnZombie(grave)
            end
            ObjModified(self.parent.context)
          end,
        }, {
          PlaceObj('XTemplateFunc', {
            'name', "GetRolloverTitle",
            'func', function (self, ...)  
              local icon = self:GetIcon()           
              if self.context then
                return self.context.name
              else
                return T{translationIdBase+1, "Empty grave"}
              end
            end,
          }),
          PlaceObj('XTemplateFunc', {
            'name', "GetRolloverText",
            'func', function (self, ...)
              if self.context then
                return T{translationIdBase+2, "Maybe someone you knew..."}
              else
                return T{translationIdBase+3, "An empty grave."}
              end
            end,
          }),
          PlaceObj('XTemplateFunc', {
            'name', "GetRolloverHint",
            'func', function (self, ...) 
              local building = self.parent.context
              if building.max_graves then
                if self.context then
                  return T{translationIdBase+4, "<right_click> Destroy this grave"}
                end
              end
            end,
          }),
          PlaceObj('XTemplateFunc', {
            'name', "GetRolloverHintGamepad",
            'func', function (self, ...)
              local building = self.parent.context
              if building.max_graves then
                if self.context then
                  return T{translationIdBase+5, "<ButtonX> Destroy this grave"}
                end
              end
            end,
          }),
          }),
        }),
      }),
  })
    
  PlaceObj("XTemplate", {
    group = "Infopanel Sections",
    id = "sectionGraves",
    PlaceObj("XTemplateTemplate", {
        "__context_of_kind", "LH_Cemetery",
        "__template", "InfopanelSection",
        "RolloverText", T(translationIdBase+6, "Do not disturb the dead..."),
        "Title", T(translationIdBase+7, "Graves"),
        "Icon", "UI/Icons/Sections/colonist.tga"
    }, {
      PlaceObj('XTemplateTemplate', {
        'sectionGravesList', true,
        '__template', "sectionGravesList",
      })
    })
  })

  local idx = table.find(XTemplates.ipBuilding[1][1], "__template", "sectionGraves")
  if not idx then
    table.insert_unique(XTemplates.ipBuilding[1][1], PlaceObj("XTemplateTemplate", {
          "__template", "sectionGraves"
        })
      )
  end

  DeathReasons["undead"] = T(translationIdBase+8, "Turned undead")
end

function OnMsg.SelectedObjChange(obj)
  if (not obj) or (not (UIColony.hour < 5 or UIColony.hour >= 21)) then
    return
  end
  if obj:IsKindOf("LH_Cemetery") then    
    for i,grave in ipairs(obj.graves) do
      LH_SpawnZombie(grave, obj.parent_dome)
      DoneObject(grave)
    end
    obj.graves = {}
    obj:GameInit()
  elseif obj:IsKindOf("LH_Grave") then    
    obj.cemetery:SpawnZombie(obj)    
  end
end