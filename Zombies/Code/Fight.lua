function Undead:CanAttack(colonist, ignore_command)
  if not ignore_command and self.command == "InterceptColonist" then
    return false
  end
  if self:GetVisualDist2D(colonist) < (10 * guim) then    
    if colonist.specialist and (colonist.specialist == "security") then      
      colonist:SetCommand("InterceptUndead", self, self.dome)            
    else
      self.dome:AddPanicMarker(self:GetPos(), 10 * guim, const.HourDuration/60)
      local panic_pos, panic_radius = colonist:ShouldPanic(self.dome)
      if panic_pos then
        colonist:SetCommand("Panic", panic_pos, panic_radius)
      end
    end
  end
  if GetDomeAtPoint(GetObjectHexGrid(self), self) ~= GetDomeAtPoint(GetObjectHexGrid(colonist), colonist) or self:IsVisitingBuilding() or self:GetVisualDist2D(colonist) > (2 * guim) then
    return false
  end
  if colonist.holder and colonist.holder ~= colonist.residence then
    colonist:GetFired()
    colonist:AssignToService(false)
    colonist:ExitHolderImmediately()
    if colonist.specialist and (colonist.specialist == "security") then      
      colonist:SetCommand("InterceptUndead", self, self.dome)            
    else
      self.dome:AddPanicMarker(self:GetPos(), 10 * guim, const.HourDuration/60)
      local panic_pos, panic_radius = colonist:ShouldPanic(self.dome)
      if panic_pos then
        colonist:SetCommand("Panic", panic_pos, panic_radius)
      end
    end
  end
  return IsObjectVisibleFromPoint(colonist, self:GetVisualPos() + point(0, 0, 180 * guic))
end

function Undead:InterceptColonistMoveSleep(...)
  Unit.MoveSleep(self, ...)
  if not self.intercept_params then
    return
  end
  local colonist = self.intercept_params.colonist
  if not IsValid(colonist) or colonist:IsDead() then
    self:SetCommand("Idle")
  end
  if self:CanAttack(colonist, true) and self:AttackColonist(colonist) then
    self:SetCommand("Idle")
  end
end

function Undead:InterceptColonist(colonist, dome, dist)
  local moving = false
  self:PushDestructor(function(self)
    if dome and IsValid(colonist) and colonist.intercepted then 
      colonist.intercepted[dome] = nil
    end
    self.intercept_params = nil
    self:SetMoveAnim("walk")
    self.MoveSleep = nil
  end)
  self.intercept_params = {
    colonist = colonist,
    dome = dome,
    dist = dist
  }
  self.MoveSleep = self.InterceptColonistMoveSleep
  self:SetMoveAnim("run")
  if dome then
    colonist.intercepted = colonist.intercepted or {}
    colonist.intercepted[dome] = true
  end
  self:ExitHolder(colonist)
  self:Goto(colonist, 2 * guim, 0)
  self:PopAndCallDestructor()
end

function Undead:AttackColonist(colonist)
  self:Face(colonist)
  self:PlayState("attack")
  local killed
  if not colonist:IsDead() then
    if colonist.specialist and colonist.specialist == "security" and (self.dome:Random(100) > 50) then
      return false
    end
    colonist:SetCommand("Die","undead")
    killed = true    
  end
  return killed
end

function Undead:CanBeShoot(unit, ignore_command)
  if unit:IsDying() or not ignore_command and unit.command == "InterceptUndead" then
    return false
  end
  if GetDomeAtPoint(GetObjectHexGrid(unit), unit) ~= GetDomeAtPoint(GetObjectHexGrid(self), self)
          or unit:IsVisitingBuilding() or unit:GetVisualDist2D(self) > (10 * guim) then
    return false
  end
  return IsObjectVisibleFromPoint(self, unit:GetVisualPos() + point(0, 0, 180 * guic))
end

function Colonist:InterceptUndeadMoveSleep(...)
  Unit.MoveSleep(self, ...)
  if not self.intercept_params then
    return
  end
  local target = self.intercept_params.target
  if not IsValid(target) or target:IsDead() then
    self:SetCommand("Idle")
  end
  if target:CanBeShoot(self, true) and self:ShootUndead(target) then
    self:SetCommand("Idle")
  end
end

function Colonist:InterceptUndead(target, dome, dist)
  local moving = false
  self:PushDestructor(function(self)
    if dome and IsValid(target) and target.intercepted then 
      target.intercepted[dome] = nil
    end
    self.intercept_params = nil
    self:SetMoveAnim("moveWalk")
    self.MoveSleep = nil
  end)
  self.intercept_params = {
    target = target,
    dome = dome,
    dist = dist
  }
  self.MoveSleep = self.InterceptUndeadMoveSleep
  self:SetMoveAnim("moveRun")
  if dome then
    target.intercepted = target.intercepted or {}
    target.intercepted[dome] = true
  end
  self:ExitHolder(target)
  self:Goto(target, 2 * guim, 0)
  self:PopAndCallDestructor()
end

function Colonist:ShootUndead(undead)
  self:Face(undead)
  undead:Face(self)
  self:SetAnim(1, "attackStart")
  Sleep(self:TimeToAnimEnd())
  self:SetAnim(1, "attackIdle")
  PlayFX("Shoot", "start", self, undead)
  Sleep(self:TimeToAnimEnd())
  local dead
  if not undead:IsDead() then
    if (undead.dome:Random(100) < 50) then
      undead:SetCommand("Die")
      PlayFX("GetShot", "start", undead)
      dead = true
    else
      undead:SetCommand("InterceptColonist", self, dome)
    end
  end
  self:SetAnim(1, "attackEnd")
  Sleep(self:TimeToAnimEnd())
  return dead
end

local BuildingUpdate_SecurityStation = SecurityStation.BuildingUpdate
function SecurityStation:BuildingUpdate(...)
  local dome = self.parent_dome
  if dome and dome.labels.Undead and (#dome.labels.Undead > 0) then
    local undead = table.rand(dome.labels.Undead)
    if undead then
      if self.workers and self.current_shift and (#self.workers[self.current_shift] > 0) then        
        local officer = FindNearestObject(self.workers[self.current_shift],self:GetPos(), function (obj)
          return IsValid(obj) and not obj:IsDying() and not obj:IsDead() and obj.specialist and (obj.specialist == "security")
        end)      
        if officer then
          officer:SetCommand("InterceptUndead", undead, dome)
        end
      end 
    end
  end
  if BuildingUpdate_SecurityStation then
    BuildingUpdate_SecurityStation(self, ...)
  end
end