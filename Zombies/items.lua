return {
PlaceObj('ModItemActionFXColorization', {
	Action = "GraveAdded",
	Moment = "start",
	RealTime = true,
	Time = 2000,
	group = "Default",
	id = "",
}),
PlaceObj('ModItemActionFXDecal', {
	Action = "GraveAdded",
	Decal1 = "DecDebris_03",
	FadeIn = 100,
	FadeOut = 100,
	Moment = "start",
	Scale = 50,
	ScaleVariation = 10,
	Time = 10000,
	group = "Default",
	id = "",
}),
PlaceObj('ModItemActionFXLight', {
	Action = "GraveAdded",
	Color = -21191,
	FadeIn = 1000,
	FadeOut = 1000,
	FadeOutColor = -21191,
	Intensity = 180,
	Moment = "start",
	Radius = 4000,
	StartColor = -21191,
	Target = "ignore",
	Time = 2000,
	group = "Default",
	id = "",
}),
PlaceObj('ModItemActionFXObject', {
	Action = "GraveAdded",
	FadeIn = 400,
	FadeOut = 400,
	Moment = "start",
	Object = "ArtificialSunSphere",
	Offset = point(-90, 0, 0),
	Scale = 3,
	Target = "ignore",
	Time = 2000,
	group = "Default",
	id = "",
}),
PlaceObj('ModItemActionFXParticles', {
	Action = "GraveAdded",
	Moment = "start",
	OffsetDir = "ActionDir",
	Orientation = "ActionDir",
	OrientationAxis = 3,
	Particles = "CemeteryParticleSystem",
	Scale = 80,
	Source = "ActionPos",
	group = "Default",
	id = "",
}),
PlaceObj('ModItemActionFXSound', {
	Action = "GraveAdded",
	Moment = "start",
	Sound = "GravePlacement",
	Target = "ignore",
	group = "Default",
	id = "",
}),
PlaceObj('ModItemAnimal', {
	__copy_group = "",
	ambient_life_suffix = "Penguin",
	description = T(260362393660, --[[ModItemAnimal FemaleZombie description]] "Maybe someone you knew..."),
	display_icon = "UI/GraveSkull",
	display_name = T(450344564242, --[[ModItemAnimal FemaleZombie display_name]] "Zombie"),
	entities = {
		"FemaleZombie",
	},
	group = "Undead",
	health = 50,
	herd_size = 5,
	id = "FemaleZombie",
	lifetime = 180000,
}),
PlaceObj('ModItemAnimal', {
	__copy_group = "",
	ambient_life_suffix = "Penguin",
	description = T(260362393660, --[[ModItemAnimal MaleZombie description]] "Maybe someone you knew..."),
	display_icon = "UI/GraveSkull",
	display_name = T(450344564242, --[[ModItemAnimal MaleZombie display_name]] "Zombie"),
	entities = {
		"MaleZombie",
	},
	group = "Undead",
	health = 50,
	herd_size = 5,
	id = "MaleZombie",
	lifetime = 180000,
}),
PlaceObj('ModItemBuildingTemplate', {
	'Group', "Habitats",
	'Id', "LH_Cemetery",
	'template_class', "LH_Cemetery",
	'pin_rollover_hint', T(759838946251, --[[ModItemBuildingTemplate LH_Cemetery pin_rollover_hint]] "<image UI/Infopanel/left_click.tga 1400> Select"),
	'pin_rollover_hint_xbox', T(231124721534, --[[ModItemBuildingTemplate LH_Cemetery pin_rollover_hint_xbox]] "<image UI/PS4/Cross.tga> View"),
	'construction_cost_Concrete', 10000,
	'construction_cost_Metals', 5000,
	'is_tall', true,
	'dome_required', true,
	'maintenance_resource_type', "Concrete",
	'display_name', T(694875956665, --[[ModItemBuildingTemplate LH_Cemetery display_name]] "Cemetery"),
	'display_name_pl', T(828847683570, --[[ModItemBuildingTemplate LH_Cemetery display_name_pl]] "Cemeteries"),
	'description', T(211858511222, --[[ModItemBuildingTemplate LH_Cemetery description]] "Do not disturb the dead..."),
	'build_category', "Habitats",
	'display_icon', "UI/Cemetery.tga",
	'build_pos', 13,
	'entity', "Cemetery",
	'label1', "Decorations",
	'color_modifier', RGBA(0, 133, 43, 255),
}),
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Cemetery",
	'FileName', "Code/Cemetery.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Fight",
	'FileName', "Code/Fight.lua",
}),
PlaceObj('ModItemEntity', {
	'name', "Cemetery",
	'entity_name', "Cemetery",
	'import', "D:/Projects/SM/MarsAssets/Source/Mods/Cemetery/ent_src/Cemetery.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "CemeteryGrave_01",
	'entity_name', "CemeteryGrave_01",
	'import', "D:/Projects/SM/MarsAssets/Source/Mods/Cemetery/ent_src/CemeteryGrave_01.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "CemeteryGrave_02",
	'entity_name', "CemeteryGrave_02",
	'import', "D:/Projects/SM/MarsAssets/Source/Mods/Cemetery/ent_src/CemeteryGrave_02.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "CemeteryGrave_03",
	'entity_name', "CemeteryGrave_03",
	'import', "D:/Projects/SM/MarsAssets/Source/Mods/Cemetery/ent_src/CemeteryGrave_03.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "FemaleZombie",
	'entity_name', "FemaleZombie",
	'import', "Entities/FemaleZombie.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "MaleZombie",
	'entity_name', "MaleZombie",
	'import', "Entities/MaleZombie.ent",
}),
PlaceObj('ModItemParticleSystemPreset', {
	group = "Default",
	id = "CemeteryParticleSystem",
	saving = true,
	PlaceObj('ParticleBehaviorColorize', {
		'bins', set( "B" ),
		'start_color_min', RGBA(179, 179, 179, 255),
		'type', "Start color only",
	}, nil, nil),
	PlaceObj('ParticleEmitter', {
		'label', "Skull",
		'bins', set( "B" ),
		'time_stop', 200,
		'max_live_count', 1,
		'parts_per_sec', 100000,
		'lifetime_min', 1400,
		'lifetime_max', 1400,
		'texture', "Textures/Particles/GraveSkull.dds",
		'emit_detail_level', 80,
		'emit_fade', range(0, 15000),
		'view_dependent_opacity', 100,
		'shader', "Add",
		'size_max', 1000,
		'alpha', range(255, 255),
		'outlines', {},
		'texture_hash', -630916258,
	}, nil, nil),
	PlaceObj('ParticleBehaviorRandomSpeedSpray', {
		'bins', set( "B" ),
		'direction', point(0, 100, 0),
		'spread_angle', 0,
		'vel_min', 5000,
		'vel_max', 5000,
	}, nil, nil),
	PlaceObj('ParticleBehaviorFriction', {
		'bins', set( "B" ),
		'friction', 950,
	}, nil, nil),
	PlaceObj('ParticleBehaviorFadeInOut', {
		'bins', set( "B" ),
		'fade_in_time', 100,
		'fade_out_time', 100,
		'fade_out_alpha', 20,
	}, nil, nil),
}),
PlaceObj('ModItemSoundPreset', {
	group = "Default",
	id = "GravePlacement",
	mindistance = 2000,
	type = "AmbienceSpot",
	PlaceObj('Sample', {
		'file', "Sounds/add_grave",
	}),
}),
}
