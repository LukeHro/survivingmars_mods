AppearLocationPresets.RandomDome = {
    text = "Random Dome",
    resolve = function(unit)
        local dome = IsKindOf(unit, "Colonist") and unit.residence and unit.residence.parent_dome or unit.dome
        if dome then
            return dome:GetRandomPos()
        end
    end
}

function OnMsg.MapSectorsReady(exploration)
    local map_data = ActiveMaps[exploration.map_id]
	if IsGameRuleActive("LH_DeepScannedRule") and map_data.Environment == "Surface" then
		for i = 1, 10 do
			for j = 1, 10 do
				local sector = exploration.MapSectors[i][j]
				sector:Scan("deep scanned")
			end
		end
	end
end

local BiasValueByTag_DumbAIPlayer = DumbAIPlayer.BiasValueByTag
function DumbAIPlayer:BiasValueByTag(value, tag)
	local biased = BiasValueByTag_DumbAIPlayer(self, value, tag)
	if IsGameRuleActive("LH_FasterRivals") and (tag=="action_delay" or tag=="ai_think_interval") then
		biased = MulDivRound(biased, 3, 4)
		for _, rule in ipairs(self.production_rules) do
			rule.production_interval = self.production_interval
		end
	end
	return biased
end

local CreateAIThinkThread_DumbAIPlayer = DumbAIPlayer.CreateAIThinkThread
function DumbAIPlayer:CreateAIThinkThread(...)
	if IsGameRuleActive("LH_FasterRivals") then
		self.production_interval = MulDivRound(self.production_interval, 3,4)
		self.think_interval = MulDivRound(self.think_interval, 3,4)
	end
	return CreateAIThinkThread_DumbAIPlayer(self, ...)		
end


local Getui_command_Colonist = Colonist.Getui_command
function Colonist:Getui_command(...)
	if self.outpost then
		return T{"Working at <outpost>", outpost = self.outpost.display_name}
	elseif self.command == "WaitToAppear" then 
		if self.holder then
			return T{"Away in <holder>", holder = self.holder.name}
		else
			return T{"N/A"}
		end
	end
	return Getui_command_Colonist(self, ...)
end

local Select_Colonist = Colonist.Select
function Colonist:Select(cycle_func)
	if self.command == "WaitToAppear" then 
		SelectObjWithCycle(self, cycle_func)
	else
		Select_Colonist(self, cycle_func)
	end
end

local GetCommandCenterColonists_vanilla = GetCommandCenterColonists
function GetCommandCenterColonists(context)
	if context.dome or not UICity.labels.Disappeared then
		return GetCommandCenterColonists_vanilla(context)
	end
	local city_colonists = table.icopy(UICity.labels.Colonist)
	for _,v in ipairs(UICity.labels.Disappeared) do
		if IsKindOf(v, "Colonist") and v.command ~= "Die" then
			table.insert_unique(UICity.labels.Colonist, v)
		end
	end
	local colonists = GetCommandCenterColonists_vanilla(context)
	UICity.labels.Colonist = city_colonists
	return colonists
end

local UpdateColonistsProc_City = City.UpdateColonistsProc
function City:UpdateColonistsProc()
	UpdateColonistsProc_City(self)
	local before_start_first_shift = (const.DefaultWorkshifts[1][1] + const.HoursPerDay - 1) % const.HoursPerDay
	if self.hour == before_start_first_shift then
		for _,v in ipairs(UICity.labels.Disappeared or {}) do
			if IsKindOf(v, "Colonist") then
                v:UpdateAgeTrait()
				v:DailyUpdate()		
			end
		end
	end
	for _,v in ipairs(UICity.labels.Disappeared or {}) do
		if IsKindOf(v, "Colonist") then
			local outpost = v.outpost
			if v.command == "Die" and outpost then
				local idx = table.find(outpost.crew, "handle", v.handle)
				if idx then 
					table.remove(outpost.crew, idx) 
					print("removed") 
					local reason = "Old age"
					Msg("ColonistDie", self, reason)
					local city = v.city
					local dome = v.dome
					v:ClearTransportRequest()
					v.dying = true
					v.dying_reason = reason
					v.stat_health = 0
					v:OnDie(reason)
					v.last_workplace = v.workplace
					v.last_residence = v.residence
					v:SetWorkplace(false)
					v:SetResidence(false)
					v:AssignToService(false) -- unassign from building when interupted
					table.clear(v.status_effects)
					v:ProjectPhoenixEffect()
					if IsValid(v) then
						DoneObject(v)
					end
					if v.traits.Infected then
						table.remove_entry(g_InfectedDeadCitizens,v)
					end
				end
			end
		end
	end
end
