HasConsumption.additionalConsumption = false

local GameInit_HasConsumption = HasConsumption.GameInit
function HasConsumption:GameInit(...)
    self.additionalConsumption = {}
    GameInit_HasConsumption(self, ...)
end

function HasConsumption:AddAdditionalConsumption(resource, amount, max, type)
    local consumption = HasConsumption:new({ building = self, city = self.city })
    consumption.consumption_resource_type = resource
    consumption.consumption_max_storage = max
    consumption.consumption_amount = amount
    consumption.consumption_type = type
    consumption.consumption_stored_resources = 0
    local resource_unit_count = 5 + (consumption.consumption_max_storage / (const.ResourceScale * 10))
    local d_req = self:AddDemandRequest(consumption.consumption_resource_type, consumption.consumption_max_storage, const.rfWaitToFill, resource_unit_count)
    consumption.consumption_resource_request = d_req
    consumption:ConnectToCommandCenters()
    consumption.building = self
    self:Attach(consumption)
    self.additionalConsumption = self.additionalConsumption or {}
    self.additionalConsumption[resource] = consumption
    return consumption
end

local ConsumptionDroneUnload_HasConsumption = HasConsumption.ConsumptionDroneUnload
function HasConsumption:ConsumptionDroneUnload(drone, req, resource, amount)
    local additionalConsumption = self.additionalConsumption and self.additionalConsumption[resource] or self["need" .. resource]
    if additionalConsumption and additionalConsumption.consumption_resource_request == req then
        local was_work_possible = self:CanConsume()
        additionalConsumption.consumption_stored_resources = additionalConsumption.consumption_stored_resources + amount
        self:UpdateVisualStockpile()
        self:UpdateRequestConnectivity()
        if not was_work_possible and self:CanConsume() then
            if self:IsKindOf("Building") then
                self:AttachSign(false, "SignNoConsumptionResource")
            end
            if not self.working then
                self:UpdateWorking()
            end
        end
    else
        ConsumptionDroneUnload_HasConsumption(self, drone, req, resource, amount)
    end
end

function HasConsumption:ConsumeResourcePerDay(consumption, dt)
    local amount_to_consume = MulDivRound(consumption.consumption_amount, dt, const.DayDuration)
    amount_to_consume = Min(amount_to_consume, consumption.consumption_stored_resources)
    if amount_to_consume == 0 then
        return false
    end
    consumption.consumption_stored_resources = consumption.consumption_stored_resources - amount_to_consume
    consumption.consumption_resource_request:AddAmount(amount_to_consume)
    self.city:OnConsumptionResourceConsumed(consumption.consumption_resource_type, amount_to_consume)
    self:UpdateVisualStockpile()
    self:UpdateRequestConnectivity()
    return amount_to_consume
end

function Building:GetAdditionalConsumptionText(short, consumption)
    if consumption and consumption.consumption_stored_resources and consumption.consumption_stored_resources > 0 then
        return short and T { "<resource(stored,max,type)>",
                             type = consumption.consumption_resource_type,
                             stored = consumption.consumption_stored_resources,
                             max = consumption.consumption_max_storage }
                or T { "Stored <resource(type)><right><resource(stored,max,type)>",
                       type = consumption.consumption_resource_type,
                       stored = consumption.consumption_stored_resources,
                       max = consumption.consumption_max_storage }
    else
        return short and T { "<red><resource(stored,max,type)></red>",
                             type = consumption.consumption_resource_type,
                             stored = consumption.consumption_stored_resources,
                             max = consumption.consumption_max_storage }
                or T { "Insufficient <resource(type)><right><red><resource(stored,max,type)></red>",
                       type = consumption.consumption_resource_type,
                       stored = consumption.consumption_stored_resources,
                       max = consumption.consumption_max_storage }
    end
end

local GetConstructionDescription_vanilla = GetConstructionDescription
function GetConstructionDescription(class, ...)
    local description = GetConstructionDescription_vanilla(class, ...)
    local t = description and description[1] and description[1].table
    for i, v in ipairs(t or empty_table) do
        if TGetID(v) == 3959 then --Consumption
            t[i] = GetTemplateConsumptionDescription(class, t[i])
        end
    end
    return description
end

function GetTemplateConsumptionDescription(template, base)
    return base
end

function LH_Res_AddDeposit(city, resource, base_amount, layer)
    local map_id = city.map_id
    local game_map = GameMaps[map_id]
    local buildable_grid = game_map.buildable
    local terrain = game_map.terrain
    local marker = PlaceObjectIn("SubsurfaceDepositMarker", map_id)
    marker.resource = resource
    marker.grade = DepositGradesTable[city:Random(1, 5)]
    marker.depth_layer = layer or city:Random(1, 2)
    marker.max_amount = city:Random(base_amount / 2, base_amount) * const.ResourceScale
    for _ = 1, 10 do --retries
        local sector = city.MapSectors[city:Random(1, 10)][city:Random(1, 10)]
        local minx, miny = sector.area:minxyz()
        local maxx, maxy = sector.area:maxxyz()
        local x = city:Random(minx, maxx)
        local y = city:Random(miny, maxy)
        local q, r = WorldToHex(x, y)
        local pt = point(x, y)
        if buildable_grid:IsBuildable(q, r) and terrain:IsPassable(pt) then
            marker:SetPos(pt)
            if marker:IsValidPos() then
                sector:RegisterDeposit(marker)
                break
            else
                printf("couldn't find position to place %s deposit", resource)
                DoneObject(marker)
            end
        end
    end
end

function OnMsg.ClassesPostprocess()
    if IsDlcAvailable("prunariu") then
        PlaceObj("ModItemBuildingTemplate", {
            "Group", "Storages",
            "Id", "DynamicStorageDepot",
            "template_class", "DynamicStorageDepot",
            "instant_build", true,
            "dome_forbidden", true,
            "can_refab", false,
            "display_name", T("Dynamic Depot"),
            "display_name_pl", T("Dynamic Depots"),
            "description", T("Stores a total of <resource(max_storage_per_resource)> units of transportable resources. Some resources will be transported to other depots within Drone range."),
            "build_category", "Storages",
            "display_icon", "UI/Icons/Buildings/universal_storage.tga",
            "entity", "StorageDepot",
            "encyclopedia_id", "UniversalStorageDepot",
            "encyclopedia_image", "UI/Encyclopedia/UniversalDepot.tga",
            "build_shortcut1", "U",
            "on_off_button", false,
            "prio_button", false,
            "count_as_building", false,
            "disabled_in_environment1", "",
            "disabled_in_environment2", "",
            "desired_amount", 30000,
            "desire_slider_max", 36,
            "storable_resources", {
                "Concrete",
                "Metals",
                "PreciousMetals",
                "PreciousMinerals",
                "Food",
                "Meat",
                "Booze",
                "Seeds",
                "Polymers",
                "MachineParts",
                "Electronics",
                "Fuel",
                "WasteRock",
                "MysteryResource",
                "BlackCube"
            },
        })
    end
end