lh = {}

function lh.deep_search(root, query, value)
    if type(root) ~= "table" then return false end
    local isQueryFunc = type(query) == "function"
    for i, v in ipairs(root) do
        if not isQueryFunc then
            if v[query] == value then return i, root end
        elseif value == nil then
            if query(v) then return i, root end
        elseif query(v) == value then return i, root end
    end
    for _, child in ipairs(root) do
        local index, parent = lh.deep_search(child, query, value)
        if parent then return index, parent end
    end
end

function lh.deep_find(root, query, value, remove)
    local index, parent = lh.deep_search(root, query, value)
    if not index then return false end
    if remove then
        return table.remove(parent, index), parent
    else
        return parent[index], parent
    end
end

function lh.safe_insert(parent, value, index)
    if value then
        if index then
            index = Min(index, #parent + 1)
            if parent[index] ~= value then
                table.insert(parent, index, value)
            end
        else
            table.insert_unique(parent, value)
        end
    end
end

function lh.insert_after(tbl, value, pos)
    if not table.find(tbl, value) then
        local idx = pos and table.find(tbl, pos) or #tbl
        table.insert(tbl, idx + 1, value)
    end
end

function lh.safe_notEmpty(table)
    return table and #table > 0
end

function lh.safe_pairs(table)
    return pairs(table or empty_table)
end