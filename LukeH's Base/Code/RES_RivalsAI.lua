if IsDlcAccessible("gagarin") then
    local food = table.remove(HelpOfferBasic, table.find(HelpOfferBasic, "ai_res", "food"))
    HelpOfferLSRes = { food }
    HelpOfferOtherRes = {} --add support here eventually

    function DumbAIPlayer:GetHelpResources(resource_type)
        return resource_type == "basic" and HelpOfferBasic
                or resource_type == "advanced" and HelpOfferAdvanced
                or resource_type == "ls_res" and HelpOfferLSRes
                or HelpOfferOtherRes
    end

    --overwritten cause vanilla uses localed resources lists
    function DumbAIPlayer:GetResourcesHelpOffer(resource_type)
        local items = {}
        local resources = self:GetHelpResources(resource_type)
        for i, res in ipairs(resources) do
            local count = self.resources[res.ai_res]
            items[#items + 1] = {
                choice_text = T({883661504598, "<resource(res)>", res = res.game_res }),
                extra_text = T({11535, "send rocket carrying <resource>", resource = FormatResourceValueMaxResource(empty_table, res.help, res.game_res) }),
                enabled = count <= res.min,
                offer = res
            }
        end
        return items
    end

    DistressCallBasic = {
        {message = T(993967459548, "We need Metals"), ai_res = "metals", game_res = "Metals", amount = 50},
        {message = T(169968368423, "We need Concrete"), ai_res = "concrete", game_res = "Concrete", amount = 50}
    }

    DistressCallAdvanced = {
        {message = T(367527373955, "We need Polymers"), ai_res = "polymers", game_res = "Polymers", amount = 50},
        {message = T(328754292111, "We need Machine Parts"), ai_res = "machineparts", game_res = "MachineParts", amount = 50},
        {message = T(541621217058, "We need Electronics"), ai_res = "electronics", game_res = "Electronics", amount = 50}
    }

    DistressCallLS = {
        {message = T(128954319804, "We need Food"), ai_res = "food", game_res = "Food", amount = 50}
    }

    DistressCallOtherRes = {} --add support here eventually
end

function OnMsg.ClassesPostprocess()
    if not IsDlcAccessible("gagarin") then
        return
    end
    if not Presets.Negotiation.Replies.RepliesDistressCall_LSRes then
        PlaceObj("Negotiation", {
            Effects = {},
            Prerequisites = {},
            Text = T(799274647381, "What do you want?"),
            Title = T(11546, "<NegotiationsContactTitle>"),
            group = "Replies",
            id = "RepliesDistressCall_LSRes",
            save_in = "gagarin",
            PlaceObj("NegotiationReply", {
                "Text", T(990315714894, "On second thought..."),
                "Prerequisites", {},
                "DefaultCloseAction", true
            }),
            PlaceObj("NegotiationOutcome", {
                "Prerequisites", {},
                "Next", "RepliesDistressCall_1",
                "Effects", {}
            })
        })
        local distressRoot = Presets.Negotiation.Replies.RepliesDistressCall_1
        local idx = table.find(distressRoot, "Next", "RepliesDistressCall_3")
        table. insert(distressRoot, idx + 1, PlaceObj("NegotiationReply", {
            "Comment", "LSR", "Text", T("Life Support resource"), "Prerequisites", {}
        }))
        table.insert(distressRoot, idx + 2, PlaceObj("NegotiationOutcome", {
            "Id", "LSO", "Prerequisites", {}, "Next", "RepliesDistressCall_LSRes", "Effects", {}
        }))
    end
    for i = #Presets.Negotiation.Replies.RepliesDistressCall_2 - 2, 1, -1 do
        table.remove(Presets.Negotiation.Replies.RepliesDistressCall_2, i) --basic
    end
    for i = #Presets.Negotiation.Replies.RepliesDistressCall_3 - 2, 1, -1 do
        table.remove(Presets.Negotiation.Replies.RepliesDistressCall_3, i) --advanced
    end
    for i = #Presets.Negotiation.Replies.RepliesDistressCall_LSRes - 2, 1, -1 do
        table.remove(Presets.Negotiation.Replies.RepliesDistressCall_LSRes, i)
    end
    if not Presets.Negotiation.Replies.RepliesHelpOffer_LSRes then
        PlaceObj("Negotiation", {
            Effects = {},
            Prerequisites = {},
            Text = T(297659968639, "What do you offer?"),
            Title = T(11546, "<NegotiationsContactTitle>"),
            group = "Replies",
            id = "RepliesHelpOffer_LSRes",
            save_in = "gagarin",
            PlaceObj("ReplyGenerator_HelpRocket", { "idResourceType", "ls_res" }),
            PlaceObj("NegotiationReply", {
                "Text", T(424168816047, "On second thought..."),
                "Prerequisites", {},
                "DefaultCloseAction", true
            }),
            PlaceObj("NegotiationOutcome", {
                "Prerequisites", {},
                "Next", "RepliesHelpOffer_1",
                "Effects", {}
            })
        })
        local helpRoot = Presets.Negotiation.Replies.RepliesHelpOffer_1
        local idx = table.find(helpRoot, "Next", "RepliesHelpOffer_3")
        table.insert(helpRoot, idx + 1, PlaceObj("NegotiationReply", {
            "Comment", "LSR", "Text", T("Life Support resource"), "Prerequisites", {}
        }))
        table.insert(helpRoot, idx + 2, PlaceObj("NegotiationOutcome", {
            "Id", "LSO", "Prerequisites", {}, "Next", "RepliesHelpOffer_LSRes", "Effects", {}
        }))
    end
end

local function AddDistressRes(resources, replies)
    for i = 1, #resources do
        local res = resources[i]
        table.insert(replies, 2 * i - 1, PlaceObj("NegotiationReply", {
            "Text", res.message, "CustomDisabledText", T(521255950570, "They can't help"),
            "Prerequisites", { PlaceObj("CountRivalResource", { "Resource", res.ai_res, "Amount", res.amount }) }
        }))
        table.insert(replies, 2 * i , PlaceObj("NegotiationOutcome", {
            "Prerequisites", {}, "NextGroup", "Distress Calls (Accepted)",
            "Effects", { PlaceObj("RivalSetDistressCallResource", { "idResource", res.game_res }) }
        }))
    end
end

function OnMsg.ModsReloaded()
    if not IsDlcAccessible("gagarin") then
        return
    end
    AddDistressRes(DistressCallBasic, Presets.Negotiation.Replies.RepliesDistressCall_2)
    AddDistressRes(DistressCallAdvanced, Presets.Negotiation.Replies.RepliesDistressCall_3)
    AddDistressRes(DistressCallLS, Presets.Negotiation.Replies.RepliesDistressCall_LSRes)
end

function UpdateRivalAI(ai, id, preset)
    if not preset then
        return
    end
    for _, res in ipairs(preset.initial_resources) do
        local resource = res.resource
        ai.resources[resource] = (ai.resources[resource] and ai.resources[resource] or 0)
    end
    ai:RemoveAIDef(preset)
    ai:AddAIDef(preset)
    print(id," AI updated")
end

--function GetTradePrice(resource)
--    local presets = Presets.Cargo
--    local def = presets["Basic Resources"][resource]
--    def = def or presets["Advanced Resources"][resource]
--    def = def or presets["Other Resources"][resource]
--    return def and def.price / def.pack
--end
--local TradeOfferCallback = function(obj, params, res) --cause in game is localed and used by the function bellow
--    if res == 1 then
--        local rocket = PlaceBuildingIn("ForeignTradeRocket", MainMapID, {
--            city = MainCity,
--            export_res = params.params.import_res,
--            export_amount = params.params.import_amount,
--            import_res = params.params.export_res,
--            import_amount = params.params.export_amount,
--            ai_player = params.params.ai_player,
--            trade_pad = obj
--        })
--        rocket:SetCommand("BeginTrade")
--        obj.trade_rocket = rocket
--        ObjModified(obj)
--    elseif res == 2 then
--        obj.next_offer_time = GameTime() + obj.TradeInterval
--    end
--end
----I had to copy paste the function below as there was no convenient way to feed the price there
----Unfortunately this means two mods trying to do this won't be compatible
----If devs could make the small change to just extract GetTradePrice, as I did, it would be more modable
--function TradePad:GenerateOffer(force)
--    if not (self.import_resource and self.export_resource) or IsValid(self.trade_rocket) or self.demolishing then
--        return
--    end
--    local list = {}
--    local import = string.lower(self.import_resource)
--    for name, ai_player in pairs(RivalAIs) do
--        if ai_player.resources.standing >= self.MinStanding and ai_player.resources[import] >= self.MinAIExportAmount or force then
--            table.insert(list, name)
--        end
--    end
--    if #list == 0 then
--        self.trade_fail_reason = true
--        return
--    end
--    self.trade_fail_reason = nil
--    local rival = table.rand(list)
--    local import_price = GetTradePrice(self.export_resource)
--    local export_price = GetTradePrice(self.import_resource)
--    local import_amount = MulDivRound(import_price, self.TradeAmount, export_price)
--    import_amount = 10 * (MulDivRound(import_amount, SessionRandom:Random(80, 120), 100) / 10)
--    AddOnScreenNotification("NewTradeOffer", TradeOfferCallback, {
--        import_res = self.import_resource,
--        import_amount = import_amount,
--        export_res = self.export_resource,
--        export_amount = self.TradeAmount,
--        ai_player = RivalAIs[rival],
--        rival = RivalAIs[rival].display_name
--    }, {self}, self:GetMapID())
--    NotificationHoldingPad = self
--end