local translationIdBase = 1107000200

function OnMsg.ClassesBuilt()
	PlaceObj('XTemplate', {
		__is_kind_of = "XContextControl",
		group = "Infopanel Sections",
		id = "sectionVisitorsList",
		PlaceObj('XTemplateWindow', {
			'__class', "XContextControl",
			'HAlign', "center",
			'LayoutMethod', "HWrap",
			'OnContextUpdate', function (self, context, ...)
				for i, win in ipairs(self) do
					local person = context.visitors[i]
					win.idSpecialization:SetVisible(person)
					if person then
						win:SetIcon(person:GetInfopanelIcon())
						win.idSpecialization:SetImage(person.ip_specialization_icon)
					elseif i <= context.max_visitors then
						win:SetIcon("UI/Infopanel/colonist_empty.tga")
					else
						win:SetIcon("UI/Infopanel/colonist_closed.tga")
					end
					win:SetContext(person)
					XRecreateRolloverWindow(win)
				end
			end,
		}, {
			PlaceObj('XTemplateForEach', {
				'array', function (parent, context) return context.visitors, 1, context:GetClassValue("max_visitors") or context.max_visitors end,
				'__context', function (parent, context, item, i, n) return item end,
			}, {
				PlaceObj('XTemplateTemplate', {
					'__template', "InfopanelPerson",
					'OnPress', function (self, gamepad)  
						if self.context then
							self.context:Select(ResidenceCycle)
						end
					end,
					'OnAltPress', function (self, gamepad)
						local building = self.parent.context
						local icon =  self:GetIcon()
						local i = table.find(self.parent, self)
						if self.context then
							building:Unassign(self.context, i)
						elseif building:GetClassValue("max_visitors") then
							if icon == "UI/Infopanel/colonist_empty.tga" then
								building.max_visitors = i - 1
							else
								building.max_visitors = i
							end
						end
						ObjModified(building)
					end,
				}, {
					PlaceObj('XTemplateFunc', {
						'name', "GetRolloverTitle",
						'func', function (self, ...)  
							local icon = self:GetIcon()						
							if self.context then
								return self.context:GetDisplayName()
							elseif icon == "UI/Infopanel/colonist_empty.tga" then
								return T{3976, "Free slot"}
							else
								return T{4176, "Closed slot"}
							end
						end,
					}),
					PlaceObj('XTemplateFunc', {
						'name', "GetRolloverText",
						'func', function (self, ...)  
						local icon = self:GetIcon()
							if self.context then
								return T{translationIdBase+1, "A Colonist visiting in this building.<newline><newline>Interest: <em><daily_interest></em>"}
							elseif icon == "UI/Infopanel/colonist_empty.tga" then
								return T{translationIdBase+2, "A free slot."}
							else
								return T{4177, "This slot is closed. Colonists will never occupy it."}
							end
						end,
					}),
					PlaceObj('XTemplateFunc', {
						'name', "GetRolloverHint",
						'func', function (self, ...) 
							local building = self.parent.context
							if building:GetClassValue("max_visitors") then
								local icon = self:GetIcon()
								if self.context then
									return T{8987, "<right_click> Evict this Colonist"}
								elseif icon == "UI/Infopanel/colonist_empty.tga" then
									return T{translationIdBase+3, "<right_click> Close this slot"}
								else
									return T{translationIdBase+4, "<right_click> Open this slot"}
								end
							end
						end,
					}),
					PlaceObj('XTemplateFunc', {
						'name', "GetRolloverHintGamepad",
						'func', function (self, ...)
							local building = self.parent.context
							if building:GetClassValue("max_visitors") then
								local icon = self:GetIcon()
								if self.context then
									return T{8991, "<ButtonX> Evict this Colonist"}
								elseif icon == "UI/Infopanel/colonist_empty.tga" then
									return T{translationIdBase+5, "<ButtonX> Close this slot"}
								else
									return T{translationIdBase+6, "<ButtonX> Open this slot"}
								end
							end
						end,
					}),
					}),
				}),
			}),
	})
		
	XTemplates.sectionVisitors[1][1] = PlaceObj('XTemplateTemplate', {
		'sectionVisitorsList', true,
		'__template', "sectionVisitorsList",
		--Title = T{526, --[[XTemplate sectionVisitors Title]] "Visitors"},
		--Icon = "UI/Icons/Sections/colonist.tga",
	})
	XTemplates.sectionVisitors[1].Title = T{translationIdBase+7, "Visitors<right><count(visitors)> / <colonist(max_visitors)>"}
end

