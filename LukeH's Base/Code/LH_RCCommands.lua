local translationIdBase = 1107000600

function OnMsg.ClassesBuilt()
	local idx = table.find(XTemplates.ipAnomaly[1],"lh_scan", true)
	if idx then
		table.remove(XTemplates.ipAnomaly[1],idx)
	end 
	XTemplates.ipAnomaly[1][#XTemplates.ipAnomaly[1]+1] = PlaceObj('XTemplateTemplate', {
			"lh_scan",true,
			'Id',"lh_scan",
			'__template', "InfopanelButton",
			'RolloverText', T{translationIdBase+1, "Call idle Explorer to scan."},
			'RolloverDisabledText', T(translationIdBase+2, "No idle Explorer."),
			'RolloverTitle', T{translationIdBase+3, "Call Explorer"},
			'RolloverHint', T{translationIdBase+4, "<left_click> Call <newline>"},
			'RolloverHintGamepad', T{translationIdBase+5, "<ButtonA> Call <newline>"},
			'OnContextUpdate', function(self, context, ...)
								local explorer = MapFindNearest(self.context, "map", "ExplorerRover", 
								function(r)
									return r:GetStateText() == "idle"
								end)
								self:SetEnabled(explorer or false)
							end,
			'OnPress', function (self, gamepad)
							local explorer = MapFindNearest(self.context, "map", "ExplorerRover", 
								function(r)
									return r:GetStateText() == "idle"
								end)
							if explorer then
								explorer:SetCommand("Analyze", self.context)
							end

						end,
			'Icon', "UI/Icons/IPButtons/expedition.tga",
			})		

	idx = table.find(XTemplates.ipSurfaceDeposit[1],"lh_collect", true)
	if idx then
		table.remove(XTemplates.ipSurfaceDeposit[1],idx)
	end 
	XTemplates.ipSurfaceDeposit[1][#XTemplates.ipSurfaceDeposit[1]+1] = PlaceObj('XTemplateTemplate', {
			"lh_collect",true,
			'Id',"lh_collect",
			'__template', "InfopanelButton",
			'RolloverText', T{translationIdBase+6, "Call idle Transport to collect."},
			'RolloverDisabledText', T(translationIdBase+7, "No idle Transport."),
			'RolloverTitle', T{translationIdBase+8, "Call Transport"},
			'RolloverHint', T{translationIdBase+9, "<left_click> Collect <newline>"},
			'RolloverHintGamepad', T{translationIdBase+10, "<ButtonA> Collect <newline>"},
			'OnContextUpdate', function(self, context, ...)
								local transport = MapFindNearest(self.context, "map", "RCTransport", "RCConstructor", "RCHarvester", 
								function(r)
									return r:GetStateText() == "idle"
								end)
								self:SetEnabled(transport or false)
							end,
			'OnPress', function (self, gamepad)
							local transport = MapFindNearest(self.context, "map", "RCTransport", "RCConstructor", "RCHarvester", 
								function(r)
									return r:GetStateText() == "idle"
								end)
							if transport then
								local resource = self.context.group and self.context.group[1] or self.context
								transport:SetCommand("PickupResource", resource.transport_request)
							end

						end,
			'Icon', "UI/Icons/IPButtons/load.tga",
			})		
end