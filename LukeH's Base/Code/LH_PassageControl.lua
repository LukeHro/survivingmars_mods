local translationIdBase = 1107000500

local vanillaCreateDomeNetworks = CreateDomeNetworks

local function LH_ClearConnections()
	for _, d in ipairs(UICity.labels.Dome or empty_table) do
		for dome in pairs(d.connected_domes or empty_table) do
			if d.connected_domes[dome] <= 0 then
				d.connected_domes[dome] = nil
			end
		end
	end
end

local function LH_AddAllConnections()
	local networks = vanillaCreateDomeNetworks(UICity)
	for _, d in ipairs(UICity.labels.Dome or empty_table) do
		local n = networks[d]
		for dome, network in pairs(networks) do
			if network == n and d ~= dome and not d.connected_domes[dome] then
				d.connected_domes[dome] = -1
			end
		end
	end
    return networks
end

local function LH_Add2ndConnections(i)
	for _, d in ipairs(UICity.labels.Dome or empty_table) do
		for dc, j in pairs(d.connected_domes or empty_table) do
			if j > i then
				for dcc, k in pairs(dc.connected_domes or empty_table) do
					if k > 0 and d~=dcc and not d.connected_domes[dcc] then
						d.connected_domes[dcc] = i
					end
				end
			end
		end
	end
end

local function LH_AddConnections(city)
	if city.lh_passages and city.lh_passages == 0 then
		return LH_AddAllConnections()
	else
		local networks = vanillaCreateDomeNetworks(city)
		if city.lh_passages and city.lh_passages > 1 then
			for i = 2, city.lh_passages do
				LH_Add2ndConnections(1 - i)
			end
		end
        return networks
	end
end

local vanillaConnectDomesWithPassage = ConnectDomesWithPassage
function ConnectDomesWithPassage(d1, d2)
	LH_ClearConnections()
	vanillaConnectDomesWithPassage(d1, d2)
	LH_AddConnections(UICity)
end

local vanillaDisconnectDomesConnectedWithPassage = DisconnectDomesConnectedWithPassage
function DisconnectDomesConnectedWithPassage(d1, d2)
	LH_ClearConnections()
	vanillaDisconnectDomesConnectedWithPassage(d1, d2)
	LH_AddConnections(UICity)
end

function CreateDomeNetworks(city)
	LH_ClearConnections()
	return LH_AddConnections(city)
end

function GetNumDomesConnectedToDome(dome) --some sponsor goal
	local num = 0
	for v, k in pairs(dome.connected_domes) do
		if k > 0 then
			num = num + 1
		end
	end
	return num
end

function OnMsg.ClassesBuilt()
	local idx = table.find(XTemplates.ipPassage[1],"lh_travel_distance", true)
	if idx then
		table.remove(XTemplates.ipPassage[1],idx)
	end
	XTemplates.ipPassage[1][#XTemplates.ipPassage[1]+1] = PlaceObj('XTemplateTemplate', {
			"lh_travel_distance",true,
			'Id',"lh_travel_distance",
			'__template', "InfopanelButton",
			'__condition', function (parent, context) return context:ShouldShowDemolishButton() end,
			'RolloverText', T{translationIdBase+1, "How many passages a colonist may use to travel to a workplace or service."},
			'RolloverTitle', T{translationIdBase+2, "Travel Distance"},
			'RolloverHint', T{translationIdBase+3, "<left_click> Increase<newline><right_click> Decrease"},
			'RolloverHintGamepad', T{translationIdBase+4, "<ButtonA> Increase<newline><ButtonX> Decrease"},
			'OnContextUpdate', function(self, context, ...)
							if not UICity.lh_passages or UICity.lh_passages == 1 then
								UICity.lh_passages = 1
								self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/travel_1.tga")
							elseif UICity.lh_passages == 2 then
								self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/travel_2.tga")
							elseif UICity.lh_passages == 3 then
								self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/travel_3.tga")
							else
								self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/travel_0.tga")
							end
						end,
			'OnPress', function (self, gamepad)
							if UICity.lh_passages < 3 and UICity.lh_passages > 0 then
								UICity.lh_passages = UICity.lh_passages + 1
								XContextWindow.OnContextUpdate(self, self.context)
								CreateDomeNetworks(UICity)
							else
								UICity.lh_passages = 0
								XContextWindow.OnContextUpdate(self, self.context)
								CreateDomeNetworks(UICity)
							end
						end,
			'AltPress', true,
			'OnAltPress', function (self, gamepad)
							if UICity.lh_passages == 0 then
								UICity.lh_passages = 3
								XContextWindow.OnContextUpdate(self, self.context)
								CreateDomeNetworks(UICity)
							elseif UICity.lh_passages > 1 then
								XContextWindow.OnContextUpdate(self, self.context)
								UICity.lh_passages = UICity.lh_passages - 1
								CreateDomeNetworks(UICity)
							end
						end,
			'Icon', CurrentModPath.."UI/Icons/IPButtons/travel_1.tga",
			})
end