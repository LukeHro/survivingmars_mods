function OnMsg.ClassesBuilt()
    local _, food = lh.deep_search(XTemplates.Infobar, "Id", "idFood")
    if food then
        local idAdvancedResources, root = lh.deep_search(XTemplates.Infobar, "Id", "idAdvancedResources")
        root[idAdvancedResources].MinWidth = nil
        root[idAdvancedResources].Padding = box(16, 0, 16, 0)
        if not table.find(root, "Id", "idLifeResources") then
            lh.deep_find(root, function(v) return v end, food, true)
            local lhLifeResources = PlaceObj('XTemplateWindow', {
                '__class', "XContextControl",
                'Id', "idLifeResources",
                'Padding', box(16, 0, 16, 0),
                'RolloverOnFocus', true,
            }, {
                PlaceObj('XTemplateFunc', {
                    'name', "OnSetFocus",
                    'func', function (self, ...)
                        XCreateRolloverWindow(self, true)
                        XContextWindow.OnSetFocus(self, ...)
                    end,
                }),
                PlaceObj('XTemplateWindow', {
                    'comment', "centers the texts",
                    'HAlign', "center",
                    'LayoutMethod', "HList",
                })
            })
            table.insert(root, idAdvancedResources + 1, lhLifeResources)
            table.insert(lhLifeResources[2], food)
            if IsDlcAccessible("armstrong") then
                local _, seeds = lh.deep_search(XTemplates.Infobar,"Id", "idSeeds")
                table.insert_unique(lhLifeResources[2], seeds)
                lh.deep_find(XTemplates.Infobar, "Id", "idSeedsResources", true)
            end
        end
    end
    local basics, _ = lh.deep_find(XTemplates.Infobar, "Id", "idBasicResources")
    basics.MinWidth = nil
    basics.Padding = box(16, 0, 16, 0)
    if IsDlcAccessible("picard") then
        local _, precious = lh.deep_find(XTemplates.Infobar,"Id","idPreciousMinerals")
        table.insert_unique(basics[2], precious)
        lh.deep_find(XTemplates.Infobar, "Id", "idRareMineralsResources", true)
    end
    local other, _ = lh.deep_find(XTemplates.Infobar, "Id", "idWasteRockResources")
    if other then
        other.MinWidth = nil
        other.Padding = box(16, 0, 16, 0)
        local _, fuel = lh.deep_find(XTemplates.Infobar, "Id", "idFuel")
        lh.deep_find(XTemplates.Infobar, function(v) return v end, fuel, true)
        table.insert(other[2], 1, fuel)
        other.Id = "idOtherResources"
    end
    Msg("LHResInfobarReady")
end

function OnMsg.ClassesBuilt()
    local basic, parent = lh.deep_find(XTemplates.CommandCenterCategories, "Id", "idBasicResources")
    local advancedIdx = lh.deep_search(parent, "Id", "idAdvancedResources")
    local other = lh.deep_find(parent, "Id", "idOtherResources")
    local exotics = lh.deep_find(other, "comment", "exotic minerals", true)
    lh.safe_insert(basic, exotics)
    local fuel = lh.deep_find(parent[advancedIdx], "comment", "fuel", true)
    lh.safe_insert(other, fuel, 1)
    if not table.find(parent, "Id", "idLifeResources") then
        local food = lh.deep_find(basic, "comment", "food", true)
        local seeds = lh.deep_find(other, "comment", "seeds", true)
        local lhLife = PlaceObj('XTemplateTemplate', {
            '__template', "CommandCenterStatsBox",
            'Id', "idLifeResources",
            'Margins', basic.Margins,
            'Padding', basic.Padding,
            'HandleKeyboard', false,
            'HandleMouse', false,
            'Title', T("LIFE SUPPORT RESOURCES"),
        }, { food, seeds })
        table.insert(parent, advancedIdx + 1, lhLife)
    end
    Msg("LHResCCCReady")
end

function OnMsg.ClassesPostprocess()
    local colonyIdx, parent = lh.deep_search(XTemplates.PlanetaryViewResources, "comment", "Advanced Resources")
    if not table.find(parent, "comment", "Life Support Resources") then
        local colonyLS = PlaceObj("XTemplateWindow", {
            "comment", "Life Support Resources",
            "__class", "XText",
            "Id", "idLifeSupportResourceLabel",
            "Padding", box(0, 0, 0, 0),
            "HandleMouse", false,
            "TextStyle", "PGLandingPosDetails",
            "Translate", true,
            "Text", T("Life Support Resources")
        })
        table.insert(parent, colonyIdx + 1, colonyLS)
    end
    if IsDlcAccessible("gagarin") then
        local rivalIdx, parent = lh.deep_search(XTemplates.PlanetaryViewResources, "Id", "idAdvancedResources")
        if not table.find(parent, "Id", "idLifeSupportResources") then
            local rivalLS = PlaceObj("XTemplateWindow", {
                "__class", "XText",
                "Id", "idLifeSupportResources",
                "Padding", box(0, 0, 0, 0),
                "HandleMouse", false,
                "TextStyle", "PGChallengeDescription",
                "Translate", true
            })
            table.insert(parent, rivalIdx +1, rivalLS)
        end
    end
    Msg("LHResPlanetReady")
end

local SetUIResourceValues_LandingSiteObject = LandingSiteObject.SetUIResourceValues
function LandingSiteObject:SetUIResourceValues(...)
    SetUIResourceValues_LandingSiteObject(self, ...)
    local spot = self.selected_spot
    if spot and spot.spot_type == "rival" then
        local obj = RivalAIs[spot.id]
        local resources = obj.resources
        local basic = self.dialog:ResolveId("idBasicResources")
        if basic then
            local row = {}
            row[#row + 1] = T({"<metals(metals)>", metals = Max(resources.metals * const.ResourceScale, 0)})
            row[#row + 1] = T({"<concrete(concrete)>", concrete = Max(resources.concrete * const.ResourceScale, 0)})
            row[#row + 1] = T({"<preciousmetals(preciousmetals)>", preciousmetals = Max(resources.raremetals * const.ResourceScale, 0)})
            self:EnhanceBasicRow(row, obj)
            basic:SetText(table.concat(row, " "))
        end
        local advanced = self.dialog:ResolveId("idAdvancedResources")
        if advanced then
            local row = {}
            row[#row + 1] = T({"<polymers(polymers)>", polymers = Max(resources.polymers * const.ResourceScale, 0)})
            row[#row + 1] = T({"<machineparts(machineparts)>", machineparts = Max(resources.machineparts * const.ResourceScale, 0)})
            row[#row + 1] = T({"<electronics(electronics)>", electronics = Max(resources.electronics * const.ResourceScale, 0)})
            self:EnhanceAdvancedRow(row, obj)
            advanced:SetText(table.concat(row, " "))
        end
        local life = self.dialog:ResolveId("idLifeSupportResources")
        if life then
            local row = {}
            row[#row +1] = T({"<food(food)>", food = Max(resources.food * const.ResourceScale, 0)})
            self:EnhanceLifeRow(row, obj)
            life:SetText(table.concat(row, " "))
        end
        local other = self.dialog:ResolveId("idOtherResources")
        if other then
            local row = {}
            row[#row +1] = T({"<fuel(fuel)>", fuel = Max(resources.fuel * const.ResourceScale, 0)})
            self:EnhanceOthersRow(row, obj)
            other:SetText(table.concat(row, " "))
        end
        return
    else
        local resourcesOverview = GetCityResourceOverview(UICity)
        local basic = self.dialog:ResolveId("idBasicResources")
        if basic then
            local row = {}
            row[#row + 1] = T({"<metals(metals)>", metals = resourcesOverview:GetAvailableMetals()})
            row[#row + 1] = T({"<concrete(concrete)>", concrete = resourcesOverview:GetAvailableConcrete()})
            row[#row + 1] = T({"<preciousmetals(preciousmetals)>", preciousmetals = resourcesOverview:GetAvailablePreciousMetals()})
            if IsDlcAccessible("picard") then
                row[#row + 1] = T({"<preciousminerals(preciousminerals)>", preciousminerals = resourcesOverview:GetAvailablePreciousMinerals()})
            end
            self:EnhanceBasicRow(row)
            basic:SetText(table.concat(row, " "))
        end
        local advanced = self.dialog:ResolveId("idAdvancedResources")
        if advanced then
            local row = {}
            row[#row + 1] = T({"<polymers(polymers)>", polymers = resourcesOverview:GetAvailablePolymers()})
            row[#row + 1] = T({"<machineparts(machineparts)>", machineparts = resourcesOverview:GetAvailableMachineParts()})
            row[#row + 1] = T({"<electronics(electronics)>", electronics = resourcesOverview:GetAvailableElectronics()})
            self:EnhanceAdvancedRow(row)
            advanced:SetText(table.concat(row, " "))
        end
        local life = self.dialog:ResolveId("idLifeSupportResources")
        if life then
            local row = {}
            row[#row +1] = T({"<food(food)>", food = resourcesOverview:GetAvailableFood()})
            if IsDlcAccessible("armstrong") then
                row[#row +1] = T({"<seeds(seeds)>", seeds = resourcesOverview:GetAvailableSeeds()})
            end
            self:EnhanceLifeRow(row)
            life:SetText(table.concat(row, " "))
        end
        local other = self.dialog:ResolveId("idOtherResources")
        if other then
            local row = {}
            row[#row +1] = T({"<fuel(fuel)>", fuel = resourcesOverview:GetAvailableFuel()})
            row[#row +1] = T({"<wasterock(wasterock)>", wasterock = resourcesOverview:GetAvailableWasteRock()})
            self:EnhanceOthersRow(row)
            other:SetText(table.concat(row, " "))
        end
    end
end

function LandingSiteObject:EnhanceBasicRow(row, rival)
    --override in mods
end

function LandingSiteObject:EnhanceAdvancedRow(row, rival)
    --override in mods
end

function LandingSiteObject:EnhanceLifeRow(row, rival)
    --override in mods
end

function LandingSiteObject:EnhanceOthersRow(row, rival)
    --override in mods
end