local translationIdBase = 1107000700

function OnMsg.ClassesBuilt()
    local lightning = PlaceObj('XTemplateTemplate', {
        '__template', "HUDButtonTemplate",
        'RolloverText', T(translationIdBase + 1, --[[XTemplate HUD RolloverText]] "Play at ten times normal speed."),
        'RolloverTitle', T(translationIdBase + 2, --[[XTemplate HUD RolloverTitle]] "Lightning"),
        'RolloverHint', T(4021, --[[XTemplate HUD RolloverHint]] "<em><ShortcutName('actionSpeedUp')>/<ShortcutName('actionSpeedDown')></em> Change game speed"),
        'Id', "idLightning",
        'Image', CurrentModPath .. "UI/HUD/lightning_speed.tga",
        'ImageShine', "UI/HUD/double_speed_shine.tga",
        'FXMouseIn', "SpeedControlMouseOver",
        'Rows', 2,
        'OnPress', function(self, gamepad)
            UIColony:SetGameSpeed(const.fastGameSpeed * 2)
            UISpeedState = "lightning"
        end,
        'ImageScale', 900,
    })
    local fastIdx, parent = lh.deep_search(XTemplates.HUD, "Id", "idFast")
    if not table.find(parent,"Id", "idLightning") then
        table.insert(parent, fastIdx + 1, lightning)
    end
end

function ChangeGameSpeedState(delta)
    local states = {
        "pause",
        "play",
        "medium",
        "fast",
        "lightning"
    }
    if IsDevelopmentSandbox() then
        states[#states + 1] = "ultra"
    end
    local idx = table.find(states, UISpeedState)
    local new_idx = Clamp(idx + delta, 1, #states)
    if new_idx ~= idx then
        local new_state = states[new_idx]
        SetGameSpeedState(new_state)
    end
end

local SetGameSpeedState_vanilla = SetGameSpeedState
function SetGameSpeedState(speed)
    SetGameSpeedState_vanilla(speed)
    if speed == "lightning" then
        UIColony:SetGameSpeed(const.fastGameSpeed*2)
    end
    UISpeedState = speed
end

local UpdateTimeButtons_HUD = HUD.UpdateTimeButtons
function HUD:UpdateTimeButtons()
    UpdateTimeButtons_HUD(self)
    local time_factor = GetTimeFactor()
    local isLightning = time_factor >= const.DefaultTimeFactor * const.fastGameSpeed * 2 and time_factor < const.DefaultTimeFactor * const.ultraGameSpeed
    self.idLightning:SetToggled(isLightning)
end

local GetEstimatedGameSpeedState_vanilla = GetEstimatedGameSpeedState
function GetEstimatedGameSpeedState()
    local time_factor = GetTimeFactor()
    if time_factor >= const.DefaultTimeFactor * const.fastGameSpeed * 2 and time_factor < const.DefaultTimeFactor * const.ultraGameSpeed then
        return "lightning"
    else
        return GetEstimatedGameSpeedState_vanilla()
    end
end

local my_mode_to_img = {
    no_mode = false,
    play = "UI/Onscreen/onscreen_normalspeed.tga",
    medium = "UI/Onscreen/onscreen_fastspeed.tga",
    fast = "UI/Onscreen/onscreen_super_fastspeed.tga",
    lightning = CurrentModPath .. "UI/HUD/onscreen_lightning.tga",
    ultra = "UI/Onscreen/onscreen_super_fastspeed.tga",
    paused = "UI/Onscreen/onscreen_pause.tga"
}

function OnScreenIndication:UpdateIcon()
    local time_mode = self:GetTimeMode()
    local mode = time_mode
    local img = my_mode_to_img[mode or self.mode]
    if img then
        self.idImage:SetImage(img)
    end
    if time_mode == "paused" then
        self.idImage:SetVisible(true)
    elseif self.mode ~= time_mode then
        self.temp_icon_remove = RealTime() + 3000
        self.idImage:SetVisible(true)
    elseif self.temp_icon_remove < RealTime() then
        self.idImage:SetVisible(false)
    end
    self.mode = mode
end