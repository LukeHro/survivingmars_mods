function ResourceStockpileBase:DoesAcceptBasicResources()
    return self:DoesAcceptResource("Metals") or self:DoesAcceptResource("Concrete")
            or self:DoesAcceptResource("PreciousMetals") or self:DoesAcceptResource("PreciousMinerals")
end

function ResourceStockpileBase:DoesAcceptAdvancedResources()
    return self:DoesAcceptResource("Polymers") or self:DoesAcceptResource("MachineParts")
            or self:DoesAcceptResource("Electronics")
end

function ResourceStockpileBase:DoesAcceptLifeSupportResources()
    return self:DoesAcceptResource("Food") or self:DoesAcceptResource("Seeds")
end

function ResourceStockpileBase:DoesAcceptOtherResources()
    return self:DoesAcceptResource("Fuel") or self:DoesAcceptResource("WasteRock")
            or self:DoesAcceptResource("MysteryResource")
end

function UniversalStorageDepot:SetDepotEntity()
    if self.template_name == "UniversalStorageDepot" then
        local has_minerals = IsDlcAccessible("picard")
        local has_seeds = IsDlcAccessible("armstrong")
        local entity = self:GetEntity()
        if has_minerals and has_seeds then
            entity = "StorageDepotAIO"
        elseif has_minerals or has_seeds then
            entity = "StorageDepotBB"
        end
        self:ChangeEntity(entity)
    end
end

function UniversalStorageDepot:SetStorableResources()
    self.storable_resources = table.copy(self.storable_resources)
    if self.template_name == "UniversalStorageDepot" then
        local has_minerals = IsDlcAccessible("picard")
        local has_seeds = IsDlcAccessible("armstrong")
        if has_minerals then
            self.storable_resources[#self.storable_resources + 1] = "PreciousMinerals"
        end
        if has_seeds then
            self.storable_resources[#self.storable_resources + 1] = "Seeds"
        end
    end
end

local vanillaDepotGameInit = UniversalStorageDepot.GameInit
function UniversalStorageDepot:GameInit()
    if IsKindOf(self, "DynamicStorageDepot") then
        return vanillaDepotGameInit(self)
    end
    if #self.storable_resources > 10 then
        local x = self:GetSpotBeginIndex("Origin")
        self.GetSpotBeginIndex = function ( ... )
            return x
        end
    end
    vanillaDepotGameInit(self)
    if #self.storable_resources > 10 then
        local len = #self.storable_resources
        for i = 1, len do
            local resource_name = self.storable_resources[i]
            if #self.storable_resources == 12 then
                self.placement_offset[resource_name] = point(-1260+(i-1)*220,-231,30)  -- *seeds *minerals +meat +booze, ugly
            else
                self.placement_offset[resource_name] = point(-1200+(i-1)*229,-231,30)  -- *seeds *minerals +meat +booze -fuel
            end
        end
    end
    self.GetSpotBeginIndex = nil
end

function OnMsg.ClassesPostprocess()
    PlaceObj("XTemplate", {
        group = "Infopanel Sections",
        id = "sectionStorage",
        PlaceObj("XTemplateTemplate", {
            "__condition", function(parent, context)
                return (IsKindOf(context, "UniversalStorageDepot") or IsKindOf(context, "MechanizedDepot")) and not context:IsKindOf("RocketBase") and not IsKindOf(context, "SpaceElevator")
            end,
            "__template", "InfopanelSection",
            "RolloverText", T(10461, "Drones and Shuttles will attempt to stockpile at least <DesiredAmountUI> of each resources stored here."),
            "RolloverHint", T(116367034467, "<left_click> Set Desired Amount <newline><em>Ctrl + <left_click></em> Set Desired Amount in all <display_name_pl>"),
            "RolloverHintGamepad", T(10462, "<LB> / <RB>    change desired amount"),
            "Title", T(10463, "Desired Amount <DesiredAmountUI>"),
            "Icon", "UI/Icons/Sections/facility.tga"
        }, {
            PlaceObj("XTemplateTemplate", {
                "__template", "InfopanelSlider",
                "BindTo", "DesiredAmountSlider"
            })
        }),
        PlaceObj("XTemplateGroup", {
            "__condition", function(parent, context)
                return (IsKindOf(context, "UniversalStorageDepot") or IsKindOf(context, "MechanizedDepot")) and not context:IsKindOf("RocketBase") and not IsKindOf(context, "SpaceElevator")
            end
        }, {
            PlaceObj("XTemplateTemplate", {
                "__condition", function(parent, context)
                    return context:DoesAcceptBasicResources()
                end,
                "__template", "InfopanelSection",
                "RelativeFocusOrder", "",
                "Title", T(494, "Basic Resources"),
                "Icon", ""
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Metals" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Metals",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Concrete" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Concrete",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "PreciousMetals" })
                end,
                "__template", "sectionStorageRow",
                "Id", "PreciousMetals",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "PreciousMinerals" })
                end,
                "__template", "sectionStorageRow",
                "Id", "PreciousMinerals",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__condition", function(parent, context)
                    return context:DoesAcceptAdvancedResources()
                end,
                "__template", "InfopanelSection",
                "RelativeFocusOrder", "",
                "Title", T(347750586259, "Advanced Resources"),
                "Icon", ""
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Polymers" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Polymers",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "MachineParts" })
                end,
                "__template", "sectionStorageRow",
                "Id", "MachineParts",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Electronics" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Electronics",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__condition", function(parent, context)
                    return context:DoesAcceptLifeSupportResources()
                end,
                "__template", "InfopanelSection",
                "RelativeFocusOrder", "",
                "Title", T("Life Support Resources"),
                "Icon", ""
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Food" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Food",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Seeds" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Seeds",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__condition", function(parent, context)
                    return context:DoesAcceptOtherResources()
                end,
                "__template", "InfopanelSection",
                "RelativeFocusOrder", "",
                "Title", T(12018, "Other Resources"),
                "Icon", ""
            }),
            PlaceObj("XTemplateTemplate", {
                "__context", function(parent, context)
                    return SubContext(context, { res = "Fuel" })
                end,
                "__template", "sectionStorageRow",
                "Id", "Fuel",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
            PlaceObj("XTemplateTemplate", {
                "__context",
                function(parent, context)
                    return SubContext(context, { res = "MysteryResource" })
                end,
                "__template", "sectionStorageRow",
                "Id", "MysteryResource",
                "Title", T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
                "Icon", "UI/Icons/Sections/workshifts_active.tga",
                "TitleHAlign", "stretch"
            }),
        })
    })
    Msg("LHResStorageReady")
end