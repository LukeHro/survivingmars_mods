local translationIdBase = 1107000400

-- Any(not enforce and enforce_mode="none") => Restricted(not enforce and enforce_mode="restricted") => Strict(enforce and enforce_mode="strict") => NoSpec(not enforce and enforce_mode="no_spec") => Any
local function CanWorkRestricted(workplace, colonist)
	return workplace.enforce_mode=="restricted" and ((colonist.specialist or "none")=="none" or (colonist.specialist or "none")==workplace.specialist)
end

local function CanWorkNoSpec(workplace, colonist)
	return workplace.enforce_mode=="no_spec" and (colonist.specialist or "none")=="none"
end

local vanillaToggleSpecialization = Workplace.ToggleSpecialistEnforce
function Workplace:ToggleSpecialistEnforce(broadcast)
	if (self.specialist or "none")=="none" then 
		return vanillaToggleSpecialization(self, broadcast)
	end
	if not self.enforce_mode then
		self.enforce_mode = "none"
	end
	self:SetSpecialistEnforce(self.enforce_mode=="none" and "restricted" or self.enforce_mode=="restricted" and "strict" or self.enforce_mode=="strict" and "no_spec" or "none")
	if broadcast then
		BroadcastAction(self, "SetSpecialistEnforce", self.enforce_mode)
	end
end

local vanillaSetSpecialistEnforce = Workplace.SetSpecialistEnforce
function Workplace:SetSpecialistEnforce(enforce_mode)
	if (self.specialist or "none")=="none" then 
		return vanillaSetSpecialistEnforce(self, enforce_mode)
	end
	self.specialist_enforce_mode = (enforce_mode=="strict")
	if self.enforce_mode == enforce_mode then
		return
	end
	self.enforce_mode = enforce_mode
	if enforce_mode == "none" then
		self:CheckWorkForUnemployed()
		return
	end
	local specialist = self.specialist or "none"
	for _, list in ipairs(self.workers) do
		for i = #list,1,-1 do
			local worker = list[i]
            if not self:CanWorkHere(worker) and not worker:IsDying() then
                worker:SetWorkplace(false)
                worker.user_forced_workplace = false
                worker:UpdateWorkplace()
            end
		end
	end
	self:CheckWorkForUnemployed()
end

local vanillaToggleSpecialistEnforceUpdate = Workplace.ToggleSpecialistEnforce_Update
function Workplace:ToggleSpecialistEnforce_Update(button)
	if (self.specialist or "none")=="none" then 
		return vanillaToggleSpecialistEnforceUpdate(self, button)
	end
	local enforce = self.enforce_mode or "none"	
	if enforce=="none" then
		button:SetIcon(CurrentModPath.."UI/Icons/IPButtons/specialization_any.tga")
		button:SetRolloverTitle(T(8742, "Workforce: No Restrictions"))
		button:SetRolloverText(T(8743, "Set accepted workforce for this building.<newline><newline>Current status: <em>No restrictions</em>"))
	elseif enforce=="restricted" then
		button:SetIcon(CurrentModPath.."UI/Icons/IPButtons/specialization_restricted.tga")
		button:SetRolloverTitle(T(translationIdBase+1, "Workforce: No Wrong Specialization"))
		button:SetRolloverText(T(translationIdBase+2, "Set accepted workforce for this building.<newline><newline>Current status: <em>No Wrong Specialization</em>"))
	elseif enforce=="no_spec" then
		button:SetIcon("UI/Icons/IPButtons/specialization_off.tga")
		button:SetRolloverTitle(T(8744, "Workforce: Reject Specialists"))
		button:SetRolloverText(T(8745, "Set accepted workforce for this building.<newline><newline>Current status: <em>Specialists not accepted</em>"))
	else
		button:SetIcon("UI/Icons/IPButtons/specialization_on.tga")
		button:SetRolloverTitle(T(8746, "Workforce: Enforce Specialists"))
		button:SetRolloverText(T(8747, "Set accepted workforce for this building.<newline><newline>Current status: <em>Only specialists accepted</em>"))
	end
	
	if enforce=="none" then
		button:SetRolloverHint(T(translationIdBase+3, "<left_click> No Wrong Specialization <newline><em>Ctrl + <left_click></em> No Wrong Specialization for all <display_name_pl>"))
		button:SetRolloverHintGamepad(T(translationIdBase+4, "<ButtonA> No Wrong Specialization <newline><ButtonX> No Wrong Specialization for all <display_name_pl>"))
	elseif enforce=="strict" then
		button:SetRolloverHint(T(8750, "<left_click> Reject specialists <newline><em>Ctrl + <left_click></em> Reject specialists for all <display_name_pl>"))
		button:SetRolloverHintGamepad(T(8751, "<ButtonA> Reject specialists <newline><ButtonX> Reject specialists for all <display_name_pl>"))
	elseif enforce=="no_spec" then
		button:SetRolloverHint(T(8748, "<left_click> No restrictions <newline><em>Ctrl + <left_click></em> No restrictions for all <display_name_pl>"))
		button:SetRolloverHintGamepad(T(8749, "<ButtonA> No restrictions <newline><ButtonX> No restrictions for all <display_name_pl>"))	
	else
		button:SetRolloverHint(T(8752, "<left_click> Enforce specialists <newline><em>Ctrl + <left_click></em> Enforce specialists for all <display_name_pl>"))
		button:SetRolloverHintGamepad(T(8753, "<ButtonA> Enforce specialists <newline><ButtonX> Enforce specialists for all <display_name_pl>"))
	end
end

local vanillaCanWorkHere = Workplace.CanWorkHere
function Workplace:CanWorkHere(colonist)
	return CanWorkRestricted(self, colonist) or CanWorkNoSpec(self, colonist) or self.enforce_mode~="no_spec" and self.enforce_mode~="restricted" and vanillaCanWorkHere(self, colonist)
end

local vanillaColonistCanInteract = Workplace.ColonistCanInteract
function Workplace:ColonistCanInteract(col)
	if self.enforce_mode=="restricted" and not CanWorkRestricted(self, col) then
		return false, T(8769, "Required specialization mismatch")
	end
	if self.enforce_mode=="no_spec" and not CanWorkNoSpec(self, col) then
		return false, T(8769, "Required specialization mismatch")
	end
	return vanillaColonistCanInteract(self, col)
end

local vanillaChooseWorkplace = ChooseWorkplace
function ChooseWorkplace(unit, workplaces, allow_exchange)
    local filteredWorkplaces = { }
    local tbl1 = type(workplaces[1]) == "table" and not IsValid(workplaces[1]) and workplaces or {workplaces}
    for i = 1, #tbl1 do
        workplaces = tbl1[i]
        for _, workplace in ipairs(workplaces or empty_table) do
            if workplace.enforce_mode~="restricted" and workplace.enforce_mode~="no_spec" or CanWorkNoSpec(workplace,unit) or CanWorkRestricted(workplace,unit) then
                filteredWorkplaces[#filteredWorkplaces +1] = workplace
            end
        end
    end
    return vanillaChooseWorkplace(unit, filteredWorkplaces, allow_exchange)
end