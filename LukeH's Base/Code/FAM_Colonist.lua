Colonist.stat_affection = 60 * const.Scale.Stat
Colonist.same_sex_match = false
Colonist.partner = false
Colonist.parents = false
Colonist.children = false
Colonist.ancestors = false

GlobalVar("fam_log", false)
GlobalVar("fam_FormCouples", false)
GlobalVar("fam_SameSexMarriageChance", 0)
GlobalVar("fam_AssignChildToFamily", false)
GlobalVar("fam_ChangeNameOnMarriage", false)

function family_log(...)
    if fam_log then
        print(...)
    end
end

function OnMsg.ClassesPostprocess()
    PlaceObj('TraitPreset', {
        auto = false,
        description = T("A small baby under care of its parents."),
        display_name = T("Baby"),
        group = "Age Group",
        id = "ChildBaby",
        incompatible = {},
        show_in_traits_ui = false,
        hidden_on_start = true,
        dome_filter_only = true
    })
    PlaceObj('TraitPreset', {
        auto = false,
        category = "other",
        description = T("Parent of an underage kid"),
        display_name = T("Parent"),
        group = "other",
        id = "ChildParent",
        incompatible = {},
        name = "ChildParent",
        show_in_traits_ui = false,
        hidden_on_start = true,
        dome_filter_only = true,
    })
    g_HiddenTraits.ChildBaby = true
end

local AddTrait_Colonist = Colonist.AddTrait
function Colonist:AddTrait(trait_id, ...)
    AddTrait_Colonist(self, trait_id, ...)
    --vanilla unhides added traits and apparently no other way to keep them hidden in dome filters
    if g_HiddenTraits then
        g_HiddenTraits.ChildBaby = true
    end
end

local function RemoveParentTraitIfNeeded(colonist)
    for parent in pairs(colonist.parents or empty_table) do
        if IsValid(parent) then
            local has_kids = false
            for _, kid in ipairs(parent.children) do
                has_kids = has_kids or IsValid(kid) and not self:IsDead() and kid.traits.Child
            end
            if not has_kids then
                parent:RemoveTrait("ChildParent")
            end
        end
    end
end
OnMsg.ColonistBecameYouth = RemoveParentTraitIfNeeded
OnMsg.ColonistDied = RemoveParentTraitIfNeeded

function UpdateFamilyName(colonist, other_name)
    --TODO some races don't have family as last name
    local family
    for word in string.gmatch(other_name, "%a+") do
        family = word
    end
    colonist.name[3] = family
    return family
end

function FindFamily(dome, debug)
    if not fam_AssignChildToFamily then
        return
    end
    local parent = false
    local grade = 0
    local candidates = dome.labels.Colonist
    local offset = dome:Random(#candidates)
    for i = 1, #candidates do
        local colonist = candidates[1 + (offset + i) % #candidates]
        if not colonist.traits.Child and colonist.gender == "Female" and
                (g_SeniorsCanWork or (not colonist.traits.Senior and (not colonist.partner or not colonist.partner.traits.Senior))) then
            local children = colonist.children or empty_table
            local partner_children = colonist.partner and colonist.partner.children or empty_table
            local tmp_grade = 5 - #children + (not colonist.partner and -1 or 0)
                    + (colonist.partner and colonist.partner.gender ~= colonist.gender and 2 or 0)
            if debug then
                print(colonist:GetRenameInitText(), ", children=" .. #children, ", score=" .. tmp_grade,
                        ", partner=" .. (colonist.partner and (colonist.partner:GetRenameInitText() .. "(" .. #partner_children .. ")") or "false"))
            end
            tmp_grade = (#children >= 5 or #partner_children >= 5) and -100 or tmp_grade
            if tmp_grade > grade or (tmp_grade == grade and not parent or colonist.stat_affection > parent.stat_affection) then
                parent = colonist
                grade = tmp_grade
            end
        end
    end
    return parent
end

function OnMsg.ColonistBorn(colonist, message)
    if fam_AssignChildToFamily and message == "born" and colonist.dome then
        local parent = FindFamily(colonist.dome)
        colonist.parents = colonist.parents or {}
        if parent then
            colonist.race = parent.partner and parent:Random(100) < 30 and parent.partner.race or parent.race
            colonist:ChooseEntity()
            UpdateFamilyName(colonist, parent:GetRenameInitText())
            colonist.parents[1] = parent
            parent.children = parent.children or {}
            parent.children[#parent.children + 1] = colonist
            parent:AddTrait("ChildParent")
            colonist.ancestors = colonist.ancestors or {}
            table.insert_unique(colonist.ancestors, parent)
            for _, grandparent in pairs(parent.parents or empty_table) do
                table.insert_unique(colonist.ancestors, grandparent)
            end
            if parent.partner then
                local partner = parent.partner
                colonist.parents[2] = partner
                partner.children = partner.children or {}
                partner.children[#parent.partner.children + 1] = colonist
                partner:AddTrait("ChildParent")
                table.insert_unique(colonist.ancestors, partner)
                for _, grandparent in pairs(partner.parents or empty_table) do
                    table.insert_unique(colonist.ancestors, grandparent)
                end
            end
            colonist:SetDome(parent.dome)
        end
        Msg("ChildBorn", colonist, colonist.parents[1], colonist.parents[2])
    end
end

local Init_Colonist = Colonist.Init
function Colonist:Init()
    Init_Colonist(self)
    self.same_sex_match = self:Random(100) < fam_SameSexMarriageChance
    --don't init self.parents here; false means not initialized, empty means orphan
end

local Done_Colonist = Colonist.Done
function Colonist:Done()
    if self.partner then
        self.partner.partner = false
    end
    self.partner = false
    self.parents = {}
    self.children = {}
    self.ancestors = {}
    Done_Colonist(self)
end

function Colonist:FindPartner(candidates, dont_check_holder, debug)
    if self.traits.Child or self.partner or self.stat_affection < g_Consts.HighStatLevel then
        return false
    end
    local chance = self:Random(100) + (self.age_trait == "Adult" and 10 or self.age_trait == "Senior" and -10 or 0)
    if self.traits.Loner then
        chance = chance / 2
    end
    if debug then
        print("Finding partner for", self:GetRenameInitText(), "same", self.same_sex_match)
    end
    local reduce_chance = 0
    local offset = self:Random(#candidates)
    for i = 1, #candidates do
        local candidate = candidates[1 + (offset + i) % #candidates]
        if candidate ~= self and not candidate.traits.Child and self.same_sex_match == candidate.same_sex_match then
            if not candidate.partner and candidate.stat_affection >= g_Consts.HighStatLevel and (self.holder == candidate.holder or dont_check_holder)
                    and (self.same_sex_match and self.entity_gender == candidate.entity_gender or self.entity_gender ~= candidate.entity_gender) then
                local isRelated = (table.find(candidate.ancestors or empty_table, self)
                        or table.find(self.ancestors or empty_table, candidate))
                if not isRelated then
                    for _, ancestor in pairs(self.ancestors or empty_table) do
                        isRelated = isRelated or (ancestor and table.find(candidate.ancestors or empty_table, ancestor))
                    end
                end
                if debug then
                    print("Checking with", candidate:GetRenameInitText(),"isRelated=", isRelated)
                end
                if not isRelated then
                    chance = chance - reduce_chance
                    local age_dif = Max(self.age, candidate.age) - Min(self.age, candidate.age)
                    local match = self:Random(100) + (candidate.age_trait == "Adult" and 10 or candidate.age_trait == "Senior" and -10 or 0)
                            + (candidate.age_trait == self.age_trait and 10 or 0) + (self.specialist == candidate.specialist and 10 or 0)
                            + (self.same_sex_match and 20 or 0) - (candidate.traits.Loner and 20 or 0) - DivRound(age_dif, 10)
                    if debug then
                        print("     score", chance, "+", match)
                    end
                    if chance + match > 150 then
                        return candidate
                    end
                    reduce_chance = reduce_chance + 10
                end
            end
        end
    end
end

function Colonist:FormCouple(colonist)
    self.partner = colonist
    Msg("CoupleFormed", self, colonist)
    colonist.partner = self
    if fam_ChangeNameOnMarriage then
        if self.entity_gender == "Female" and self:Random(100) < 70 then
            UpdateFamilyName(self, colonist:GetRenameInitText())
        else
            if self:Random(100) < 60 then
                UpdateFamilyName(colonist, self:GetRenameInitText())
            end
        end
    end
end

function Colonist:IsOrphan()
    if not self.parents then
        return false
    end
    for _, parent in pairs(self.parents) do
        if not Colonist.IsDead(parent) then
            return false
        end
    end
    return true
end

local Service_Residence = Residence.Service
function Residence:Service(unit, duration, ...)
    Service_Residence(self, unit, duration, ...)
    if fam_FormCouples then
        local found = unit:FindPartner(self.units or empty_table)
        if found then
            unit:FormCouple(found)
        end
    end
end