return {
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemCode', {
	'name', "lh_lib",
	'FileName', "Code/lh_lib.lua",
}),
PlaceObj('ModItemCode', {
	'name', "LH_RCCommands",
	'FileName', "Code/LH_RCCommands.lua",
}),
PlaceObj('ModItemCode', {
	'name', "LH_LightningSpeed",
	'FileName', "Code/LH_LightningSpeed.lua",
}),
PlaceObj('ModItemCode', {
	'name', "LH_PassageControl",
	'FileName', "Code/LH_PassageControl.lua",
}),
PlaceObj('ModItemCode', {
	'name', "LH_WorkplaceRestrictions",
	'FileName', "Code/LH_WorkplaceRestrictions.lua",
}),
PlaceObj('ModItemCode', {
	'name', "LH_VisitorsControl",
	'FileName', "Code/LH_VisitorsControl.lua",
}),
PlaceObj('ModItemCode', {
	'name', "FAM_Colonist",
	'FileName', "Code/FAM_Colonist.lua",
}),
PlaceObj('ModItemCode', {
	'name', "RES_Storage",
	'FileName', "Code/RES_Storage.lua",
}),
PlaceObj('ModItemCode', {
	'name', "RES_Infobar",
	'FileName', "Code/RES_Infobar.lua",
}),
PlaceObj('ModItemCode', {
	'name', "RES_ColonyControlCenter",
	'FileName', "Code/RES_ColonyControlCenter.lua",
}),
PlaceObj('ModItemCode', {
	'name', "RES_Helper",
	'FileName', "Code/RES_Helper.lua",
}),
PlaceObj('ModItemCode', {
	'name', "RES_UI",
	'FileName', "Code/RES_UI.lua",
}),
PlaceObj('ModItemCode', {
	'name', "RES_RivalsAI",
	'FileName', "Code/RES_RivalsAI.lua",
}),
PlaceObj('ModItemGameRules', {
	SortKey = 100230,
	challenge_mod = -200,
	description = T(110700003, --[[ModItemGameRules LH_DeepScannedRule description]] "Starts with all sectors deep scanned. <NoAchievements()>"),
	display_name = T(110700004, --[[ModItemGameRules LH_DeepScannedRule display_name]] "Deep scanned"),
	exclusionlist = "FastScan",
	flavor = T(11070003, --[[ModItemGameRules LH_DeepScannedRule flavor]] '<color  158 158 158>"We shall not cease from exploration, and the end of all our exploring will be to arrive where we started and know the place for the first time."\n<right>T. S. Eliot</color><left>'),
	group = "Default",
	id = "LH_DeepScannedRule",
	new_in = "LukeH",
}),
PlaceObj('ModItemGameRules', {
	SortKey = 100240,
	challenge_mod = 10,
	description = T(633557817728, --[[ModItemGameRules LH_FasterRivals description]] "Rivals develop 25% faster"),
	display_name = T(156594670717, --[[ModItemGameRules LH_FasterRivals display_name]] "Smarter Rivals"),
	flavor = T(981280589071, --[[ModItemGameRules LH_FasterRivals flavor]] '<color  158 158 158>"The art of war teaches us to rely not on the likelihood of the enemy not coming, but on our own readiness to receive him"\n<right>Sun Tzu</color><left>'),
	group = "Default",
	id = "LH_FasterRivals",
	new_in = "LukeH",
	param1 = 75,
}),
}
