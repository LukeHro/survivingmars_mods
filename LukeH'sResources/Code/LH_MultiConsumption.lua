local translationIdBase = 1107012500

DefineClass.AdditionalConsumption = {
  __parents = { "HasConsumption" },
  entity = "InvisibleObject",
  --not autofill
  building = false,
  upgrade_id = false,
  upgrade_tier = false,
  city = false,
  
  --autofill
  working = false,
  upgrade_modifiers = false,
  is_upgrade = false, --id
  auto_connect = false,
  
  my_props = false,
}

function AdditionalConsumption:UpdateVisualStockpile(...)
  return self.building:UpdateVisualStockpile(...)
end

function AdditionalConsumption:UpdateRequestConnectivity(...)
  return self.building:UpdateRequestConnectivity(...)
end

function AdditionalConsumption:Consume_Internal(input_amount_to_consume)
  local amount_to_consume = HasConsumption.Consume_Internal(self, input_amount_to_consume)
  if not self.building:CanConsume() then
    if self.building:IsKindOf("Building") then
      self.building:AttachSign(true, "SignNoConsumptionResource")
    end
    self.building:UpdateWorking(false)
  end
  if SelectedObj == self.building then
    RebuildInfopanel(self.building)
  end
  return amount_to_consume
end


local CanService_Service = Service.CanService
function Service:CanService(unit) --"or" consumption  
  local is_child = unit.traits["Child"]
  if is_child and not self.usable_by_children or not is_child and self.children_only then
    return false
  end
  local sumRes = self.consumption_stored_resources or 0
  for _,v in pairs(self.multiConsumption or {}) do
    sumRes = sumRes + (v.consumption_stored_resources or 0)
  end
  if self:DoesHaveConsumption() and self.consumption_type == g_ConsumptionType.Visit and (sumRes <= 0) then
    return false
  end
  return true
end

local Service_Service = Service.Service
function Service:Service(unit, duration, daily_interest, ...)
  unit.current_service = daily_interest
  Service_Service(self, unit, duration, daily_interest, ...)
end


function HasConsumption:Init( ... )
  self.multiConsumption = {}
end

local CanConsume_HasConsumption = HasConsumption.CanConsume
function HasConsumption:CanConsume(...)	-- "or" consumption
  local orRes = CanConsume_HasConsumption(self, ...)
  for _,v in pairs(self.multiConsumption or {}) do
    orRes = orRes or CanConsume_HasConsumption(v, ...)
  end
	return orRes
end

local InitConsumptionRequest_HasConsumption = HasConsumption.InitConsumptionRequest
function HasConsumption:InitConsumptionRequest( ... )  
  if self:IsKindOf("Service") and self:IsOneOfInterests("interestDining") then
    self.multiConsumption.Meat = AdditionalConsumption:new({building=self,city=self.city})
    self.multiConsumption.Meat.consumption_resource_type = "Meat"
    self.multiConsumption.Meat.consumption_max_storage = self.consumption_max_storage
    self.multiConsumption.Meat.consumption_amount = 100
    self.multiConsumption.Meat.consumption_type = self.consumption_type
    self.multiConsumption.Meat.consumption_stored_resources = 0
  end
  if self:IsKindOf("Service") and self:IsOneOfInterests("interestDrinking") then
    if (not self:DoesHaveConsumption()) then
      self.consumption_resource_type = "Booze"
      self.consumption_max_storage = 10000
      self.consumption_amount = 100
      self.consumption_type = 2
    elseif not (self.consumption_resource_type == "Booze") then
      self.multiConsumption.Booze = AdditionalConsumption:new({building=self,city=self.city})
      self.multiConsumption.Booze.consumption_resource_type = "Booze"
      self.multiConsumption.Booze.consumption_max_storage = self.consumption_max_storage
      self.multiConsumption.Booze.consumption_amount = 100
      self.multiConsumption.Booze.consumption_type = self.consumption_type      
      self.multiConsumption.Booze.consumption_stored_resources = 0
    end
  end
	InitConsumptionRequest_HasConsumption(self, ...)  
  for _,v in pairs(self.multiConsumption or {}) do
    v.building = self
    self:Attach(v)
    local resource_unit_count = 5 + (v.consumption_max_storage / (const.ResourceScale * 10)) --5 + 1 per 10
    local d_req = self:AddDemandRequest(v.consumption_resource_type, v.consumption_max_storage, const.rfWaitToFill, resource_unit_count)
    v.consumption_resource_request = d_req
    v:ConnectToCommandCenters()
  end
end

local Consume_Visit_HasConsumption = HasConsumption.Consume_Visit
function HasConsumption:Consume_Visit(unit, interest)
  interest = interest or unit.current_service or unit.daily_interest    
  if self.multiConsumption.Booze and interest == "interestDrinking" then    
    local consumed = self.multiConsumption.Booze:Consume_Internal(self.multiConsumption.Booze.consumption_amount)
    if consumed then
      if self:Random(100) > (interest == "interestDrinking" and 0 or 60) then
        unit:AddTrait("LH_Drunk")
      end
      ResourceOverviewObj.data.booze_in_service_buildings = (ResourceOverviewObj.data.booze_in_service_buildings or 0) - consumed
    end
    self.multiConsumption.Booze.building = self --backward compatibility
    self.multiConsumption.Booze.city = self.city
  end
	if self.consumption_resource_type == "Food" then    
    if unit.traits.Vegan then
      return Consume_Visit_HasConsumption(self, unit)
    end
		if (GameTime() - unit.last_meal) >= const.DayDuration then
			local max = self.consumption_stored_resources + (self.multiConsumption.Meat and self.multiConsumption.Meat.consumption_stored_resources or 0)
			if unit.assigned_to_service_with_amount then
				max = Min(max, unit.assigned_to_service_with_amount)
			end
			local use = unit:Eat(max)
			if self.multiConsumption.Meat and self.multiConsumption.Meat.consumption_stored_resources > 0 then
				local meat_input_amount_to_consume
				if (use/2 < self.consumption_stored_resources) then
					meat_input_amount_to_consume = use/2
					unit:AddTrait("LH_WellFed")
				else
					meat_input_amount_to_consume = use - self.consumption_stored_resources
				end
        ResourceOverviewObj.data.meat_in_service_buildings = (ResourceOverviewObj.data.meat_in_service_buildings or 0) - meat_input_amount_to_consume
        self.multiConsumption.Meat.building = self --backward compatibility
        self.multiConsumption.Meat.city = self.city
        self.multiConsumption.Meat:Consume_Internal(meat_input_amount_to_consume)
				return self:Consume_Internal(use - meat_input_amount_to_consume)
			else
				return self:Consume_Internal(use)
			end
		end
	else
		local consumed = Consume_Visit_HasConsumption(self, unit)
    if consumed and self.consumption_resource_type == "Booze" then
      if self:Random(100) > (interest == "interestDrinking" and 0 or 60) then
        unit:AddTrait("LH_Drunk")
      end
      ResourceOverviewObj.data.booze_in_service_buildings = (ResourceOverviewObj.data.booze_in_service_buildings or 0) - self.consumption_amount
    end
    return consumed
	end
end

local ConsumptionDroneUnload_HasConsumption = HasConsumption.ConsumptionDroneUnload
function HasConsumption:ConsumptionDroneUnload(drone, req, resource, amount)
	if self.consumption_resource_request == req then	
    if self.consumption_resource_type == "Meat" then
      ResourceOverviewObj.data.meat_in_service_buildings = (ResourceOverviewObj.data.meat_in_service_buildings or 0) + amount
    elseif self.consumption_resource_type == "Booze" then
      ResourceOverviewObj.data.booze_in_service_buildings = (ResourceOverviewObj.data.booze_in_service_buildings or 0) + amount
    end
	end
  ConsumptionDroneUnload_HasConsumption(self, drone, req, resource, amount)
  for _,v in pairs(self.multiConsumption or {}) do
    local was_work_possible = self:CanConsume()
    HasConsumption.ConsumptionDroneUnload(v, drone, req, resource, amount)
    if not was_work_possible and self:CanConsume() then
      if self:IsKindOf("Building") then
        self:AttachSign(false, "SignNoConsumptionResource")
      end
      if not self.working then
        self:UpdateWorking()
      end
    end
  end  
end

function AdditionalConsumption:GetUIConsumptionTexts(short)
  local ret
  if self.consumption_stored_resources and self.consumption_stored_resources > 0 then
    ret = short and T{translationIdBase+4, "<resource(consumption_stored_resources,consumption_max_storage,consumption_resource_type)>", 
      consumption_resource_type = self.consumption_resource_type, consumption_stored_resources = self.consumption_stored_resources, consumption_max_storage = self.consumption_max_storage}
        or T{translationIdBase+2, "Stored <resource(consumption_resource_type)><right><resource(consumption_stored_resources,consumption_max_storage,consumption_resource_type)>", 
      consumption_resource_type = self.consumption_resource_type, consumption_stored_resources = self.consumption_stored_resources, consumption_max_storage = self.consumption_max_storage}
  else
    ret = short and T{translationIdBase+3, "<red><resource(consumption_stored_resources,consumption_max_storage,consumption_resource_type)></red>", 
      consumption_resource_type = self.consumption_resource_type, consumption_stored_resources = self.consumption_stored_resources, consumption_max_storage = self.consumption_max_storage}
        or T{translationIdBase+1, "Insufficient <resource(consumption_resource_type)><right><red><resource(consumption_stored_resources,consumption_max_storage,consumption_resource_type)></red>", 
      consumption_resource_type = self.consumption_resource_type, consumption_stored_resources = self.consumption_stored_resources, consumption_max_storage = self.consumption_max_storage}
  end
  return ret
end

local GetUIConsumptionTexts_Building = Building.GetUIConsumptionTexts
function Building:GetUIConsumptionTexts(short)
  local res = GetUIConsumptionTexts_Building(self, short)
  for _,v in pairs(self.multiConsumption or {}) do
    res.resource = res.resource .. (short and " " or "<newline><left>") .. v:GetUIConsumptionTexts(short)
  end
  return res
end

--megamall fixes
function MegaMall:CanService(unit)
  return unit.daily_interest ~= "interestDining" and unit.daily_interest ~= "interestDrinking" and unit.daily_interest ~="needFood" or Service.CanService(self, unit)
end

function MegaMall:ConsumeOnVisit(unit, interest)
  if interest == "needFood" or interest == "interestDining"  or interest == "interestDrinking" then
    self:Consume_Visit(unit, interest)
  end
end
