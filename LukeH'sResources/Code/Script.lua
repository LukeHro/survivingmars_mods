local translationIdBase = 1107010000

local vanillaCityInitExploration = City.InitExploration
function City:InitExploration()
	vanillaCityInitExploration(self)
	local pmDeposits = 0
	for i = 1, 10 do
		for j = 1, 10 do
			local sector = g_MapSectors[i][j]
			for k = 1, #sector.markers.subsurface do
				if sector.markers.subsurface[k].resource == "PreciousMetals" then
					pmDeposits = pmDeposits +1
				end
			end
			for k = 1, #sector.markers.deep do
				if sector.markers.deep[k].resource == "PreciousMetals" then
					pmDeposits = pmDeposits +1
				end
			end
		end
	end
	for i = 1, pmDeposits-1 do
		LH_AddDeposit(self, "Crystals", 500)
	end
	for i = 1, pmDeposits+2 do
		LH_AddDeposit(self, "Hydrocarbon", 5000)
	end
	for i = 1, pmDeposits-3 do
		LH_AddDeposit(self, "Radioactive", 1000)
	end
	--[[local pmDeposits1 = MapCount("map","SubsurfaceDepositMarker",function(deposit)
		if deposit.resource == "PreciousMetals" then
				return true
		end
	end)]]

end

local UnbuildableZ = buildUnbuildableZ()
function LH_AddDeposit(city, lh_resource, base_amount, layer_amount)
	local marker
	local gradeIndex = city:Random(1, 5)
	marker = PlaceObject("SubsurfaceDepositMarker")
	marker.resource = lh_resource
	marker.grade = DepositGradesTable[gradeIndex]
	marker.depth_layer = city:Random(1, 2)
	marker.max_amount = city:Random(base_amount, 2 * base_amount * marker.depth_layer) * const.ResourceScale
	-- pick position	
	for i = 1, 10 do
		local sector_x = city:Random(1, 10)
		local sector_y = city:Random(1, 10)
		local sector = g_MapSectors[sector_x][sector_y]
		
		local minx, miny = sector.area:minxyz()		
		local maxx, maxy = sector.area:maxxyz()
		
		local x = city:Random(minx, maxx)
		local y = city:Random(miny, maxy)
		
		local q, r = WorldToHex(x, y)
		local pt = point(x, y)
		if GetBuildableZ(q, r) ~= UnbuildableZ and terrain.IsPassable(pt) then
			marker:SetPos(pt)
			if marker:IsValidPos() then
				sector:RegisterDeposit(marker)
				break
			else
				printf("couldn't find position to place %s deposit", resource)
				DoneObject(marker)
			end
		end
	end
end

local LH_FormatResource = function(format_id, resource)
	FormattableResources[#FormattableResources + 1] = {text = resource, value = format_id}
	TFormat[format_id] = function(context_obj, value, max)
		return FormatResourceValueMaxResource(context_obj, value, max, resource)
	end
end

function OnMsg.ClassesPostprocess()	

	LH_FormatResource("crystals", "Crystals")
	LH_FormatResource("hydrocarbon", "Hydrocarbon")	
	LH_FormatResource("radioactive", "Radioactive")	
	LH_FormatResource("kerosene", "Kerosene")
	LH_FormatResource("meat", "Meat")
	LH_FormatResource("booze", "Booze")

	table.remove_value(BuildingTemplates.UniversalStorageDepot.storable_resources,"Fuel")
	table.insert_unique(BuildingTemplates.UniversalStorageDepot.storable_resources,"Crystals")
	if IsDlcAvailable("armstrong") then
		table.insert_unique(BuildingTemplates.UniversalStorageDepot.storable_resources,"Seeds")
	end

	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Radioactive")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Meat")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Booze")

	table.insert_unique(BuildingTemplates.SpaceElevator.storable_resources, "Radioactive")
	table.insert_unique(BuildingTemplates.SpaceElevator.storable_resources, "Meat")
	table.insert_unique(BuildingTemplates.SpaceElevator.storable_resources, "Booze")

	PlaceObj("OnScreenNotificationPreset", {
		SortKey = 1008310,
		expiration = 25000,
		fx_action = "UINotificationResource",
		group = "Default",
		id = "CrystalsExport",
		text = T(5675, "Funding received: <funding(funding)>"),
		title = T(translationIdBase+1, "Crystals Export"),
		voiced_text = T(7068, "Funding received")
	})

	PlaceObj("Cargo", {
		SortKey = 3002500,
		description = T(translationIdBase+2, "Radioactive materials used for destruction and power production"),
		group = "Basic Resources",
		id = "Radioactive",
		kg = 2000,
		name = T(translationIdBase+3, "Radioactive Materials"),
		pack = 5,
		price = 120000000,
	})
	PlaceObj("Cargo", {
		SortKey = 10003500,
		description = T(translationIdBase+4, "Produces Fuel from Hydrocarbon."),
		group = "Prefabs",
		icon = "UI/Icons/Payload/FuelFactory.tga",
		id = "LH_FuelRefinery",
		kg = 5000,
		name = T(translationIdBase+5, "Fuel Refinery"),
		price = 200000000
	})
	PlaceObj('Cargo', {
		SortKey = 3003300,
		description = T(translationIdBase+6, "More expensive food to keep the colonists in good shape."),
		group = "Basic Resources",
		id = "Meat",
		kg = 2000,
		name = T(translationIdBase+7, "Meat"),
		pack = 5,
		price = 30000000,
	})
	PlaceObj('Cargo', {
		SortKey = 3003600,
		description = T(translationIdBase+8, "If can't be locally produced alcohol from Earth is good enough."),
		group = "Basic Resources",
		id = "Booze",
		kg = 2000,
		name = T(translationIdBase+9, "Booze"),
		pack = 5,
		price = 30000000,
	})

	table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotCrystals" }))
	BuildingTechRequirements["LH_MechanizedDepotCrystals"] = {{ tech = "AutomatedStorage", hide = false }}

	table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotRadioactive" }))
	BuildingTechRequirements["LH_MechanizedDepotRadioactive"] = {{ tech = "AutomatedStorage", hide = false }}

	table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotMeat" }))
	BuildingTechRequirements["LH_MechanizedDepotMeat"] = {{ tech = "AutomatedStorage", hide = false }}

	table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotBooze" }))
	BuildingTechRequirements["LH_MechanizedDepotBooze"] = {{ tech = "AutomatedStorage", hide = false }}

	table.insert_unique(Presets.TechPreset["Engineering"].LowGHydrosynthsis, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_FuelRefinery" }))
	BuildingTechRequirements["LH_FuelRefinery"] = {{ tech = "LowGHydrosynthsis", hide = false } }
	table.insert_unique(Presets.TechPreset["Engineering"].LowGHydrosynthsis, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_PolycarbonPlant" }))
	BuildingTechRequirements["LH_PolycarbonPlant"] = {{ tech = "LowGHydrosynthsis", hide = false } }

	local ExtractorAI = Presets.TechPreset.Breakthroughs.ExtractorAI
	if not ExtractorAI.LH_Resorces then
		ExtractorAI.LH_Resorces = true
		ExtractorAI.description = T(translationIdBase+17, "<em>Extractors</em> can work without crews at <param1> performance.\n\n<grey>Extracting algorithms have been successfully implemented by our new Extractor AI, allowing for a certain degree of autonomy in mining operations.</grey>")
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 1, Label = "LH_CrystalsExtractor", Prop = "automation",}))
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 50, Label = "LH_CrystalsExtractor", Prop = "auto_performance",}))
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 1, Label = "LH_RadioactiveExtractor", Prop = "automation",}))
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 50, Label = "LH_RadioactiveExtractor", Prop = "auto_performance",}))
	end
end

function OnMsg.ClassesBuilt()
	local idx = table.find(XTemplates.sectionStorage[2],"Crystals", true)
	if idx then
		table.remove(XTemplates.sectionStorage[2],idx)
	end
	for i = #XTemplates.sectionStorage[2],6,-1 do
		XTemplates.sectionStorage[2][i+1] = XTemplates.sectionStorage[2][i]
	end
	XTemplates.sectionStorage[2][6] = PlaceObj('XTemplateTemplate', {
		'Crystals', true,
		'__context', function (parent, context) return SubContext(context, {res = "Crystals"}) end,
		'__template', "sectionStorageRow",
		'Title', T(615073837286, --[[XTemplate sectionStorage Title]] "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
		'Icon', "UI/Icons/Sections/workshifts_active.tga",
		'TitleHAlign', "stretch",
	})
	local idx = table.find(XTemplates.sectionStorage[2],"Radioactive", true)
	if idx then
		table.remove(XTemplates.sectionStorage[2],idx)
	end
	for i = #XTemplates.sectionStorage[2],7,-1 do
		XTemplates.sectionStorage[2][i+1] = XTemplates.sectionStorage[2][i]
	end
	XTemplates.sectionStorage[2][7] = PlaceObj('XTemplateTemplate', {
		'Radioactive', true,
		'__context', function (parent, context) return SubContext(context, {res = "Radioactive"}) end,
		'__template', "sectionStorageRow",
		'Title', T(615073837286, --[[XTemplate sectionStorage Title]] "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
		'Icon', "UI/Icons/Sections/workshifts_active.tga",
		'TitleHAlign', "stretch",
	})
	local idx = table.find(XTemplates.sectionStorage[2],"Meat", true)
	if idx then
		table.remove(XTemplates.sectionStorage[2],idx)
	end
	for i = #XTemplates.sectionStorage[2],7,-1 do
		XTemplates.sectionStorage[2][i+1] = XTemplates.sectionStorage[2][i]
	end
	XTemplates.sectionStorage[2][7] = PlaceObj('XTemplateTemplate', {
		'Meat', true,
		'__context', function (parent, context) return SubContext(context, {res = "Meat"}) end,
		'__template', "sectionStorageRow",
		'Title', T(615073837286, --[[XTemplate sectionStorage Title]] "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
		'Icon', "UI/Icons/Sections/workshifts_active.tga",
		'TitleHAlign', "stretch",
	})
	local idx = table.find(XTemplates.sectionStorage[2],"Booze", true)
	if idx then
		table.remove(XTemplates.sectionStorage[2],idx)
	end
	for i = #XTemplates.sectionStorage[2],7,-1 do
		XTemplates.sectionStorage[2][i+1] = XTemplates.sectionStorage[2][i]
	end
	XTemplates.sectionStorage[2][7] = PlaceObj('XTemplateTemplate', {
		'Booze', true,
		'__context', function (parent, context) return SubContext(context, {res = "Booze"}) end,
		'__template', "sectionStorageRow",
		'Title', T(615073837286, --[[XTemplate sectionStorage Title]] "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
		'Icon', "UI/Icons/Sections/workshifts_active.tga",
		'TitleHAlign', "stretch",
	})
end

GlobalVar("g_ExportsCrystalsFunding", 0)

function City:CalcBaseExportFunding(amount, source)
	if not source then 
		return 0
	end
	local export_funding = MulDivRound(amount or 0, g_Consts.ExportPricePreciousMetals*1000000, const.ResourceScale)
	if (source.exported_resource and source.exported_resource == "Crystals" or source.export_resource and source.export_resource =="Crystals") then
		export_funding = MulDivRound(export_funding, 4, 5)
		if not g_ExportsCrystalsFunding or not IsOnScreenNotificationShown("CrystalsExport") then
			g_ExportsCrystalsFunding = 0
		end
		g_ExportsCrystalsFunding = g_ExportsCrystalsFunding + export_funding
		AddOnScreenNotification("CrystalsExport", nil, { funding = g_ExportsCrystalsFunding })
		source.city:MarkCrystalsExport(amount, source)
	else
		if not g_ExportsFunding or not IsOnScreenNotificationShown("RareMetalsExport") then
			g_ExportsFunding = 0
		end
		g_ExportsFunding = g_ExportsFunding + export_funding
		AddOnScreenNotification("RareMetalsExport", nil, { funding = g_ExportsFunding })
		source.city:MarkPreciousMetalsExport(amount, source)
	end
	source.city:ChangeFunding(export_funding, "Export")	
	return 0
end

function City:MarkPreciousMetalsExport(amount, source)
	if amount <= 0 or not source then 
		return 
	end
	self.last_export = {amount = amount, day = self.day, hour = self.hour, minute = self.minute}
	self.total_export = self.total_export + amount
	--print(self.total_export)
	Msg("MarkPreciousMetalsExport", self, amount, self.total_export)
end

function City:MarkCrystalsExport(amount)
	if amount <= 0 then return end
	self.last_export_crystals = { amount = amount, day = self.day, hour = self.hour, minute = self.minute}
	self.total_export_crystals = (self.total_export_crystals or 0) + amount
	--print(self.total_export_crystals)
	Msg("MarkCrystalsExport", self, amount, self.total_export_crystals)
end

local vanillaDepotGameInit = UniversalStorageDepot.GameInit
function UniversalStorageDepot:GameInit()
	if #self.storable_resources > 8 then
		local x = self:GetSpotBeginIndex("Origin")
		self.GetSpotBeginIndex = function ( ... )
			return x
		end
	end
	vanillaDepotGameInit(self)
	if #self.storable_resources > 8 then
		local len = #self.storable_resources
		for i = 1, len do
			local resource_name = self.storable_resources[i]
			self.placement_offset[resource_name] = point(-1010+(i-1)*240,-231,30)
		end
	end  
	self.GetSpotBeginIndex = nil 
end

function RCDriller:CanExploit(despoit)
  if despoit:GetDepth() >= 2 then
    return
  end
  local res = despoit.resource
  if res ~= "Crystals" and res ~= "Radioactive" and res ~= "Metals" and res ~= "PreciousMetals" then
    return
  end
  return true
end

function RCDriller:ToggleDrillMode_Update(button)
  local to_mode = self.interaction_mode ~= "drill"
  button:SetIcon(to_mode and "UI/Icons/IPButtons/drill.tga" or "UI/Icons/IPButtons/cancel.tga")
  button:SetEnabled(self:CanBeControlled())
  button:SetRolloverTitle(T(10338, "Drill"))
  button:SetRolloverText(T(translationIdBase+12, "Give command to Drill an underground deposit for storable resources."))
  if not to_mode or not T(10340, "<left_click> Select drill mode") then
  end
  button:SetRolloverHint((T(10341, "<left_click> on a depsoit to drill it  <right_click> Cancel")))
  if not to_mode or not T(10342, "<ButtonA> Select drill mode") then
  end
  button:SetRolloverHintGamepad((T(7512, "<ButtonA> Cancel")))
end


local function SaveGameFixes(version)
	if not UICity.LH_ResVersion or UICity.LH_ResVersion < version then
		print("Increasing hydrocarbon ammounts...")
		MapForEach("map", "SubsurfaceDepositHydrocarbon",function(deposit)
			deposit.max_amount = deposit.max_amount * 10 * deposit.depth_layer
			deposit.amount = deposit.amount * 10 * deposit.depth_layer
		end)
	end
	UICity.LH_ResVersion = version
end

function OnMsg.CityStart(...)
	UICity.LH_ResVersion = 122
end

function OnMsg.LoadGame(...) 
	SaveGameFixes(122)
end

local GetResourceProducedIcon_SingleResourceProducer = SingleResourceProducer.GetResourceProducedIcon
function SingleResourceProducer:GetResourceProducedIcon()
	local icon = GetResourceProducedIcon_SingleResourceProducer(self)
	if self.resource_produced == "Crystals" or self.resource_produced == "Hydrocarbon" or self.resource_produced == "Radioactive" then
		icon = CurrentModPath .. icon
	end
	return icon
end

local GetResourceProducedIcon_Mine = Mine.GetResourceProducedIcon
function Mine:GetResourceProducedIcon(idx)
	local icon = GetResourceProducedIcon_Mine(self, idx)
	if self.exploitation_resource == "Crystals" or self.exploitation_resource == "Hydrocarbon" or self.exploitation_resource == "Radioactive" then
		icon = CurrentModPath .. icon
	end
	return icon
end
