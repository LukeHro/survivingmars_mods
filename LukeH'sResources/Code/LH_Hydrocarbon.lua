local translationIdBase = 1107010100

DefineClass("SubsurfaceDepositHydrocarbon",
{ __parents = {"SubsurfaceDeposit"},
	resource = "Hydrocarbon",
	entity = "LH_HydrocarbonDeposit",
	disabled_entity = "LH_HydrocarbonDisabledDeposit",
	display_icon = "UI/Icons/Buildings/deposit_polymers.tga",
	--name and description is reassigned when setting a new 'depth_layer' (see SubsurfaceDeposit:Setdepth_layer)
	display_name = T{translationIdBase+1, "Underground Hydrocarbon"},
	description = T{translationIdBase+2, "An underground deposit of Hydrocarbon."},
	display_name_deep = T{translationIdBase+3, "Deep Hydrocarbon"},
	description_deep = T{translationIdBase+4, "Deep underground deposit of Hydrocarbon."},
	fx_actor_class = "SubsurfaceAnomaly"
})

Resources["Hydrocarbon"] = {name = "Hydrocarbon",
	display_name = T{translationIdBase+5, "Hydrocarbon"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_hydrocarbon.tga", 
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_HydrocarbonContainer", 
	description = T{translationIdBase+6, "Underground Hydrocarbon."}
}

DefineConst{
	group = "Gameplay",
	help = T{translationIdBase+7, "Deep Hydrocarbon deposits are exploitable when this is not 0"},
	id = "IsDeepHydrocarbonExploitable",
	name = T{translationIdBase+8, "Exploitable Hydrocarbon"},
	value = 0,
}

table.insert_unique(ResourceDescription, Resources["Hydrocarbon"])
--table.insert_unique(StockpileResourceList, "Hydrocarbon")
--table.insert_unique(CargoShuttle.storable_resources, "Hydrocarbon")
table.insert_unique(SubSurfaceDeposits, "Hydrocarbon")	
SubSurfaceDeposits["Hydrocarbon"] = true		
table.insert_unique(AllResourcesList, "Hydrocarbon")
--table.insert_unique(RCTransport.storable_resources,"Hydrocarbon")

DepositGradeToWasteRockMultipliers.Hydrocarbon = {
    ["Depleted"] = 200,
    ["Very Low"] = 200,
    ["Low"] = 150,
    ["Average"] = 125,
    ["High"] = 100,
    ["Very High"] = 75
}


function OnMsg.SectorScanned(status, col, row) --find a way for it to be spawned by engine
	local sector = g_MapSectors[col][row]
	for i = 1, #sector.markers.subsurface do
		local marker = sector.markers.subsurface[i]
		if not marker.revealed and marker.resource == "Hydrocarbon" then
			marker.revealed = true
			local deposit = marker:PlaceDeposit()	
		end
	end
	if status == "deep scanned" then
		for i = 1, #sector.markers.deep do
			local marker = sector.markers.deep[i]
			if not marker.revealed and marker.resource == "Hydrocarbon" then
				marker.revealed = true
				local deposit = marker:PlaceDeposit()	
				deposit:UpdateEntity()
			end
		end
	end
end

function OnMsg.LoadGame( ... )
	if not g_Consts["IsDeepHydrocarbonExploitable"] == 1 and UICity:IsTechResearched("DeepWaterExtraction") then
		g_Consts["IsDeepHydrocarbonExploitable"] = 1
		MapForEach("map", "SubsurfaceDepositHydrocarbon", function(obj)
			obj:UpdateEntity()
		end)
	end
end

function OnMsg.TechResearched(tech_id, city)
	if tech_id == "DeepWaterExtraction" then
		g_Consts["IsDeepHydrocarbonExploitable"] = 1
		MapForEach("map", "SubsurfaceDepositHydrocarbon", function(obj)
			obj:UpdateEntity()
		end)
	end	
end

local vanillaIsDeepExploitable = IsDeepExploitable
function IsDeepExploitable(res)
	if res == "Hydrocarbon" then
		return g_Consts["IsDeepHydrocarbonExploitable"] ~= 0
	else
		return vanillaIsDeepExploitable(res)
	end
end

--[[DefineClass.LH_HydrocarbonExtractor =
{
	__parents = {"BaseMetalsExtractor"},
	
	subsurface_deposit_class = "SubsurfaceDepositHydrocarbon",
	exploitation_resource = "Hydrocarbon",
}

LH_HydrocarbonExtractor.OnCalcProduction_Hydrocarbon = BaseMetalsExtractor.OnCalcProduction_Metals
LH_HydrocarbonExtractor.OnProduce_Hydrocarbon = BaseMetalsExtractor.OnProduce_Metals
LH_HydrocarbonExtractor.GetPredictedProduction_Hydrocarbon = BaseMetalsExtractor.GetPredictedProduction_Metals
LH_HydrocarbonExtractor.GetPredictedDailyProduction_Hydrocarbon = BaseMetalsExtractor.GetPredictedDailyProduction_Metals]]

DefineClass.LH_PolymerPlant = {
  __parents = {"Factory"},
  resource_produced1 = "Polymers"
}
function LH_PolymerPlant.OnCalcProduction_Polymers(producer, amount_to_produce)
  return MulDivRound(producer.parent.performance, amount_to_produce, 100)
end
