local translationIdBase = 1107010700

DefineClass("SubsurfaceDepositRadioactive",
{ __parents = {"SubsurfaceDeposit"},
	resource = "Radioactive",
	entity = "LH_RadioactiveDeposit",
	disabled_entity = "LH_RadioactiveDisabledDeposit",
	display_icon = "UI/Icons/Buildings/deposit_polymers.tga",
	--name and description is reassigned when setting a new 'depth_layer' (see SubsurfaceDeposit:Setdepth_layer)
	display_name = T{translationIdBase+1, "Underground Radioactive Materials"},
	description = T{translationIdBase+2, "An underground deposit of Radioactive Materials."},
	display_name_deep = T{translationIdBase+3, "Deep Radioactive"},
	description_deep = T{translationIdBase+4, "Deep underground deposit of Radioactive Materials."},
	fx_actor_class = "SubsurfaceAnomaly"
})

Resources["Radioactive"] = {name = "Radioactive",
	display_name = T{translationIdBase+5, "Radioactive Materials"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_radioactive.tga", 
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_RadioactiveContainer", 
	description = T{translationIdBase+6, "Underground Radioactive Materials."}
}

DefineConst{
	group = "Gameplay",
	help = T{translationIdBase+7, "Deep Radioactive Materials deposits are exploitable when this is not 0"},
	id = "IsDeepRadioactiveExploitable",
	name = T{translationIdBase+8, "Exploitable Radioactive Materials"},
	value = 0,
}

table.insert_unique(ResourceDescription, Resources["Radioactive"])
table.insert_unique(StockpileResourceList, "Radioactive")
table.insert_unique(CargoShuttle.storable_resources, "Radioactive")
table.insert_unique(SubSurfaceDeposits, "Radioactive")	
SubSurfaceDeposits["Radioactive"] = true		
table.insert_unique(ConstructionResourceList,"Radioactive")
table.insert_unique(AllResourcesList, "Radioactive")
table.insert_unique(RCTransport.storable_resources,"Radioactive")

DepositGradeToWasteRockMultipliers.Radioactive = {
    ["Depleted"] = 3000,
    ["Very Low"] = 2500,
    ["Low"] = 2000,
    ["Average"] = 1500,
    ["High"] = 1200,
    ["Very High"] = 1000
}

function OnMsg.SectorScanned(status, col, row) --find a way for it to be spawned by engine
	local sector = g_MapSectors[col][row]
	for i = 1, #sector.markers.subsurface do
		local marker = sector.markers.subsurface[i]
		if not marker.revealed and marker.resource == "Radioactive" then
			marker.revealed = true
			local deposit = marker:PlaceDeposit()	
		end
	end
	if status == "deep scanned" then
		for i = 1, #sector.markers.deep do
			local marker = sector.markers.deep[i]
			if not marker.revealed and marker.resource == "Radioactive" then
				marker.revealed = true
				local deposit = marker:PlaceDeposit()	
				deposit:UpdateEntity()
			end
		end
	end
end

function OnMsg.LoadGame( ... )
	if not g_Consts["IsDeepRadioactiveExploitable"] == 1 and UICity:IsTechResearched("DeepMetalExtraction") then
		g_Consts["IsDeepRadioactiveExploitable"] = 1
		MapForEach("map", "SubsurfaceDepositRadioactive", function(obj)
			obj:UpdateEntity()
		end)
	end
end

function OnMsg.TechResearched(tech_id, city)
	if tech_id == "DeepMetalExtraction" then
		g_Consts["IsDeepRadioactiveExploitable"] = 1
		MapForEach("map", "SubsurfaceDepositRadioactive", function(obj)
			obj:UpdateEntity()
		end)
	end	
end

local vanillaIsDeepExploitable = IsDeepExploitable
function IsDeepExploitable(res)
	if res == "Radioactive" then
		return g_Consts["IsDeepRadioactiveExploitable"] ~= 0
	else
		return vanillaIsDeepExploitable(res)
	end
end

DefineClass.LH_RadioactiveExtractor =
{
	__parents = {"MetalExtractorWorkplace"},
	
	subsurface_deposit_class = "SubsurfaceDepositRadioactive",
	exploitation_resource = "Radioactive",
}

LH_RadioactiveExtractor.OnCalcProduction_Radioactive = BaseMetalsExtractor.OnCalcProduction_Metals
LH_RadioactiveExtractor.OnProduce_Radioactive = BaseMetalsExtractor.OnProduce_Metals
LH_RadioactiveExtractor.GetPredictedProduction_Radioactive = BaseMetalsExtractor.GetPredictedProduction_Metals
LH_RadioactiveExtractor.GetPredictedDailyProduction_Radioactive = BaseMetalsExtractor.GetPredictedDailyProduction_Metals