local translationIdBase = 1107012200


function OnMsg.RocketReachedEarth(rocket)
	rocket.city:CalcBaseExportFunding(rocket.exported_amount, rocket)
end

function SupplyRocket:OnDemolish()
	if self.allow_export_pmetals then self:ToggleAutoExport() end
	if self.allow_export_crystals then self:ToggleAllowExportCrystals() end
	RocketBase.OnDemolish(self)
end

function SupplyRocket:UIStatusDeparting(...)
	local exported = self.exported_amount
	if self.exported_resource == "Crystals" then
		self.exported_amount = 0
	end
	RocketBase.UIStatusDeparting(self, ...)
	self.exported_amount = exported
	if self.exported_resource == "Crystals" and 0 < (self.exported_amount or 0) then
		self.pin_rollover = self.pin_rollover .. "<newline>" .. T({
			7674,
			"Exporting <resource(amount,res)>",
			amount = self.exported_amount,
			res = self.exported_resource
		})
	end
end

function SupplyRocket:CreateExportRequests() --try to refactor to call vanilla func
	if self.allow_export then
		if not self.exported_resource then
			self.exported_resource = "PreciousMetals"
			self.allow_export_pmetals = true
		end
		local unit_count = self:GetRequestUnitCount(self.max_export_storage)
		self.export_requests = {
			self:AddDemandRequest(self.exported_resource, self.max_export_storage, 0, unit_count)
		}
	else
		self.export_requests = nil
	end
end


function SupplyRocket:ResetDemandRequests() --try to refactor to call vanilla func
	if self.allow_export and not self.exported_resource then
		self.exported_resource = "PreciousMetals"
		self.allow_export_pmetals = true
	end
	self:ResetFuelDemandRequests()
	local req = self:GetExportRequest()
	if self.allow_export then
		if req then
			req:ResetAmount(self.max_export_storage)
		else
			local unit_count = self:GetRequestUnitCount(self.max_export_storage)
			self.export_requests = self.export_requests or {}
			self.export_requests[#self.export_requests + 1] = self:AddDemandRequest(self.exported_resource, self.max_export_storage, 0, unit_count)
		end
	else
		if self.export_requests then
			table.remove_entry(self.task_requests, req) 
		end
		self.export_requests = nil
	end
end

function SupplyRocket:GetExportRequest(rocket, res)
	local res = res or self.exported_resource or "PreciousMetals"
	return RocketBase.GetExportRequest(self, rocket, res)	
end

function SupplyRocket:ToggleAllowExport(broadcast)
	local allow = not self.allow_export_pmetals
	if broadcast then
		local list = self.city.labels.SupplyRocket or empty_table
		for _, rocket in ipairs(list) do
			if rocket.allow_export_pmetals ~= allow then
				rocket:ToggleAllowExport()
			end
		end
		return
	end
	self.allow_export_pmetals = not self.allow_export_pmetals
	if self.allow_export_pmetals and self.allow_export_crystals then
		self:ToggleAllowExportCrystals()
	end
	if not self.allow_export_pmetals and not self.allow_export_crystals and self.auto_export then
		self:ToggleAutoExport()
	end
	if self.command == "Refuel" or self.command == "WaitLaunchOrder" then
		if self.allow_export_pmetals then       
			self.exported_resource = "PreciousMetals" 
			self:InterruptDrones(nil, function(drone)
				if drone.target == self or drone.d_request and drone.d_request:GetBuilding() == self or drone.s_request and drone.s_request:GetBuilding() == self then
					return drone
				end
			end)
			self:DisconnectFromCommandCenters()
			local stored = self:GetStoredAmount("PreciousMetals")
			self:AddResource(-Min(self.max_export_storage,stored),"PreciousMetals")
			local unit_count = 1 + self.max_export_storage / (const.ResourceScale * 10)
			self.export_requests = {
				self:AddDemandRequest("PreciousMetals", self.max_export_storage - Min(self.max_export_storage,stored), 0, unit_count)
			}
			if self.unload_request then
				table.remove_entry(self.task_requests, self.unload_request)
				self.unload_request = nil
			end
			self:ConnectToCommandCenters()
		elseif self.export_requests and 0 < #self.export_requests then
			self.exported_resource = false
			self:InterruptDrones(nil, function(drone)
				if drone.target == self or drone.d_request and drone.d_request:GetBuilding() == self or drone.s_request and drone.s_request:GetBuilding() == self then
					return drone
				end
			end)
			self:DisconnectFromCommandCenters()
			local amount = self.max_export_storage - self.export_requests[1]:GetActualAmount()
			self:AddResource(amount, "PreciousMetals")
			table.remove_entry(self.task_requests, self.export_requests[1])
			self.export_requests = nil
			self.exported_resource = false
			self:ConnectToCommandCenters()
		else
			self.exported_resource = false
		end
	end
	self.allow_export = self.allow_export_pmetals or self.allow_export_crystals
	ObjModified(self)
end

function SupplyRocket:ToggleAllowExportCrystals(broadcast)
	local allow = not self.allow_export_crystals
	if broadcast then
		local list = self.city.labels.SupplyRocket or empty_table
		for _, rocket in ipairs(list) do
			if rocket.allow_export_crystals ~= allow then
				rocket:ToggleAllowExport()
			end
		end
		return
	end
	self.allow_export_crystals = not self.allow_export_crystals
	if self.allow_export_crystals and self.allow_export_pmetals then
		self:ToggleAllowExport()
	end
	if not self.allow_export_pmetals and not self.allow_export_crystals and self.auto_export then
		self:ToggleAutoExport()
	end
	if self.command == "Refuel" or self.command == "WaitLaunchOrder" then
		if self.allow_export_crystals then      
			self.exported_resource = "Crystals"
			self:InterruptDrones(nil, function(drone)
				if drone.target == self or drone.d_request and drone.d_request:GetBuilding() == self or drone.s_request and drone.s_request:GetBuilding() == self then
					return drone
				end
			end)
			self:DisconnectFromCommandCenters()
			local stored = self:GetStoredAmount("Crystals")
			self:AddResource(-Min(self.max_export_storage,stored),"Crystals")
			local unit_count = 1 + self.max_export_storage / (const.ResourceScale * 10)
			self.export_requests = {
				self:AddDemandRequest("Crystals", self.max_export_storage - Min(self.max_export_storage,stored), 0, unit_count)
			}
			if self.unload_request then
				table.remove_entry(self.task_requests, self.unload_request)
				self.unload_request = nil
			end
			self:ConnectToCommandCenters()
		elseif self.export_requests and 0 < #self.export_requests then
				self.exported_resource = false
			self:InterruptDrones(nil, function(drone)
				if drone.target == self or drone.d_request and drone.d_request:GetBuilding() == self or drone.s_request and drone.s_request:GetBuilding() == self then
					return drone
				end
			end)
			self:DisconnectFromCommandCenters()
			local amount = self.max_export_storage - self.export_requests[1]:GetActualAmount()
			self:AddResource(amount, "Crystals")
			table.remove_entry(self.task_requests, self.export_requests[1])
			self.export_requests = nil
			self.exported_resource = false
			self:ConnectToCommandCenters()
		else
			self.exported_resource = false
		end 
	end
	self.allow_export = self.allow_export_pmetals or self.allow_export_crystals
	ObjModified(self)
end

function SupplyRocket:AddResource(amount, resource)
	if self:IsKindOf("RocketExpedition") or self:IsKindOf("TradeRocket") then
		RocketBase.AddResource(self, amount, resource)
		return
	end
	if amount > 0 and resource == self.exported_resource then
		local export_request = self:GetExportRequest(self)
		local new_amount = Max(0, self.max_export_storage - amount)
		if export_request then
			export_request:SetAmount(new_amount)
		else
			local unit_count = self:GetRequestUnitCount(self.max_export_storage)
			self.export_requests = self.export_requests or {}
			self.export_requests[#self.export_requests + 1] = self:AddDemandRequest(resource, new_amount, 0, unit_count)
		end
		return
	end
	if not self.supply[resource] then
		PlaceResourceStockpile_Delayed(self:GetVisualPos(), resource, amount, self:GetAngle(), true)
	else
		UniversalStorageDepot.AddResource(self, amount, resource)
	end
end

function SupplyRocket:GetStoredExportResourceAmount()
	local amount = self:GetStoredAmount(self.exported_resource)
	if self.unload_request then
		amount = amount + self.unload_request:GetActualAmount()
	end
	if not self.export_requests then
		return amount
	end
	return amount + self.max_export_storage - self.export_requests[1]:GetActualAmount()
end

function SupplyRocket:GetStoredPMetalsAmount()
	return self:GetStoredAmount("PreciousMetals")
end

function SupplyRocket:GetStoredCrystalsAmount()
	return self:GetStoredAmount("Crystals")
end

function SupplyRocket:GetUIExportStatus()
	local status = ""
	if self.exported_resource == "PreciousMetals" then
		status = status..T(286, "Gathering exports<right><preciousmetals(StoredExportResourceAmount, max_export_storage)>")
	elseif self.exported_resource == "Crystals" then
		status = status..T(translationIdBase+1, "Gathering exports<right><crystals(StoredExportResourceAmount, max_export_storage)>")
	end
	if status ~= "" then status = status.."\n" end
	if self:GetStoredPMetalsAmount() > 0 then
		status = status..T(translationIdBase+2, "<left>Unloading <right><preciousmetals(StoredPMetalsAmount, max_export_storage)>")
	end
	if self:GetStoredCrystalsAmount() > 0 then
		if self:GetStoredPMetalsAmount() > 0 then status = status.."\n" end
		status = status..T(translationIdBase+3, "<left>Unloading <right><crystals(StoredCrystalsAmount, max_export_storage)>")
	end
	return status
end

local vanillaRocketExpeditionRefurbishRocket = RocketExpedition.RefurbishRocket
function RocketExpedition:RefurbishRocket(orig_rocket, target_rocket, connect_cc)
	target_rocket.allow_export_crystals = orig_rocket.allow_export_crystals
	target_rocket.allow_export_pmetals = orig_rocket.allow_export_pmetals
	target_rocket.exported_resource = orig_rocket.exported_resource
	return vanillaRocketExpeditionRefurbishRocket(self, orig_rocket, target_rocket, connect_cc)
end

function OnMsg.ClassesBuilt()
	local idx = table.find(XTemplates.customSupplyRocket[1],"lh_export_crystals", true)
	if idx then
		table.remove(XTemplates.customSupplyRocket[1],idx)
	end 
	for i = #XTemplates.customSupplyRocket[1], 5, -1 do
		XTemplates.customSupplyRocket[1][i+1] = XTemplates.customSupplyRocket[1][i]
	end

	XTemplates.customSupplyRocket[1][4] = PlaceObj("XTemplateTemplate", {
		"comment", "alllow/forbid export",
		"__template", "InfopanelButton",
		"OnContextUpdate", function(self, context)
			if context.allow_export_pmetals then
				self:SetIcon("UI/Icons/IPButtons/forbid_exports_on.tga")
				self:SetRolloverTitle(T(8040, "Allow/Forbid Exports"))
				self:SetRolloverText(T(8041, [[Allow or forbid the exports of Rare Metals to Earth. If precious metals are exported, the colony will gain Funding in exchange.

Current status: Exports are <em>allowed</em>.]]))
				self:SetRolloverHint(T(10114, "<left_click> Forbid Exports<newline><em>Ctrl + <left_click></em> Forbid Exports in all Rockets"))
				self:SetRolloverHintGamepad(T(10115, "<ButtonA> Forbid Exports<newline><ButtonX> Forbid Exports in all Rockets"))
			else
				self:SetIcon("UI/Icons/IPButtons/forbid_exports_off.tga")
				self:SetRolloverTitle(T(8040, "Allow/Forbid Exports"))
				self:SetRolloverText(T(8042, [[Allow or forbid the exports of Rare Metals to Earth. If precious metals are exported, the colony will gain Funding in exchange.

Current status: Exports are <em>forbidden</em>.]]))
				self:SetRolloverHint(T(8109, "<left_click> Allow Exports<newline><em>Ctrl + <left_click></em> Allow Exports in all Rockets"))
				self:SetRolloverHintGamepad(T(8110, "<ButtonA> Allow Exports<newline><ButtonX> Allow Exports in all Rockets"))
			end
		end,
		"OnPressParam", "ToggleAllowExport",
		"OnPress", function(self, gamepad)
			self.context:ToggleAllowExport(not gamepad and IsMassUIModifierPressed())
		end,
		"AltPress", true,
		"OnAltPress", function(self, gamepad)
			if gamepad then
				self.context:ToggleAllowExport(true)
			end
		end
	})

	XTemplates.customSupplyRocket[1][5] = PlaceObj("XTemplateTemplate", {
		"lh_export_crystals", true,
		"id", "lh_export_crystals",
		"comment", "allow/forbid crystls",
		"__template", "InfopanelButton",
		"OnContextUpdate", function(self, context)
			if context.allow_export_crystals then
				self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/lh_crystals_forbid_exports_on.tga")
				self:SetRolloverTitle(T(8040, "Allow/Forbid Exports"))
				self:SetRolloverText(T(translationIdBase+4, [[Allow or forbid the exports of Crystals to Earth. If crystals are exported, the colony will gain Funding in exchange.

Current status: Exports are <em>allowed</em>.]]))
				self:SetRolloverHint(T(10114, "<left_click> Forbid Exports<newline><em>Ctrl + <left_click></em> Forbid Exports in all Rockets"))
				self:SetRolloverHintGamepad(T(10115, "<ButtonA> Forbid Exports<newline><ButtonX> Forbid Exports in all Rockets"))
			else
				self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/lh_crystals_forbid_exports_off.tga")
				self:SetRolloverTitle(T(8040, "Allow/Forbid Exports"))
				self:SetRolloverText(T(translationIdBase+5, [[Allow or forbid the exports of Crystals to Earth. If crystls are exported, the colony will gain Funding in exchange.

Current status: Exports are <em>forbidden</em>.]]))
				self:SetRolloverHint(T(8109, "<left_click> Allow Exports<newline><em>Ctrl + <left_click></em> Allow Exports in all Rockets"))
				self:SetRolloverHintGamepad(T(8110, "<ButtonA> Allow Exports<newline><ButtonX> Allow Exports in all Rockets"))
			end
		end,
		"OnPressParam", "ToggleAllowExportCrystals",
		"OnPress", function(self, gamepad)
		self.context:ToggleAllowExportCrystals(not gamepad and IsMassUIModifierPressed())
		end,
		"AltPress", true,
		"OnAltPress", function(self, gamepad)
			if gamepad then
				self.context:ToggleAllowExportCrystals(true)
			end
		end
	})

	local idx = table.find(XTemplates.customSupplyRocket[1][10],"lh_radioactive", true)
	if idx then
		table.remove(XTemplates.customSupplyRocket[1][10],idx)
	end 
	for i = #XTemplates.customSupplyRocket[1][10], 2, -1 do
		XTemplates.customSupplyRocket[1][10][i+1] = XTemplates.customSupplyRocket[1][10][i]
	end
	XTemplates.customSupplyRocket[1][10][2] = PlaceObj("XTemplateTemplate", {
		"lh_radioactive", true,
		"id", "lh_radioactive",
		"__template", "InfopanelText",
		"Text", T{"<resource('Radioactive')><right><radioactive(Stored_Radioactive)>"}
	})
	local idx = table.find(XTemplates.customSupplyRocket[1][10],"lh_meat", true)
	if idx then
		table.remove(XTemplates.customSupplyRocket[1][10],idx)
	end 
	XTemplates.customSupplyRocket[1][10][#XTemplates.customSupplyRocket[1][10]+1] = PlaceObj("XTemplateTemplate", {
		"lh_meat", true,
		"id", "lh_meat",
		"__template", "InfopanelText",
		"Text", T{"<resource('Meat')><right><meat(Stored_Meat)>"}
	})
	local idx = table.find(XTemplates.customSupplyRocket[1][10],"lh_booze", true)
	if idx then
		table.remove(XTemplates.customSupplyRocket[1][10],idx)
	end 
	XTemplates.customSupplyRocket[1][10][#XTemplates.customSupplyRocket[1][10]+1] = PlaceObj("XTemplateTemplate", {
		"lh_booze", true,
		"id", "lh_booze",
		"__template", "InfopanelText",
		"Text", T{"<resource('Booze')><right><booze(Stored_Booze)>"}
	})

	table.insert_unique(BuildingTemplates.RocketExpedition.storable_resources, "Radioactive")
	table.insert_unique(BuildingTemplates.RocketExpedition.storable_resources, "Hydrocarbon")
	table.insert_unique(BuildingTemplates.RocketExpedition.storable_resources, "Crystals")
	table.insert_unique(BuildingTemplates.RocketExpedition.storable_resources, "Fuel")
    table.insert_unique(BuildingTemplates.RocketExpedition.storable_resources, "Meat")
	table.insert_unique(BuildingTemplates.RocketExpedition.storable_resources, "Booze")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "PreciousMetals")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Radioactive")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Hydrocarbon")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Crystals")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Fuel")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Meat")
	table.insert_unique(BuildingTemplates.SupplyRocket.storable_resources, "Booze")
	ClassTemplates.Building.SupplyRocket.pin_summary1 =  T{"<AccumulatedFuel>"}
	ClassTemplates.Building.SupplyRocket.pin_summary2 =  T{"<AccumulatedExport>"}
	if IsDlcAvailable("gagarin") then
		table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "PreciousMetals")
		table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "Radioactive")
		table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "Hydrocarbon")
		table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "Crystals")
		table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "Fuel")
        table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "Meat")
		table.insert_unique(BuildingTemplates.ZeusRocket.storable_resources, "Booze")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "PreciousMetals")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "Radioactive")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "Hydrocarbon")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "Crystals")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "Fuel")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "Meat")
		table.insert_unique(BuildingTemplates.DragonRocket.storable_resources, "Booze")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "PreciousMetals")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "Radioactive")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "Hydrocarbon")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "Crystals")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "Fuel")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "Meat")
		table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "Booze")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "PreciousMetals")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "Radioactive")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "Hydrocarbon")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "Crystals")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "Fuel")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "Meat")
		table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "Booze")
		ClassTemplates.Building.ZeusRocket.pin_summary1 =  T{"<AccumulatedFuel>"}
		ClassTemplates.Building.ZeusRocket.pin_summary2 =  T{"<AccumulatedExport>"}
		ClassTemplates.Building.DragonRocket.pin_summary1 =  T{"<AccumulatedFuel>"}
		ClassTemplates.Building.DragonRocket.pin_summary2 =  T{"<AccumulatedExport>"}
	end
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "PreciousMetals")
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "Radioactive")
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "Hydrocarbon")
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "Crystals")
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "Fuel")
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "Meat")
	table.insert_unique(BuildingTemplates.TradeRocket.storable_resources, "Booze")
	
	local idx = table.find(XTemplates.customRocketExpedition[1][6],"comment", "radioactive")
	if idx then table.remove(XTemplates.customRocketExpedition[1][6],idx) end
	local idx = table.find(XTemplates.customRocketExpedition[1][6],"comment", "hydrocarbon")
	if idx then table.remove(XTemplates.customRocketExpedition[1][6],idx) end
	local idx = table.find(XTemplates.customRocketExpedition[1][6],"comment", "crystals")
	if idx then table.remove(XTemplates.customRocketExpedition[1][6],idx) end
	local idx = table.find(XTemplates.customRocketExpedition[1][6],"comment", "meat")
	if idx then table.remove(XTemplates.customRocketExpedition[1][6],idx) end
	local idx = table.find(XTemplates.customRocketExpedition[1][6],"comment", "booze")
	if idx then table.remove(XTemplates.customRocketExpedition[1][6],idx) end
	XTemplates.customRocketExpedition[1][6][#XTemplates.customRocketExpedition[1][6]+1] = PlaceObj("XTemplateTemplate", {
		"comment", "radioactive",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local export = context.expedition and context.expedition.export
			local amount = export and export.Radioactive or 0
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
		end,
		"Text", T("<resource('Radioactive' )><right><radioactive(ExportAmount_Radioactive, ExportTarget_Radioactive)>")
	})
	XTemplates.customRocketExpedition[1][6][#XTemplates.customRocketExpedition[1][6]+1] = PlaceObj("XTemplateTemplate", {
		"comment", "hydrocarbon",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local export = context.expedition and context.expedition.export
			local amount = export and export.Hydrocarbon or 0
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
		end,
		"Text", T("<resource('Hydrocarbon' )><right><hydrocarbon(ExportAmount_Hydrocarbon, ExportTarget_Hydrocarbon)>")
	})
	XTemplates.customRocketExpedition[1][6][#XTemplates.customRocketExpedition[1][6]+1] = PlaceObj("XTemplateTemplate", {
		"comment", "crystals",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local export = context.expedition and context.expedition.export
			local amount = export and export.Crystals or 0
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
		end,
		"Text", T("<resource('Crystals' )><right><crystals(ExportAmount_Crystals, ExportTarget_Crystals)>")
	})
	XTemplates.customRocketExpedition[1][6][#XTemplates.customRocketExpedition[1][6]+1] = PlaceObj("XTemplateTemplate", {
		"comment", "meat",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local export = context.expedition and context.expedition.export
			local amount = export and export.Meat or 0
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
		end,
		"Text", T("<resource('Meat' )><right><meat(ExportAmount_Meat, ExportTarget_Meat)>")
	})
	XTemplates.customRocketExpedition[1][6][#XTemplates.customRocketExpedition[1][6]+1] = PlaceObj("XTemplateTemplate", {
		"comment", "booze",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local export = context.expedition and context.expedition.export
			local amount = export and export.Booze or 0
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
		end,
		"Text", T("<resource('Booze' )><right><booze(ExportAmount_Booze, ExportTarget_Booze)>")
	})

	local resForeignAidRocket = XTemplates.customForeignAidRocket[1][3]
	local idx = table.find(resForeignAidRocket,"comment", "radioactive")
	if idx then table.remove(resForeignAidRocket,idx) end
	local idx = table.find(resForeignAidRocket,"comment", "hydrocarbon")
	if idx then table.remove(resForeignAidRocket,idx) end
	local idx = table.find(resForeignAidRocket,"comment", "crystals")
	if idx then table.remove(resForeignAidRocket,idx) end
	local idx = table.find(resForeignAidRocket,"comment", "meat")
	if idx then table.remove(resForeignAidRocket,idx) end
	local idx = table.find(resForeignAidRocket,"comment", "booze")
	if idx then table.remove(resForeignAidRocket,idx) end
	resForeignAidRocket[#resForeignAidRocket+1] = PlaceObj("XTemplateTemplate", {
		"comment", "radioactive",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local amount = context:GetStoredAmount("Radioactive")
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
        end,
		"Text", T("<resource('Radioactive' )><right><radioactive(Stored_Radioactive)>")
	})
	resForeignAidRocket[#resForeignAidRocket+1] = PlaceObj("XTemplateTemplate", {
		"comment", "hydrocarbon",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local amount = context:GetStoredAmount("Hydrocarbon")
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
        end,
		"Text", T("<resource('Hydrocarbon' )><right><hydrocarbon(Stored_Hydrocarbon)>")
	})
	resForeignAidRocket[#resForeignAidRocket+1] = PlaceObj("XTemplateTemplate", {
		"comment", "crystals",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local amount = context:GetStoredAmount("Crystals")
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
        end,
		"Text", T("<resource('Crystals' )><right><crystals(Stored_Crystals)>")
	})
	resForeignAidRocket[#resForeignAidRocket+1] = PlaceObj("XTemplateTemplate", {
		"comment", "meat",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local amount = context:GetStoredAmount("Meat")
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
        end,
		"Text", T("<resource('Meat' )><right><meat(Stored_Meat)>")
	})
	resForeignAidRocket[#resForeignAidRocket+1] = PlaceObj("XTemplateTemplate", {
		"comment", "booze",
		"__template", "InfopanelText",
		"FoldWhenHidden", true,
		"ContextUpdateOnOpen", true,
		"OnContextUpdate", function(self, context)
			local amount = context:GetStoredAmount("Booze")
			self:SetVisible(amount > 0)
			self:SetText(self.Text)
        end,
		"Text", T("<resource('Booze' )><right><booze(Stored_Booze)>")
	})
end

local CreateGetters_RocketExpedition = RocketExpedition.CreateGetters
function RocketExpedition:CreateGetters(...)
	CreateGetters_RocketExpedition(self, ...)
	local resources = {"Crystals","Radioactive","Meat","Booze","Hydrocarbon"}
	for _, res in ipairs(resources) do
		self["GetExportTarget_" .. res] = function(self)
			if not self.expedition then
				return 0
			end
			if self.expedition.route_id then
				return self.TradeAmount
			end
			return self.expedition.export and self.expedition.export[res] or 0
		end
		self["GetExportAmount_" .. res] = function(self, exclude_stored)
			if not self.expedition then
				return 0
			end
			local capacity = self.expedition.route_id and self.TradeAmount or self.expedition.export and self.expedition.export[res] or 0
			if capacity <= 0 then
				return 0
			end
			local stored = 0
			if not exclude_stored and table.find(self.storable_resources, res) then
				stored = self:GetStoredAmount(res)
			end
			for _, req in ipairs(self.export_requests or empty_table) do
				if req:GetResource() == res then
					return capacity - req:GetActualAmount() + stored
				end
			end
			return stored
		end
	end
end


--thanks Silva
function SupplyRocket:GetAccumulatedFuel() 
	local extra = self.unload_fuel_request and self.unload_fuel_request:GetActualAmount() or 0	
	local amount = (self.launch_fuel - self.refuel_request:GetActualAmount() + extra) / const.ResourceScale
	if amount < 0 then amount = 0 end
	return  T{"<fuel><icon_Fuel_small>", fuel = amount}
end
function SupplyRocket:GetAccumulatedExport() 
	local amount = self:GetStoredExportResourceAmount() / 1000
	if self.exported_amount and self.command == "FlyToEarth" then
		amount = self.exported_amount / 1000
	end
	if self.exported_resource == "PreciousMetals" then
		return T{"<export><icon_PreciousMetals_small>", export = amount}
	elseif self.exported_resource == "Crystals" then
		return T{"<export><icon_Crystals_small>", export = amount}
	end
	return T(translationIdBase+10,"N/A")
end
