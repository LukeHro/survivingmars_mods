local translationIdBase = 1107012300

local CreateResourceRequests_SpaceElevator = SpaceElevator.CreateResourceRequests
function SpaceElevator:CreateResourceRequests()
  CreateResourceRequests_SpaceElevator(self)
  self.allow_export_pmetals = self.allow_export
  self.allow_export_crystals = not self.allow_export
  self.all_export_request = {}
  self.all_unload_request = {}
  self.all_export_request.PreciousMetals = self.export_request
  self.all_unload_request.PreciousMetals = self.unload_request
  local unit_count = 1 + self.max_export_storage / (const.ResourceScale * 10)
  self.all_export_request.Crystals = self:AddDemandRequest("Crystals", 0, 0, unit_count)
  local unit_count = 1 + self.max_export_storage / (const.ResourceScale * 10)
  self.all_unload_request.Crystals = self:AddSupplyRequest("Crystals", 0, 0, unit_count)
  table.remove_entry(self.task_requests, self.all_export_request.Crystals)
  table.remove_entry(self.task_requests, self.all_unload_request.Crystals)
  print("CreateResourceRequests")
end

--[[local ExportGoods_SpaceElevator = SpaceElevator.ExportGoods
function SpaceElevator:ExportGoods()
	local MarkPreciousMetalsExport_City = City.MarkPreciousMetalsExport
	local CalcBaseExportFunding_City = City.CalcBaseExportFunding
	if self.export_resource == "Crystals" then
		City.MarkPreciousMetalsExport = City.MarkCrystalsExport
		City.CalcBaseExportFunding = function(...)
			local export_funding = CalcBaseExportFunding_City(...)
			if export_funding > 0 then
				export_funding = export_funding*4/5
		  		export_funding = UICity:ChangeFunding(export_funding, "Export")
		  		AddOnScreenNotification("CrystalsExport", nil, {funding = export_funding})			
			end
			return 0
		end	
	end
	ExportGoods_SpaceElevator(self)
	City.MarkPreciousMetalsExport = MarkPreciousMetalsExport_City
	City.CalcBaseExportFunding = CalcBaseExportFunding_City
end]]

function SpaceElevator:ToggleAllowExportPMetals()
	 if not self.allow_export_pmetals and self.allow_export_crystals then
		self:ToggleAllowExportCrystals()
	end
  if not self.allow_export_pmetals and self.export_resource == "Crystals" then
  	self.export_resource = "PreciousMetals"
  	self.export_request = self.all_export_request.PreciousMetals
  	self.unload_request = self.all_unload_request.PreciousMetals
  end	
  self.allow_export = self.allow_export_pmetals
  SpaceElevator.ToggleAllowExport(self)
  self.allow_export_pmetals = self.allow_export
end


function SpaceElevator:ToggleAllowExportCrystals()
  if self.allow_export_pmetals and not self.allow_export_crystals then
  	self:ToggleAllowExportPMetals()
  end
  if not self.allow_export_crystals and self.export_resource == "PreciousMetals" then
  	self.export_resource = "Crystals"
  	self.export_request = self.all_export_request.Crystals
  	self.unload_request = self.all_unload_request.Crystals
  end
  self.allow_export = self.allow_export_crystals
  SpaceElevator.ToggleAllowExport(self)
  self.allow_export_crystals = self.allow_export
end


function SpaceElevator:GetExportManifest()
  local texts = {}
  if self.current_export and self.export_resource == "PreciousMetals" then
    texts[#texts + 1] = T({
      760,
      "<left>Exporting <right><preciousmetals(amount)>",
      amount = self.current_export
    })
  end
  if self.current_export and self.export_resource == "Crystals" then
    texts[#texts + 1] = T({
      translationIdBase+3,
      "<left>Exporting <right><crystals(amount)>",
      amount = self.current_export
    })
  end
  local amount = self:GetStoredExportResourceAmount()
  local rt = FormatResourceValueMaxResource(self, amount, self.max_export_storage, self.export_resource)
  texts[#texts + 1] = T({
    761,
    "<left>Waiting to export <right><res>",
    res = rt
  })
  return table.concat(texts, "<newline>")
end

function OnMsg.ClassesBuilt()
	local idx = table.find(XTemplates.customSpaceElevator[1],"lh_export_crystals", true)
	if idx then
		table.remove(XTemplates.customSpaceElevator[1],idx)
	end 
	for i = #XTemplates.customSpaceElevator, 2, -1 do
		XTemplates.customSpaceElevator[1][i+1] = XTemplates.customSpaceElevator[1][i]
	end

	XTemplates.customSpaceElevator[1][1] = PlaceObj("XTemplateTemplate", {
		"comment", "allow/forbid export",
      "__template", "InfopanelButton",
      "OnContextUpdate", function(self, context)
        if context.allow_export_pmetals then
          self:SetIcon("UI/Icons/IPButtons/forbid_exports_on.tga")
          self:SetRolloverTitle(T(7818, "Forbid Exports"))
          self:SetRolloverText(T(7819, "Forbid the export of Rare Metals."))
        else
          self:SetIcon("UI/Icons/IPButtons/forbid_exports_off.tga")
          self:SetRolloverTitle(T(7820, "Allow Exports"))
          self:SetRolloverText(T(7821, "Allow the export of Rare Metals."))
        end
      end,
      "OnPressParam", "ToggleAllowExportPMetals"
    })

	XTemplates.customSpaceElevator[1][2] = PlaceObj("XTemplateTemplate", {
		"lh_export_crystals", true,
		"id", "lh_export_crystals",
		"comment", "allow/forbid crystals",
      "__template", "InfopanelButton",
      "OnContextUpdate", function(self, context)
        if context.allow_export_crystals then
          self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/lh_crystals_forbid_exports_on.tga")
          self:SetRolloverTitle(T(7818, "Forbid Exports"))
          self:SetRolloverText(T(translationIdBase+2, "Forbid the export of Crystals."))
        else
          self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/lh_crystals_forbid_exports_off.tga")
          self:SetRolloverTitle(T(7820, "Allow Exports"))
          self:SetRolloverText(T(translationIdBase+3, "Allow the export of Crystals."))
        end
      end,
      "OnPressParam", "ToggleAllowExportCrystals"
    })
end

function SpaceElevator:ExportGoods()
  repeat 
    -- empty the export request, store the amount, update the last export time
    self.current_export = self.allow_export and self:GetStoredExportResourceAmount() or 0
    self:ResetDemandRequests()
    self.last_export_time = GameTime()
    
    -- launch the pod
    local base_pos = self:GetPos()
    self.pin_status_img = "UI/Icons/pin_rocket_outgoing.tga"
    local tick, t = 1000, 0
    self.move_dir = 1
    
    PlayFX("ElevatorMoving", "start", self)
    PlayFX("ElevatorMoving", "start", self.pod)
    while t < self.travel_time do
      if self.working then
        t = Min(self.travel_time, t + tick)
        local h = MulDivRound(self.rope_height, t, self.travel_time)
        self.pod:SetPos(base_pos + point(0, 0, h), tick)
      end
      Sleep(tick)
      if not IsValid(self) or self.destroyed then return end            
    end
    PlayFX("ElevatorMoving", "end", self)
    PlayFX("ElevatorMoving", "end", self.pod)
    
    -- grant extra funding
    local export_funding = self.city:CalcBaseExportFunding(self.current_export, self)
    if export_funding > 0 then
      export_funding = self.city:ChangeFunding(export_funding, "Export")
      AddOnScreenNotification("RareMetalsExport", nil, { funding = export_funding })
    end
    self.current_export = nil
            
    -- check for imports & store them locally
    if #self.import_queue > 0 then
      self.current_imports = table.remove(self.import_queue, 1)
    end
    
    -- launch back
    self.pin_status_img = "UI/Icons/pin_rocket_incoming.tga"
    self.move_dir = -1
    t = 0
    PlayFX("ElevatorMoving", "start", self)
    PlayFX("ElevatorMoving", "start", self.pod)
    while t < self.travel_time do
      if self.working then
        t = Min(self.travel_time, t + tick)
        local h = self.rope_height - MulDivRound(self.rope_height, t, self.travel_time)
        self.pod:SetPos(base_pos + point(0, 0, h), tick)
      end
      Sleep(tick)
      if not IsValid(self) or self.destroyed then return end            
    end       
    PlayFX("ElevatorMoving", "end", self)
    PlayFX("ElevatorMoving", "end", self.pod)
    
     self.pin_status_img = nil
    self.move_dir = 0
    if self.current_imports then
      self:UnloadCargo(self.current_imports)
      self.current_imports = nil
    end 
  until #self.import_queue == 0 or g_Consts.SupplyMissionsEnabled ~= 1 -- repeat immediately if there are queued import items
end