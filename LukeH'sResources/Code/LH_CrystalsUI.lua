local translationIdBase = 1107010600

const.TagLookupTable["icon_Crystals"] = "<image "..CurrentModPath.."UI/Icons/res_crystals.tga 1300>"
const.TagLookupTable["icon_Crystals_small"] = "<image "..CurrentModPath.."UI/Icons/res_crystals.tga 800>"
const.TagLookupTable["icon_Crystals_orig"] = "<image "..CurrentModPath.."UI/Icons/res_crystals.tga>"

function StorageDepot:Getui_Crystals()
	return T({translationIdBase+1, "Crystals<right><crystals(Stored_Crystals,MaxAmount_Crystals)>", self})
end


function InfobarObj:GetCrystalsText()
	local crystals = ResourceOverviewObj:GetAvailableCrystals() / const.ResourceScale
	return T({translationIdBase+2, "<crystals><icon_Crystals_orig>", crystals = self:FmtRes(crystals)})
end

function InfobarObj:GetCrystalsRollover()
  return ResourceOverviewObj:GetCrystalsRollover()
end

function ResourceOverview:GetCrystalsRollover()
  local ret = { self:GetBasicResourcesHeading(),
    T(316, "<newline>"),
    T({translationIdBase+3, "Crystals production<right><crystals(CrystalsProducedYesterday)>", self}),
    T({translationIdBase+4, "Crystals consumption<right><crystals(CrystalsConsumedByConsumptionYesterday)>", self}),
    T({translationIdBase+5, "Crystals maintenance<right><crystals(CrystalsConsumedByMaintenanceYesterday)>", self}),
    T({translationIdBase+6, "Upgrade construction<right><crystals(CrystalsUpgradeConstructionActual, CrystalsUpgradeConstructionTotal)>", self}),
    T({translationIdBase+7, "<LastExportCrystalsStr>", self})
  }
  return table.concat(ret, "<newline><left>")
end

function ResourceOverview:GetLastExportCrystalsStr()
  if UICity.last_export_crystals then
    local t = UICity.last_export_crystals
    return T({translationIdBase+8, "Last export<right>Sol <day>, Hour <hour><newline><left>Crystals exported<right><crystals(amount)>",
      day = t.day,
      hour = t.hour,
      amount = t.amount
    })
  else
    return T(3612, "Last export<right>N/A")
  end
end


ResourceOverview["GetCrystalsProducedYesterday"] = function(self)
	return self:GetProducedYesterday("Crystals")
end
ResourceOverview["GetCrystalsGatheredYesterday"] = function(self)
	return self:GetGatheredYesterday("Crystals")
end
ResourceOverview["GetCrystalsConsumedByConsumptionYesterday"] = function(self)
	return self:GetConsumedByConsumptionYesterday("Crystals")
end
ResourceOverview["GetCrystalsConsumedByMaintenanceYesterday"] = function(self)
	return self:GetEstimatedDailyMaintenance("Crystals")
end
ResourceOverview["GetAvailableCrystals"] = function(self)
	return self:GetAvailable("Crystals")
end
ResourceOverview["GetCrystalsInConstructionSitesActual"] = function(self)
	return self:GetInConstructionSites("Crystals", "actual")
end
ResourceOverview["GetCrystalsInConstructionSitesTotal"] = function(self)
	return self:GetInConstructionSites("Crystals", "total")
end
ResourceOverview["GetCrystalsUpgradeConstructionActual"] = function(self)
	return self:GetUpgradeConstruction("Crystals", "actual")
end
ResourceOverview["GetCrystalsUpgradeConstructionTotal"] = function(self)
	return self:GetUpgradeConstruction("Crystals", "total")
end

function OnMsg.ClassesBuilt()
    local idx = table.find(XTemplates.ipRover[1][20],"Id", "idCrystals")
    if idx then
      table.remove(XTemplates.ipRover[1][20], idx)
    end
    for i = #XTemplates.ipRover[1][20],5,-1 do
        XTemplates.ipRover[1][20][i+1] = XTemplates.ipRover[1][20][i]
    end
    XTemplates.ipRover[1][20][5] = PlaceObj('XTemplateTemplate', {
        '__condition', function (parent, context) return context:HasMember("GetStored_Crystals") end,
        'Id', "idCrystals",
        '__template', "InfopanelText",
        'Text', T(translationIdBase+9, --[[XTemplate ipRover Text]] "Crystals<right><crystals(Stored_Crystals)>"),
      })
end