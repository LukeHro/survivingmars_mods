local translationIdBase = 1107011200

const.TagLookupTable["icon_Booze"] = "<image "..CurrentModPath.."UI/Icons/res_booze.tga 1300>"
const.TagLookupTable["icon_Booze_small"] = "<image "..CurrentModPath.."UI/Icons/res_booze.tga 800>"
const.TagLookupTable["icon_Booze_orig"] = "<image "..CurrentModPath.."UI/Icons/res_booze.tga>"

function StorageDepot:Getui_Booze()
	return T({translationIdBase+1, "Booze<right><booze(Stored_Booze,MaxAmount_Booze)>", self})
end

function InfobarObj:GetBoozeText()
	local booze = ResourceOverviewObj:GetAvailableBooze() / const.ResourceScale
	return T({translationIdBase+2, "<booze><icon_Booze_orig>", booze = self:FmtRes(booze)})
end

function InfobarObj:GetBoozeRollover()
  return ResourceOverviewObj:GetBoozeRollover()
end

function ResourceOverview:GetBoozeRollover()
  local ret = {self:GetBasicResourcesHeading(), 
    T(316, "<newline>"),
    T({translationIdBase+3, "Booze production<right><booze(BoozeProducedYesterday)>", self}),
    T({translationIdBase+4, "Booze consumption<right><booze(BoozeConsumedByConsumptionYesterday)>", self}),
    T({translationIdBase+5, "Stored in service buildings<right><booze(BoozeStoredInServiceBuildings)>", self})
  }
  return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetBoozeProducedYesterday"] = function(self)
	return self:GetProducedYesterday("Booze")
end
ResourceOverview["GetBoozeConsumedByConsumptionYesterday"] = function(self)
	return self:GetConsumedByConsumptionYesterday("Booze")
end
ResourceOverview["GetAvailableBooze"] = function(self)
	return self:GetAvailable("Booze")
end

function ResourceOverview:GetBoozeStoredInServiceBuildings()
  return self.data.booze_in_service_buildings or 0
end

function OnMsg.ClassesBuilt()    
    local idx = table.find(XTemplates.ipRover[1][20],"Id", "idBooze")
    if idx then
      table.remove(XTemplates.ipRover[1][20], idx)
    end
    for i = #XTemplates.ipRover[1][20],8,-1 do
        XTemplates.ipRover[1][20][i+1] = XTemplates.ipRover[1][20][i]
    end
    XTemplates.ipRover[1][20][8] = PlaceObj('XTemplateTemplate', {
        '__condition', function (parent, context) return context:HasMember("GetStored_Booze") end,
        'Id', "idBooze",
        '__template', "InfopanelText",
        'Text', T(translationIdBase+6, --[[XTemplate ipRover Text]] "Booze<right><booze(Stored_Booze)>"),
      })
end