local translationIdBase = 1107010200

const.TagLookupTable["icon_Hydrocarbon"] = "<image "..CurrentModPath.."UI/Icons/res_hydrocarbon.tga 1300>"
const.TagLookupTable["icon_Hydrocarbon_small"] = "<image "..CurrentModPath.."UI/Icons/res_hydrocarbon.tga 800>"
const.TagLookupTable["icon_Hydrocarbon_orig"] = "<image "..CurrentModPath.."UI/Icons/res_hydrocarbon.tga>"

function StorageDepot:Getui_Hydrocarbon()
	return T({translationIdBase+1, "Hydrocarbon<right><hydrocarbon(Stored_Hydrocarbon,MaxAmount_Hydrocarbon)>", self})
end

function InfobarObj:GetHydrocarbonText()
	local hydrocarbon = ResourceOverviewObj:GetHydrocarbonNumber() / const.ResourceScale
	return T({translationIdBase+2, "<hydrocarbon><icon_Hydrocarbon_orig>", hydrocarbon = self:FmtRes(hydrocarbon, "colorized")})
end

function InfobarObj:GetHydrocarbonGridRollover()
  return ResourceOverviewObj:GetHydrocarbonGridRollover()
end

function ResourceOverview:GetHydrocarbonGridRollover()
  local ret = {
    T(translationIdBase+3, "Hydrocarbon is distributed via pipe grids.<newline>") ,
    T{translationIdBase+4, "Hydrocarbon production<right><hydrocarbon(TotalProducedHydrocarbon)>", self}, 
    T{translationIdBase+5, "Hydrocarbon demand<right><hydrocarbon(TotalRequiredHydrocarbon)>", self}, 
    T{translationIdBase+6, "Stored Hydrocarbon<right><hydrocarbon(TotalStoredHydrocarbon)>", self}, 
    T{translationIdBase+7, "Capacity<right><hydrocarbon(HydrocarbonStorageCapacity)>", self}, 
  }
  return table.concat(ret, "<newline><left>")
end

function ResourceOverview:GetTotalProducedHydrocarbon()
  return self.data.total_hydrocarbon_production or 0
end

function ResourceOverview:GetTotalStoredHydrocarbon()
  return self.data.total_hydrocarbon_storage or 0
end

function ResourceOverview:GetTotalRequiredHydrocarbon()
  return self.data.total_hydrocarbon_demand or 0
end

function ResourceOverview:GetHydrocarbonStorageCapacity()
  local buildings = MapGet("map","HydrocarbonStorage", nil, const.efVisible ) 
  local capacity = 0
  for _, building in ipairs(buildings)do
    capacity = capacity + building.hydrocarbon_capacity
  end
  return capacity
end

function ResourceOverview:GetHydrocarbonNumber()
  return self:GetTotalProducedHydrocarbon() - self:GetTotalRequiredHydrocarbon()
end