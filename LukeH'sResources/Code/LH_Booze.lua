local translationIdBase = 1107011100

Resources["Booze"] = {name = "Booze",
	display_name = T{translationIdBase+1, "Booze"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_booze.tga", 
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_BoozeContainer", 
	description = T{translationIdBase+2, "Booze for consumption."}
}

table.insert_unique(ResourceDescription, Resources["Booze"])	
table.insert_unique(StockpileResourceList, "Booze")
table.insert_unique(CargoShuttle.storable_resources, "Booze")
table.insert_unique(AllResourcesList, "Booze")
table.insert_unique(RCTransport.storable_resources,"Booze")

function OnMsg.ClassesPostprocess()
    if not TraitPresets.LH_Drunk then
        PlaceObj('TraitPreset', {
            auto = false,
            category = "Negative",
            description = T(translationIdBase+3, "Decreased work performance until resting"),
            display_name = T(translationIdBase+4, "Drunk"),
            dome_filter_only = true,
            hidden_on_start = true,
            group = "Negative",
            id = "LH_Drunk",
            incompatible = {},
            name = "LH_Drunk",  
            modify_amount = -20,
            modify_property = "performance",
            modify_target = "self",
        })
    end
    ColonistPerformanceReasons.LH_Drunk = T(translationIdBase+6, "Working drunk <amount>")
end

local Rest_Colonist = Colonist.Rest
function Colonist:Rest(...)
	Rest_Colonist(self, ...)
	--print("removed drunk")
	self:RemoveTrait("LH_Drunk")
end
