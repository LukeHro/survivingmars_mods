local translationIdBase = 1107011000

const.TagLookupTable["icon_Meat"] = "<image "..CurrentModPath.."UI/Icons/res_meat.tga 1300>"
const.TagLookupTable["icon_Meat_small"] = "<image "..CurrentModPath.."UI/Icons/res_meat.tga 800>"
const.TagLookupTable["icon_Meat_orig"] = "<image "..CurrentModPath.."UI/Icons/res_meat.tga>"

function StorageDepot:Getui_Meat()
	return T({translationIdBase+1, "Meat<right><meat(Stored_Meat,MaxAmount_Meat)>", self})
end


function InfobarObj:GetMeatText()
	local meat = ResourceOverviewObj:GetAvailableMeat() / const.ResourceScale
	return T({translationIdBase+2, "<meat><icon_Meat_orig>", meat = self:FmtRes(meat)})
end

function InfobarObj:GetMeatRollover()
	return ResourceOverviewObj:GetMeatRollover()
end

function ResourceOverview:GetMeatRollover()
	local ret = {self:GetBasicResourcesHeading(),
		T(316, "<newline>"),
		T({translationIdBase+3, "Meat production<right><meat(MeatProducedYesterday)>", self}),
		T({translationIdBase+4, "Meat consumption<right><meat(MeatConsumedByConsumptionYesterday)>", self}),
		T({translationIdBase+5, "Stored in service buildings<right><meat(MeatStoredInServiceBuildings)>", self})
	}
	return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetMeatProducedYesterday"] = function(self)
	return self:GetProducedYesterday("Meat")
end
ResourceOverview["GetMeatConsumedByConsumptionYesterday"] = function(self)
	return self:GetConsumedByConsumptionYesterday("Meat")
end
ResourceOverview["GetAvailableMeat"] = function(self)
	return self:GetAvailable("Meat")
end

function ResourceOverview:GetMeatStoredInServiceBuildings()
	return self.data.meat_in_service_buildings or 0
end

function OnMsg.ClassesBuilt()		
	local idx = table.find(XTemplates.ipRover[1][20],"Id", "idMeat")
	if idx then
		table.remove(XTemplates.ipRover[1][20], idx)
	end
	for i = #XTemplates.ipRover[1][20],7,-1 do
			XTemplates.ipRover[1][20][i+1] = XTemplates.ipRover[1][20][i]
	end
	XTemplates.ipRover[1][20][7] = PlaceObj('XTemplateTemplate', {
			'__condition', function (parent, context) return context:HasMember("GetStored_Meat") end,
			'Id', "idMeat",
			'__template', "InfopanelText",
			'Text', T(translationIdBase+6, "Meat<right><meat(Stored_Meat)>"),
		})
	if IsDlcAvailable("shepard") and XTemplates.sectionPasture then
		XTemplates.sectionPasture[1][2][1].Text = T(translationIdBase+7, "Stored Meat<right><resource(StoredAmount, max_storage, 'Meat')>")
	end
end
