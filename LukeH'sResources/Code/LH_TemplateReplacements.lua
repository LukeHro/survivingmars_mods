local translationIdBase = 1107012700

local function UpdateBuildings(buildings, initial)

	if IsDlcAvailable("shepard") then
		buildings.OpenPasture.resource_produced1 = "Meat"
		buildings.InsidePasture.resource_produced1 = "Meat"
		buildings.OpenPasture.description = T("Produces Meat. Consumes Food, Water and Oxygen depending on animal type.")
		if IsDlcAvailable("armstrong") then
			buildings.InsidePasture.description = T("Produces Meat. Consumes Seeds, Water and Oxygen depending on animal type.")
		else
			buildings.InsidePasture.description = T("Produces Meat. Consumes Food, Water and Oxygen depending on animal type.")
		end
	end

	buildings.RegolithExtractor.build_category = "LH_BasicResourcesProduction"
	buildings.MetalsExtractor.build_category = "LH_BasicResourcesProduction"
	buildings.PreciousMetalsExtractor.build_category = "LH_BasicResourcesProduction"
	buildings.ElectronicsFactory.build_category = "LH_AdvancedResourcesProduction"
	buildings.MachinePartsFactory.build_category = "LH_AdvancedResourcesProduction"
	if IsDlcAvailable("contentpack3") then
		buildings.ElectronicsFactory_Small.build_category = "LH_AdvancedResourcesProduction"
		buildings.MachinePartsFactory_Small.build_category = "LH_AdvancedResourcesProduction"
	end
	buildings.FuelFactory.build_category = "LH_AdvancedResourcesProduction"
	buildings.PolymerPlant.build_category = "LH_AdvancedResourcesProduction"

	buildings.FuelFactory.display_name = T(translationIdBase+6, "Fuel Factory")
	buildings.FuelFactory.display_name_pl = T(translationIdBase+7, "Fuel Factories")
	Presets.Cargo.Prefabs.FuelFactory.name = T(translationIdBase+8, "Fuel Factory")

	buildings.MDSLaser.construction_cost_Crystals = 3000
	buildings.DefenceTower.construction_cost_Crystals = 5000
	buildings.PowerDecoy.construction_cost_Crystals = 5000
	buildings.PowerDecoy.construction_cost_Polymers = nil
	buildings.LightTrap.construction_cost_Crystals = 5000
	buildings.LightTrap.construction_cost_Electronics = nil
	buildings.HangingGardens.construction_cost_Crystals = 10000
	buildings.HangingGardens.construction_cost_Concrete = 30000
	buildings["Casino Complex"].construction_cost_Crystals = 5000
	buildings.ShopsJewelry.construction_cost_Crystals = 2000
	if IsDlcAvailable("contentpack3") then
		buildings.ShopsJewelry_Small.construction_cost_Crystals = 1000
		buildings.ShopsJewelry_Small.consumption_resource_type="Crystals"
	end
	buildings.ArtWorkshop.construction_cost_Crystals = 5000
	buildings.BioroboticsWorkshop.construction_cost_Crystals = 5000
	buildings.VRWorkshop.construction_cost_Crystals = 5000
	if IsDlcAvailable("gagarin") then
		buildings.OutsideLamp.construction_cost_Crystals = 1000
		buildings.TaiChiGarden.construction_cost_Crystals = 10000
		buildings.MegaMall.construction_cost_Crystals = 5000
		buildings.Temple.construction_cost_Crystals = 5000
		buildings.LowGLab.construction_cost_Radioactive = 2000
		buildings.RCDrillerBuilding.description = T(translationIdBase+11, "Remote-controlled vehicle that extracts storable resources from underground deposits (but not deep deposits). Drilling wastes 50% of the materials in the deposit.")
		CargoPreset.RCDriller.description = T(translationIdBase+11, "Remote-controlled vehicle that extracts storable resources from underground deposits (but not deep deposits). Drilling wastes 50% of the materials in the deposit.")
		g_Classes.RCDriller.description = T(translationIdBase+11, "Remote-controlled vehicle that extracts storable resources from underground deposits (but not deep deposits). Drilling wastes 50% of the materials in the deposit.")
		g_Classes.RCDriller.pin_rollover = T(translationIdBase+11, "Remote-controlled vehicle that extracts storable resources from underground deposits (but not deep deposits). Drilling wastes 50% of the materials in the deposit.")
	end
	if IsDlcAvailable("armstrong") then
		buildings.GHGFactory.template_class="LH_GHGFactory"
		buildings.GHGFactory.hydrocarbon_consumption = 5000
		buildings.GHGFactory.consumption_resource_type = nil
		buildings.GHGFactory.consumption_max_storage = nil
		buildings.GHGFactory.consumption_amount = nil
		buildings.GHGFactory.description = T(translationIdBase+9, "Burns extracted hydrocarbon to release Greenhouse gases,"
				.." effectively improving the global Temperature<icon_TemperatureTP_alt>. Has significantly boosted terraforming effect at low Temperature<icon_TemperatureTP_alt>.")
	end

	buildings.FusionReactor.construction_cost_Radioactive = 10000
	buildings.FusionReactor.construction_cost_Polymers = 5000
	buildings.AtomicBattery.construction_cost_Radioactive = 5000
	buildings.AtomicBattery.construction_cost_Polymers = nil
	buildings.MartianUniversity.construction_cost_Radioactive = 5000
	buildings.MartianUniversity.construction_cost_Electronics = 10000
	buildings.ScienceInstitute.construction_cost_Radioactive = 10000
	buildings.ScienceInstitute.construction_cost_Electronics = 10000
	buildings.ArtificialSun.construction_cost_Radioactive = 300000
	buildings.ArtificialSun.construction_cost_Metals = 150000
	buildings.ArtificialSun.construction_cost_Polymers = 150000
	buildings.ArtificialSun.maintenance_resource_type = "Radioactive"
	buildings.ArtificialSun.maintenance_resource_amount = 10000

	buildings.MDSLaser.construction_cost_Crystals = 3000
	buildings.DefenceTower.construction_cost_Crystals = 5000
	buildings.PowerDecoy.construction_cost_Crystals = 5000
	buildings.PowerDecoy.construction_cost_Polymers = nil
	buildings.LightTrap.construction_cost_Crystals = 5000
	buildings.LightTrap.construction_cost_Electronics = nil
	buildings.HangingGardens.construction_cost_Crystals = 10000
	buildings.HangingGardens.construction_cost_Concrete = 30000
	buildings["Casino Complex"].construction_cost_Crystals = 5000
	if IsDlcAvailable("contentpack3") then
		buildings.ShopsJewelry.construction_cost_Crystals = 2000
		buildings.ShopsJewelry_Small.construction_cost_Crystals = 1000
	end
	buildings.ArtWorkshop.construction_cost_Crystals = 5000
	buildings.BioroboticsWorkshop.construction_cost_Crystals = 5000
	buildings.VRWorkshop.construction_cost_Crystals = 5000
	if IsDlcAvailable("gagarin") then
		buildings.OutsideLamp.construction_cost_Crystals = 1000
		buildings.MegaMall.construction_cost_Crystals = 5000
		buildings.TaiChiGarden.construction_cost_Crystals = 10000
		buildings.Temple.construction_cost_Crystals = 5000
		buildings.LowGLab.construction_cost_Radioactive = 2000
	end

	buildings.FusionReactor.construction_cost_Radioactive = 10000
	buildings.FusionReactor.construction_cost_Polymers = 5000
	buildings.AtomicBattery.construction_cost_Radioactive = 5000
	buildings.AtomicBattery.construction_cost_Polymers = nil
	buildings.MartianUniversity.construction_cost_Radioactive = 5000
	buildings.MartianUniversity.construction_cost_Electronics = 10000
	buildings.ScienceInstitute.construction_cost_Radioactive = 10000
	buildings.ScienceInstitute.construction_cost_Electronics = 10000
	buildings.ArtificialSun.construction_cost_Radioactive = 300000
	buildings.ArtificialSun.construction_cost_Metals = 150000
	buildings.ArtificialSun.construction_cost_Polymers = 150000

	buildings.MedicalCenter.upgrade1_upgrade_cost_Crystals = 5000--Holographic Scanner
	buildings.MedicalCenter.upgrade1_upgrade_cost_Polymers = 5000
	buildings.MedicalCenter.upgrade2_upgrade_cost_Crystals = 5000 --Rejuvenation Treatment
	buildings.MedicalCenter.upgrade2_upgrade_cost_Polymers = 5000
	buildings.Infirmary.upgrade1_upgrade_cost_Crystals = 2000 --Rejuvenation Treatment
	buildings.Infirmary.upgrade1_upgrade_cost_Polymers = 3000
	buildings.Arcology.upgrade1_upgrade_cost_Crystals = 5000 --Home Collective
	buildings.Arcology.upgrade1_upgrade_cost_Polymers = 5000
	buildings.Apartments.upgrade1_upgrade_cost_Crystals = 2000 --Home Collective
	buildings.Apartments.upgrade1_upgrade_cost_Polymers = 3000
	buildings.LivingQuarters.upgrade1_upgrade_cost_Crystals = 2000 --Home Collective
	buildings.LivingQuarters.upgrade1_upgrade_cost_Polymers = 3000
	if IsDlcAvailable("contentpack3") then
		buildings.LivingQuarters_Small.upgrade1_upgrade_cost_Crystals = 1000 --Home Collective
		buildings.LivingQuarters_Small.upgrade1_upgrade_cost_Polymers = 2000
		buildings.SmartHome_Small.upgrade1_upgrade_cost_Crystals = 1000 --Home Collective
		buildings.SmartHome_Small.upgrade1_upgrade_cost_Polymers = 1000
	end
	buildings.SmartHome.upgrade1_upgrade_cost_Crystals = 2000 --Home Collective
	buildings.SmartHome.upgrade1_upgrade_cost_Polymers = 2000

	buildings.NetworkNode.upgrade1_upgrade_cost_Radioactive = 5000 --Amplify
	buildings.NetworkNode.upgrade1_upgrade_cost_Polymers = nil
	buildings.ScienceInstitute.upgrade2_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.ScienceInstitute.upgrade2_upgrade_cost_Polymers = nil
	buildings.ResearchLab.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.ResearchLab.upgrade2_upgrade_cost_Polymers = nil
	buildings.WaterExtractor.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.WaterExtractor.upgrade2_upgrade_cost_Polymers = nil
	buildings.ElectronicsFactory.upgrade3_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.ElectronicsFactory.upgrade3_upgrade_cost_Polymers = nil
	buildings.FuelFactory.upgrade1_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.FuelFactory.upgrade1_upgrade_cost_Polymers = nil
	buildings.MachinePartsFactory.upgrade3_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.MachinePartsFactory.upgrade3_upgrade_cost_Polymers = nil
	buildings.MetalsExtractor.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.MetalsExtractor.upgrade2_upgrade_cost_Polymers = nil
	buildings.PolymerPlant.upgrade3_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.PolymerPlant.upgrade3_upgrade_cost_Polymers = nil
	buildings.PreciousMetalsExtractor.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.PreciousMetalsExtractor.upgrade2_upgrade_cost_Polymers = nil
	buildings.RegolithExtractor.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.RegolithExtractor.upgrade2_upgrade_cost_Polymers = nil
	buildings.WasteRockProcessor.upgrade3_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.WasteRockProcessor.upgrade3_upgrade_cost_Polymers = nil
	if IsDlcAvailable("armstrong") then
		buildings.CarbonateProcessor.upgrade1_upgrade_cost_Radioactive = 5000 --Amplify
		buildings.CarbonateProcessor.upgrade1_upgrade_cost_Polymers = nil
		buildings.CoreHeatConvector.upgrade1_upgrade_cost_Radioactive = 5000 --Amplify
		buildings.CoreHeatConvector.upgrade1_upgrade_cost_Polymers = nil
		buildings.MagneticFieldGenerator.upgrade1_upgrade_cost_Radioactive = 5000 --Amplify
		buildings.MagneticFieldGenerator.upgrade1_upgrade_cost_Polymers = nil
		buildings.GHGFactory.upgrade1_upgrade_cost_Radioactive = 2000 --Amplify
		buildings.GHGFactory.upgrade1_upgrade_cost_Polymers = nil
		buildings.GHGFactory.template_class="LH_GHGFactory"
		table.insert_unique(buildings.GHGFactory.__parents, "HydrocarbonConsumer")
		buildings.GHGFactory.hydrocarbon_consumption = 5000
		buildings.GHGFactory.consumption_resource_type = nil
		buildings.GHGFactory.consumption_max_storage = nil
		buildings.GHGFactory.consumption_amount = nil
	end
	if IsDlcAvailable("contentpack3") then
		buildings.ElectronicsFactory_Small.upgrade3_upgrade_cost_Radioactive = 1000 --Amplify
		buildings.ElectronicsFactory_Small.upgrade3_upgrade_cost_Polymers = nil
		buildings.MachinePartsFactory_Small.upgrade3_upgrade_cost_Radioactive = 1000 --Amplify
		buildings.MachinePartsFactory_Small.upgrade3_upgrade_cost_Polymers = nil
	end
	if IsDlcAvailable("gagarin") then
		buildings.LowGLab.upgrade2_upgrade_cost_Radioactive = 2000 --Amplify
		buildings.LowGLab.upgrade2_upgrade_cost_Polymers = nil
	end
	buildings.AutomaticMetalsExtractor.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.AutomaticMetalsExtractor.upgrade2_upgrade_cost_Polymers = nil
	buildings.ConcretePlant.upgrade2_upgrade_cost_Radioactive = 1000 --Amplify
	buildings.ConcretePlant.upgrade2_upgrade_cost_Polymers = nil
	buildings.MetalsRefinery.upgrade3_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.MetalsRefinery.upgrade3_upgrade_cost_Polymers = nil
	buildings.RareMetalsRefinery.upgrade3_upgrade_cost_Radioactive = 2000 --Amplify
	buildings.RareMetalsRefinery.upgrade3_upgrade_cost_Polymers = nil
	buildings.StirlingGenerator.upgrade1_upgrade_cost_Radioactive = 2000 --Plutonium Core, consumption?
	buildings.StirlingGenerator.upgrade1_upgrade_cost_Polymers = nil
	buildings.StirlingGenerator.upgrade1_consumption_resource_type="Radioactive"
	buildings.StirlingGenerator.upgrade1_consumption_amount=100
	buildings.StirlingGenerator.upgrade1_consumption_resource_stockpile_spot_name=""
	buildings.AdvancedStirlingGenerator.upgrade1_upgrade_cost_Radioactive = 2000 --Plutonium Core, consumption?
	buildings.AdvancedStirlingGenerator.upgrade1_upgrade_cost_Polymers = nil
	buildings.AdvancedStirlingGenerator.upgrade1_consumption_resource_type="Radioactive"
	buildings.AdvancedStirlingGenerator.upgrade1_consumption_amount=100
	buildings.AdvancedStirlingGenerator.upgrade1_consumption_resource_stockpile_spot_name=""
	buildings.FusionReactor.upgrade2_upgrade_cost_Radioactive = 20000 --Eternal Fusion
	buildings.FusionReactor.upgrade2_upgrade_cost_Polymers = nil

	if initial then return end
	--from here down, those modded ones are needed cause otherwise mod editor is blanking them

	buildings.LH_StorageCrystals.description = T(translationIdBase+31, "Stores <crystals(max_storage_per_resource)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_StorageCrystals.pin_summary1 = T{"<crystals(Stored_Crystals)>"}
	buildings.LH_MechanizedDepotCrystals.description = T(translationIdBase+32, "Stores <crystals(UIMaxStorageAmount)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_MechanizedDepotCrystals.pin_summary1 = T{"<crystals(Stored_Crystals)>"}
	buildings.LH_StorageRadioactive.description = T(translationIdBase+33, "Stores <radioactive(max_storage_per_resource)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_StorageRadioactive.pin_summary1 = T{"<radioactive(Stored_Radioactive)>"}
	buildings.LH_MechanizedDepotRadioactive.description = T(translationIdBase+34, "Stores <radioactive(UIMaxStorageAmount)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_MechanizedDepotRadioactive.pin_summary1 = T{"<radioactive(Stored_Radioactive)>"}
	buildings.LH_StorageMeat.description = T(translationIdBase+35, "Stores <meat(max_storage_per_resource)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_StorageMeat.pin_summary1 = T{"<meat(Stored_Meat)>"}
	buildings.LH_MechanizedDepotMeat.description = T(translationIdBase+36, "Stores <meat(UIMaxStorageAmount)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_MechanizedDepotMeat.pin_summary1 = T{"<meat(Stored_Meat)>"}
	buildings.LH_StorageBooze.description = T(translationIdBase+37, "Stores <booze(max_storage_per_resource)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_StorageBooze.pin_summary1 = T{"<booze(Stored_Booze)>"}
	buildings.LH_MechanizedDepotBooze.description = T(translationIdBase+38, "Stores <booze(UIMaxStorageAmount)>. Some resources will be transported to other depots within Drone range.")
	buildings.LH_MechanizedDepotBooze.pin_summary1 = T{"<booze(Stored_Booze)>"}
	
	buildings.LH_CrystalsExtractor.upgrade1_icon="UI/Icons/Upgrades/fueled_extractor_01.tga"
	buildings.LH_CrystalsExtractor.upgrade2_icon="UI/Icons/Upgrades/amplify_01.tga"
	buildings.LH_CrystalsExtractor.upgrade3_icon="UI/Icons/Upgrades/magnetic_extraction_01.tga"

	buildings.LH_HydrocarbonExtractor.upgrade1_icon="UI/Icons/Upgrades/amplify_01.tga"

	buildings.LH_RadioactiveExtractor.upgrade1_icon="UI/Icons/Upgrades/fueled_extractor_01.tga"
	buildings.LH_RadioactiveExtractor.upgrade2_icon="UI/Icons/Upgrades/amplify_01.tga"
	buildings.LH_RadioactiveExtractor.upgrade3_icon="UI/Icons/Upgrades/magnetic_extraction_01.tga"

	buildings.LH_AtomicGenerator.upgrade1_icon="UI/Icons/Upgrades/plutonium_core_01.tga"

	buildings.LH_FuelRefinery.upgrade1_icon="UI/Icons/Upgrades/amplify_01.tga"
	buildings.LH_FuelRefinery.upgrade2_icon="UI/Icons/Upgrades/factory_ai_01.tga"

	buildings.LH_PolycarbonPlant.upgrade1_icon="UI/Icons/Upgrades/factory_ai_01.tga"
	buildings.LH_PolycarbonPlant.upgrade2_icon="UI/Icons/Upgrades/automation_01.tga"
	buildings.LH_PolycarbonPlant.upgrade3_icon="UI/Icons/Upgrades/amplify_01.tga"

	buildings.LH_AtomicGenerator.construction_cost_Radioactive = 3000
	buildings.LH_AtomicGenerator.construction_cost_Radioactive = 3000
	buildings.LH_CrystalsExtractor.upgrade2_upgrade_cost_Radioactive = 1000
	buildings.LH_HydrocarbonExtractor.upgrade1_upgrade_cost_Radioactive = 1000
	buildings.LH_RadioactiveExtractor.upgrade2_upgrade_cost_Radioactive = 1000
	buildings.LH_AtomicGenerator.upgrade1_upgrade_cost_Radioactive = 2000
	buildings.LH_FuelRefinery.upgrade1_upgrade_cost_Radioactive = 1000
	buildings.LH_PolycarbonPlant.upgrade3_upgrade_cost_Radioactive = 2000

	for _,building in pairs(buildings) do
		local i = 1
		while true do
			local interest = building["interest"..i]
			if not interest then break end
			if interest == "interestDining" then
				building.needsMeat = true
			elseif interest == "interestDrinking" then
				building.needsBooze = true
			end
			i = i + 1
		end
	end
end

function OnMsg.CityStart(...)
	BuildMenuSubcategories.LH_BasicResourcesProduction.icon = CurrentModPath.."UI/Icons/Buildings/lh_mines.tga"
	BuildMenuSubcategories.LH_AdvancedResourcesProduction.icon = CurrentModPath.."UI/Icons/Buildings/lh_factories.tga"

	UpdateBuildings(ClassTemplates.Building)
end

function OnMsg.LoadGame(...) 
	BuildMenuSubcategories.LH_BasicResourcesProduction.icon = CurrentModPath.."UI/Icons/Buildings/lh_mines.tga"
	BuildMenuSubcategories.LH_AdvancedResourcesProduction.icon = CurrentModPath.."UI/Icons/Buildings/lh_factories.tga"

	UpdateBuildings(ClassTemplates.Building)
end

function OnMsg.ModsReloaded()
	
	UpdateBuildings(BuildingTemplates)
end

function OnMsg.ClassesPostprocess()	

	UpdateBuildings(BuildingTemplates, true)


	Presets.POI.Default.CaptureMeteors.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
		"resource", "Radioactive",
		"amount", 60000
	})
	Presets.POI.Default.HeighSpeedComSatellite.rocket_required_resources[2] = PlaceObj("ResourceAmount", { --sic
		"resource", "Radioactive",
		"amount", 50000
	})
	Presets.POI.Default.SETISatellite.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
		"resource", "Radioactive",
		"amount", 50000
	})
	if IsDlcAvailable("armstrong") then
		Presets.POI.Default.MeltThePolarCaps.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
			"resource", "Radioactive",
			"amount", 100000
		})
		--[[Presets.POI.Default.ImportGreenhouseGases.rocket_required_resources[2] = PlaceObj("ResourceAmount", {
			"resource", "Hydrocarbon",
			"amount", 30000
		})]]
		Presets.POI.Default.CaptureIceAsteroids.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
			"resource", "Radioactive",
			"amount", 60000
		})
		Presets.POI.Default.LaunchSpaceSunshade.rocket_required_resources[2] = PlaceObj("ResourceAmount", {
			"resource", "Radioactive",
			"amount", 30000
		})
		Presets.POI.Default.LaunchSpaceMirror.rocket_required_resources[2] = PlaceObj("ResourceAmount", {
			"resource", "Radioactive",
			"amount", 30000
		})
		Presets.POI.Default.LaunchSpaceMirror.rocket_required_resources[3] = PlaceObj("ResourceAmount", {
			"resource", "Crystals",
			"amount", 15000
		})
	end

	Presets.TechPreset["Engineering"].LowGHydrosynthsis.description = T(translationIdBase+10, [[
New Building: <em>Polymer Factory</em> (<buildinginfo('PolymerPlant')>) - Produces Polymers from Water and Fuel.
New Building: <em>Polycarbon Factory</em> (<buildinginfo('LH_PolycarbonPlant')>) - Produces Polymers from Hydrocarbon.
New Building: <em>Fuel Factory</em> (<buildinginfo('FuelFactory')>)  - Produces Fuel from Water.
New Building: <em>Fuel Refinery</em> (<buildinginfo('LH_FuelRefinery')>)  - Produces Fuel Hydrocarbon.

<grey>Resolving the obvious problems with water pressure in Low-G environments will allow for more complex hydro-synthetic procedures to be done on-site on Mars.</grey>]])

	Presets.TechPreset.Physics.DeepWaterExtraction.display_name = T(translationIdBase+13, "Deep Liquid Deposits Extraction")
	Presets.TechPreset.Physics.DeepWaterExtraction.description = T(translationIdBase+14, "Can exploit deep <em>Water</em> deposits and deep <em>Hydrocarbon</em> deposits.\n\n<grey>Water reserves the size of Lake Superior are predicted to be present deep beneath the surface of Mars.</grey>")

	Presets.TechPreset.Physics.DeepMetalExtraction.display_name = T(translationIdBase+15, "Deep Deposits Mining")
	Presets.TechPreset.Physics.DeepMetalExtraction.description = T(translationIdBase+16, "Can exploit deep <em>Metal</em>, <em>Rare Metal</em>, <em>Crystal</em> and <em>Radioactive</em> deposits.\n\n<grey>Digging deep can be dangerous but it is necessary for sustained metal production.</grey>")

	local ExtractorAI = Presets.TechPreset.Breakthroughs.ExtractorAI
	if not ExtractorAI.LH_Resorces then
		ExtractorAI.LH_Resorces = true
		ExtractorAI.description = T(translationIdBase+17, "<em>Extractors</em> can work without crews at <param1> performance.\n\n<grey>Extracting algorithms have been successfully implemented by our new Extractor AI, allowing for a certain degree of autonomy in mining operations.</grey>")
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 1, Label = "LH_CrystalsExtractor", Prop = "automation",}))
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 50, Label = "LH_CrystalsExtractor", Prop = "auto_performance",}))
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 1, Label = "LH_RadioactiveExtractor", Prop = "automation",}))
		table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 50, Label = "LH_RadioactiveExtractor", Prop = "auto_performance",}))
	end
end

local GetConstructionDescription_Vanilla = GetConstructionDescription
function GetConstructionDescription(class, cost1, dont_modify)
	local template_class = ClassTemplates.Building[class.id]
	local properties = template_class.properties
	local FormatResource_orig = FormatResource
	FormatResource = function ( t, val, resource )
		local formated = FormatResource_orig(t, val, resource)
		if resource == "Food" and template_class.consumption_resource_type == "Food" then
			formated = T("<icon_Food>") --don't show "1" Food
		end
		if resource == "Power" and IsKindOf(template_class, "InsidePasture") then
			formated = formated..(IsDlcAvailable("shepard") and T(" <icon_Seeds>") or T(" <icon_Food>"))
		end
		if resource == "Power" and IsKindOf(template_class, "OpenPasture") then
			formated = formated..T(" <icon_Food>")
		end
		if resource == "Power" and IsKindOf(template_class, "HydrocarbonConsumer") then
			formated = formated.." "..FormatResource_orig(t, template_class.hydrocarbon_consumption, "Hydrocarbon")
		end
		if resource == "Power" and template_class.needsBooze then
			formated = formated..T(" <icon_Booze>")
		end
		if resource == "Food" and template_class.needsMeat then
			formated = formated..T("/<icon_Meat>")
		end
		return formated
	end
	local ret = GetConstructionDescription_Vanilla(class, cost1, dont_modify)
	FormatResource = FormatResource_orig
	if IsKindOf(template_class, "Pasture") then
		ret[1].table[5] = T("Base production: <icon_Meat> based on breed")
	end
	if IsKindOf(template_class, "HydrocarbonProducer") then
		ret[1].table[#ret[1].table+1] = T("Base production: ") .. table.concat( FormatResource({}, 5000, "Hydrocarbon"), " ")
		ret[1].j = ret[1].j+1
	end
	return ret
end