local translationIdBase = 1107012000

DefineClass.HydrocarbonGridFragment = {
  __parents = {
    "SupplyGridFragment"
  },
  supply_resource = "hydrocarbon"
}
DefineClass.KeroseneGridFragment = {
  __parents = {
    "SupplyGridFragment"
  },
  supply_resource = "kerosene"
}

function SupplyGridElement:GetUISectionHydrocarbonGridRollover()
  local grid = self.grid
  grid = grid:IsKindOf("WaterGrid") and grid.hydrocarbon_grid --or grid
  return grid and grid:GetUISectionHydrocarbonGridRollover()
end

local GameInit_LifeSupportGridObject = LifeSupportGridObject.GameInit
--beter for compatibility would be to call the overriden function but CreateLifeSupportElements has to be called first and only once
--then SupplyGridConnectElement needs to be called at the end and only once too
function LifeSupportGridObject:GameInit()
	self:CreateLifeSupportElements()
	if self.air then
		self.water = self.water or SupplyGridElement:new{ building = self }
	end
	if self.wastewater then
		self.water = self.water or SupplyGridElement:new{ building = self }
	end
	if self.oil then
		self.water = self.water or SupplyGridElement:new{ building = self }
		self:SupplyGridConnectElement(self.water, WaterGrid, "Chrome")
	else
		self:SupplyGridConnectElement(self.water, WaterGrid)
	end
end


local orig_SupplyGridExpand = SupplyGridExpand
function SupplyGridExpand( ... )
	local potential_neighbours = orig_SupplyGridExpand(...)
	local res ={}
	  for i = 1, #(potential_neighbours or ""), 2 do
	  	local pipe1 = HexGetPipe(potential_neighbours[i]:x(),potential_neighbours[i]:y()) or HexGetPillaredPipe(potential_neighbours[i]:x(),potential_neighbours[i]:y())
	  	local pipe2 = HexGetPipe(potential_neighbours[i+1]:x(),potential_neighbours[i+1]:y()) or HexGetPillaredPipe(potential_neighbours[i+1]:x(),potential_neighbours[i+1]:y())	  	
	  	if pipe1 and pipe2 then
			if (pipe1.water and pipe1.water.oil or pipe1.oil or pipe1.grid and pipe1.grid.hydrocarbon_grid) 
					and (pipe2.water and pipe2.water.oil or pipe2.oil or pipe2.grid and pipe2.grid.hydrocarbon_grid) then		
				table.insert(res, potential_neighbours[i])
				table.insert(res, potential_neighbours[i+1])
			elseif not (pipe1.water and pipe1.water.oil or pipe1.oil or pipe1.grid and pipe1.grid.hydrocarbon_grid) 
					and not (pipe2.water and pipe2.water.oil or pipe2.oil or pipe2.grid and pipe2.grid.hydrocarbon_grid) then		
				table.insert(res, potential_neighbours[i])
				table.insert(res, potential_neighbours[i+1])
			end
		end
	end
	return res
end

local orig_SupplyGridApplyBuilding = SupplyGridApplyBuilding
function SupplyGridApplyBuilding(grid, obj, ...)
	local potential_neighbours = orig_SupplyGridApplyBuilding(grid, obj, ...)
	if (grid==SupplyGridConnections["electricity"]) then  	
		return potential_neighbours
	end
	local res ={}
	for i = 1, #(potential_neighbours or ""), 2 do
	  	local pt, other_pt = potential_neighbours[i], potential_neighbours[i + 1]
		local adjascent = HexGridGetObject(ObjectGrid, other_pt, nil, nil, function(o) return not not (rawget(o, WaterGrid.supply_resource)) end)								
		local adjascent_element = adjascent[WaterGrid.supply_resource]
		local other_grid = adjascent_element.grid
		if other_grid and (obj.water and obj.water.oil or obj.oil or obj.grid and obj.grid.hydrocarbon_grid) 
				and (other_grid.water and other_grid.water.oil or other_grid.oil or other_grid.hydrocarbon_grid or other_grid.element_skin == "Chrome") then		
			table.insert(res, pt)
			table.insert(res, other_pt)
		elseif other_grid and not (obj.water and obj.water.oil or obj.oil or obj.grid and obj.grid.hydrocarbon_grid) 
				and not (other_grid.water and other_grid.water.oil or other_grid.oil or other_grid.hydrocarbon_grid or other_grid.element_skin == "Chrome") then		
			table.insert(res, pt)
			table.insert(res, other_pt)
		end
	end
	return res
end

function LifeSupportGridObject:GetUISectionHydrocarbonGridRollover()
	local grid = (not self.hydrocarbon or not self.hydrocarbon.grid) and self.water and self.water.grid and self.water.grid.hydrocarbon_grid
	if grid then
		return grid:GetUISectionHydrocarbonGridRollover()
	end
end

function HydrocarbonGridFragment:GetUISectionHydrocarbonGridRollover()
  local ret = {
    T(translationIdBase+1, "Oil grid parameters. Oil is consumed only when demanded.<newline>"),
    T({
      translationIdBase+2,
      "Max production<right><hydrocarbon(production)>",
      self
    }),
    T({
      translationIdBase+3,
      "Hydrocarbon consumption<right><hydrocarbon(current_consumption)>",
      current_consumption = self.current_consumption
    })
  }
  if self.production > self.consumption then
    table.insert(ret, T({
      translationIdBase+4,
      "Unused production<right><hydrocarbon(number)>",
      number = function(obj)
        local el = ResolvePropObj(obj)
        local grid = el.grid
        grid = grid:IsKindOf("WaterGrid") and grid.hydrocarbon_grid or grid
        return grid.production - grid.consumption
      end,
      self
    }))
  elseif self.production < self.consumption then
    table.insert(ret, T({
      translationIdBase+5,
      "Insufficient production<right><hydrocarbon(number)>",
      number = function(obj)
        local el = ResolvePropObj(obj)
        local grid = el.grid
        grid = grid:IsKindOf("WaterGrid") and grid.hydrocarbon_grid or grid
        return el.grid.consumption - el.grid.production
      end,
      self
    }))
  end
  return table.concat(ret, "<newline><left>")
end

local GetDescription_GridConstructionController = GridConstructionController.GetDescription
function GridConstructionController:GetDescription(...)
	if self.oil then return T("Pipes that transport Hydrocarbon and have to be connected to buildings on the spots indicated with a pipe connection icon.") end
	return GetDescription_GridConstructionController(self,  ...)
end


function LifeSupportGridElement:GameInit()
	if self.oil then
		self.description = T("Transport Hydrocarbon.")
  		self.display_name = T("Hydrocarbon Pipe")
  		self.display_name_pl = T("Hydrocarbon Pipes")
	end
end

function LifeSupportGridElement:CreateLifeSupportElements()
	self.water = SupplyGridElement:new({building = self, is_cable_or_pipe = true})
	if (self.construction_grid_skin == "Chrome") then
		self.oil = true
		self.water.oil = true
	end
end

local Init_WaterGrid = WaterGrid.Init
function WaterGrid:Init()	
	if self.element_skin == "Chrome" then
		UICity.hydrocarbon = UICity.hydrocarbon or SupplyGrid:new({city = self})
        UICity.kerosene = UICity.kerosene or SupplyGrid:new({city = self})
		self.hydrocarbon_grid = HydrocarbonGridFragment:new({ city = self.city})
		self.kerosene_grid = KeroseneGridFragment:new({ city = self.city })
	else
		Init_WaterGrid(self)
	end
end

local Done_WaterGrid = WaterGrid.Done
function WaterGrid:Done()
	if self.element_skin == "Chrome" then
		if self.hydrocarbon_grid then
			self.hydrocarbon_grid:delete()
			self.hydrocarbon_grid = nil
		end
		if self.kerosene_grid then
			self.kerosene_grid:delete()
			self.kerosene_grid = nil
		end
	else
		Done_WaterGrid(self)
	end
end

local AddElement_WaterGrid = WaterGrid.AddElement
function WaterGrid:AddElement(element, update)	
	local add = false
	local oil_element = element.building.hydrocarbon
	if oil_element and self.hydrocarbon_grid then
		self.hydrocarbon_grid:AddElement(oil_element, update)
		add = true
	end
	local oil_element = element.building.kerosene
	if oil_element and self.kerosene_grid then
		self.kerosene_grid:AddElement(oil_element, update)
		add = true
	end
	if add then--or (element.oil and self.hydrocarbon_grid or not element.oil and not self.hydrocarbon_grid) then
		return SupplyGridFragment.AddElement(self, element, update)
	else
		return AddElement_WaterGrid(self, element, update)
	end
end

local RemoveElement_WaterGrid = WaterGrid.RemoveElement
function WaterGrid:RemoveElement(element, update)	
	local oil_element = element.building.hydrocarbon
	if oil_element and self.hydrocarbon_grid then
		self.hydrocarbon_grid:RemoveElement(oil_element, update)
	end
	local oil_element = element.building.kerosene
	if oil_element and self.kerosene_grid then
		self.kerosene_grid:RemoveElement(oil_element, update)
	end
	RemoveElement_WaterGrid(self, element, update)
end


function OnMsg.ClassesPostprocess()	
	PlaceObj("XTemplateAction", {
      "ActionId",
      "actionPlaceOilPipes",
      "ActionName",
      T(882, "Pipes"),
      "ActionShortcut",
      "V",
      "ActionBindable",
      true,
      "OnAction",
      function(self, host, source)
        if g_Tutorial and g_Tutorial.BuildMenuWhitelist and not g_Tutorial.BuildMenuWhitelist.Pipes then
          return
        end
        local igi = GetInGameInterface()
        if not igi or not igi:GetVisible() then
          return
        end
        local dlg = GetHUD()
        if not dlg or not dlg:GetVisible() then
          return
        end
        local require_construction = not g_Consts or g_Consts.InstantPipes == 0
        igi:SetMode("life_support_grid", {grid_elements_require_construction = require_construction})
      end,
      "IgnoreRepeated",
      true
    })	
end

local vanilla_UIUIItemMenu = UIItemMenu
function UIItemMenu(category_id, bCreateItems)
	if not bCreateItems or category_id ~= "Production" then
		return vanilla_UIUIItemMenu(category_id, bCreateItems)
	else
		local items, count = vanilla_UIUIItemMenu(category_id, bCreateItems)
		count = count + 1
		local construction_cost = T(translationIdBase+20, "<formatedbuildinginfo('LifeSupportGridElement')>")
        local description = T(translationIdBase+21, "Transport Oil.")
        local require_construction = not g_Consts or g_Consts.InstantPipes == 0
        if require_construction and construction_cost ~= "" then
          description = description .. Untranslated([[


]]) .. construction_cost
        end
        local hint = ""
        local binding = "actionPlaceOilPipes"
        local shortcuts = GetShortcuts(binding)
        if shortcuts and (shortcuts[1] or shortcuts[2]) then
          hint = T({
            10930,
            "Shortcut - <em><ShortcutName(shortcut, 'keyboard')></em>",
            shortcut = binding
          })
        end
        items[#items + 1] = {
          name = "Pipes",
          Id = "Pipes",
          display_name = T(882, "Pipes"),
          icon = CurrentModPath.."UI/Icons/Buildings/lh_oil_pipes.tga",
          description = description,
          hint = hint,
          enabled = not g_Tutorial or not g_Tutorial.BuildMenuWhitelist or g_Tutorial.BuildMenuWhitelist.Pipes or false,
          action = function()
            g_LastBuildItem = "Pipes"
            GetInGameInterface():SetMode("oil_grid", {grid_elements_require_construction = require_construction})
          end,
          build_pos = 8,
          close_parent = true
        }
        return items, count
	end
end

DefineClass.GridConstructionDialogOil = {
  __parents = {
    "GridConstructionDialog"
  },
  mode_name = "oil_grid",
  MouseCursor = "UI/Cursors/PipePlacement.tga",
  success_sound = "PipeConstructionSuccess"
}

function OnMsg.ClassesBuilt()
	XTemplates.sectionWaterGrid[1].__condition = function (parent, context) return context and context.grid and not context.oil end
	IGIModeClasses.oil_grid = 'GridConstructionDialogOil'
	PlaceObj("XTemplate", {
	  group = "Infopanel Sections",
	  id = "sectionOilGrid",
	  PlaceObj("XTemplateTemplate", {
	    "__context",
	    function(parent, context)
	      return context:IsKindOfClasses("HydrocarbonProducer", "HydrocarbonStorage") and context.hydrocarbon or context:IsKindOf("LifeSupportGridElement") and context.pillar and context.water
	    end,
	    "__condition",
	    function(parent, context)
	      if not context then
	        return
	      end
	      local building = context.building
	      return context:IsKindOfClasses("HydrocarbonProducer", "HydrocarbonStorage") and context.hydrocarbon and context.hydrocarbon.grid or building:IsKindOf("LifeSupportGridElement") and building.pillar and building.water and building.water.grid and building.water.grid.hydrocarbon_grid
	    end,
	    "__template",
	    "InfopanelSection",
	    "RolloverText",
	    T(translationIdBase+22, "<UISectionHydrocarbonGridRollover>"),
	    "OnContextUpdate",
	    function(self, context, ...)
	      local building = context.building
	      local grid = context:IsKindOfClasses("HydrocarbonProducer", "HydrocarbonStorage") and context.hydrocarbon and context.hydrocarbon.grid or building:IsKindOf("LifeSupportGridElement") and building.pillar and building.water and building.water.grid and building.water.grid.hydrocarbon_grid
	      self.idCurrentProduction:SetText(T({
	        translationIdBase+23,
	        "Hydrocarbon production<right><hydrocarbon(current_production)>",
	        grid
	      }))
	      self.idConsumption:SetText(T({
	        translationIdBase+24,
	        "Total demand<right><hydrocarbon(consumption)>",
	        grid
	      }))
	      self.idCurrentStorage:SetText(T({
	        translationIdBase+25,
	        "Stored Hydrocarbon<right><hydrocarbon(current_storage)>",
	        current_storage = grid.current_storage
	      }))
	      XSection.OnContextUpdate(self, context, ...)
	    end,
	    "Title",
	    T(translationIdBase+26, "Hydrocarbon grid"),
	    "Icon",
	    "UI/Icons/Sections/life_support_grid.tga"
	  }, {
	    PlaceObj("XTemplateTemplate", {
	      "__template",
	      "InfopanelText",
	      "Id",
	      "idCurrentProduction"
	    }),
	    PlaceObj("XTemplateTemplate", {
	      "__template",
	      "InfopanelText",
	      "Id",
	      "idConsumption"
	    }),
	    PlaceObj("XTemplateTemplate", {
	      "__template",
	      "InfopanelText",
	      "Id",
	      "idCurrentStorage"
	    })
	  })
	})
    LH_AddXTemplate__(XTemplates.ipBuilding, "sectionOilGrid", "sectionWaterGrid")
    LH_AddXTemplate__(XTemplates.ipPillaredPipe, "sectionOilGrid", "sectionWaterGrid")


    PlaceObj('XTemplate', {
		group = "Infopanel Sections",
		id = "sectionOilProduction",
		PlaceObj('XTemplateTemplate', {
			'__context_of_kind', "HydrocarbonProducer",
			'__template', "InfopanelSection",
			'RolloverText', T(translationIdBase+27, "<UISectionOilProductionRollover>"),
			'Title', T(80, "Production"),
			'Icon', CurrentModPath.."UI/Icons/Sections/Hydrocarbon_2.tga",
		}, {
			PlaceObj('XTemplateTemplate', {
				'__template', "InfopanelText",
				'Text', T(translationIdBase+28, "<HydrocarbonProductionText>"),
			}),
			PlaceObj('XTemplateTemplate', {
				'__condition', function (parent, context) return context:IsKindOf("ResourceProducer") and context.wasterock_producer end,
				'__template', "InfopanelText",
				'Text', T(474, "Stored Waste Rock<right><wasterock(GetWasterockAmountStored,wasterock_max_storage)>"),
			}),
		}),
	})
    LH_AddXTemplate__(XTemplates.ipBuilding, "sectionOilProduction", "sectionWaterProduction")

	PlaceObj('XTemplate', {
		group = "Infopanel Sections",
		id = "sectionHydrocarbonStorage",
		PlaceObj('XTemplateTemplate', {
			'__context_of_kind', "HydrocarbonStorage",
			'__template', "InfopanelSection",
			'RolloverText', T(translationIdBase+29, "Hydrocarbon storage parameters and status. Hydrocarbon is consumed only when demanded."),
			'RolloverTitle', T(translationIdBase+30, "Hydrocarbon Storage"),
			'Icon', CurrentModPath.."UI/Icons/Sections/Hydrocarbon_4.tga",
		}, {
			PlaceObj('XTemplateTemplate', {
				'__context', function (parent, context) return context.hydrocarbon end,
				'__template', "InfopanelProgress",
				'BindTo', "StoragePercent",
			}),
			PlaceObj('XTemplateTemplate', {
				'__context', function (parent, context) return context.hydrocarbon end,
				'__template', "InfopanelText",
				'Text', T(167218330040, "<UIMode>"),
			}),
			PlaceObj('XTemplateTemplate', {
				'__template', "InfopanelText",
				'Text', T(translationIdBase+31, "Stored Hydrocarbon<right><hydrocarbon(StoredHydrocarbon)>"),
			}),
			PlaceObj('XTemplateTemplate', {
				'__template', "InfopanelText",
				'Text', T(translationIdBase+32, "Capacity<right><hydrocarbon(hydrocarbon_capacity)>"),
			}),
			PlaceObj('XTemplateTemplate', {
				'__template', "InfopanelText",
				'Text', T(translationIdBase+33, "Max output<right><hydrocarbon(max_hydrocarbon_discharge)>"),
			}),
		}),
	})
    LH_AddXTemplate__(XTemplates.ipBuilding, "sectionHydrocarbonStorage", "sectionWaterStorage")
    LH_AddXTemplateById(XTemplates.sectionConsumption, PlaceObj('XTemplateTemplate', {
        '__template', "InfopanelText",
        'Id', "idHydrocarbon",
    }), "idWater")
end

local vanilla_PlacePipeLine = PlacePipeLine
function PlacePipeLine(city, start_q, start_r, dir, steps, test, elements_require_construction, input_constr_grp, input_data, skin_name, supplied)
	local pipe = HexGetPipe(start_q, start_r) or HexGetPillaredPipe(start_q, start_r)
    if pipe and pipe.water then
		if pipe.water.oil and not (skin_name == "Chrome") then
			--print("1")
			return false, false, {}, {}
		end
		if not pipe.water.oil and (skin_name == "Chrome") then
			--print("2")
			return false, false, {}, {}
		end
	end
	local lh_IsBuildableZoneQR = IsBuildableZoneQR
	IsBuildableZoneQR = function (q, r)
	    local pipe = HexGetPipe(q, r) or HexGetPillaredPipe(q, r)
	    if pipe and pipe.water then
			if pipe.water.oil and not (skin_name == "Chrome") then
				--print("3")
				return false
			end
			if not pipe.water.oil and (skin_name == "Chrome") then
				--print("4")
				return false
			end
		end
		return lh_IsBuildableZoneQR(q, r)
	end
	local can_constr, constr_grp, obstructors, data, unbuildable_chunks, rocks, total_cost = vanilla_PlacePipeLine(city, start_q, start_r, dir, steps, test, elements_require_construction, input_constr_grp, input_data, skin_name, supplied)
	IsBuildableZoneQR = lh_IsBuildableZoneQR
	return can_constr, constr_grp, obstructors, data, unbuildable_chunks, rocks, total_cost
end

local CreateConnection_WaterGrid = WaterGrid.CreateConnection
function WaterGrid:CreateConnection(pt1, pt2, building1, building2)
	if (building1.oil or building1.hydrocarbon) and (building2.oil or building2.hydrocarbon) then
		return CreateConnection_WaterGrid(self, pt1, pt2, building1, building2)	
	end

	if building1.oil and not building2.oil then
		--print("5")
	elseif not building1.oil and building2.oil then
		--print("6")
	elseif building1.air and building2.oil then
		--print("7")
	elseif building1.oil and building2.air then
		--print("8")
	elseif building1.oil and not building2.oil then
		--print("9")
	elseif not building1.oil and building2.oil then
		--print("0")
	else
		return CreateConnection_WaterGrid(self, pt1, pt2, building1, building2)
	end
	return false
end

function LifeSupportGridElement:TryConnectInDir(dir)
	local conn = self.conn
	if not testbit(conn, dir) and testbit(shift(conn, -8), dir) then --not connected yet and has potential
		local dq, dr = HexNeighbours[dir + 1]:xy()
		local my_q, my_r = WorldToHex(self)
		local obj = HexGetPipe(my_q + dq, my_r + dr)
		if obj and obj.water then
			local his_conn = HexGridGet(SupplyGridConnections["water"], obj)
			if testbit(shift(his_conn, -8), dir) then --he has ptoential
				if not WaterGrid:CreateConnection(point(my_q, my_r), point(my_q + dq, my_r + dr), self, obj) then
					return --added by LukeH
				end					
				if self.class == obj.class and self.water.grid ~= obj.water.grid then --both constr or both finished but diff grids, merge
					local my_grid_count = #self.water.grid.elements
					local his_grid_count = #obj.water.grid.elements
					local my_grid_has_more_elements = my_grid_count > his_grid_count
					MergeGrids(my_grid_has_more_elements and self.water.grid or obj.water.grid,
									my_grid_has_more_elements and obj.water.grid or self.water.grid)
				elseif self.class ~= obj.class then --constr grid vs real grid, acknowledge conn
					ConnectGrids(self.water, obj.water)
				end
			end
		end
	end
end


local SetMode_GridConstructionController = GridConstructionController.SetMode
function GridConstructionController:SetMode(mode)
	if mode == "oil_grid" then
		SetMode_GridConstructionController(self, "life_support_grid")
		self.oil = true
		self.skin_name = "Chrome"
	else
		SetMode_GridConstructionController(self, mode)
		self.oil = false
		self.skin_name = "Default"
	end
end

local CanConstructLine_GridConstructionController = GridConstructionController.CanConstructLine
function GridConstructionController:CanConstructLine(...)
	if self.mode == "life_support_grid" then
		if self.oil then
			self.skin_name = "Chrome"
		else
			self.skin_name = "Default"
		end
	end
	return CanConstructLine_GridConstructionController(self, ...)
end

local ConstructLine_GridConstructionController = GridConstructionController.ConstructLine
function GridConstructionController:ConstructLine(...)
	if self.mode == "life_support_grid" then
		if self.oil then
			self.skin_name = "Chrome"
		else
			self.skin_name = "Default"
		end
	end
	return ConstructLine_GridConstructionController(self, ...)
end

local ChangeElementSkin_SupplyGridFragment = SupplyGridFragment.ChangeElementSkin
function SupplyGridFragment:ChangeElementSkin(new_skin, change_building_ents, propagate)
end

local UpdateShortConstructionStatus_GridConstructionController = GridConstructionController.UpdateShortConstructionStatus
function GridConstructionController:UpdateShortConstructionStatus(last_data)
	local vanilla_HexGetPipe = HexGetPipe
	local controller = self
	HexGetPipe = function (q,r)
		local pipe = vanilla_HexGetPipe(q,r)
		if pipe and controller.oil and not pipe.oil then
			--print("a")
			return false
		end
		if pipe and not controller.oil and pipe.oil then
			--print("b")
			return false
		end
		return pipe
	end
	UpdateShortConstructionStatus_GridConstructionController(self, last_data)
	HexGetPipe = vanilla_HexGetPipe
end

  
const.BreakDrainWastewaterMin = 5000
const.BreakDrainWastewaterMax = 15000

local Break_BreakableSupplyGridElement = BreakableSupplyGridElement.Break
function BreakableSupplyGridElement:Break(...)
	assert(IsValid(self))
	if not self:CanBreak() then return end
	if not self.oil then
		return Break_BreakableSupplyGridElement(self,...)
	end
	local element = self[self.supply_resource]
	local grid = element.grid
	grid:RemoveElement(element)
	element.variable_consumption = true --consume as much as available
	if self.supply_resource == "electricity" then
		local consumption = self:Random(const.BreakDrainPowerMax - const.BreakDrainPowerMin) + const.BreakDrainPowerMin
		consumption = MulDivRound(consumption, const.BreakDrainModifierPct, 100)
		element.consumption = consumption
	else
		self.hydrocarbon = NewSupplyGridConsumer(self, true)
		self.hydrocarbon.is_cable_or_pipe = true
		local hydrocarbon_consumption = self:Random(const.BreakDrainWastewaterMax - const.BreakDrainWastewaterMin) + const.BreakDrainOxygenMin
		hydrocarbon_consumption = MulDivRound(hydrocarbon_consumption, const.BreakDrainModifierPct, 100)
		self.hydrocarbon:SetConsumption(hydrocarbon_consumption)
	end
	grid:AddElement(element)
	self:InternalCreateResourceRequests()
	self.repair_resource_request:AddAmount(1 * const.ResourceScale)
	self.auto_connect = true
	self:ConnectToCommandCenters()
	self:Presentation(true)
end

local Repair_BreakableSupplyGridElement = BreakableSupplyGridElement.Repair
function BreakableSupplyGridElement:Repair(...)
	if not IsValid(self) or self.auto_connect == false then return end
	if not self.oil then
		return Repair_BreakableSupplyGridElement(self,...)
	end
	local element = self[self.supply_resource]
	local grid = element.grid
	grid:RemoveElement(element)
	element.variable_consumption = false
	element.consumption = false
	self.hydrocarbon = false
	grid:AddElement(element)
	self.auto_connect = false
	self:DisconnectFromCommandCenters()
	self:InternalDestroyResourceRequests()
	self:Presentation()
	Msg("Repaired", self)
	if SelectedObj == self then
		SelectObj(false)
	end
	if self:IsPinned() then
		self:TogglePin()
	end
end

function HandleLeakDetectedNotif()	
	local pipe_leaks = g_BrokenSupplyGridElements.water
	local cable_faults = g_BrokenSupplyGridElements.electricity
	if #pipe_leaks + #cable_faults == 0 then
		RemoveOnScreenNotification("LeakDetected")
	elseif g_NewLeak or IsOnScreenNotificationShown("LeakDetected") then
		g_NewLeak = false
		local air_lost = 0
		local water_lost = 0
		local hydrocarbon_lost = 0
		for i = 1, #pipe_leaks do
			if pipe_leaks[i].air and pipe_leaks[i].air.current_consumption then
				air_lost = air_lost + pipe_leaks[i].air.current_consumption
			end
			if pipe_leaks[i].water and pipe_leaks[i].water.current_consumption then
				water_lost = water_lost + pipe_leaks[i].water.current_consumption
			end
			if pipe_leaks[i].hydrocarbon and pipe_leaks[i].hydrocarbon.current_consumption then
				hydrocarbon_lost = water_lost + pipe_leaks[i].hydrocarbon.current_consumption
			end
		end
		local power_lost = 0
		for i = 1, #cable_faults do
			if cable_faults[i].electricity and cable_faults[i].electricity.current_consumption then
				power_lost = power_lost + cable_faults[i].electricity.current_consumption
			end
		end
		local displayed_in_notif = table.iappend(table.copy(pipe_leaks), cable_faults)
		local text
		if #cable_faults > 0 then
			if #pipe_leaks > 0 then
				text = T{"<power(power)> <air(air)> <water(water)> <hydrocarbon(hydrocarbon)>", power = power_lost, air = air_lost, water = water_lost, hydrocarbon = hydrocarbon_lost}
			else
				text = T{10982, "<power(power)>", power = power_lost}
			end
		elseif #pipe_leaks > 0 then
			text = T{"<air(air)> <water(water)> <hydrocarbon(hydrocarbon)>", air = air_lost, water = water_lost, hydrocarbon = hydrocarbon_lost}
		end
		local rollover = T{10984, "Cable faults: <cables><newline>Pipe leaks: <pipes>", cables = #cable_faults, pipes = #pipe_leaks}
		AddOnScreenNotification("LeakDetected", nil, { leaks = text, rollover_title = T(522588249261, "Leak Detected"), rollover_text = rollover }, displayed_in_notif)
	end
end