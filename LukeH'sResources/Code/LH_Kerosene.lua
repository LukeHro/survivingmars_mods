local translationIdBase = 1107010300

Resources["Kerosene"] = {name = "Kerosene",
	display_name = T{translationIdBase+5, "Kerosene"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_hydrocarbon.tga", 
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_HydrocarbonContainer", 
	description = T{translationIdBase+6, "Kerosene"}
}

table.insert_unique(ResourceDescription, Resources["Kerosene"])
table.insert_unique(AllResourcesList, "Hydrocarbon")
