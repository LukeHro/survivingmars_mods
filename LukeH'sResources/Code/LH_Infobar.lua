local translationIdBase = 1107012600

function OnMsg.ClassesBuilt()
    local parent = XTemplates.Infobar[1][3][1]
    local idx = table.find(parent,"Id", "idOilSupport")
    if idx then
      table.remove(parent,idx)
    end 
    local idx = table.find(parent,"Id", "idLifeSupport")
    for i = #parent,idx+1,-1 do
      parent[i+1] = parent[i]
    end  
    parent[idx+1] = PlaceObj("XTemplateWindow", {
        "__class", "XButton",
        "Id", "idOilSupport",
        "RolloverTemplate", "Rollover",
        "RolloverAnchor", "bottom",
        "RolloverText", T(translationIdBase+1, "<HydrocarbonGridRollover>"),
        "RolloverTitle", T(translationIdBase+2, "Hydrocarbon"),
        'RolloverHint', T(11684, "<left_click> Toggle Display"),
        'RolloverHintGamepad', T(11685, "<ButtonA> Toggle Display<newline><DPad> Navigate <DPadDown> Close"),  
        "HAlign", "center",
        "BorderColor", RGBA(0, 0, 0, 0),
        "Background", RGBA(0, 0, 0, 0),
        "MouseCursor", "UI/Cursors/Rollover.tga",
        "RelativeFocusOrder", "next-in-line",
        "FXMouseIn", "UIButtonMouseIn",
        "FXPress", "BuildMenuButtonClick",
        "FocusedBorderColor", RGBA(0, 0, 0, 0),
        "FocusedBackground", RGBA(0, 0, 0, 0),
        "DisabledBorderColor", RGBA(0, 0, 0, 0),
        'OnPress', function (self, gamepad)
            ToggleHydrocarbonOverlay()
        end,
        "RolloverBackground", RGBA(0, 0, 0, 0),
        "PressedBackground", RGBA(0, 0, 0, 0)
    }, {
        PlaceObj("XTemplateWindow", {
            "__class", "XImage",
            "Id", "idRollover",
            "Dock", "box",
            "Visible", false,
            "Image", "UI/Common/row_shine_2.tga",
            "ImageFit", "stretch"
        }),
        PlaceObj("XTemplateWindow", {
            "__class", "XText",
            "Id", "idHydrocarbonText",
            "Padding", box(3, 4, 3, 4),
            "TextStyle", "Infobar",
            "Translate", true,
            "Text", T(translationIdBase+3, "<HydrocarbonText>")
        })
    })
    
    local idBasic = table.find(parent,"Id", "idBasicResources")
    local food = false
    for i=1,#parent[idBasic][2] do
        if parent[idBasic][2][i] and parent[idBasic][2][i][2] and (parent[idBasic][2][i][2].Id=="idFood") then
            food = parent[idBasic][2][i]            
            table.remove(parent[idBasic][2],i)
        end
    end
    if food then
        parent[idBasic][2][#parent[idBasic][2]+1] = PlaceObj("XTemplateWindow", {
            "__class", "XButton",
            "RolloverTemplate", "Rollover",
            "RolloverAnchor", "bottom",
            "RolloverText", T(translationIdBase+4, "<CrystalsRollover>"),
            "RolloverTitle", T(translationIdBase+5, "Crystals"),
            "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
            "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
            "HAlign", "center",
            "BorderColor", RGBA(0, 0, 0, 0),
            "Background", RGBA(0, 0, 0, 0),
            "MouseCursor", "UI/Cursors/Rollover.tga",
            "RelativeFocusOrder", "next-in-line",
            "FXMouseIn", "UIButtonMouseIn",
            "FXPress", "BuildMenuButtonClick",
            "FocusedBorderColor", RGBA(0, 0, 0, 0),
            "FocusedBackground", RGBA(0, 0, 0, 0),
            "DisabledBorderColor", RGBA(0, 0, 0, 0),
            "OnPress", function(self, gamepad)
                self.context:CycleResources("Crystals")
            end,
            "RolloverBackground", RGBA(0, 0, 0, 0),
            "PressedBackground", RGBA(0, 0, 0, 0)
        }, {
            PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idRollover",
                "Dock", "box",
                "Visible", false,
                "Image", "UI/Common/row_shine_2.tga",
                "ImageFit", "stretch"
            }),
            PlaceObj("XTemplateWindow", {
                "__class", "XText",
                "Id", "idCrystals",
                "Padding", box(3, 4, 3, 4),
                "TextStyle", "Infobar",
                "Translate", true,
                "Text", T(translationIdBase+6, "<CrystalsText>")
            })
        })
        parent[idBasic][2][#parent[idBasic][2]+1] = PlaceObj("XTemplateWindow", {
            "__class", "XButton",
            "RolloverTemplate", "Rollover",
            "RolloverAnchor", "bottom",
            "RolloverText", T(translationIdBase+7, "<RadioactiveRollover>"),
            "RolloverTitle", T(translationIdBase+8, "Radioactive Materials"),
            "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
            "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
            "HAlign", "center",
            "BorderColor", RGBA(0, 0, 0, 0),
            "Background", RGBA(0, 0, 0, 0),
            "MouseCursor", "UI/Cursors/Rollover.tga",
            "RelativeFocusOrder", "next-in-line",
            "FXMouseIn", "UIButtonMouseIn",
            "FXPress", "BuildMenuButtonClick",
            "FocusedBorderColor", RGBA(0, 0, 0, 0),
            "FocusedBackground", RGBA(0, 0, 0, 0),
            "DisabledBorderColor", RGBA(0, 0, 0, 0),
            "OnPress", function(self, gamepad)
                self.context:CycleResources("Radioactive")
            end,
            "RolloverBackground", RGBA(0, 0, 0, 0),
            "PressedBackground", RGBA(0, 0, 0, 0)
        }, {
            PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idRollover",
                "Dock", "box",
                "Visible", false,
                "Image", "UI/Common/row_shine_2.tga",
                "ImageFit", "stretch"
            }),
            PlaceObj("XTemplateWindow", {
                "__class", "XText",
                "Id", "idRadioactive",
                "Padding", box(3, 4, 3, 4),
                "TextStyle", "Infobar",
                "Translate", true,
                "Text", T(translationIdBase+9, "<RadioactiveText>")
            })
        })
    
        local idAdvanced = table.find(parent,"Id", "idAdvancedResources")
        for i = #parent,idAdvanced+1,-1 do
          parent[i+1] = parent[i]
        end
        parent[idAdvanced+1]=PlaceObj('XTemplateWindow', {
                '__class', "XContextControl",
                'Id', "idSupportResources",
                'MinWidth', 250,
                'RolloverOnFocus', true,
            }, {
                PlaceObj('XTemplateFunc', {
                    'name', "OnSetFocus",
                    'func', function (self, ...)
                        XCreateRolloverWindow(self, true)
                        XContextWindow.OnSetFocus(self, ...)
                    end,
                }),
                PlaceObj('XTemplateWindow', {
                    'comment', "centers the texts",
                    'HAlign', "center",
                    'LayoutMethod', "HList",
                })
            })
        parent[idAdvanced+1][2][1] = food
        parent[idAdvanced+1][2][2] = PlaceObj("XTemplateWindow", {
            "__class", "XButton",
            "RolloverTemplate", "Rollover",
            "RolloverAnchor", "bottom",
            "RolloverText", T(translationIdBase+10, "<MeatRollover>"),
            "RolloverTitle", T(translationIdBase+11, "Meat"),
            "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
            "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
            "HAlign", "center",
            "BorderColor", RGBA(0, 0, 0, 0),
            "Background", RGBA(0, 0, 0, 0),
            "MouseCursor", "UI/Cursors/Rollover.tga",
            "RelativeFocusOrder", "next-in-line",
            "FXMouseIn", "UIButtonMouseIn",
            "FXPress", "BuildMenuButtonClick",
            "FocusedBorderColor", RGBA(0, 0, 0, 0),
            "FocusedBackground", RGBA(0, 0, 0, 0),
            "DisabledBorderColor", RGBA(0, 0, 0, 0),
            "OnPress", function(self, gamepad)
                self.context:CycleResources("Meat")
            end,
            "RolloverBackground", RGBA(0, 0, 0, 0),
            "PressedBackground", RGBA(0, 0, 0, 0)
        }, {
            PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idRollover",
                "Dock", "box",
                "Visible", false,
                "Image", "UI/Common/row_shine_2.tga",
                "ImageFit", "stretch"
            }),
            PlaceObj("XTemplateWindow", {
                "__class", "XText",
                "Id", "idMeat",
                "Padding", box(3, 4, 3, 4),
                "TextStyle", "Infobar",
                "Translate", true,
                "Text", T(translationIdBase+12, "<MeatText>")
            })
        })
        parent[idAdvanced+1][2][3] = PlaceObj("XTemplateWindow", {
            "__class", "XButton",
            "RolloverTemplate", "Rollover",
            "RolloverAnchor", "bottom",
            "RolloverText", T(translationIdBase+13, "<BoozeRollover>"),
            "RolloverTitle", T(translationIdBase+14, "Booze"),
            "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
            "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
            "HAlign", "center",
            "BorderColor", RGBA(0, 0, 0, 0),
            "Background", RGBA(0, 0, 0, 0),
            "MouseCursor", "UI/Cursors/Rollover.tga",
            "RelativeFocusOrder", "next-in-line",
            "FXMouseIn", "UIButtonMouseIn",
            "FXPress", "BuildMenuButtonClick",
            "FocusedBorderColor", RGBA(0, 0, 0, 0),
            "FocusedBackground", RGBA(0, 0, 0, 0),
            "DisabledBorderColor", RGBA(0, 0, 0, 0),
            "OnPress", function(self, gamepad)
                self.context:CycleResources("Booze")
            end,
            "RolloverBackground", RGBA(0, 0, 0, 0),
            "PressedBackground", RGBA(0, 0, 0, 0)
        }, {
            PlaceObj("XTemplateWindow", {
                "__class", "XImage",
                "Id", "idRollover",
                "Dock", "box",
                "Visible", false,
                "Image", "UI/Common/row_shine_2.tga",
                "ImageFit", "stretch"
            }),
            PlaceObj("XTemplateWindow", {
                "__class", "XText",
                "Id", "idBooze",
                "Padding", box(3, 4, 3, 4),
                "TextStyle", "Infobar",
                "Translate", true,
                "Text", T(translationIdBase+15, "<BoozeText>")
            })
        })
    end
end
