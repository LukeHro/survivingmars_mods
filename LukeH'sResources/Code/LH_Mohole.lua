local translationIdBase = 1107012400

function OnMsg.ClassesPostprocess()
  Presets.TechPreset["Robotics"].ProjectMohole.description=T(translationIdBase+1, "Wonder: <em>Mohole Mine</em> (<buildinginfo('MoholeMine')>) - Extracts Metals, Rare Metals, Crystals, Radioactive Materials and Waste Rock without the need of a deposit, while heating the surrounding area.\n\n"..
    "<grey>Moholes are shafts that dig exceptionally deep in the Martian crust, releasing heat as part of a potential terraforming effort and, as a bonus, mining ore.</grey>")
  Presets.BuildingTemplate["Wonders"].MoholeMine.description=T(translationIdBase+2, "Mining deep into the crust of Mars, the Mohole Mine produces Metals, Rare Metals, Crystals Radioactive Materials and Waste Rock, while heating the surrounding area.")
  Presets.BuildingTemplate["Wonders"].MoholeMine.upgrade1_description=T(translationIdBase+3, "+<upgrade1_mul_value_1>% Metals Production; +<upgrade1_mul_value_2>% Rare Metals Production; +<upgrade1_mul_value_2>% Crystals Production;  +<upgrade1_mul_value_2>% Radioactive Materials Production; +<power(upgrade1_add_value_3)> Consumption.")
  Presets.BuildingTemplate["Wonders"].MoholeMine.upgrade2_description=T(translationIdBase+4, "+<upgrade2_mul_value_1>% Metals Production; +<upgrade2_mul_value_2>% Rare Metals Production; +<upgrade2_mul_value_2>% Crystals Production;  +<upgrade1_mul_value_2>% Radioactive Materials Production; +<power(upgrade2_add_value_3)> Consumption.")
  Presets.BuildingTemplate["Wonders"].MoholeMine.upgrade3_description=T(translationIdBase+5, "+<upgrade3_mul_value_1>% Metals Production; +<upgrade3_mul_value_2>% Rare Metals Production; +<upgrade3_mul_value_2>% Crystals Production;  +<upgrade1_mul_value_2>% Radioactive Materials Production; +<power(upgrade3_add_value_3)> Consumption.")
end

function MoholeMine:GameInit( ... )
  self.max_resources_produced = 5
  self.resource_produced4="Crystals"
  self.max_storage4=50000
  self.production_per_day4=MulDivRound(self.producers.PreciousMetals.production_per_day,3,4)
  self.stockpile_class4='ResourceStockpile'
  self.stockpile_spots4=self.stockpile_spots1
  self.additional_stockpile_params4=self.additional_stockpile_params1
  self:AddProducer(4)
  self.resource_produced5="Radioactive"
  self.max_storage5=50000
  self.production_per_day5=MulDivRound(self.producers.PreciousMetals.production_per_day,2,4)
  self.stockpile_class5='ResourceStockpile'
  self.stockpile_spots5=self.stockpile_spots1
  self.additional_stockpile_params5=self.additional_stockpile_params1
  self:AddProducer(5)
  CreateGameTimeThread(function()
    Sleep(100)
    self:GetAttaches("WasteRockStockpile")[1]:SetAttachOffset(2030,-530,0)
    self:GetAttaches("ResourceStockpile")[1]:SetAttachOffset(700,0,0)
    self:GetAttaches("ResourceStockpile")[2]:SetAttachOffset(-3350,40,0)
  end)
end

function MoholeMine:OnModifiableValueChanged(prop, old_value, new_value)
  ResourceProducer.OnModifiableValueChanged(self, prop, old_value, new_value)
  if prop == "production_per_day2" then
    self.producers.Crystals:SetBase("production_per_day", MulDivRound(new_value,3,4))
    self.producers.Radioactive:SetBase("production_per_day", MulDivRound(new_value,2,4))
  end
end