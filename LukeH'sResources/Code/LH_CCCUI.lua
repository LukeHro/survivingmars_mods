local translationIdBase = 1107012700

function OnMsg.ClassesBuilt()
    local idx = table.find(XTemplates.CommandCenterCategories[1][4][2][4],"comment", "food")
    if idx then
        table.remove(XTemplates.CommandCenterCategories[1][4][2][4],idx)
    end 
    local idx = table.find(XTemplates.CommandCenterCategories[1][4][2],"Id", "idSupportResources")
    if idx then
        table.remove(XTemplates.CommandCenterCategories[1][4][2],idx)
    end 
    XTemplates.CommandCenterCategories[1][4][2][#XTemplates.CommandCenterCategories[1][4][2]+1] = PlaceObj('XTemplateTemplate', {
                    '__template', "CommandCenterStatsBox",
                    'Id', "idSupportResources",
                    'Margins', box(0, -12, 0, 0),
                    'Padding', box(42, 12, 42, 32),
                    'HandleKeyboard', false,
                    'HandleMouse', false,
                    'Title', T(translationIdBase+13, "LIFE SUPPORT RESOURCES"),
                }, {PlaceObj('XTemplateTemplate', {
                        'comment', "food",
                        '__template', "CommandCenterStatsRow",
                        'Name', T(281038741589, "Food"),
                        'Value', T(313049198339, "<food(AvailableFood)>"),                    
                }), PlaceObj('XTemplateTemplate', {
                        'comment', "meat",
                        '__template', "CommandCenterStatsRow",
                        'Name', T(translationIdBase+14, "Meat"),
                        'Value', T(translationIdBase+15, "<meat(AvailableMeat)>"),
                }), PlaceObj('XTemplateTemplate', {
                        'comment', "booze",
                        '__template', "CommandCenterStatsRow",
                        'Name', T(translationIdBase+16, "Booze"),
                        'Value', T(translationIdBase+17, "<booze(AvailableBooze)>"),
                })
            })

	local idx = table.find(XTemplates.CommandCenterCategories[1][4][2][2],"Hydrocarbon", true)
	if idx then
		table.remove(XTemplates.CommandCenterCategories[1][4][2][2],idx)
	end 
	for i = #XTemplates.CommandCenterCategories[1][4][2][2],4,-1 do
		XTemplates.CommandCenterCategories[1][4][2][2][i+1] = XTemplates.CommandCenterCategories[1][4][2][2][i]
	end
	XTemplates.CommandCenterCategories[1][4][2][2][4] = PlaceObj('XTemplateTemplate', {
                        'Hydrocarbon', true,
                        "comment", "hydrocarbon",
                        '__template', "CommandCenterStatsRow",
                        'Name', T(translationIdBase+3, "Hydrocarbon"),
                        'Value', T(translationIdBase+4, "<hydrocarbon(TotalStoredHydrocarbon)>"),
                    }, {
                        PlaceObj('XTemplateWindow', {
                            'comment', "delta",
                            '__class', "XText",
                            'Dock', "right",
                            'MinWidth', 100,
                            'MaxWidth', 100,
                            'TextStyle', "CCCStatsValue",
                            'ContextUpdateOnOpen', true,
                            'OnContextUpdate', function (self, context, ...)
                                local delta = context:GetHydrocarbonNumber()
                                if delta < 0 then
                                    self:SetText(T(translationIdBase+5, "<red><resource(HydrocarbonNumber)></red>"))
                                elseif delta > 0 then
                                    self:SetText(T(translationIdBase+6, "+<resource(HydrocarbonNumber)>"))
                                else
                                    self:SetText(T(translationIdBase+7, "<resource(HydrocarbonNumber)>")) --zero
                                end
                            end,
                            'Translate', true,
                            'Text', T(translationIdBase+7, "<resource(HydrocarbonNumber)>"),
                            'TextHAlign', "right",
                        }),
                        })
	local idx = table.find(XTemplates.CommandCenterCategories[1][4][2][4],"Crystals", true)
	if idx then
		table.remove(XTemplates.CommandCenterCategories[1][4][2][4],idx)
	end 
	for i = #XTemplates.CommandCenterCategories[1][4][2][4],5,-1 do
		XTemplates.CommandCenterCategories[1][4][2][4][i+1] = XTemplates.CommandCenterCategories[1][4][2][4][i]
	end
	XTemplates.CommandCenterCategories[1][4][2][4][5] = PlaceObj("XTemplateTemplate", {
			'Crystals', true,
            "comment", "crystals",
            "__template", "CommandCenterStatsRow",
            "Name", T(translationIdBase+1, "Crystals"),
            "Value", T(translationIdBase+2, "<crystals(AvailableCrystals)>")
          })
    local idx = table.find(XTemplates.CommandCenterCategories[1][4][2][4],"Radioactive", true)
    if idx then
        table.remove(XTemplates.CommandCenterCategories[1][4][2][4],idx)
    end
    for i = #XTemplates.CommandCenterCategories[1][4][2][4],6,-1 do
        XTemplates.CommandCenterCategories[1][4][2][4][i+1] = XTemplates.CommandCenterCategories[1][4][2][4][i]
    end
    XTemplates.CommandCenterCategories[1][4][2][4][6] = PlaceObj("XTemplateTemplate", {
        'Radioactive', true,
        "comment", "radioactive",
        "__template", "CommandCenterStatsRow",
        "Name", T(translationIdBase+3, "Radioactive"),
        "Value", T(translationIdBase+4, "<radioactive(AvailableRadioactive)>")
    })

end

local GetColonyStatsButtons_City = City.GetColonyStatsButtons
function City:GetColonyStatsButtons()
    table.remove_value(StockpileResourceList, "Crystals")
    table.remove_value(StockpileResourceList, "Radioactive")
    table.remove_value(StockpileResourceList, "Meat")
    table.remove_value(StockpileResourceList, "Booze")
    local t = GetColonyStatsButtons_City(self)
    table.insert_unique(StockpileResourceList, "Crystals")
    table.insert_unique(StockpileResourceList, "Radioactive")
    table.insert_unique(StockpileResourceList, "Meat")
    table.insert_unique(StockpileResourceList, "Booze")

    self.ts_resources["Crystals"] = self.ts_resources["Crystals"] or  {
        stockpile = TimeSeries:new(),
        produced = TimeSeries:new(),
        consumed = TimeSeries:new()
    }
    for i = #t, 11, -1 do
        t[i+1] = t[i]
    end
    local resource_name = FormatResourceName("Crystals")
    t[11] = {
        button_caption = T({
            9647,
            "<resource>",
            resource = resource_name
        }),
        button_text = T({
            9648,
            "Show historical data for <resource> - stockpiled <resource>, production and consumption per Sol.",
            resource = resource_name
        }),
        on_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/crystals_on.tga",
        off_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/crystals_off.tga",
        {
            caption = function()
                return T({
                    8977,
                    "<graph_right>Stockpiled <resource></graph_right> <em>(<amount>)</em>",
                    resource = resource_name,
                    amount = ResourceOverviewObj["GetAvailableCrystals"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Crystals"].stockpile,
                scale = const.ResourceScale
            }
        },
        {
            caption = function()
                return T({
                    8979,
                    "<graph_left>Produced</graph_left> <em>(<produced>)</em> and <graph_right>Consumed</graph_right> <em>(<consumed>)</em> (per Sol)",
                    produced = ResourceOverviewObj["GetCrystalsProducedYesterday"](ResourceOverviewObj) / const.ResourceScale,
                    consumed = ResourceOverviewObj["GetCrystalsConsumedByConsumptionYesterday"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Crystals"].produced,
                self.ts_resources["Crystals"].consumed,
                scale = const.ResourceScale
            }
        },
        margin_right = 11 % 4 == 0 and 11 ~= #StockpileResourceList and 40 or nil,
        Crystals = "Crystals"
    }

    self.ts_resources_grid["hydrocarbon"] = self.ts_resources_grid["hydrocarbon"] or   {
            stored = TimeSeries:new(),
            production = TimeSeries:new(),
            consumption = TimeSeries:new(),
        }
    for i = #t, 12, -1 do
        t[i+1] = t[i]
    end
    local resource_name = FormatResourceName("Hydrocarbon")
    t[12] = {
        button_caption = T(translationIdBase+3, "Hydrocarbon"),
        button_text = T(translationIdBase+8, "Show historical data for Hydrocarbon - stored Hydrocarbon, average production and demand per hour."),
        on_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/hydrocarbon_on.tga",
        off_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/hydrocarbon_off.tga",
        {
            caption = function()
                return T{translationIdBase+9, "<graph_right>Stored Hydrocarbon</graph_right> <em>(<hydrocarbon>)</em>",
                    hydrocarbon = FormatResourceValueMaxResource(ResourceOverviewObj, ResourceOverviewObj:GetTotalStoredHydrocarbon()),
                }
            end,
            data = { self.ts_resources_grid.hydrocarbon.stored, scale = const.ResourceScale, }
        },
        {
            caption = function()
                return T{translationIdBase+10, "<graph_left>Produced</graph_left> <em>(<produced>)</em> and <graph_right>Demanded</graph_right> <em>(<demand>)</em> Hydrocarbon (average per hour)",
                    produced = FormatResourceValueMaxResource(ResourceOverviewObj,ResourceOverviewObj:GetTotalProducedHydrocarbon()),
                    demand = FormatResourceValueMaxResource(ResourceOverviewObj,ResourceOverviewObj:GetTotalRequiredHydrocarbon()),
                }
            end,
            data = { self.ts_resources_grid.hydrocarbon.production, self.ts_resources_grid.hydrocarbon.consumption, scale = const.ResourceScale}
        },
        id = "hydrocarbon",
    }

    self.ts_resources["Radioactive"] = self.ts_resources["Radioactive"] or  {
        stockpile = TimeSeries:new(),
        produced = TimeSeries:new(),
        consumed = TimeSeries:new()
    }
    for i = #t, 13, -1 do
        t[i+1] = t[i]
    end
    local resource_name = FormatResourceName("Radioactive")
    t[13] = {
        button_caption = T({
            9647,
            "<resource>",
            resource = resource_name
        }),
        button_text = T({
            9648,
            "Show historical data for <resource> - stockpiled <resource>, production and consumption per Sol.",
            resource = resource_name
        }),
        on_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/radioactive_on.tga",
        off_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/radioactive_off.tga",
        {
            caption = function()
                return T({
                    8977,
                    "<graph_right>Stockpiled <resource></graph_right> <em>(<amount>)</em>",
                    resource = resource_name,
                    amount = ResourceOverviewObj["GetAvailableRadioactive"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Radioactive"].stockpile,
                scale = const.ResourceScale
            }
        },
        {
            caption = function()
                return T({
                    8979,
                    "<graph_left>Produced</graph_left> <em>(<produced>)</em> and <graph_right>Consumed</graph_right> <em>(<consumed>)</em> (per Sol)",
                    produced = ResourceOverviewObj["GetRadioactiveProducedYesterday"](ResourceOverviewObj) / const.ResourceScale,
                    consumed = ResourceOverviewObj["GetRadioactiveConsumedByConsumptionYesterday"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Radioactive"].produced,
                self.ts_resources["Radioactive"].consumed,
                scale = const.ResourceScale
            }
        },
        margin_right = 11 % 4 == 0 and 11 ~= #StockpileResourceList and 40 or nil,
        Radioactive = "Radioactive"
    }

    for i = #t, 14, -1 do
        t[i+1] = t[i]
    end
    local resource_name = FormatResourceName("Meat")
    t[14] = {
        button_caption = T({
            9647,
            "<resource>",
            resource = resource_name
        }),
        button_text = T({
            9648,
            "Show historical data for <resource> - stockpiled <resource>, production and consumption per Sol.",
            resource = resource_name
        }),
        on_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/meat_on.tga",
        off_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/meat_off.tga",
        {
            caption = function()
                return T({
                    8977,
                    "<graph_right>Stockpiled <resource></graph_right> <em>(<amount>)</em>",
                    resource = resource_name,
                    amount = ResourceOverviewObj["GetAvailableMeat"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Meat"].stockpile,
                scale = const.ResourceScale
            }
        },
        {
            caption = function()
                return T({
                    8979,
                    "<graph_left>Produced</graph_left> <em>(<produced>)</em> and <graph_right>Consumed</graph_right> <em>(<consumed>)</em> (per Sol)",
                    produced = ResourceOverviewObj["GetMeatProducedYesterday"](ResourceOverviewObj) / const.ResourceScale,
                    consumed = ResourceOverviewObj["GetMeatConsumedByConsumptionYesterday"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Meat"].produced,
                self.ts_resources["Meat"].consumed,
                scale = const.ResourceScale
            }
        },
        margin_right = 11 % 4 == 0 and 11 ~= #StockpileResourceList and 40 or nil,
        Meat = "Meat"
    }

    for i = #t, 13, -1 do
        t[i+1] = t[i]
    end
    local resource_name = FormatResourceName("Booze")
    t[13] = {
        button_caption = T({
            9647,
            "<resource>",
            resource = resource_name
        }),
        button_text = T({
            9648,
            "Show historical data for <resource> - stockpiled <resource>, production and consumption per Sol.",
            resource = resource_name
        }),
        on_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/booze_on.tga",
        off_icon = CurrentModPath.."UI/Icons/ColonyControlCenter/booze_off.tga",
        {
            caption = function()
                return T({
                    8977,
                    "<graph_right>Stockpiled <resource></graph_right> <em>(<amount>)</em>",
                    resource = resource_name,
                    amount = ResourceOverviewObj["GetAvailableBooze"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Booze"].stockpile,
                scale = const.ResourceScale
            }
        },
        {
            caption = function()
                return T({
                    8979,
                    "<graph_left>Produced</graph_left> <em>(<produced>)</em> and <graph_right>Consumed</graph_right> <em>(<consumed>)</em> (per Sol)",
                    produced = ResourceOverviewObj["GetBoozeProducedYesterday"](ResourceOverviewObj) / const.ResourceScale,
                    consumed = ResourceOverviewObj["GetBoozeConsumedByConsumptionYesterday"](ResourceOverviewObj) / const.ResourceScale
                })
            end,
            data = {
                self.ts_resources["Booze"].produced,
                self.ts_resources["Booze"].consumed,
                scale = const.ResourceScale
            }
        },
        margin_right = 11 % 4 == 0 and 11 ~= #StockpileResourceList and 40 or nil,
        Booze = "Booze"
    }

    return t
end

local InitTimeSeries_City = City.InitTimeSeries
function City:InitTimeSeries()
    InitTimeSeries_City(self)
    self.ts_resources_grid["hydrocarbon"] = {
            stored = TimeSeries:new(),
            production = TimeSeries:new(),
            consumption = TimeSeries:new(),
        }
end

local UpdateTimeSeries_City = City.UpdateTimeSeries
function City:UpdateTimeSeries()
    UICity.ts_resources_grid["hydrocarbon"] = UICity.ts_resources_grid["hydrocarbon"] or   {
            stored = TimeSeries:new(),
            production = TimeSeries:new(),
            consumption = TimeSeries:new(),
        }
    local hydrocarbon = self.ts_resources_grid.hydrocarbon
    hydrocarbon.stored:AddValue(ResourceOverviewObj:GetTotalStoredHydrocarbon()) 
    local data = ResourceOverviewObj.data
    if (data and data.total_grid_samples or 0) > 0 then
        hydrocarbon.consumption:AddValue(data.total_hydrocarbon_consumption_sum / data.total_grid_samples)
        hydrocarbon.production:AddValue(data.total_hydrocarbon_production_sum / data.total_grid_samples)
        data.total_hydrocarbon_consumption_sum = 0
        data.total_hydrocarbon_production_sum = 0
    end
    UpdateTimeSeries_City(self)
end

function OnMsg.NewHour(hour)
    UICity.ts_resources_grid["hydrocarbon"] = UICity.ts_resources_grid["hydrocarbon"] or   {
            stored = TimeSeries:new(),
            production = TimeSeries:new(),
            consumption = TimeSeries:new(),
        }
    local data = next(ResourceOverviewObj.data) and ResourceOverviewObj.data 
    if not data then
        data = {}
        GatherResourceOverviewData(data)    
    end
    local hydrocarbon_stored = ResourceOverviewObj:GetTotalStoredHydrocarbon()
    local ts_grid = UICity.ts_resources_grid
    local ts_grid_hydrocarbon = ts_grid.hydrocarbon
    local hydrocarbon_consumption   = ts_grid_hydrocarbon.consumption:GetLastValue()
    local hydrocarbon_production    = ts_grid_hydrocarbon.production:GetLastValue()
    if const.MinHoursWaterResourceSupplyBeforeNotification*(hydrocarbon_consumption - hydrocarbon_production) > hydrocarbon_stored then
        table.insert_unique(g_InsufficientMaintenanceResources, "Hydrocarbon")
    else
        table.remove_entry(g_InsufficientMaintenanceResources,"Hydrocarbon")      
    end
end

local GatherResourceOverviewData_Vanilla = GatherResourceOverviewData
function GatherResourceOverviewData( data )
    GatherResourceOverviewData_Vanilla(data)
    local city = UICity
    local current_production = 0
    local consumption_for_overview = 0
    local current_storage = 0
    for i=1,(#city.water or 0) do
        if (city.water[i].hydrocarbon_grid) then
            current_production = (current_production or 0) + city.water[i].hydrocarbon_grid.current_production
            consumption_for_overview = (consumption_for_overview or 0) + city.water[i].hydrocarbon_grid.consumption_for_overview
            current_storage = (current_storage or 0) + city.water[i].hydrocarbon_grid.current_storage
        end
    end
    data.total_hydrocarbon_production = current_production
    data.total_hydrocarbon_demand = consumption_for_overview
    data.total_hydrocarbon_storage = current_storage
end

local ResourceOverviewThreadBody_Vanilla = ResourceOverviewThreadBody
function ResourceOverviewThreadBody( ... )
    local data = ResourceOverviewObj.data
    local samples = (data and data.total_grid_samples or 0)
    ResourceOverviewThreadBody_Vanilla(...)
    if samples ~= (data and data.total_grid_samples or 0) then
        data.total_hydrocarbon_production_sum = (data.total_hydrocarbon_production_sum or 0) + data.total_hydrocarbon_production
        data.total_hydrocarbon_consumption_sum = (data.total_hydrocarbon_consumption_sum or 0) + data.total_hydrocarbon_demand
    end
end

local GetCommandCenterTransportInfo_Vanilla = GetCommandCenterTransportInfo

function GetCommandCenterTransportInfo(self)
    if IsKindOf(self, "RCTransport") then
        local rows = {self.description}
        rows[#rows + 1] = T(9805, "<newline><center><em>Resources carried</em>")
        if self:HasMember("GetStored_Concrete") then
            rows[#rows + 1] = T(343032565187, "Concrete<right><concrete(Stored_Concrete)>")
        end
        if self:HasMember("GetStored_Metals") then
            rows[#rows + 1] = T(455677282700, "Metals<right><metals(Stored_Metals)>")
        end
        if self:HasMember("GetStored_Food") then
            rows[#rows + 1] = T(788345915802, "Food<right><food(Stored_Food)>")
        end
        if self:HasMember("GetStored_PreciousMetals") then
            rows[#rows + 1] = T(925417865592, "Rare Metals<right><preciousmetals(Stored_PreciousMetals)>")
        end
        if self:HasMember("GetStored_Crystals") then
            rows[#rows + 1] = T(translationIdBase+11, "Crystals<right><crystals(Stored_Crystals)>")
        end
        if self:HasMember("GetStored_Radioactive") then
            rows[#rows + 1] = T(translationIdBase+12, "Radioactive materials<right><radioactive(Stored_Radioactive)>")
        end
        if self:HasMember("GetStored_Polymers") then
            rows[#rows + 1] = T(157677153453, "Polymers<right><polymers(Stored_Polymers)>")
        end
        if self:HasMember("GetStored_Electronics") then
            rows[#rows + 1] = T(624861249564, "Electronics<right><electronics(Stored_Electronics)>")
        end
        if self:HasMember("GetStored_MachineParts") then
            rows[#rows + 1] = T(407728864620, "Machine Parts<right><machineparts(Stored_MachineParts)>")
        end
        if self:HasMember("GetStored_Fuel") then
            rows[#rows + 1] = T(317815331128, "Fuel<right><fuel(Stored_Fuel)>")
        end
        if UICity:IsTechResearched("MartianVegetation") and self:HasMember("GetStored_Seeds") then
            rows[#rows + 1] = T(12002, "Seeds<right><seeds(Stored_Seeds)>")
        end
        local warning = self:GetUIWarning()
        if warning then
            rows[#rows + 1] = "<center>" .. T(47, "<red>Warning</red>")
            rows[#rows + 1] = warning
        end
        return #rows > 0 and table.concat(rows, "<newline><left>") or ""
    end
    return GetCommandCenterTransportInfo_Vanilla(self)
end
