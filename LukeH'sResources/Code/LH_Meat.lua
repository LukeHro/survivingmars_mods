local translationIdBase = 1107010900

Resources["Meat"] = {name = "Meat",
	display_name = T{translationIdBase+1, "Meat"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_meat.tga", 
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_MeatContainer", 
	description = T{translationIdBase+2, "Meat for consumption."}
}

table.insert_unique(ResourceDescription, Resources["Meat"])	
table.insert_unique(StockpileResourceList, "Meat")
table.insert_unique(CargoShuttle.storable_resources, "Meat")
table.insert_unique(AllResourcesList, "Meat")
table.insert_unique(RCTransport.storable_resources,"Meat")

function _ENV:UIBreedUpdate(pasture, index)
  local herd = pasture:GetHarvestTypeInfo(index)
  local active
  if pasture.current_herd then
    active = herd and pasture.current_harvest_idx == index and true
  else
    active = herd and pasture:GetNextHarvestIndex() == index
  end
  self.idActive:SetVisible(active)
  self.idProgress:SetVisible(active)
  local name = herd and herd.display_name
  self:SetTitle(active and "" or name or T(12423, "Select new breed"))
  self.idCropProduction:SetVisible(herd)
  if herd then
    local context = self.idCropProduction.context
    local production = pasture:CalcExpectedProduction(index, herd)
    context.prod = production / 1000
    context.frac = production / 100 % 10
    context.warn = active and (production == 0 and TLookupTag("<red>") or TLookupTag("<em>")) or ""
    context.icon = const.TagLookupTable.icon_Meat
    ObjModified(context)
  end
  function self:OnActivate(context)
    OpenInfopanelItems(pasture, self, context.crop_index)
  end
  self:SetIcon(herd and herd.display_icon or "UI/Icons/Buildings/crops_empty.tga")
  self.idIcon:SetColumns(2)
  if active then
  else
  end
  self:SetRolloverTitle(T({
    7603,
    "<name>  <percent(p)>",
    name = name,
    p = pasture:GetGrowthProgress()
  }) or "")
  function self:GetRolloverText()
    local texts = {}
    if herd then
      texts[#texts + 1] = T({
        248,
        "Water consumption<right><water(number)>",
        number = herd.water_consumption
      })
      texts[#texts + 1] = T({
        12586,
        "Oxygen consumption<right><air(number)>",
        number = herd.air_consumption
      })
      texts[#texts + 1] = T({
        translationIdBase+3,
        "Optimal meat output<right><meat(number)>",
        number = herd.food * herd.herd_size
      })
      texts[#texts + 1] = T({
        12588,
        "Growth time<right><time(lifetime)>",
        lifetime = herd.lifetime
      })
    else
      texts[#texts + 1] = T(12589, "Animals vary in growth time, Water and Oxygen consumption, and produce different amounts of Meat.")
    end
    texts[#texts + 1] = T(316, "<newline>")
    if not pasture.working then
      if pasture.current_herd then
        texts[#texts + 1] = T(12590, "<em>The ranch is not working. The animals are dying</em>")
      else
        texts[#texts + 1] = T(12591, "<em>The ranch is not working. New animals won't be bred</em>")
      end
    end
    return table.concat(texts, [[

<left>]])
  end
end

local oldGetInfopanelSelectorItems = GetInfopanelSelectorItems
function GetInfopanelSelectorItems(dataset, list)
  local object = dataset.object
  if object:IsKindOf("Pasture") then
    local items = {}
    local align = "bottom"
    for _, animal in ipairs(object:GetAnimalsInfo()) do
        local desctiption_texts = {
          animal.description
        }
        desctiption_texts[#desctiption_texts + 1] = T(316, "<newline>")
        desctiption_texts[#desctiption_texts + 1] = T({
          248,
          "Water consumption<right><water(number)>",
          number = animal.water_consumption
        })
        desctiption_texts[#desctiption_texts + 1] = T({
          12586,
          "Oxygen consumption<right><air(number)>",
          number = animal.air_consumption
        })
        desctiption_texts[#desctiption_texts + 1] = T({
          translationIdBase+3,
          "Optimal meat output<right><meat(number)>",
          number = animal.food * animal.herd_size
        })
        desctiption_texts[#desctiption_texts + 1] = T({
          12588,
          "Growth time<right><time(lifetime)>",
          lifetime = animal.lifetime
        })
        table.insert(items, HexButtonInfopanel:new({
          ButtonAlign = align,
          name = animal.display_name,
          icon = animal.display_icon,
          display_name = animal.display_name,
          description = table.concat(desctiption_texts, [[

<left>]]),
          hint = object:UIGetHint(),
          enabled = true,
          lock_image = "UI/Icons/bmc_lock.tga",
          gamepad_hint = object:UIGetHintGamepad(),
          action = function(dataset, delta, controller)
            local pasture = dataset.object
            if not IsValid(pasture) then
              return
            end
            local broadcast = controller and delta < 0 or not controller and IsMassUIModifierPressed()
            pasture:SetHarvestType(animal.id, dataset.idx, broadcast)
          end
        }, list))
        align = align == "top" and "bottom" or "top"
    end
    table.insert(items, HexButtonInfopanel:new({
      ButtonAlign = align,
      name = "",
      display_name = T(6864, "None"),
      description = T(6865, "None"),
      icon = "UI/Icons/Buildings/crops_empty.tga",
      action = function(dataset, delta, controller)
        local pasture = dataset.object
        if not IsValid(pasture) then
          return
        end
        local broadcast = controller and delta < 0 or not controller and IsMassUIModifierPressed()
        pasture:SetHarvestType("None", dataset.idx, broadcast)
      end
    }, list))
    return items
  else
    return oldGetInfopanelSelectorItems(dataset, list)
  end
end

function OnMsg.ClassesPostprocess()
  if not TraitPresets.LH_WellFed then
    PlaceObj('TraitPreset', {
      auto = false,
      category = "Positive",
      description = T(translationIdBase+4, "Increased work performance for the next workshift."),
      display_name = T(translationIdBase+5, "Well fed"),
      dome_filter_only = true,
      hidden_on_start = true,
      group = "Positive",
      id = "LH_WellFed",
      incompatible = {},
      name = "LH_WellFed",  
      modify_amount = 10,
      modify_property = "performance",
      modify_target = "self",
    })
  end
  ColonistPerformanceReasons.LH_WellFed = T(translationIdBase+6, "Equilibrated meal <amount>")
end

local StopWorkCycle_Workplace = Workplace.StopWorkCycle
function Workplace:StopWorkCycle(unit, ...)
  StopWorkCycle_Workplace(self, unit, ...)
  --print("removed fed")
  unit:RemoveTrait("LH_WellFed")
end

local StopWorkCycle_TrainingBuilding = TrainingBuilding.StopWorkCycle
function TrainingBuilding:StopWorkCycle(unit, ...)
  StopWorkCycle_TrainingBuilding(self, unit, ...)
  --print("removed fed")
  unit:RemoveTrait("LH_WellFed")
end

function Pasture:InitConsumptionRequest( ... )  
  self.consumption_resource_type = "Food"
  self.consumption_max_storage = 20000
  self.consumption_amount = 3
  self.consumption_type = g_ConsumptionType.Workshop
  if IsKindOf(self, "InsidePasture") and IsDlcAvailable("shepard") then  
    self.consumption_resource_type = "Seeds" 
    self.consumption_max_storage = 10000
    self.consumption_amount = 8
  end
  HasConsumption.InitConsumptionRequest(self, ...)
  CreateGameTimeThread(function()
    Sleep(100)
    if IsKindOf(self, "InsidePasture") then  
      self:GetAttaches("ConsumptionResourceStockpile")[1]:SetAttachOffset(1000,10,0)
    else
      self:GetAttaches("ConsumptionResourceStockpile")[1]:SetAttachOffset(-1000,3700,0)
    end
  end)
end

function Pasture:OnChangeWorkshift(old, new)
  if self:DoesHaveConsumption() and self.consumption_type == g_ConsumptionType.Workshop then
    local amount_to_consume = self:GetHerdConsumption(shift)
    if amount_to_consume > self.consumption_stored_resources then
      amount_to_consume = self.consumption_stored_resources
    end
    self.consumption_stored_resources = self.consumption_stored_resources - amount_to_consume
    self.consumption_resource_request:AddAmount(amount_to_consume)
    self.city:OnConsumptionResourceConsumed(self.consumption_resource_type, amount_to_consume)
    self:UpdateVisualStockpile()
    self:UpdateRequestConnectivity()
    if not self:CanConsume(new) then
      self:AttachSign(true, "SignNoConsumptionResource")
    end
    if SelectedObj == self then
      RebuildInfopanel(self)
    end
  end

end

function Pasture:GetHerdConsumption()
  local herd = self:GetCurrentHarvestType()
  local day_consumption = MulDivRound(herd.food, const.DayDuration, herd.lifetime)  
  local scaled_consumption = MulDivRound(day_consumption, #(self.current_herd or {}), self.consumption_amount)
  --print(#(self.current_herd or {})*day_consumption," ",scaled_consumption)
  return scaled_consumption
end