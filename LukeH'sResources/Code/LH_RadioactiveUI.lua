local translationIdBase = 1107010800

const.TagLookupTable["icon_Radioactive"] = "<image "..CurrentModPath.."UI/Icons/res_radioactive.tga 1300>"
const.TagLookupTable["icon_Radioactive_small"] = "<image "..CurrentModPath.."UI/Icons/res_radioactive.tga 800>"
const.TagLookupTable["icon_Radioactive_orig"] = "<image "..CurrentModPath.."UI/Icons/res_radioactive.tga>"

function StorageDepot:Getui_Radioactive()
	return T({translationIdBase+1, "Radioactive<right><radioactive(Stored_Radioactive,MaxAmount_Radioactive)>", self})
end


function InfobarObj:GetRadioactiveText()
	local radioactive = ResourceOverviewObj:GetAvailableRadioactive() / const.ResourceScale
	return T({translationIdBase+2, "<radioactive><icon_Radioactive_orig>", radioactive = self:FmtRes(radioactive)})
end

function InfobarObj:GetRadioactiveRollover()
  return ResourceOverviewObj:GetRadioactiveRollover()
end

function ResourceOverview:GetRadioactiveRollover()
  local ret = {self:GetBasicResourcesHeading(),
    T(316, "<newline>"),
    T({translationIdBase+3, "Radioactive production<right><radioactive(RadioactiveProducedYesterday)>", self}),
    T({translationIdBase+4, "Radioactive consumption<right><radioactive(RadioactiveConsumedByConsumptionYesterday)>", self}),
    T({translationIdBase+5, "Radioactive maintenance<right><radioactive(RadioactiveConsumedByMaintenanceYesterday)>", self}),
    T({translationIdBase+6, "Upgrade construction<right><radioactive(RadioactiveUpgradeConstructionActual, RadioactiveUpgradeConstructionTotal)>", self})
  }
  return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetRadioactiveProducedYesterday"] = function(self)
	return self:GetProducedYesterday("Radioactive")
end
ResourceOverview["GetRadioactiveGatheredYesterday"] = function(self)
	return self:GetGatheredYesterday("Radioactive")
end
ResourceOverview["GetRadioactiveConsumedByConsumptionYesterday"] = function(self)
	return self:GetConsumedByConsumptionYesterday("Radioactive")
end
ResourceOverview["GetRadioactiveConsumedByMaintenanceYesterday"] = function(self)
	return self:GetEstimatedDailyMaintenance("Radioactive")
end
ResourceOverview["GetAvailableRadioactive"] = function(self)
	return self:GetAvailable("Radioactive")
end
ResourceOverview["GetRadioactiveInConstructionSitesActual"] = function(self)
	return self:GetInConstructionSites("Radioactive", "actual")
end
ResourceOverview["GetRadioactiveInConstructionSitesTotal"] = function(self)
	return self:GetInConstructionSites("Radioactive", "total")
end
ResourceOverview["GetRadioactiveUpgradeConstructionActual"] = function(self)
	return self:GetUpgradeConstruction("Radioactive", "actual")
end
ResourceOverview["GetRadioactiveUpgradeConstructionTotal"] = function(self)
	return self:GetUpgradeConstruction("Radioactive", "total")
end

function OnMsg.ClassesBuilt()        
    local idx = table.find(XTemplates.ipRover[1][20],"Id", "idRadioactive")
    if idx then
      table.remove(XTemplates.ipRover[1][20], idx)
    end
    for i = #XTemplates.ipRover[1][20],5,-1 do
        XTemplates.ipRover[1][20][i+1] = XTemplates.ipRover[1][20][i]
    end
    XTemplates.ipRover[1][20][5] = PlaceObj('XTemplateTemplate', {
        '__condition', function (parent, context) return context:HasMember("GetStored_Radioactive") end,
        'Id', "idRadioactive",
        '__template', "InfopanelText",
        'Text', T(translationIdBase+7, --[[XTemplate ipRover Text]] "Radioactive<right><radioactive(Stored_Radioactive)>"),
      })
end