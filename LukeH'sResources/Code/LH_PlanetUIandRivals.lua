
local SetUIResourceValues_LandingSiteObject = LandingSiteObject.SetUIResourceValues
function LandingSiteObject:SetUIResourceValues(...)
	SetUIResourceValues_LandingSiteObject(self,...)
	local spot = self.selected_spot
	if spot and spot.spot_type == "rival" then
		local obj = RivalAIs[spot.id]
		local resources = obj.resources
		local basic = self.dialog:ResolveId("idBasicResources")
		if basic then
			basic:SetText(T{
				"<metals(metals)>  <concrete(concrete)>  <preciousmetals(preciousmetals)>  <crystals(crystals)>  <radioactive(radioactive)>",
				metals = Max(resources.metals * const.ResourceScale, 0),
				concrete = Max(resources.concrete * const.ResourceScale, 0),        
				preciousmetals = Max(resources.raremetals * const.ResourceScale, 0),
				crystals = resources.crystals and Max(resources.crystals * const.ResourceScale, 0) or 0,
				radioactive = resources.radioactive and Max(resources.radioactive * const.ResourceScale, 0) or 0
			})
		end
		local advanced = self.dialog:ResolveId("idAdvancedResources")
		if advanced then
			advanced:SetText(T{
				"<polymers(polymers)>  <electronics(electronics)>  <machineparts(machineparts)>  <fuel(fuel)>",
				polymers = Max(resources.polymers * const.ResourceScale, 0),
				electronics = Max(resources.electronics * const.ResourceScale, 0),
				machineparts = Max(resources.machineparts * const.ResourceScale, 0),
				fuel = Max(resources.fuel * const.ResourceScale, 0)
			})
		end
		local grid = self.dialog:ResolveId("idGridResources")
		if grid then
			grid:SetText(T{
				"<power(power)>  <air(oxygen)>  <water(water)>  <hydrocarbon(hydrocarbon)>",
				power = Max(resources.power_production * const.ResourceScale, 0),
				oxygen = Max(resources.oxygen_production * const.ResourceScale, 0),
				water = Max(resources.water_production * const.ResourceScale, 0),
				hydrocarbon = resources.hydrocarbon_production and Max(resources.hydrocarbon_production * const.ResourceScale, 0) or 0
			})
		end
			local lifes = self.dialog:ResolveId("idLifeSupportResources")
			if lifes then
				lifes:SetText(T{
					"<food(food)>  <meat(meat)>  <booze(booze)>",      
					food = Max(resources.food * const.ResourceScale, 0),
					meat = resources.meat and Max(resources.meat * const.ResourceScale, 0) or 0,
					booze = resources.booze and Max(resources.booze * const.ResourceScale, 0) or 0
				})
			end
		return
	end
	local basic = self.dialog:ResolveId("idBasicResources")
	if basic then
		basic:SetText(T{
			"<metals(metals)>  <concrete(concrete)>  <preciousmetals(preciousmetals)>  <crystals(crystals)>  <radioactive(radioactive)>",
			metals = ResourceOverviewObj:GetAvailableMetals(),
			concrete = ResourceOverviewObj:GetAvailableConcrete(),
			preciousmetals = ResourceOverviewObj:GetAvailablePreciousMetals(),
			crystals = ResourceOverviewObj:GetAvailableCrystals(),
			radioactive = ResourceOverviewObj:GetAvailableRadioactive()
		})
	end
	local advanced = self.dialog:ResolveId("idAdvancedResources")
	if advanced then
		advanced:SetText(T{
			"<polymers(polymers)>  <electronics(electronics)>  <machineparts(machineparts)>  <fuel(fuel)>",
			polymers = ResourceOverviewObj:GetAvailablePolymers(),
			electronics = ResourceOverviewObj:GetAvailableElectronics(),
			machineparts = ResourceOverviewObj:GetAvailableMachineParts(),
			fuel = ResourceOverviewObj:GetAvailableFuel()
		})
	end
	local grid = self.dialog:ResolveId("idGridResources")
	if grid then
		grid:SetText(T{
			"<power(power)>  <air(oxygen)>  <water(water)>  <hydrocarbon(hydrocarbon)>",
			power = ResourceOverviewObj:GetTotalProducedPower(),
			oxygen = ResourceOverviewObj:GetTotalProducedAir(),
			water = ResourceOverviewObj:GetTotalProducedWater(),
			hydrocarbon = ResourceOverviewObj:GetTotalProducedHydrocarbon()
		})
	end
	local lifes = self.dialog:ResolveId("idLifeSupportResources")
	if lifes then
		lifes:SetText(T{
			"<food(food)>  <meat(meat)>  <booze(booze)>",      
			food = ResourceOverviewObj:GetAvailableFood(),
			meat = ResourceOverviewObj:GetAvailableMeat(),
			booze = ResourceOverviewObj:GetAvailableBooze()
		})
	end
end

function OnMsg.ClassesPostprocess()
	local idx = table.find(XTemplates.PlanetaryViewResources[1][2],"Id", "idLifeSupportResources")
	if idx then
		table.remove(XTemplates.PlanetaryViewResources[1][1], idx)
		table.remove(XTemplates.PlanetaryViewResources[1][2], idx)
	end
	local idx = table.find(XTemplates.PlanetaryViewResources[1][2],"Id", "idGridResources")
	for i = #XTemplates.PlanetaryViewResources[1][2],idx,-1 do
		XTemplates.PlanetaryViewResources[1][1][i+1] = XTemplates.PlanetaryViewResources[1][1][i]
		XTemplates.PlanetaryViewResources[1][2][i+1] = XTemplates.PlanetaryViewResources[1][2][i]
	end
	XTemplates.PlanetaryViewResources[1][1][idx] = PlaceObj("XTemplateWindow", {
			"comment", "Life Support Resources",
			"__class", "XText",
			"Id", "idLifeSupportResourceLabel",
			"Padding", box(0, 0, 0, 0),
			"HandleMouse", false,
			"TextStyle", "PGLandingPosDetails",
			"Translate", true,
			"Text", T("Life Support Resources")
		})

	XTemplates.PlanetaryViewResources[1][2][idx] = PlaceObj("XTemplateWindow", {
			"__class", "XText",
			"Id", "idLifeSupportResources",
			"Padding", box(0, 0, 0, 0),
			"HandleMouse", false,
			"TextStyle", "PGChallengeDescription",
			"Translate", true
		})
end

local SpawnRivalAI_vanilla = SpawnRivalAI
function SpawnRivalAI(...)
	local obj = SpawnRivalAI_vanilla(...)
	local resources = obj.resources
	for _, res in ipairs(Presets.DumbAIDef.Default.lh_resources.initial_resources) do
		local resource = res.resource
		resources[resource] = (resources[resource] and resources[resource] or 0)
	end
	obj:AddAIDef(Presets.DumbAIDef.Default.lh_resources)
	obj.LH_ResAIv = 1
	print("Adding LH_Resources AI")
	return obj
end

function LH_ResetAI()
	if not IsDlcAvailable("gagarin") then
		return
	end
	for k,v in pairs(RivalAIs) do
		if not v.LH_ResAIv or v.LH_ResAIv < 1 then
			v.actions = {}
			v.production_rules = {}
			v.biases = {}
			for _, res in ipairs(Presets.DumbAIDef.Default.lh_resources.initial_resources) do
				local resource = res.resource
				v.resources[resource] = (v.resources[resource] and v.resources[resource] or 0)
			end
			v:AddAIDef(Presets.DumbAIDef.MissionSponsors[k])
			v:AddAIDef(Presets.DumbAIDef.Default.default)
			v:AddAIDef(Presets.DumbAIDef.Default.lh_resources)
			print("Reseted ",k," AI")
			v.LH_ResAIv = 1
		end
	end
end

OnMsg.LoadGame = LH_ResetAI

function OnMsg.ClassesPostprocess()
	if not IsDlcAvailable("gagarin") then
		return
	end
	PlaceObj("DumbAIDef", {
		biases = {},
		group = "Default",
		id = "lh_resources",
		initial_resources = {
			PlaceObj("AIResourceAmount", { "resource", "workforce", "amount", 0 }), --vanilla issue fix
			PlaceObj("AIResourceAmount", { "resource", "meat", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "booze", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "crystals", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "radioactive", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "hydrocarbon", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "meat_production", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "booze_production", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "crystals_production", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "radioactive_production", "amount", 0 }),
			PlaceObj("AIResourceAmount", { "resource", "hydrocarbon_production", "amount", 0 })
		},
		production_rules = {
			PlaceObj("AIProductionRule", {
				"rule_id", "Resource Production and Consumption (lukeh_resources)",
				"Run", function(self, resources, ai_player)
					local res_references = {
						{ "crystals", 10000, "Crystals", 0 },
						{ "meat", 50000, "Meat", 0 },
						{ "booze", 50000, "Booze", 0 },
						{ "radioactive", 10000, "Radioactive", 20 }          
					}
					local rival_stock, player_stock, change, diff
					for _, res in pairs(res_references) do
						local r1 = res[1]
						rival_stock = Max(1000, resources[r1] * const.ResourceScale)
						player_stock = Max(1000, ResourceOverviewObj:GetAvailable(res[3])) + 1000
						change = resources[res[1] .. "_production"]
						diff = rival_stock - player_stock
						diff = diff / player_stock
						diff = diff / 100
						if rival_stock > res[2] and diff > 20 then
							change = change * (100 - Min(95, diff)) / 100
						elseif diff < -20 then
							change = change + change * -diff / 100
						end
						if rival_stock > res[2] and 0 < res[4] then
							change = change - resources.buildings * res[4] / const.ResourceScale
						end
						resources[r1] = resources[r1] + change
					end
				end
			}),
			PlaceObj("AIProductionRule", {
				"from_resource", "power",
				"mul", -50,
				"produce_resource", "radioactive",
				"rule_id", "Radioactive Consumption"
			}),
			PlaceObj("AIProductionRule", {
				"from_resource", "colonists",
				"mul", -50,
				"produce_resource", "meat",
				"rule_id", "Meat Consumption"
			}),
			PlaceObj("AIProductionRule", {
				"from_resource", "colonists",
				"mul", -50,
				"produce_resource", "booze",
				"rule_id", "Booze Consumption"
			}),
		},
		PlaceObj("AIAction", {
			"id", "increase Crystals production",
			"log_entry", T("Constructed Crystals Extractor"),
			"base_eval", 75000000,
			"delay", 360000,
			"tags", {"mining"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 15 }),
				PlaceObj("AIResourceAmount", { "resource", "machineparts", "amount", 4 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 12 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "deposits_underground", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "crystals_production", "amount", 3 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"IsAllowed", function(self, ai_player)
				return true
			end,
			"Eval", function(self, ai_player)
				if ai_player.resources.crystals < 1 then
					return 5000 * const.Scale.mil
				end
				if ai_player.resources.crystals < 10 then
					return 150 * const.Scale.mil
				elseif ai_player.resources.crystals > 250 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.crystals > 500 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Radioactive production",
			"log_entry", T("Constructed Radioactive Extractor"),
			"base_eval", 75000000,
			"delay", 360000,
			"tags", {"mining"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 15 }),
				PlaceObj("AIResourceAmount", { "resource", "machineparts", "amount", 4 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 12 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "deposits_underground", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "radioactive_production", "amount", 2 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"IsAllowed", function(self, ai_player)
				return true
			end,
			"Eval", function(self, ai_player)
				if ai_player.resources.radioactive < 1 then
					return 5000 * const.Scale.mil
				end
				if ai_player.resources.radioactive < 10 then
					return 150 * const.Scale.mil
				elseif ai_player.resources.radioactive > 250 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.radioactive > 500 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Power production from Radioactives",
			"base_eval", 50000000,
			"delay", 90000,
			"tags", {"mining", "production"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "radioactive", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "power_production", "amount", 8 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 8 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"Eval", function(self, ai_player)
				if ai_player.resources.power > 30 and ai_player.resources.buildings < 50 or ai_player.resources.radioactive < 25 and ai_player.resources.power > 10 then
					return 1
				end
				if ai_player.resources.power < 50 then
					return 150 * const.Scale.mil
				elseif ai_player.resources.power > 100 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.power > 1000 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Hydrocarbon production",
			"log_entry", T("Constructed Hydrocarbon Extractor"),
			"delay", 360000,
			"tags", {},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 4 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 2 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "hydrocarbon_production", "amount", 2 }),
				PlaceObj("AIResourceAmount", { "resource", "hydrocarbon", "amount", 2 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"IsAllowed", function(self, ai_player)
				return ai_player.resources.oxygen < 1 or ai_player.resources.metals > 200
			end,
			"Eval", function(self, ai_player)
				if ai_player.resources.hydrocarbon < 1 then
					return 150 * const.Scale.mil
				elseif ai_player.resources.hydrocarbon > 5 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.hydrocarbon > 50 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Fuel production from Hydrocarbon",
			"log_entry", T{"Constructed Fuel Refinery"},
			"base_eval", 50000000,
			"delay", 360000,
			"tags", {"production"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 4 }),
				PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 4 }),
				PlaceObj("AIResourceAmount", { "resource", "machineparts", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "hydrocarbon", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "fuel_production", "amount", 6 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"IsAllowed", function(self, ai_player)
				return ai_player.resources.machineparts > 20 or ai_player.resources.fuel_production < 1
			end,
			"Eval", function(self, ai_player)
				if ai_player.resources.fuel < 50 then
					return 150 * const.Scale.mil
				elseif ai_player.resources.fuel > 500 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.fuel > 2500 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Polymers production from Hydrocarbon",
			"log_entry", T{"Constructed Polymer Factory"},
			"base_eval", 50000000,
			"delay", 360000,
			"tags", {"production"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 10 }),
				PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "machineparts", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "hydrocarbon", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 9 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "polymers_production", "amount", 5 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"IsAllowed", function(self, ai_player)
				return ai_player.resources.machineparts > 20 or ai_player.resources.polymers_production < 1
			end,
			"Eval", function(self, ai_player)
				if ai_player.resources.polymers < 50 then
					return 150 * const.Scale.mil
				elseif ai_player.resources.polymers > 250 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.polymers > 2500 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Meat production",
			"log_entry", T("Constructed ranches"),
			"base_eval", 75000000,
			"delay", 90000,
			"tags", {"colonists"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 8 }),
				PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 6 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "meat_production", "amount", 3 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"Eval", function(self, ai_player)
				local rations = ai_player.resources.colonists / 5
				if ai_player.resources.meat < 100 then
					return 250 * const.Scale.mil
				elseif ai_player.resources.meat > 250 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.meat > 1000 then
					return 1 * const.Scale.mil
				end
			end,
			"Run", function(self, ai_player)
				if not IsRivalRecentlySabotaged(ai_player) then
					AIContestMilestone(ai_player, "ProduceFood", 72, 96)
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "increase Booze production",
			"log_entry", T("Constructed distilery"),
			"base_eval", 75000000,
			"delay", 90000,
			"tags", {"colonists"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 8 }),
				PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 6 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "booze_production", "amount", 2 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
			},
			"Eval", function(self, ai_player)
				local rations = ai_player.resources.colonists / 5
				if ai_player.resources.booze < 100 then
					return 250 * const.Scale.mil
				elseif ai_player.resources.booze > 250 then
					return 10 * const.Scale.mil
				elseif ai_player.resources.booze > 1000 then
					return 1 * const.Scale.mil
				end
			end
		}),
		PlaceObj("AIAction", {
			"id", "call supply Rocket (Radioactive)",
			"log_entry", T("Imported Radioactive Materials"),
			"base_eval", 150000000,
			"delay", 240000,
			"tags", { "stockpiling" },
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 800 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "radioactive", "amount", 30 })
			},
			"IsAllowed", function(self, ai_player)
				return ai_player.resources.radioactive < -25 and g_Consts.SupplyMissionsEnabled == 1
			end
		}),
		PlaceObj("AIAction", {
			"id", "call supply Rocket (Meat)",
			"log_entry", T("Imported Meat"),
			"base_eval", 150000000,
			"delay", 240000,
			"tags", { "stockpiling" },
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 250 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "meat", "amount", 40 })
			},
			"IsAllowed", function(self, ai_player)
				return ai_player.resources.meat < 0 and ai_player.resources.colonists > 0 and g_Consts.SupplyMissionsEnabled == 1
			end
		}),
		PlaceObj("AIAction", {
			"id", "call supply Rocket (Booze)",
			"log_entry", T("Imported Booze"),
			"base_eval", 150000000,
			"delay", 240000,
			"tags", { "stockpiling" },
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 250 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "booze", "amount", 40 })
			},
			"IsAllowed", function(self, ai_player)
				return ai_player.resources.booze < 0 and ai_player.resources.colonists > 0 and g_Consts.SupplyMissionsEnabled == 1
			end
		}),
		PlaceObj("AIAction", {
			"id", "send crystals Rocket",
			"log_entry", T("Exported Crystals"),
			"base_eval", 1000000000,
			"delay", 720000,
			"max_running", 5,
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "fuel", "amount", 50 }),
				PlaceObj("AIResourceAmount", { "resource", "crystals", "amount", 30 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 750 })
			},
			"Run", function(self, ai_player)
				AIContestMilestone(ai_player, "ReturnRocket", 23, 25)
			end,
			"OnEnd", function(self, ai_player)
				return
			end
		}),
		PlaceObj("AIAction", {
			"id", "construct Second Dome", --this is needed earlier cause AI now needs to produce more resources
			"log_entry", T{"Constructed a small Dome"},
			"base_eval", 150000000,
			"delay", 1440000,
			"tags", {"colonists"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 100 }),
				PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 25 }),
				PlaceObj("AIResourceAmount", { "resource", "polymers", "amount", 10 }),
				PlaceObj("AIResourceAmount", { "resource", "oxygen", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
				PlaceObj("AIResourceAmount", { "resource", "power", "amount", 30 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "colonists_max", "amount", 50 }),
				PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
			},
			"IsAllowed", function(self, ai_player)
				return not IsRivalRecentlySabotaged(ai_player) and (ai_player.resources.colonists_workforce < 10 or ai_player.resources.food > 100)
			end,
			"Eval", function(self, ai_player)
				if ai_player.resources.colonists_max > 40 and ai_player.resources.colonists_max < 110 then
					local player_colonists = ResourceOverviewObj:GetColonistCount()
					if ai_player.resources.colonists_max > player_colonists then
						return 0
					elseif ai_player.resources.colonists_max < player_colonists then
						return 250 * const.Scale.mil
					end
				end
			end,
			"Run", function(self, ai_player)
				AIContestMilestone(ai_player, "ConstructDome", 48, 49)
			end
		}),
		PlaceObj("AIAction", {
			"id", "get workforce from new borns", --fixes a vanilla bug
			"base_eval", 250000000,
			"delay", 360000,
			"tags", {"colonists"},
			"required_resources", {
				PlaceObj("AIResourceAmount", { "resource", "workforce", "amount", 1 })
			},
			"resulting_resources", {
				PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 1 })
			},
			"IsAllowed",
			function(self, ai_player)
				return ai_player.resources.workforce and ai_player.resources.workforce > 0
			end
		}),
	})

	local distressRoot = Presets.Negotiation.Replies.RepliesDistressCall_1
	if Presets.Negotiation.Replies.RepliesDistressCall_LSRes then
		Presets.Negotiation.Replies.RepliesDistressCall_LSRes = nil
		local idx = table.find(distressRoot,"Comment","LSR")
		if idx then table.remove(distressRoot, idx) end
		local idx = table.find(distressRoot,"Id","LSO")
		if idx then table.remove(distressRoot, idx) end
	end
	PlaceObj("Negotiation", {
		Effects = {},
		Prerequisites = {},
		Text = T("What do you want?"),
		Title = T("<NegotiationsContactTitle>"),
		group = "Replies",
		id = "RepliesDistressCall_LSRes",
		save_in = "gagarin",
		PlaceObj("NegotiationReply", {
			"Text", T("We need Food"),
			"CustomDisabledText", T("They can't help"),
			"Prerequisites", {
				PlaceObj("CountRivalResource", { "Resource", "food", "Amount", 50 })
			}
		}),
		PlaceObj("NegotiationOutcome", {
			"Prerequisites", {},
			"NextGroup", "Distress Calls (Accepted)",
			"Effects", {
				PlaceObj("RivalSetDistressCallResource", {"idResource", "Food"})
			}
		}),
		PlaceObj("NegotiationReply", {
			"Text", T("We need Meat"),
			"CustomDisabledText", T("They can't help"),
			"Prerequisites", {
				PlaceObj("CountRivalResource", { "Resource", "meat", "Amount", 40 })
			}
		}),
		PlaceObj("NegotiationOutcome", {
			"Prerequisites", {},
			"NextGroup", "Distress Calls (Accepted)",
			"Effects", {
				PlaceObj("RivalSetDistressCallResource", { "idResource", "Meat" })
			}
		}),
		PlaceObj("NegotiationReply", {
			"Text", T("We need Booze"),
			"CustomDisabledText", T("They can't help"),
			"Prerequisites", {
				PlaceObj("CountRivalResource", { "Resource", "booze", "Amount", 30 })
			}
		}),
		PlaceObj("NegotiationOutcome", {
			"Prerequisites", {},
			"NextGroup", "Distress Calls (Accepted)",
			"Effects", {
				PlaceObj("RivalSetDistressCallResource", { "idResource", "Booze" })
			}
		}),
		PlaceObj("NegotiationReply", {
			"Text", T("On second thought..."),
			"Prerequisites", {},
			"DefaultCloseAction", true
		}),
		PlaceObj("NegotiationOutcome", {
			"Prerequisites", {},
			"Next", "RepliesDistressCall_1",
			"Effects", {}
		})
	})

	Presets.Negotiation.Replies.RepliesDistressCall_2[5] = PlaceObj("NegotiationReply", {
			"Text", T("We need Radioactive Materials"),
			"CustomDisabledText", T("They can't help"),
			"Prerequisites", {
				PlaceObj("CountRivalResource", { "Resource", "radioactive", "Amount", 50 })
			}
		})
	Presets.Negotiation.Replies.RepliesDistressCall_2[6] = PlaceObj("NegotiationOutcome", {
			"Prerequisites", {},
			"NextGroup", "Distress Calls (Accepted)",
			"Effects", {
				PlaceObj("RivalSetDistressCallResource", {"idResource", "Radioactive"})
			}
		})
	local distressSize = #distressRoot
	distressRoot[distressSize+1] = distressRoot[distressSize-1]
	distressRoot[distressSize+2] = distressRoot[distressSize]
	distressRoot[distressSize-1] = PlaceObj("NegotiationReply", { 
		"Comment", "LSR", "Text", T("Life Support resource"), "Prerequisites", {} 
	})
	distressRoot[distressSize] = PlaceObj("NegotiationOutcome", {
		"Id", "LSO", "Prerequisites", {}, "Next", "RepliesDistressCall_LSRes", "Effects", {} 
	})

	local helpRoot = Presets.Negotiation.Replies.RepliesHelpOffer_1
	if Presets.Negotiation.Replies.RepliesHelpOffer_LSRes then
		Presets.Negotiation.Replies.RepliesHelpOffer_LSRes = nil
		local idx = table.find(helpRoot,"Comment","LSR")
		if idx then table.remove(helpRoot, idx) end
		local idx = table.find(helpRoot,"Id","LSO")
		if idx then table.remove(helpRoot, idx) end
	end
	PlaceObj("Negotiation", {
		Effects = {},
		Prerequisites = {},
		Text = T(297659968639, "What do you offer?"),
		Title = T(11546, "<NegotiationsContactTitle>"),
		group = "Replies",
		id = "RepliesHelpOffer_LSRes",
		save_in = "gagarin",
		PlaceObj("ReplyGenerator_HelpRocket", { "idResourceType", "ls_res" }),
		PlaceObj("NegotiationReply", {
			"Text", T(424168816047, "On second thought..."),
			"Prerequisites", {},
			"DefaultCloseAction", true
		}),
		PlaceObj("NegotiationOutcome", {
			"Prerequisites", {},
			"Next", "RepliesHelpOffer_1",
			"Effects", {}
		})
	})
	local helpSize = #helpRoot
	helpRoot[helpSize+1] = helpRoot[helpSize-1]
	helpRoot[helpSize+2] = helpRoot[helpSize]
	helpRoot[helpSize-1] = PlaceObj("NegotiationReply", {
		"Comment", "LSR", "Text", T("Life Support resource"), "Prerequisites", {}
	})
	helpRoot[helpSize] = PlaceObj("NegotiationOutcome", {
		"Id", "LSO", "Prerequisites", {}, "Next", "RepliesHelpOffer_LSRes", "Effects", {}
	})

	
end

local HelpOfferBasic = {
	{ ai_res = "metals", game_res = "Metals", min = 200, help = 100000 },
	{ ai_res = "concrete", game_res = "Concrete", min = 200, help = 100000 },
	{ ai_res = "radioactive", game_res = "Radioactive", min = 500, help = 50000 }
}

local HelpOfferAdvanced = {
	{ ai_res = "polymers", game_res = "Polymers", min = 500, help = 50000 },
	{ ai_res = "machineparts", game_res = "MachineParts", min = 500, help = 25000 },
	{ ai_res = "electronics", game_res = "Electronics", min = 500, help = 25000 }
}

local HelpOfferLSRes = {
	{ ai_res = "food", game_res = "Food", min = 500, help = 50000 },
	{ ai_res = "meat", game_res = "Meat", min = 500, help = 50000 },
	{ ai_res = "booze", game_res = "Booze", min = 500, help = 50000 }
}

function DumbAIPlayer:GetResourcesHelpOffer(resource_type)
	local items = {}
	local resources = resource_type == "basic" and HelpOfferBasic or resource_type == "advanced" and HelpOfferAdvanced or HelpOfferLSRes
	for i, res in ipairs(resources) do
		local count = self.resources[res.ai_res]
		items[#items + 1] = {
			choice_text = T({"<resource(res)>", res = res.game_res }),
			extra_text = T({"send rocket carrying <resource>", resource = FormatResourceValueMaxResource(empty_table, res.help, res.game_res)}),
			enabled = count <= res.min,
			offer = res
		}
	end
	return items
end

local Initialize_MarsTradeRoute = MarsTradeRoute.Initialize
function MarsTradeRoute:Initialize(sponsor_id, ...)	
	table.insert_unique(self.basic_import, "Meat")
	table.insert_unique(self.basic_import, "Booze")
	table.insert_unique(self.advanced_import, "Radioactive")
	table.insert_unique(self.export_resource, "Meat")
	table.insert_unique(self.export_resource, "Booze")
	table.insert_unique(self.export_resource, "Radioactive")
	return Initialize_MarsTradeRoute(self, sponsor_id, ...)  
end

OnMsg.ModsReloaded = function ( ... )
	if (XTemplates.customTradePad) then
		table.insert_unique(TradePad.trade_resources, "Radioactive")
		table.insert_unique(TradePad.trade_resources, "Meat")
		table.insert_unique(TradePad.trade_resources, "Booze")
		local export = XTemplates.customTradePad[3].OnContextUpdate
		XTemplates.customTradePad[3].OnContextUpdate = function(self, context)
			local resource = ResolveValue(context, "export_resource")
			if resource=="Meat" or resource=="Booze" or resource=="Radioactive" then
				self:SetIcon(CurrentModPath.."UI/Icons/Sections/" .. resource .. "_6.tga")
				self:SetTitle(T({11503, "Offering: <resource(res)>", res = resource }))
			else
				export(self, context)
			end
			self:SetVisible(context:IsAvailable())
		end
		local import = XTemplates.customTradePad[4].OnContextUpdate
		XTemplates.customTradePad[4].OnContextUpdate = function(self, context)
			local resource = ResolveValue(context, "import_resource")
			if resource=="Meat" or resource=="Booze" or resource=="Radioactive" then
				self:SetIcon(CurrentModPath.."UI/Icons/Sections/" .. resource .. "_5.tga")
				self:SetTitle(T({11505, "Requesting: <resource(res)>", res = resource }))
			else
				import(self, context)
			end
			self:SetVisible(context:IsAvailable())
		end
	end
end

function lh_top_table(t) --helper testing rivals
	for k,v in pairs(t) do
		t[k] = 100
	end
end
