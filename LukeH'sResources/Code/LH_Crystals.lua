local translationIdBase = 1107010500

DefineClass("SubsurfaceDepositCrystals",
{ __parents = {"SubsurfaceDeposit"},
	resource = "Crystals",
	entity = "LH_CrystalsDeposit",
	disabled_entity = "LH_CrystalsDisabledDeposit",
	display_icon = "UI/Icons/Buildings/deposit_polymers.tga",
	--name and description is reassigned when setting a new 'depth_layer' (see SubsurfaceDeposit:Setdepth_layer)
	display_name = T{translationIdBase+1, "Underground Crystals"},
	description = T{translationIdBase+2, "An underground deposit of Crystals."},
	display_name_deep = T{translationIdBase+3, "Deep Crystals"},
	description_deep = T{translationIdBase+4, "Deep underground deposit of Crystals."},
	fx_actor_class = "SubsurfaceAnomaly"
})

Resources["Crystals"] = {name = "Crystals",
	display_name = T{translationIdBase+5, "Crystals"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_crystals.tga", 
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_CrystalsContainer", 
	description = T{translationIdBase+6, "Underground Crystals."}
}

DefineConst{
	group = "Gameplay",
	help = T{translationIdBase+7, "Deep Crystals deposits are exploitable when this is not 0"},
	id = "IsDeepCrystalsExploitable",
	name = T{translationIdBase+8, "Exploitable Crystals"},
	value = 0,
}

table.insert_unique(ResourceDescription, Resources["Crystals"])	
table.insert_unique(StockpileResourceList, "Crystals")
table.insert_unique(CargoShuttle.storable_resources, "Crystals")
table.insert_unique(SubSurfaceDeposits, "Crystals")
SubSurfaceDeposits["Crystals"] = true		
table.insert_unique(ConstructionResourceList,"Crystals")
table.insert_unique(AllResourcesList, "Crystals")
table.insert_unique(RCTransport.storable_resources,"Crystals")

DepositGradeToWasteRockMultipliers.Crystals = {
    ["Depleted"] = 2000,
    ["Very Low"] = 1600,
    ["Low"] = 1400,
    ["Average"] = 1200,
    ["High"] = 1000,
    ["Very High"] = 800
}

function OnMsg.SectorScanned(status, col, row) --find a way for it to be spawned by engine
	local sector = g_MapSectors[col][row]
	for i = 1, #sector.markers.subsurface do
		local marker = sector.markers.subsurface[i]
		if not marker.revealed and marker.resource == "Crystals" then
			marker.revealed = true
			local deposit = marker:PlaceDeposit()	
		end
	end
	if status == "deep scanned" then
		for i = 1, #sector.markers.deep do
			local marker = sector.markers.deep[i]
			if not marker.revealed and marker.resource == "Crystals" then
				marker.revealed = true
				local deposit = marker:PlaceDeposit()	
				deposit:UpdateEntity()
			end
		end
	end
end

function OnMsg.LoadGame( ... )
	if not g_Consts["IsDeepCrystalsExploitable"] == 1 and UICity:IsTechResearched("DeepMetalExtraction") then
		g_Consts["IsDeepCrystalsExploitable"] = 1
		MapForEach("map", "SubsurfaceDepositCrystals", function(obj)
			obj:UpdateEntity()
		end)
	end
end

function OnMsg.TechResearched(tech_id, city)
	if tech_id == "DeepMetalExtraction" then
		g_Consts["IsDeepCrystalsExploitable"] = 1
		MapForEach("map", "SubsurfaceDepositCrystals", function(obj)
			obj:UpdateEntity()
		end)
	end	
end

local vanillaIsDeepExploitable = IsDeepExploitable
function IsDeepExploitable(res)
	if res == "Crystals" then
		return g_Consts["IsDeepCrystalsExploitable"] ~= 0
	else
		return vanillaIsDeepExploitable(res)
	end
end

DefineClass.LH_CrystalsExtractor =
{
	__parents = {"MetalExtractorWorkplace"},
	
	subsurface_deposit_class = "SubsurfaceDepositCrystals",
	exploitation_resource = "Crystals",
}

LH_CrystalsExtractor.OnCalcProduction_Crystals = BaseMetalsExtractor.OnCalcProduction_Metals
LH_CrystalsExtractor.OnProduce_Crystals = BaseMetalsExtractor.OnProduce_Metals
LH_CrystalsExtractor.GetPredictedProduction_Crystals = BaseMetalsExtractor.GetPredictedProduction_Metals
LH_CrystalsExtractor.GetPredictedDailyProduction_Crystals = BaseMetalsExtractor.GetPredictedDailyProduction_Metals