local translationIdBase = 1107012100

DefineClass.HydrocarbonProducer = {
	__parents = {
		"Building",
		"LifeSupportGridObject"
	},
	properties = {
		{
			template = true,
			id = "hydrocarbon_production",
			name = T(translationIdBase+6, "Oil production"),
			category = "Oil Production",
			editor = "number",
			default = 10000,
			help = Untranslated("This is the amount produced per hour."),
			scale = const.ResourceScale,
			modifiable = true
		}
	},
	is_tall = true
}

function HydrocarbonProducer:CreateLifeSupportElements()
	UICity.hydrocarbon = UICity.hydrocarbon or SupplyGrid:new({city = self})
	self.oil = true
	self.hydrocarbon = NewSupplyGridProducer(self)
	self.hydrocarbon:SetProduction(self.working and self.hydrocarbon_production or 0)
	self.water = SupplyGridElement:new({building = self, is_cable_or_pipe = false})
	
end
function HydrocarbonProducer:OnSetWorking(working)
	Building.OnSetWorking(self, working)
	if self.hydrocarbon then
		self.hydrocarbon:SetProduction(working and self.hydrocarbon_production or 0)
	end
end
function HydrocarbonProducer:MoveInside(dome)
	return LifeSupportGridObject.MoveInside(self, dome)
end
function HydrocarbonProducer:OnModifiableValueChanged(prop)
	if prop == "hydrocarbon_production" and self.hydrocarbon then
		self.hydrocarbon:SetProduction(self.working and self.hydrocarbon_production or 0)
	end
end
function HydrocarbonProducer:ShouldShowNotConnectedToGridSign()
	return self:ShouldShowNotConnectedToLifeSupportGridSign()
end
function HydrocarbonProducer:ShouldShowNotConnectedToLifeSupportGridSign()
	local not_under_dome = not self.parent_dome
	if not_under_dome then
		if not self:HasPipes() then
			return true
		end
		if self.hydrocarbon.grid and #self.hydrocarbon.grid.consumers <= 0 and 0 >= #self.hydrocarbon.grid.storages then
			return true
		end
	end
	return false
end
function HydrocarbonProducer:UpdateAttachedSigns()
	self:AttachSign(self:ShouldShowNotConnectedToLifeSupportGridSign(), "SignNoPipeConnection")
end

function HydrocarbonProducer:GetHydrocarbonProductionText(short)
	local real_production = self.hydrocarbon.production > 0 and Max(self.hydrocarbon.production - self.hydrocarbon.current_throttled_production, 0) or 0
	local max_production = self.hydrocarbon.production
	return real_production < max_production
		and (short and T{translationIdBase+7, "<hydrocarbon(number1,number2)>", number1 = real_production, number2 = max_production} or T{translationIdBase+8, "Hydrocarbon production<right><hydrocarbon(number1,number2)>", number1 = real_production, number2 = max_production})
		or (short and T{translationIdBase+9, "<hydrocarbon(number)>", number = real_production} or T{translationIdBase+10, "Hydrocarbon production<right><hydrocarbon(number)>", number = real_production})
end


function HydrocarbonProducer:GetUISectionOilProductionRollover()
	local lines = {
		T({
			translationIdBase+11,
			"Production capacity<right><hydrocarbon(production)>",
			self.hydrocarbon
		}),
		T({
			translationIdBase+12,
			"Production per Sol<right><hydrocarbon(ProductionEstimate)>",
			self.hydrocarbon
		}),
		T({
			translationIdBase+13,
			"Lifetime production<right><hydrocarbon(production_lifetime)>",
			self.hydrocarbon
		})
	}
	AvailableDeposits(self, lines)
	if self:HasMember("wasterock_producer") and self.wasterock_producer then
		lines[#lines + 1] = T(translationIdBase+14, "<newline><center><em>Storage</em>")
		lines[#lines + 1] = T({
			translationIdBase+15,
			"Waste Rock<right><wasterock(GetWasterockAmountStored,wasterock_max_storage)>",
			self
		})
	end
	return table.concat(lines, "<newline><left>")
end
function HydrocarbonProducer:NeedsAir()
	return false
end
function HydrocarbonProducer:NeedsWater()
	return false
end
DefineClass.HydrocarbonConsumer = {
	__parents = {
		"Building",
		"LifeSupportGridObject"
	},
	properties = {
		{
			template = true,
			id = "hydrocarbon_consumption",
			name = T(translationIdBase+16, "Hydrocarbon consumption"),
			category = "Consumption",
			editor = "number",
			default = 10000,
			scale = const.ResourceScale,
			modifiable = true,
			min = 0
		},
		{
			template = true,
			id = "disable_hydrocarbon_consumption",
			name = T(translationIdBase+17, "Disable Hydrocarbon Consumption"),
			no_edit = true,
			modifiable = true,
			editor = "number",
			default = 0,
			help = "So consumption can be turned off with modifiers"
		}
	},
	is_tall = true,
	is_lifesupport_consumer = true
}
function HydrocarbonConsumer:CreateLifeSupportElements()
	UICity.hydrocarbon = UICity.hydrocarbon or SupplyGrid:new({city = self})
	if self.hydrocarbon_consumption > 0 then
		self.oil = true
		self.hydrocarbon = NewSupplyGridConsumer(self)
		self.hydrocarbon:SetConsumption(self.hydrocarbon_consumption)
	end
	self.water = SupplyGridElement:new({building = self, is_cable_or_pipe = false})
end
function HydrocarbonConsumer:SetSupply(resource, amount)
	if resource == "hydrocarbon" and self.hydrocarbon then
		self:Gossip("SetSupply", resource, amount)
		self:UpdateWorking()
	end
end
function HydrocarbonConsumer:SetPriority(priority)
	Building.SetPriority(self, priority)
	if self.hydrocarbon then
		self.hydrocarbon:SetPriority(priority)
	end
end
function HydrocarbonConsumer:HasHydrocarbon()
	local hydrocarbon = self.hydrocarbon
	return self.hydrocarbon_consumption == 0 or hydrocarbon and (hydrocarbon.current_consumption >= hydrocarbon.consumption) and (self.hydrocarbon_consumption <= hydrocarbon.consumption)
end
function HydrocarbonConsumer:GetWorkNotPossibleReason()
	if not self:HasHydrocarbon() then
		return "NoHydrocarbon"
	end
	return Building.GetWorkNotPossibleReason(self)
end
function HydrocarbonConsumer:MoveInside(dome)
	return LifeSupportGridObject.MoveInside(self, dome)
end
function HydrocarbonConsumer:OnModifiableValueChanged(prop)
	if prop == "hydrocarbon_consumption" then
		self:UpdateConsumption()
	elseif prop == "disable_hydrocarbon_consumption" then
		if self.disable_hydrocarbon_consumption >= 1 then
			self:SetBase("hydrocarbon_consumption", 0)
		else
			self:RestoreBase("hydrocarbon_consumption")
		end
	end
end
function HydrocarbonConsumer:NeedsHydrocarbon()
	local needs_hydrocarbon = false
	if self.hydrocarbon and self.hydrocarbon.consumption and self.hydrocarbon.consumption > 0 then
		needs_hydrocarbon = true
	end
	return needs_hydrocarbon
end
function HydrocarbonConsumer:NeedsAir()
	return false
end
function HydrocarbonConsumer:NeedsWater()
	return false
end
function HydrocarbonConsumer:ShouldShowNoHydrocarbonSign()
	return self:NeedsHydrocarbon() and self:IsWorkPermitted() and not self:HasHydrocarbon()
end
function HydrocarbonConsumer:ShouldShowNotConnectedToGridSign()
	return self:ShouldShowNotConnectedToLifeSupportGridSign()
end
function HydrocarbonConsumer:ShouldShowNotConnectedToLifeSupportGridSign()
	local not_under_dome = not self.parent_dome
	if not_under_dome then
		local needs_hydrocarbon = self:NeedsHydrocarbon()
		if needs_hydrocarbon and not self:HasPipes() then
			return true
		end
		if needs_hydrocarbon and self.hydrocarbon.grid and #self.hydrocarbon.grid.producers <= 0 then
			return true
		end
	end
	return false
end

function HydrocarbonConsumer:UpdateAttachedSigns()
	self:AttachSign(self:ShouldShowNotConnectedToLifeSupportGridSign(), "SignNoPipeConnection")
	if self.hydrocarbon then
		self:AttachSign(self:ShouldShowNoHydrocarbonSign(), "SignNoWater")
	end
end

function HydrocarbonConsumer:UpdateUISectionConsumption(win) 
	Building.UpdateUISectionConsumption(self, win)
	local res = self:GetUIConsumptionTexts()
	if res.hydrocarbon then win.idHydrocarbon:SetText(res.hydrocarbon) else win.idHydrocarbon:SetText("") end
end

local LH_ConsumptionStatuses = { --TODO store to vanilla table
	Oil = T(translationIdBase+40, "Hydrocarbon<right><green><hydrocarbon(hydrocarbon_consumption)></green>"),
	OilNotEnough = T(translationIdBase+41, "Insufficient Hydrocarbon<right><red><hydrocarbon(hydrocarbon_consumption)></red>"),
	OilRequired = T(translationIdBase+42, "Required Hydrocarbon<right><hydrocarbon(hydrocarbon_consumption)>"),	
}

local LH_ConsumptionStatusesShort = {
	Oil = T(translationIdBase+43, "<green><hydrocarbon(hydrocarbon_consumption)></green>"),
	OilNotEnough = T(translationIdBase+44, "<red><hydrocarbon(hydrocarbon_consumption)></red>"),
	OilRequired = T(translationIdBase+45, "<hydrocarbon(hydrocarbon_consumption)>")
}


function HydrocarbonConsumer:UpdateConsumption(update)
	if GameInitThreads[self] then
	  self:Notify("UpdateConsumption")
	  return
	end

	BaseBuilding.UpdateConsumption(self, update)
	local demand_stopped_by_user = not self:IsWorkPermitted()
    local demand_stopped_by_game = self:IsSupplyGridDemandStoppedByGame()
    if demand_stopped_by_user or demand_stopped_by_game then
        if self.hydrocarbon then
          self.hydrocarbon:SetConsumption(0)
        end
    else
        local should_consume_ls = true
        should_consume_ls = self.is_electricity_consumer and self.electricity and self:HasPower()
        if should_consume_ls then
          if self.hydrocarbon then
            self.hydrocarbon:SetConsumption(self.hydrocarbon_consumption)
          end
        else
          if self.hydrocarbon then
            self.hydrocarbon:SetConsumption(0)
          end
        end
    end
end

function HydrocarbonConsumer:GetUIConsumptionTexts(short)
	local res = Building.GetUIConsumptionTexts(self, short)
	local intentionally_not_consuming = self:IsSupplyGridDemandStoppedByGame()
	local permited = self:IsWorkPermitted()
	local not_permited = (not permited or intentionally_not_consuming)
	local statuses = short and LH_ConsumptionStatusesShort or LH_ConsumptionStatuses
	if self.hydrocarbon_consumption and (self.hydrocarbon_consumption>0 or self:GetClassValue("hydrocarbon_consumption")>0) then
		local no_hydrocarbon = not (self:HasHydrocarbon() or intentionally_not_consuming) and permited
		res.hydrocarbon = no_hydrocarbon and statuses["OilNotEnough"]
			or not_permited and statuses["OilRequired"]
			or statuses["Oil"]
	end
	return res
end

DefineClass.HydrocarbonStorage = {
	__parents = {
		"Building",
		"LifeSupportGridObject"
	},
	properties = {
		{
			template = true,
			id = "max_hydrocarbon_charge",
			name = T(29, "Max water consumption while charging"),
			category = "Storage",
			editor = "number",
			default = 1000,
			help = "This is the amount of water the battery can charge per hour.",
			scale = const.ResourceScale
		},
		{
			template = true,
			id = "max_hydrocarbon_discharge",
			name = T(1068, "Max water output while discharging"),
			category = "Storage",
			editor = "number",
			default = 1000,
			help = "This is the amount of air the battery can discharge per hour.",
			scale = const.ResourceScale
		},
		{
			template = true,
			id = "hydrocarbon_conversion_efficiency",
			name = T(1069, "Conversion efficiency % of water (charging)"),
			category = "Storage",
			editor = "number",
			default = 100,
			help = "(100 - this number)% will go to waste when charging."
		},
		{
			template = true,
			id = "hydrocarbon_capacity",
			name = T(30, "Oil Capacity"),
			editor = "number",
			category = "Storage",
			default = 10000,
			scale = const.ResourceScale,
			modifiable = true
		},
		{
			id = "StoredHydrocarbon",
			name = T(33, "Stored Oil"),
			editor = "number",
			default = 0,
			scale = const.ResourceScale,
			no_edit = true
		}
	},
	is_tall = true
}
function HydrocarbonStorage:CreateLifeSupportElements()
	UICity.hydrocarbon = UICity.hydrocarbon or SupplyGrid:new({city = self})
	self.oil = true
	self.hydrocarbon = NewSupplyGridStorage(self, self.hydrocarbon_capacity, self.max_hydrocarbon_charge, self.max_hydrocarbon_discharge, self.hydrocarbon_conversion_efficiency, 20)
	self.hydrocarbon:SetStoredAmount(self.StoredHydrocarbon, "hydrocarbon")
	self.water = SupplyGridElement:new({building = self, is_cable_or_pipe = false})
end
function HydrocarbonStorage:OnModifiableValueChanged(prop)
	if self.hydrocarbon and (prop == "max_hydrocarbon_charge" or prop == "max_hydrocarbon_discharge" or prop == "hydrocarbon_conversion_efficiency" or prop == "hydrocarbon_capacity") then
		self.hydrocarbon.charge_efficiency = self.hydrocarbon_conversion_efficiency
		self.hydrocarbon.storage_capacity = self.hydrocarbon_capacity
		if self.hydrocarbon.grid and self.hydrocarbon.current_storage > self.hydrocarbon_capacity then
			local delta = self.hydrocarbon.current_storage - self.hydrocarbon_capacity
			self.hydrocarbon.current_storage = self.water_capacity
			self.hydrocarbon.grid.current_storage = self.hydrocarbon.grid.current_storage - delta
		end
		self.hydrocarbon:UpdateStorage()
	end
end
function HydrocarbonStorage:MoveInside(dome)
	return LifeSupportGridObject.MoveInside(self, dome)
end
function HydrocarbonStorage:OnSetWorking(working)
	Building.OnSetWorking(self, working)
	local hydrocarbon = self.hydrocarbon
	if working then
		hydrocarbon:UpdateStorage()
	else
		hydrocarbon:SetStorage(0, 0)
	end
end
function HydrocarbonStorage:ShouldShowNotConnectedToGridSign()
	return self:ShouldShowNotConnectedToLifeSupportGridSign()
end
function HydrocarbonStorage:ShouldShowNotConnectedToLifeSupportGridSign()
	local not_under_dome = not self.parent_dome
	if not_under_dome then
		if not self:HasPipes() then
			return true
		end
		if #self.hydrocarbon.grid.producers <= 0 then
			return true
		end
	end
	return false
end
function HydrocarbonStorage:UpdateAttachedSigns()
	self:AttachSign(self:ShouldShowNotConnectedToLifeSupportGridSign(), "SignNoPipeConnection")
end
function HydrocarbonStorage:CheatFill()
	self.hydrocarbon:SetStoredAmount(self.water_capacity, "hydrocarbon")
end
function HydrocarbonStorage:CheatEmpty()
	self.hydrocarbon:SetStoredAmount(0, "hydrocarbon")
end
function HydrocarbonStorage:GetStoredHydrocarbon()
	return self.hydrocarbon.current_storage
end
function HydrocarbonStorage:NeedsHydrocarbon()
	return true
end
function HydrocarbonStorage:NeedsAir()
	return false
end
function HydrocarbonStorage:NeedsWater()
	return false
end

DefineClass.LH_HydrocarbonExtractor = {
	__parents = {
		"ResourceProducer",
		"ElectricityConsumer",
		"HydrocarbonProducer",
		"DustGenerator",
		"BuildingDepositExploiterComponent",
		"OutsideBuildingWithShifts",
		"SubsurfaceDepositConstructionRevealer"
	},
	subsurface_deposit_class = "SubsurfaceDepositHydrocarbon",
	building_update_time = const.HourDuration,
	stockpile_spots1 = {
		"Resourcepile"
	},
	additional_stockpile_params1 = {
		apply_to_grids = false,
		has_platform = true,
		snap_to_grid = false,
		priority = 2,
		additional_supply_flags = const.rfSpecialDemandPairing
	},
	anim_moments_thread = false
}
function LH_HydrocarbonExtractor:BuildingUpdate()
	RebuildInfopanel(self)
end
function LH_HydrocarbonExtractor:DroneLoadResource(drone, request, resource, amount)
	TaskRequester.DroneLoadResource(self, drone, request, resource, amount)
	if not self.working then
		self:UpdateWorking()
	end
end
function LH_HydrocarbonExtractor:GameInit()
	self:DepositChanged()
end
function LH_HydrocarbonExtractor:OnDepositsLoaded()
	self:DepositChanged()
	self:UpdateConsumption()
	self:UpdateWorking()
end
function LH_HydrocarbonExtractor:ProduceSupply(resource, amount)
	if resource == "hydrocarbon" and self.nearby_deposits[1] then
		local deposit_grade = self.nearby_deposits[1].grade
		if self:DoesHaveUpgradeConsumption() then
			amount = self:Consume_Upgrades_Production(amount, 100)
		end
		amount = self:ExtractResource(amount)
		if self:ProduceWasteRock(amount, deposit_grade) then
			self:UpdateWorking(false)
		end
	else
		print("wrong prod")
	end
end
function LH_HydrocarbonExtractor:DepositChanged()
	local deposit_multipler = self:GetCurrentDepositQualityMultiplier()
	local amount = MulDivRound(self:GetClassValue("hydrocarbon_production"), deposit_multipler, 100)
	self:SetBase("hydrocarbon_production", amount)
	self:UpdateWorking()
end
function LH_HydrocarbonExtractor:OnChangeActiveDeposit()
	BuildingDepositExploiterComponent.OnChangeActiveDeposit(self)
	self:DepositChanged()
end
function LH_HydrocarbonExtractor:OnDepositDepleted(deposit)
	BuildingDepositExploiterComponent.OnDepositDepleted(self, deposit)
	self:DepositChanged()
end
function LH_HydrocarbonExtractor:OnSetWorking(working)
	Building.OnSetWorking(self, working)
	local production = working and self.hydrocarbon_production or 0
	if self.hydrocarbon then
		self.hydrocarbon:SetProduction(production, production)
	end
	if working then
		if not IsValidThread(self.anim_moments_thread) then
			local pump = self:GetAttach("WaterExtractorPump")
			if pump then
				self.anim_moments_thread = TrackAllMoments(pump, "working", self)
			end
		end
	elseif IsValidThread(self.anim_moments_thread) then
		DeleteThread(self.anim_moments_thread)
	end
end
function LH_HydrocarbonExtractor:OnModifiableValueChanged(prop)
	if prop == "hydrocarbon_production" and self.hydrocarbon then
		local production = self.working and self.hydrocarbon_production or 0
		self.hydrocarbon:SetProduction(production, production)
	end
end
function LH_HydrocarbonExtractor:IsIdle()
	return self.ui_working and not self:CanExploit() and not self.city:IsTechResearched("NanoRefinement")
end
function LH_HydrocarbonExtractor:SetUIWorking(working)
	Building.SetUIWorking(self, working)
	BuildingDepositExploiterComponent.UpdateIdleExtractorNotification(self)
end

DefineClass.LH_GHGFactory = {
  __parents = {
    "GHGFactory",
    "HydrocarbonConsumer"
  },
  use_shape_selection = true,
  initial_temp_boost_coef = 5
}

local orig_SetObjWaterMarkers = SetObjWaterMarkers
function SetObjWaterMarkers(obj, show, list, ...)
	local marked = orig_SetObjWaterMarkers(obj, show, list, ...)
	if marked and show == true and obj:IsKindOf("LH_GHGFactory") then
		obj:AddFakeMarkers(list)
	end
	return marked
end

local orig_HasSpot = g_CObjectFuncs.HasSpot
function LH_GHGFactory:HasSpot(name, ...)
	if name == "Lifesupportgrid" then
		return true
	end
	return orig_HasSpot(self, name, ...)
end
--
local orig_GetSpotRange = g_CObjectFuncs.GetSpotRange
function LH_GHGFactory:GetSpotRange(name, ...)
	if name == "Lifesupportgrid" then
		return 0, 0
	end
	return orig_GetSpotRange(self, name, ...)
end

local GetPipeConnections = {--some small entity
	{point(0, 0), 0, 0, "Basketball"},
	{point(0, 0), 1, 0, "Basketball"},
	{point(0, 0), 2, 0, "Basketball"},
	{point(0, 0), 3, 0, "Basketball"},
	{point(0, 0), 4, 0, "Basketball"},
	{point(0, 0), 5, 0, "Basketball"},
}

function LH_GHGFactory.GetPipeConnections()
	return GetPipeConnections
end

function LH_GHGFactory:AddFakeMarkers(list)
	self:ForEachAttach("GridTileWater", function(a)
		a:SetVisible(true)

		local offset = a:GetAttachOffset()
		local num = a:GetAttachSpot()

		a:SetAttachOffset(offset:SetX(-500):SetY(800))

		local marker = PlaceObject("GridTileWater", nil, const.cfComponentAttach)
		self:Attach(marker, num)
		marker:SetAttachAngle(- marker:GetAngle())
		marker:SetAttachOffset(0, 0, offset:z())			
		marker:SetAttachOffset(offset:SetX(500):SetY(800))

		if list then
			list[#list + 1] = marker
		end

	end)
end

DefineClass.LH_FuelFactory = {
  __parents = {
    "ResourceProducer",
    "ElectricityConsumer",
    "HydrocarbonConsumer",
    "OutsideBuildingWithShifts"
  },
  properties = {
		{ template = true, category = "Consumption",   name = T(translationIdBase+18, "Hydrocarbon"),                   id = "hydrocarbon_consumption",       editor = "number", default = 5000,   scale = const.ResourceScale, read_only = false, modifiable = true, min = 0, },		
		{ template = true, category = "Consumption",   name = T(683, "Power Consumption"), id = "electricity_consumption", editor = "number", default = 1000,   modifiable = true, min = 0, },
	},
  building_update_time = const.HourDuration,
  exploitation_resource = "Fuel",
  stockpile_spots1 = {
    "Resourcepile"
  },
  has_demand_request = false,
  has_supply_request = true,
  max_x = 5,
  max_y = 2
}
function LH_FuelFactory:Init()
  self.parent = self
  self.max_z = self.max_storage1 / (10 * const.ResourceScale)
end

DefineClass.LH_PolycarbonPlant = {
	__parents = { "Factory", "HydrocarbonConsumer"},
	resource_produced1 = "Polymers",
	stockpile_spots1 = { "Resourcepile" },
	properties = {
		{ template = true, category = "Consumption",   name = T(translationIdBase+18, "Hydrocarbon"),                   id = "hydrocarbon_consumption",       editor = "number", default = 5000,   scale = const.ResourceScale, read_only = false, modifiable = true, min = 0, },		
		{ template = true, category = "Consumption",   name = T(683, "Power Consumption"), id = "electricity_consumption", editor = "number", default = 1000,   modifiable = true, min = 0, },
	},

}

function LH_PolycarbonPlant.OnCalcProduction_Polymers(producer, amount_to_produce)
	return MulDivRound(producer.parent.performance, amount_to_produce, 100)
end


DefineClass.LH_HydrocarbonTank = {
	__parents = {
		"HydrocarbonStorage",
		"StorageWithIndicators",
		"ColdSensitive"
	},
	indicated_resource = "hydrocarbon",
	indicator_class = "WaterTankFloat",
	building_update_time = 10000
}
DefineClass.LH_HydrocarbonTankLarge = {
	__parents = {
		"HydrocarbonStorage",
		"StorageWithIndicators",
		"ColdSensitive"
	},
	indicated_resource = "hydrocarbon",
	indicator_class = "WaterTankLargeFloat",
	building_update_time = 10000
}

function SupplyGridElement:GetUIMode()
	local grid = self.grid
	local resource_type = grid.supply_resource == "water" and "Water" or grid.supply_resource == "air" and "Air" or grid.supply_resource == "hydrocarbon" and "Hydrocarbon" or ""
	local change = grid.current_storage_change
	if change < 0 then
		change = grid.discharge ~= 0 and MulDivRound(change, self.discharge, grid.discharge) or 0
	elseif change > 0 then
		change = grid.charge ~= 0 and MulDivRound(change, self.charge, grid.charge) or 0
	end
	if change == 0 then return self.storage_mode == "full" and T(3880, "Status<right>Full") or self.storage_mode == "empty" and T(3881, "Status<right>Empty") or T(3882, "Status<right>Idle") end
	if change > 0 then return T{3885, "Increasing<right><resource(change,resource_type)>", change = change, resource_type = resource_type} end
	return T{3886, "Decreasing<right><resource(change,resource_type)>", change = change, resource_type = resource_type} 
end