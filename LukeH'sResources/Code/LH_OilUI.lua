local GetCommandCenterBuildings_Vanilla = GetCommandCenterBuildings
local IsKindOfClasses_Vanilla = IsKindOfClasses
function GetCommandCenterBuildings(context)
    IsKindOfClasses = function(building, type, ...)
        if type == "ResourceStockpileBase" and (IsKindOf(building, "HydrocarbonStorage") or  IsKindOf(building, "WastewaterStorage")) then
            return true
        else
            return IsKindOfClasses_Vanilla(building, type, ...)
        end
    end
    local buildings = GetCommandCenterBuildings_Vanilla(context)
    IsKindOfClasses = IsKindOfClasses_Vanilla
    local any_filter = context.decorations or
        context.storages or
        context.power_producers ~= false or
        context.production_buildings ~= false or
        context.services ~= false or
        context.residential or
        context.terraforming or
        context.other
    for i = #buildings, 1, -1 do
        if any_filter and not context.storages and (IsKindOf(buildings[i], "HydrocarbonStorage") or  IsKindOf(buildings[i], "WastewaterStorage")) then
            table.remove(buildings, i)
        end
    end
    return buildings
end

--because Silva's Wastewater messes this I copy paste the vanilla
function Building:GetUIConsumptionRow()
  local res = self:GetUIConsumptionTexts("short")
  local t = {}
  if res.power then t[#t + 1] = res.power end
  if res.air then t[#t + 1] = res.air end
  if res.water then t[#t + 1] = res.water end
  if res.stored_water then t[#t + 1] = res.stored_water end
  if res.wastewater then t[#t + 1] = res.wastewater end
  if res.hydrocarbon then t[#t + 1] = res.hydrocarbon end
  if res.resource then t[#t + 1] = res.resource end
  if res.upgrade then t[#t + 1] = res.upgrade end
  return table.concat(t, " ")
end

function Building:GetUIEffectsRow()
    if self:IsKindOf("Service") then
        return T(9728, "<count(visitors)>/<colonist(max_visitors)>")
    elseif self:IsKindOf("Residence") then
        return T(9729, "<UIResidentsCount>/<colonist(UICapacity)>")
    elseif self:IsKindOf("DroneControl") then
        return T(9730, "<drone(DronesCount,MaxDronesCount)>")
    elseif self:IsKindOf("ShuttleHub") then
        return T(9770, "<count(shuttle_infos)>/<max_shuttles>")
    elseif self:IsKindOf("Dome") then
        return T(9771, "<colonist(ColonistCount, LivingSpace)>")
    elseif (IsKindOf(self, "UniversalStorageDepot") or IsKindOf(self, "MechanizedDepot")) and not self:IsKindOf("SupplyRocket") and not IsKindOf(self, "SpaceElevator") then
        local sum = type(self.resource) == "table" and #self.resource > 1
        local stored, max = 0,0
        if (self:DoesAcceptResource("Metals") or self:DoesAcceptResource("Concrete") or self:DoesAcceptResource("Food") or self:DoesAcceptResource("PreciousMetals")) then
            if self:DoesAcceptResource("Concrete") then
                if sum then
                    stored = stored + self:GetStored_Concrete()
                    max = max + self:GetMaxAmount_Concrete()
                else
                    return T(9731, "<concrete(Stored_Concrete, MaxAmount_Concrete)>")
                end
            end
            if self:DoesAcceptResource("Food") then
                if sum then
                    stored = stored + self:GetStored_Food()
                    max = max + self:GetMaxAmount_Food()
                else
                    return T(9732, "<food(Stored_Food, MaxAmount_Food)>")
                end
            end
            if self:DoesAcceptResource("PreciousMetals") then
                if sum then
                    stored = stored + self:GetStored_PreciousMetals()
                    max = max + self:GetMaxAmount_PreciousMetals()
                else
                    return T(9733, "<preciousmetals(Stored_PreciousMetals, MaxAmount_PreciousMetals)>")
                end
            end
            if self:DoesAcceptResource("Metals") then
                if sum then
                    stored = stored + self:GetStored_Metals()
                    max = max + self:GetMaxAmount_Metals()
                else
                    return T(9734, "<metals(Stored_Metals, MaxAmount_Metals)>")
                end
            end
        end
        if (self:DoesAcceptResource("Polymers") or self:DoesAcceptResource("Electronics") or self:DoesAcceptResource("MachineParts") or self:DoesAcceptResource("Fuel") or self:DoesAcceptResource("MysteryResource")) then
            if self:DoesAcceptResource("Polymers") then
                if sum then
                    stored = stored + self:GetStored_Polymers()
                    max = max + self:GetMaxAmount_Polymers()
                else
                    return T(9735, "<polymers(Stored_Polymers, MaxAmount_Polymers)>")
                end
            end
            if self:DoesAcceptResource("Electronics") then
                if sum then
                    stored = stored + self:GetStored_Electronics()
                    max = max + self:GetMaxAmount_Electronics()
                else
                    return T(9736, "<electronics(Stored_Electronics, MaxAmount_Electronics)>")
                end
            end
            if self:DoesAcceptResource("MachineParts") then
                if sum then
                    stored = stored + self:GetStored_MachineParts()
                    max = max + self:GetMaxAmount_MachineParts()
                else
                    return T(9737, "<machineparts(Stored_MachineParts, MaxAmount_MachineParts)>")
                end
            end
            if self:DoesAcceptResource("Fuel") then
                if sum then
                    stored = stored + self:GetStored_Fuel()
                    max = max + self:GetMaxAmount_Fuel()
                else
                    return T(9738, "<fuel(Stored_Fuel, MaxAmount_Fuel)>")
                end
            end
            if self:DoesAcceptResource("MysteryResource") then
                if sum then
                    stored = stored + self:GetStored_MysteryResource()
                    max = max + self:GetMaxAmount_MysteryResource()
                else
                    return T(9739, "<mysteryresource(Stored_MysteryResource, MaxAmount_MysteryResource)>")
                end
            end
        end
        if self:DoesAcceptResource("Meat") then
            if sum then
                stored = stored + self:GetStored_Meat()
                max = max + self:GetMaxAmount_Meat()
            else
                return T("<meat(Stored_Meat, MaxAmount_Meat)>")
            end
        end
        if self:DoesAcceptResource("Booze") then
            if sum then
                stored = stored + self:GetStored_Booze()
                max = max + self:GetMaxAmount_Booze()
            else
                return T("<booze(Stored_Booze, MaxAmount_Booze)>")
            end
        end
        if self:DoesAcceptResource("Crystals") then
            if sum then
                stored = stored + self:GetStored_Crystals()
                max = max + self:GetMaxAmount_Crystals()
            else
                return T("<crystals(Stored_Crystals, MaxAmount_Crystals)>")
            end
        end
        if self:DoesAcceptResource("Radioactive") then
            if sum then
                stored = stored + self:GetStored_Radioactive()
                max = max + self:GetMaxAmount_Radioactive()
            else
                return T("<radioactive(Stored_Radioactive, MaxAmount_Radioactive)>")
            end
        end
        if UICity:IsTechResearched("MartianVegetation") and self:DoesAcceptResource("Seeds") then
            if sum then
                stored = stored + self:GetStored_Seeds()
                max = max + self:GetMaxAmount_Seeds()
            else
                return Untranslated("<seeds(Stored_Seeds, MaxAmount_Seeds)>")
            end
        end
        return T{9740, "<stored>/<max>", stored = FormatResourceValueMaxResource(empty_table,stored), max = FormatResourceValueMaxResource(empty_table, max), empty_table}
    elseif self:IsKindOf("ElectricityStorage") then
        return  T(9741, "<power(StoredPower, capacity)>")
    elseif self:IsKindOf("AirStorage") then
        return  T(9742, "<air(StoredAir, air_capacity)>")
    elseif self:IsKindOf("WaterStorage") then
        return  T(9743, "<water(StoredWater, water_capacity)>")
    elseif self:IsKindOf("WastewaterStorage") then
        return T("<wastewater(StoredWastewater, wastewater_capacity)>")
    elseif self:IsKindOf("HydrocarbonStorage") then
        return T("<hydrocarbon(StoredHydrocarbon, hydrocarbon_capacity)>")
    elseif self:IsKindOf("WasteRockDumpSite") then
        return T(9744, "<wasterock(Stored_WasteRock, MaxAmount_WasteRock)>")
    elseif self:IsKindOf("Workplace") then
        local count, max = 0, 0
        if self.active_shift > 0 then --single shift building
            count = #self.workers[self.active_shift]
            max = self.max_workers
        else
            for i = 1, self.max_shifts do
                count = count + #self.workers[i]
            end
            max = self.max_shifts * self.max_workers
        end
        return T{9772, "<colonist(count, max)>", count = count, max = max, self}
    elseif self:IsKindOf("TrainingBuilding") then
        local count, max = 0, 0
        if self.active_shift > 0 then --single shift building
            count = #self.visitors[self.active_shift]
            max = self.max_visitors
        else
            for i = 1, self.max_shifts do
                count = count + #self.visitors[i]
            end
            max = self.max_shifts * self.max_visitors
        end
        return T{9772, "<colonist(count, max)>", count = count, max = max, self}
    end
    
    return ""
end

local GetUIProductionTexts_Building = Building.GetUIProductionTexts
function Building:GetUIProductionTexts(items, short)
    local items = GetUIProductionTexts_Building(self, items, short)
    if self:IsKindOf("HydrocarbonProducer") then
        items[#items + 1] = self:GetHydrocarbonProductionText(short)
        if not short and self:IsKindOf("ResourceProducer") and self.wasterock_producer then
            items[#items + 1] = T("Stored Waste Rock<right><wasterock(GetWasterockAmountStored,wasterock_max_storage)>")
        end
    end
    return items
end

local GetOverviewInfo_Building = Building.GetOverviewInfo
function Building:GetOverviewInfo()
    if self:IsKindOf("WastewaterStorage") or self:IsKindOf("HydrocarbonStorage") then
        local rows = {self.description .. T("<newline>")}
        rows = self:GetUIProductionTexts(rows)
        local res = self:GetUIConsumptionTexts()
        if self:IsKindOf("WastewaterStorage") then
            rows[#rows + 1] = T("<center><em>Wastewater Storage</em>")
            rows[#rows + 1] = self.wastewater:GetUIMode()
            rows[#rows + 1] = T("Stored Wastewater<right><wastewater(StoredWastewater)>")
            rows[#rows + 1] = T("Capacity<right><wastewater(wastewater_capacity)>")
            rows[#rows + 1] = T("Max output<right><wastewater(max_wastewater_discharge)>")
        end
        if self:IsKindOf("HydrocarbonStorage") then
            rows[#rows + 1] = T("<center><em>Hydrocarbon Storage</em>")
            rows[#rows + 1] = self.hydrocarbon:GetUIMode()
            rows[#rows + 1] = T("Stored Water<right><hydrocarbon(StoredHydrocarbon)>")
            rows[#rows + 1] = T("Capacity<right><hydrocarbon(hydrocarbon_capacity)>")
            rows[#rows + 1] = T("Max output<right><hydrocarbon(max_hydrocarbon_discharge)>")
            local hydrocarbon_grid = self.hydrocarbon and self.hydrocarbon.grid 
                or self:IsKindOf("LifeSupportGridElement") and self.pillar and self.water and self.water.grid and self.water.grid.hydrocarbon_grid
            if hydrocarbon_grid then
                rows[#rows + 1] = T("<center><em>Hydrocarbon Grid</em>")
                rows[#rows + 1] = T{"Hydrocarbon production<right><hydrocarbon(current_production)>", current_production = hydrocarbon_grid.current_production, hydrocarbon_grid}
                rows[#rows + 1] = T{"Total demand<right><hydrocarbon(consumption)>", consumption = hydrocarbon_grid.consumption, hydrocarbon_grid}
                rows[#rows + 1] = T{"Stored Hydrocarbon<right><hydrocarbon(current_storage)>", current_storage = hydrocarbon_grid.current_storage, hydrocarbon_grid}
            end
        end
        local warning = self:GetUIWarning()
        if warning then
            rows[#rows + 1] = "<center>" .. T(47, "<red>Warning</red>")
            rows[#rows + 1] = warning
        end
        return #rows > 0 and table.concat(rows, "<newline><left>") or ""
    end
    if (IsKindOf(self, "UniversalStorageDepot") or IsKindOf(self, "MechanizedDepot")) and not self:IsKindOf("SupplyRocket") and not IsKindOf(self, "SpaceElevator") then
        local rows = {self.description .. T("<newline>")}
        rows = self:GetUIProductionTexts(rows)
        local res = self:GetUIConsumptionTexts()
        if (self:DoesAcceptResource("Metals") or self:DoesAcceptResource("Concrete") or self:DoesAcceptResource("PreciousMetals") or self:DoesAcceptResource("Crystals") or self:DoesAcceptResource("Radioactive")) then
            rows[#rows + 1] = T(9726, "<center><em>Basic Resources</em>")
            if self:DoesAcceptResource("Concrete") then
                rows[#rows + 1] = T(497, "<resource('Concrete' )><right><concrete(Stored_Concrete, MaxAmount_Concrete)>")
            end
            if self:DoesAcceptResource("Metals") then
                rows[#rows + 1] = T(496, "<resource('Metals' )><right><metals(Stored_Metals, MaxAmount_Metals)>")
            end            
            if self:DoesAcceptResource("PreciousMetals") then
                rows[#rows + 1] = T(499, "<resource('PreciousMetals' )><right><preciousmetals(Stored_PreciousMetals, MaxAmount_PreciousMetals)>")
            end
            if self:DoesAcceptResource("Crystals") then
                rows[#rows + 1] = T("<resource('Crystals' )><right><crystals(Stored_Crystals, MaxAmount_Crystals)>")
            end
            if self:DoesAcceptResource("Radioactive") then
                rows[#rows + 1] = T("<resource('Radioactive' )><right><radioactive(Stored_Radioactive, MaxAmount_Radioactive)>")
            end
        end
        if (self:DoesAcceptResource("Polymers") or self:DoesAcceptResource("Electronics") or self:DoesAcceptResource("MachineParts") or self:DoesAcceptResource("Fuel") or self:DoesAcceptResource("MysteryResource")) then
            rows[#rows + 1] = T(9727, "<center><em>Advanced Resources</em>")
            if self:DoesAcceptResource("Polymers") then
                rows[#rows + 1] = T(502, "<resource('Polymers' )><right><polymers(Stored_Polymers, MaxAmount_Polymers)>")
            end
            if self:DoesAcceptResource("Electronics") then
                rows[#rows + 1] = T(503, "<resource('Electronics' )><right><electronics(Stored_Electronics, MaxAmount_Electronics)>")
            end
            if self:DoesAcceptResource("MachineParts") then
                rows[#rows + 1] = T(504, "<resource('MachineParts' )><right><machineparts(Stored_MachineParts, MaxAmount_MachineParts)>")
            end
            if self:DoesAcceptResource("Fuel") then
                rows[#rows + 1] = T(505, "<resource('Fuel' )><right><fuel(Stored_Fuel, MaxAmount_Fuel)>")
            end
            if self:DoesAcceptResource("MysteryResource") then
                rows[#rows + 1] = T(8671, "<resource('MysteryResource' )><right><mysteryresource(Stored_MysteryResource, MaxAmount_MysteryResource)>")
            end
        end
        if (self:DoesAcceptResource("Food") or self:DoesAcceptResource("Meat") or self:DoesAcceptResource("Booze")) then
            rows[#rows + 1] = T("<center><em>Life Support Resources</em>")
            if self:DoesAcceptResource("Food") then
                rows[#rows + 1] = T("<resource('Food' )><right><food(Stored_Food, MaxAmount_Food)>")
            end
            if self:DoesAcceptResource("Meat") then
                rows[#rows + 1] = T("<resource('Meat' )><right><meat(Stored_Meat, MaxAmount_Meat)>")
            end
            if self:DoesAcceptResource("Booze") then
                rows[#rows + 1] = T("<resource('Booze' )><right><booze(Stored_Booze, MaxAmount_Booze)>")
            end
        end
        if UICity:IsTechResearched("MartianVegetation") and self:DoesAcceptResource("Seeds") then
            rows[#rows + 1] = Untranslated("<center><em>") .. T(12476, "Terraforming") .. TLookupTag("</em>")
            rows[#rows + 1] = T(11843, "Seeds") .. Untranslated("<right><seeds(Stored_Seeds, MaxAmount_Seeds)>")
        end
        local warning = self:GetUIWarning()
        if warning then
            rows[#rows + 1] = "<center>" .. T(47, "<red>Warning</red>")
            rows[#rows + 1] = warning
        end
        return #rows > 0 and table.concat(rows, "<newline><left>") or ""
    end
    return GetOverviewInfo_Building(self)
end

local GetCommandCenterLifeSupportGrids_Vanilla = GetCommandCenterLifeSupportGrids
function GetCommandCenterLifeSupportGrids(context)
    local all = UICity.water
    local no_oil = {}
    for i,v in ipairs(all or {}) do        
        if v.air_grid then
            table.insert(no_oil, v)
        end
    end
    UICity.water = no_oil
    local result = GetCommandCenterLifeSupportGrids_Vanilla(context)
    UICity.water = all
    return result
end

function GetCommandCenterHydrocarbonGrids(context)
  local result = {}
  local all_grids = UICity.hydrocarbon
  for i, entry in ipairs(all_grids) do
    if #entry.storages > 0 or 0 < #entry.producers then
      table.insert(result, entry)
    elseif 0 < #entry.consumers then
      local only_autonomous = true
      for j, consumer in ipairs(entry.consumers) do
        if IsKindOf(consumer.building, "ElectricityConsumer") and consumer.building.disable_electricity_consumption == 0 then
          only_autonomous = false
          break
        end
      end
      if not only_autonomous then
        table.insert(result, entry)
      end
    end
  end
  return result
end

function CommandCenterChooseHydrocarbonGridBuilding(context)
    local combined_grid = { }
    combined_grid.consumers = context.consumers
    combined_grid.producers = context.producers
    combined_grid.elements  = context.elements
    return CommandCenterChooseGridBuilding(combined_grid)
end

function OnMsg.ClassesBuilt()
    PlaceObj('XTemplate', {
        group = "CCC",
        id = "HydrocarbonGridsOverview",
        PlaceObj('XTemplateTemplate', {
            '__context', function (parent, context) return context or {} end,
            '__template', "NewOverlayDlg",
            'MinWidth', 500,
            'InitialMode', "hydrocarbon",
            'InternalModes', "hydrocarbon",
        }, {
            PlaceObj('XTemplateTemplate', {
                '__template', "CommandCenterTitle",
                'Title', T("HYDROCARBON GRIDS"),
            }),
            PlaceObj('XTemplateWindow', {
                'comment', "content",
                '__class', "XContentTemplate",
                'Id', "idContent",
                'IdNode', false,
                'Margins', box(15, 0, 0, 0),
            }, {
                PlaceObj('XTemplateWindow', {
                    '__class', "XSizeConstrainedWindow",
                }, {
                    PlaceObj('XTemplateWindow', {
                        'Margins', box(103, 0, 0, 15),
                        'Dock', "top",
                        'LayoutMethod', "HList",
                    }, {
                        PlaceObj('XTemplateWindow', {
                            'comment', "name",
                            '__class', "XLabel",
                            'Margins', box(60, 0, 0, 0),
                            'VAlign', "top",
                            'MinWidth', 250,
                            'MaxWidth', 250,
                            'TextStyle', "OverviewItemSection",
                            'Translate', true,
                            'Text', T(550311643207, "Name"),
                        }),
                        PlaceObj('XTemplateWindow', {
                            'comment', "production",
                            '__class', "XText",
                            'MinWidth', 200,
                            'MaxWidth', 200,
                            'TextStyle', "OverviewItemSection",
                            'Translate', true,
                            'Text', T(80, "Production"),
                            'TextHAlign', "center",
                        }),
                        PlaceObj('XTemplateWindow', {
                            'comment', "consumption",
                            '__class', "XText",
                            'MinWidth', 200,
                            'MaxWidth', 200,
                            'TextStyle', "OverviewItemSection",
                            'Translate', true,
                            'Text', T(347, "Consumption"),
                            'TextHAlign', "center",
                        }),
                        PlaceObj('XTemplateWindow', {
                            'comment', "stored power",
                            '__class', "XText",
                            'MinWidth', 200,
                            'MaxWidth', 200,
                            'TextStyle', "OverviewItemSection",
                            'Translate', true,
                            'Text', T(359672804540, "Stored Resources"),
                            'TextHAlign', "center",
                        }),
                        PlaceObj('XTemplateWindow', {
                            'comment', "change",
                            '__class', "XText",
                            'MinWidth', 200,
                            'MaxWidth', 200,
                            'TextStyle', "OverviewItemSection",
                            'Translate', true,
                            'Text', T(655663611091, "Change"),
                            'TextHAlign', "center",
                        }),
                        }),
                    PlaceObj('XTemplateWindow', {
                        '__class', "XSizeConstrainedWindow",
                    }, {
                        PlaceObj('XTemplateTemplate', {
                            '__template', "ScrollbarNew",
                            'Id', "idScroll",
                            'ZOrder', 0,
                            'Margins', box(87, 0, 0, 0),
                            'Target', "idList",
                        }),
                        PlaceObj('XTemplateWindow', {
                            '__class', "XContentTemplateList",
                            'Id', "idList",
                            'BorderWidth', 0,
                            'Padding', box(0, 0, 0, 0),
                            'Background', RGBA(255, 255, 255, 0),
                            'FocusedBackground', RGBA(255, 255, 255, 0),
                            'VScroll', "idScroll",
                            'MouseScroll', true,
                            'OnContextUpdate', function (self, context, ...)
                                for _, child in ipairs(self) do
                                    child:OnContextUpdate(context, ...)
                                end
                                XContextWindow.OnContextUpdate(self, context, ...)
                            end,
                        }, {
                            PlaceObj('XTemplateForEach', {
                                'comment', "hydrocarbon grids",
                                'array', function (parent, context) return GetCommandCenterHydrocarbonGrids(context) end,
                                '__context', function (parent, context, item, i, n) return item end,
                                'run_after', function (child, context, item, i, n)
                                    child.idTitle:SetText(T{11629, "GRID <i>", i = i})
                                    child.idButtonIcon:SetImage(CurrentModPath.."UI/Icons/Sections/Hydrocarbon_1.tga")
                                    child.idButtonIcon:SetColumns(1)
                                end,
                            }, {
                                PlaceObj('XTemplateTemplate', {
                                    '__template', "HydrocarbonGridsOverviewRow",
                                }),
                                }),
                            PlaceObj('XTemplateCode', {
                                'run', function (self, parent, context)
                                    if GetUIStyleGamepad() and #parent > 0 then
                                        parent[1]:SetFocus()
                                    end
                                end,
                            }),
                            }),
                        PlaceObj('XTemplateWindow', {
                            '__class', "XText",
                            'Id', "idNoResults",
                            'Dock', "box",
                            'HAlign', "center",
                            'VAlign', "center",
                            'Visible', false,
                            'HandleMouse', false,
                            'TextStyle', "InGameTitle",
                            'Translate', true,
                            'Text', T(12189, "No objects to show."),
                        }),
                        PlaceObj('XTemplateCode', {
                            'run', function (self, parent, context)
                                local list = parent:ResolveId("idList")
                                parent:ResolveId("idNoResults"):SetVisible(#list == 0)
                            end,
                        }),
                        }),
                    }),
                }),
            PlaceObj('XTemplateAction', {
                'ActionId', "back",
                'ActionName', T(4254, "BACK"),
                'ActionToolbar', "ActionBar",
                'ActionShortcut', "Escape",
                'ActionGamepad', "ButtonB",
                'OnActionEffect', "back",
            }),
        }),
    })

    PlaceObj('XTemplate', {
        __is_kind_of = "XContextControl",
        group = "CCC",
        id = "HydrocarbonGridsOverviewRow",
        PlaceObj('XTemplateTemplate', {
            '__template', "CommandCenterRow",
            'RolloverText', T("Total production, consumption and storage of Hydrocarbon in the grid."),
            'RolloverTitle', T("Hydrocarbon"),
            'RolloverHint', T(12110, "<left_click><left_click> Show all buildings in the grid"),
            'RolloverHintGamepad', T(12111, "<ButtonA> Show all buildings in the grid"),
            'Id', "",
        }, {
            PlaceObj('XTemplateWindow', {
                '__class', "XText",
                'Id', "idTitle",
                'Margins', box(10, 0, 0, 0),
                'Padding', box(0, 0, 0, 0),
                'HAlign', "left",
                'VAlign', "center",
                'MinWidth', 250,
                'MaxWidth', 250,
                'TextStyle', "OverviewItemName",
                'Translate', true,
                'Shorten', true,
                'TextVAlign', "center",
            }),
            PlaceObj('XTemplateWindow', {
                'comment', "production",
                'MinWidth', 200,
                'MaxWidth', 200,
            }, {
                PlaceObj('XTemplateWindow', {
                    'HAlign', "center",
                    'LayoutMethod', "HList",
                }, {
                    PlaceObj('XTemplateWindow', {
                        'comment', "hydrocarbon",
                        '__context', function (parent, context) return context end,
                        '__class', "XText",
                        'Id', "idProductionHydrocarbon",
                        'Padding', box(0, 0, 0, 0),
                        'VAlign', "center",
                        'TextStyle', "OverviewItemValue",
                        'Translate', true,
                        'Text', T("<hydrocarbon(CurrentProduction)>"),
                        'WordWrap', false,
                        'TextHAlign', "center",
                        'TextVAlign', "center",
                    }),                    
                    }),
                }),
            PlaceObj('XTemplateWindow', {
                'comment', "consumption",
                'MinWidth', 200,
                'MaxWidth', 200,
            }, {
                PlaceObj('XTemplateWindow', {
                    'HAlign', "center",
                    'LayoutMethod', "HList",
                }, {
                    PlaceObj('XTemplateWindow', {
                        'comment', "hydrocarbon",
                        '__context', function (parent, context) return context end,
                        '__class', "XText",
                        'Id', "idConsumptionHydrocarbonr",
                        'Padding', box(0, 0, 0, 0),
                        'VAlign', "center",
                        'TextStyle', "OverviewItemValue",
                        'Translate', true,
                        'Text', T("<hydrocarbon(CurrentConsumption)>"),
                        'WordWrap', false,
                        'TextHAlign', "center",
                        'TextVAlign', "center",
                    }),
                    }),
                }),
            PlaceObj('XTemplateWindow', {
                'comment', "stored",
                'MinWidth', 200,
                'MaxWidth', 200,
            }, {
                PlaceObj('XTemplateWindow', {
                    'HAlign', "center",
                    'LayoutMethod', "HList",
                }, {
                    PlaceObj('XTemplateWindow', {
                        'comment', "hydrocarbon",
                        '__context', function (parent, context) return context end,
                        '__class', "XText",
                        'Id', "idStoredPowerHydrocarbon",
                        'Padding', box(0, 0, 0, 0),
                        'VAlign', "center",
                        'TextStyle', "OverviewItemValue",
                        'Translate', true,
                        'Text', T("<hydrocarbon(CurrentStorage)>"),
                        'WordWrap', false,
                        'TextHAlign', "center",
                        'TextVAlign', "center",
                    }),
                    }),
                }),
            PlaceObj('XTemplateWindow', {
                'comment', "change",
                'MinWidth', 200,
                'MaxWidth', 200,
            }, {
                PlaceObj('XTemplateWindow', {
                    'HAlign', "center",
                    'LayoutMethod', "HList",
                }, {
                    PlaceObj('XTemplateWindow', {
                        'comment', "hydrocarbon",
                        '__context', function (parent, context) return context end,
                        '__class', "XText",
                        'Id', "idChangeHydrocarbon",
                        'Padding', box(0, 0, 0, 0),
                        'VAlign', "center",
                        'TextStyle', "OverviewItemValue",
                        'ContextUpdateOnOpen', true,
                        'OnContextUpdate', function (self, context, ...)
                            local delta = context:GetCurrentProduction() - context:GetCurrentConsumption()
                            if delta < 0 then
                                self:SetText(T{"<red><hydrocarbon(delta)></red>", delta = delta})
                            elseif delta > 0 then
                                self:SetText(T{"+<hydrocarbon(delta)>", delta = delta})
                            else
                                self:SetText(T{"<hydrocarbon(delta)>", delta = delta}) --zero
                            end
                        end,
                        'Translate', true,
                        'Text', T("<hydrocarbon(0)>"),
                        'WordWrap', false,
                        'TextHAlign', "center",
                        'TextVAlign', "center",
                    }),
                    }),
                }),
            PlaceObj('XTemplateFunc', {
                'name', "OnMouseButtonDoubleClick(self, pos, button)",
                'func', function (self, pos, button)
                    if button == "L" then
                        if GetDialog("PlanetaryView") then return end
                        local bld = CommandCenterChooseHydrocarbonGridBuilding(self.context)
                        ViewObjectMars(bld)
                        SelectObj(bld)
                        DelayedCall(1, ShowHydrocarobnOverlay)
                        CloseCommandCenter()
                        return "break"
                    end
                end,
            }),
            PlaceObj('XTemplateFunc', {
                'name', "SetSelected(self, selected)",
                'func', function (self, selected)
                    self:SetFocus(selected)
                end,
            }),
            PlaceObj('XTemplateFunc', {
                'name', "OnSetFocus",
                'func', function (self, ...)
                    XCreateRolloverWindow(self, true)
                    XTextButton.OnSetFocus(self, ...)
                end,
            }),
        }),
    })

    Presets.ColonyControlCenterCategory.Default.HydrocarbonGrids = PlaceObj('ColonyControlCenterCategory', {
        SortKey = 7500,
        display_name = T("Hydrocarbon Grids"),
        group = "Default",
        id = "HydrocarbonGrids",
        template_name = "HydrocarbonGridsOverview",
        title = T("HYDROCARBON GRIDS"),
    })
    while true do
        local idx = table.find(Presets.ColonyControlCenterCategory.Default, 'id', "HydrocarbonGrids")
        if idx then table.remove(Presets.ColonyControlCenterCategory.Default, idx) else break end
    end
    local idx = table.find(Presets.ColonyControlCenterCategory.Default, 'id', "LifeSupportGrids")
    if not idx then idx = 7 end
    table.insert(Presets.ColonyControlCenterCategory.Default, idx+1, Presets.ColonyControlCenterCategory.Default.HydrocarbonGrids)


    local categories = XTemplates.ColonyControlCenter[1][5]
    local idx = table.find(categories, 'mode', "HydrocarbonGrids")
    if idx then table.remove(categories, idx) end
    categories[#categories+1] = PlaceObj('XTemplateMode', {
            'mode', "HydrocarbonGrids",
        }, {
            PlaceObj('XTemplateTemplate', {
                '__template', "HydrocarbonGridsOverview",
            }),
        }
    )
end

local OverlayRemWaterMask = 15

GlobalVar("LHOverlaySupplyGrid", function()
    if HexMapWidth == 0 or HexMapHeight == 0 then return false end
    return NewGrid(HexMapWidth, HexMapHeight, 8, 0)
end)

local ApplyIDToOverlayGrid_Vanilla
local CopySupplyFragmentToOverlayGrid_Vanilla
function OnMsg.ModsReloaded()
    if format_value(CopySupplyFragmentToOverlayGrid)=="[C](-1)" then
        CopySupplyFragmentToOverlayGrid_Vanilla = CopySupplyFragmentToOverlayGrid
        CopySupplyFragmentToOverlayGrid = function (overlay_grid, conn_grid, grids_merged, ...)
            if overlay_grid~=OverlaySupplyGrid then
                CopySupplyFragmentToOverlayGrid_Vanilla(overlay_grid, conn_grid, grids_merged, ...)
            elseif grids_merged.supply_resource == "hydrocarbon" or grids_merged.hydrocarbon_grid then
                CopySupplyFragmentToOverlayGrid_Vanilla(LHOverlaySupplyGrid, conn_grid, grids_merged, ...)
            else
                CopySupplyFragmentToOverlayGrid_Vanilla(OverlaySupplyGrid, conn_grid, grids_merged, ...)
            end
        end
    end
    if format_value(ApplyIDToOverlayGrid)=="[C](-1)" then
        ApplyIDToOverlayGrid_Vanilla = ApplyIDToOverlayGrid
        ApplyIDToOverlayGrid = function (overlay_grid, some, shape, id, band, ...)
            if overlay_grid~=OverlaySupplyGrid then
                ApplyIDToOverlayGrid_Vanilla(overlay_grid, some, shape, id, band, ...)
            elseif band then
                ApplyIDToOverlayGrid_Vanilla(LHOverlaySupplyGrid, some, shape, id, band, ...)
                ApplyIDToOverlayGrid_Vanilla(OverlaySupplyGrid, some, shape, id, band, ...)
            elseif some.oil then
                ApplyIDToOverlayGrid_Vanilla(LHOverlaySupplyGrid, some, shape, id, band, ...)
                ApplyIDToOverlayGrid_Vanilla(OverlaySupplyGrid, some, shape, OverlayRemWaterMask, "band", ...)
            else
                ApplyIDToOverlayGrid_Vanilla(OverlaySupplyGrid, some, shape, id, band, ...)
                ApplyIDToOverlayGrid_Vanilla(LHOverlaySupplyGrid, some, shape, OverlayRemWaterMask, "band", ...)
            end
        end
    end
end

function ToggleHydrocarbonOverlay()
    if show_overlay == "hydrocarbon" then
        HideOverlay()
    else
        ShowHydrocarobnOverlay()
    end
end

function ShowHydrocarobnOverlay()
    show_overlay = "hydrocarbon"
    RefreshSupplyGridOverlay(LHOverlaySupplyGrid)    
end

local showHC
local RefreshSupplyGridOverlay_Exec_Vanilla = RefreshSupplyGridOverlay_Exec
function RefreshSupplyGridOverlay_Exec()
    if show_overlay == "hydrocarbon" then
        show_overlay = "water" 
        local tmp = OverlaySupplyGrid
        OverlaySupplyGrid = LHOverlaySupplyGrid
        RefreshSupplyGridOverlay_Exec_Vanilla()
        show_overlay = "hydrocarbon" 
        OverlaySupplyGrid = tmp     
    else
        RefreshSupplyGridOverlay_Exec_Vanilla()
    end
end