function OnMsg.LoadGame()
    if IsDlcAvailable("prunariu") and not IsDlcAccessible("prunariu") then
        Presets.BuildingTemplate.Infrastructure.StationSmall.save_in = false
        Presets.BuildingTemplate.Infrastructure.StationBig.save_in = false
        Presets.BuildingTemplate.Infrastructure.Track.save_in = false
        BuildingTechRequirements.StationBig = false
    end
end

local vanilla_IsTechResearched = IsTechResearched
function IsTechResearched(tech_id)
    if IsDlcAvailable("prunariu") and not IsDlcAccessible("prunariu")
            and (tech_id == "UndergroundTrains" or tech_id == "BigStations") then
        return true
    end
    return vanilla_IsTechResearched(tech_id)
end