function OnMsg.ClassesPostprocess()
	if Presets.POI.Outposts and Presets.POI.Outposts.ForestationOutpost then
		return
	end
	local speedup = 1
	PlaceObj("POI", {
		PrerequisiteToCreate = function(self, city, idx)
            city = city or MainCity
			return city.colony:IsTechResearched(self.activation_by_tech_research)
		end,
		activation_by_tech_research = "PlanetaryProjects",
		description = T{"Remote, self sufficent, manned outpost"},
		display_icon = CurrentModPath .. "UI/Icons/outpost_forestation_0.tga",
		display_name = T{"Forestation Outpost"},
		group = "Outposts",
		id = "ForestationOutpost",
		mission_id = "ForestationOutpost_1",
		dismantle_id = "ForestationOutpost_0",
		is_orbital = false,
		max_projects_of_type = 3,
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		PrerequisiteToCreate = function(self, city, idx)
            city = city or MainCity
            return city.colony:IsTechResearched(self.activation_by_tech_research)
		end,
		activation_by_tech_research = "PlanetaryProjects",
		description = T{"The outpost is being dismantled"},
		display_icon = CurrentModPath .. "UI/Icons/outpost_forestation_0.tga",
		display_name = T{"Dismantle outpost"},
		expedition_time = 720000/speedup,
		group = "OutpostMissions",
		id = "ForestationOutpost_0",		
		is_orbital = false,
		max_projects_of_type = 3,		
		recover_drones = true,
		recover_crew = true,
		dismantle = "ForestationOutpost",
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		PrerequisiteToCreate = function(self, city, idx)
            city = city or MainCity
            return city.colony:IsTechResearched(self.activation_by_tech_research)
		end,
		activation_by_tech_research = "PlanetaryProjects",
		description = T{"Send resources to build the infrastructure"},
		display_icon = CurrentModPath .. "UI/Icons/outpost_forestation_0.tga",
		display_name = T{"Build outpost"},
		expedition_time = 1440000/speedup,
		group = "OutpostMissions",
		id = "ForestationOutpost_1",
		next_id = "ForestationOutpost_2",
		is_orbital = false,
		max_projects_of_type = 3,
		num_drones = 10,
		leave_drones = true,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Concrete",
				"amount", 90000/speedup
			}),
			PlaceObj("ResourceAmount", {
				"resource", "Metals",
				"amount", 60000/speedup
			}),
			PlaceObj("ResourceAmount", {
				"resource", "Polymers",
				"amount", 30000/speedup
			}),
			PlaceObj("ResourceAmount", {
				"resource", "MachineParts",
				"amount", 10000/speedup
			}),
			 PlaceObj("ResourceAmount", {
				"resource", "Electronics",
				"amount", 10000/speedup
			})
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		PrerequisiteToCreate = function(self, city, idx)
            city = city or MainCity
            return city.colony:IsTechResearched(self.activation_by_tech_research)
		end,
		activation_by_tech_research = "PlanetaryProjects",
		description = T{"Assign colonists to outpost"},
		display_icon = CurrentModPath .. "UI/Icons/outpost_forestation_1.tga",
		display_name = T{"Send specialists"},
		expedition_time = 720000/speedup,
		group = "OutpostMissions",
		id = "ForestationOutpost_2",
		next_id = "ForestationOutpost_3",
		is_orbital = false,
		max_projects_of_type = 3,
		num_crew = 5,
		crew_specialization = "botanist",
		leave_crew = true,
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		PrerequisiteToCreate = function(self, city, idx)
            city = city or MainCity
            return city.colony:IsTechResearched(self.activation_by_tech_research)
		end,
		activation_by_tech_research = "PlanetaryProjects",
		description = T{"Assign colonists to outpost"},
		display_icon = CurrentModPath .. "UI/Icons/outpost_forestation_1.tga",
		display_name = T{"Send specialists"},
		expedition_time = 720000/speedup,
		group = "OutpostMissions",
		id = "ForestationOutpost_re",
		next_id = "ForestationOutpost_3",
		is_orbital = false,
		max_projects_of_type = 3,
		need_crew = 8,
		crew_specialization = "botanist",
		leave_crew = true,
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		PrerequisiteToCreate = function(self, city, idx)
            city = city or MainCity
            return city.colony:IsTechResearched(self.activation_by_tech_research) and GetTerraformParamPct("Temperature") >= 0 and GetTerraformParamPct("Water")  >= 0 and GetTerraformParamPct("Vegetation") < 100
		end,
		activation_by_tech_research = "PlanetaryProjects",
		description = T{"Increases the global vegetation"},
		display_icon = CurrentModPath .. "UI/Icons/outpost_forestation_2.tga",
		display_name = T{"Plant vegetation"},
		expedition_time = 720000/speedup,
		expedition_timeout = 2880000/speedup,
		group = "OutpostMissions",
		id = "ForestationOutpost_3",
		next_id = "ForestationOutpost_3",
		refill_id = "ForestationOutpost_re",
		is_orbital = false,
		max_projects_of_type = 3,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Seeds",
				"amount", 80000/speedup
			})
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
		terraforming_changes = {
			PlaceObj("TerraformingParamAmount", {
				"param", "Vegetation",
				"amount", 2000
			})
		}
	})

	if(table.find(XTemplates.POIAdditionalContent,"ActionId","create_outpost")) then
		return
	end
	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateAction", {
			"__condition",
			function(parent, context)
				local poi = Presets.POI.Outposts.ForestationOutpost
				return not UIColony.ForestationOutpost and not context.selected_spot and poi:PrerequisiteToCreate()
			end,
			"ActionId", "create_outpost",
			"ActionName", T{"CREATE FORESTATIOM OUTPOST"},
			"ActionToolbar", "ActionBar",
			"ActionGamepad", "LeftTrigger",
			"OnAction", function(self, host, source)  
				local project = Presets.POI.Outposts.ForestationOutpost	      
				local obj = MarsSpecialOutpost:new({
					id = project.id .. (g_SpecialProjectSpawnNextIdx[poi_id] or ""),
					project_id = project.id,
					longitude = host.context.long / 60,
					latitude = host.context.lat / 60,
					is_orbital = project.is_orbital,
					display_name = project.display_name,
					description = project.description,
					consume_rocket = project.consume_rocket,
					expedition_time = project.expedition_time,
					num_drones = project.num_drones,
					rover_type = project.rover_type,
					num_crew = project.num_crew,
					crew_specialization = project.crew_specialization,
					mission_id = project.mission_id,
				})
                UIColony.ForestationOutpost = obj
				SortedMarsScreenLandingSpots = nil		
				CloseDialog("PlanetaryView")
				OpenDialog("PlanetaryView")
			end
		})
	)	
end
