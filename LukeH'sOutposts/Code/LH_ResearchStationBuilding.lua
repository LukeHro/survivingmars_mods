local LH_SpaceStationComponents

function OnMsg.ClassesPostprocess()
	LH_SpaceStationComponents = {}
	local attach = PlaceObj('ModItemAttachment', {
		'name', "Skel",
		'comment', "phase1",
		'Parent', "LH_SpaceStation",
		'Child', "HydroponicFarm",
		'AttachSpot', "Origin",
		'AttachScale', 90,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Hull",
		'comment', "phase2",
		'Parent', "LH_SpaceStation",
		'Child', "SecurityCenter",
		'AttachSpot', "Origin",
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Reactor",
		'comment', "phase3",
		'Parent', "LH_SpaceStation",
		'Child', "FusionReactor",
		'AttachSpot', "Origin",
		'AttachOffset', point(0, 570, 0),
		'AttachRotate', point(10800, 0, 5400),
		'AttachScale', 45,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Arm1",
		'comment', "phase4",
		'Parent', "LH_SpaceStation",
		'Child', "MedicalCenter",
		'AttachSpot', "Origin",
		'AttachOffset', point(0, 300, 800),
		'AttachRotate', point(5400, 0, 0),
		'AttachScale', 20,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Arm2",
		'comment', "phase4",
		'Parent', "LH_SpaceStation",
		'Child', "MedicalCenter",
		'AttachSpot', "Origin",
		'AttachOffset', point(-300, 500, 800),
		'AttachRotate', point(5400, 0, -3600),
		'AttachScale', 20,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Arm3",
		'comment', "phase4",
		'Parent', "LH_SpaceStation",
		'Child', "MedicalCenter",
		'AttachSpot', "Origin",
		'AttachOffset', point(-200, 770, 800),
		'AttachRotate', point(5400, 0, -7200),
		'AttachScale', 20,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Arm4",
		'comment', "phase4",
		'Parent', "LH_SpaceStation",
		'Child', "MedicalCenter",
		'AttachSpot', "Origin",
		'AttachOffset', point(0, 830, 800),
		'AttachRotate', point(5400, 0, 10800),
		'AttachScale', 20,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Arm5",
		'comment', "phase4",
		'Parent', "LH_SpaceStation",
		'Child', "MedicalCenter",
		'AttachSpot', "Origin",
		'AttachOffset', point(270, 660, 800),
		'AttachRotate', point(5400, 0, 7200),
		'AttachScale', 20,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Arm6",
		'comment', "phase4",
		'Parent', "LH_SpaceStation",
		'Child', "MedicalCenter",
		'AttachSpot', "Origin",
		'AttachOffset', point(230, 420, 800),
		'AttachRotate', point(5400, 0, 3600),
		'AttachScale', 20,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Antena1",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RadioDishAntenna",
		'AttachSpot', "Origin",
		'AttachOffset', point(0, 50, 1700),
		'AttachRotate', point(5400, 0, 0),
		'AttachScale', 5,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Antena2",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RadioDishAntenna",
		'AttachSpot', "Origin",
		'AttachOffset', point(-450, 300, 1700),
		'AttachRotate', point(5400, 0, -3600),
		'AttachScale', 5,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Antena3",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RadioDishAntenna",
		'AttachSpot', "Origin",
		'AttachOffset', point(-450, 820, 1700),
		'AttachRotate', point(5400, 0, -7200),
		'AttachScale', 5,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Antena4",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RadioDishAntenna",
		'AttachSpot', "Origin",
		'AttachOffset', point(0, 1080, 1700),
		'AttachRotate', point(5400, 0, 10800),
		'AttachScale', 5,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Antena5",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RadioDishAntenna",
		'AttachSpot', "Origin",
		'AttachOffset', point(470, 840, 1700),
		'AttachRotate', point(5400, 0, 7200),
		'AttachScale', 5,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Antena6",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RadioDishAntenna",
		'AttachSpot', "Origin",
		'AttachOffset', point(430, 320, 1700),
		'AttachRotate', point(5400, 0, 3600),
		'AttachScale', 5,
	})
	table.insert(LH_SpaceStationComponents, attach)
	attach = PlaceObj('ModItemAttachment', {
		'name', "Top",
		'comment', "phase5",
		'Parent', "LH_SpaceStation",
		'Child', "RoverEuropeAntena",
		'AttachSpot', "Origin",
		'AttachOffset', point(0, 570, 2330),
	})
	table.insert(LH_SpaceStationComponents, attach)
end

local function AddAttachment(parent, attachment)
	local idx = attachment:ResolveSpotIndex()
    local offset = attachment.AttachOffset
    local rotate = quaternion(attachment.AttachRotate:xyz())
    local rotate_axis, rotate_angle = rotate:GetAxisAngle()
    local child = PlaceObject("ModItemAttachmentObject", {mod_item = attachment})
    child:ChangeEntity(attachment.Child)
    child:SetAttachOffset(offset)
    child:SetAttachAxis(rotate_axis)
    child:SetAttachAngle(rotate_angle)
    child:SetScale(MulDivRound(parent:GetScale(), attachment.AttachScale, 100))
    child:SetColorModifier(attachment.AttachColor)
    parent:Attach(child, idx)
    return child
end

local PlacePlanet_vanilla = PlacePlanet
function PlacePlanet(scene)
	PlacePlanet_vanilla(scene)
	if not scene or scene ~= "PlanetMars" or not UIColony or not UIColony.ResearchStation then
		return
	end
	local spaceStation = PlaceObject("Building")
	spaceStation:ChangeEntity("BasketBall")
	PlanetRotationObj:Attach(spaceStation, PlanetRotationObj:GetSpotBeginIndex("Planet"))
	local x = MulDivRound(1300,sin(60*(UIColony.ResearchStation.longitude+90)),4096)
	local y = MulDivRound(1300,cos(60*(UIColony.ResearchStation.longitude+90)),4096)
	local z = -MulDivRound(1300,sin(60*UIColony.ResearchStation.latitude),4096)
	x = MulDivRound(x,cos(60*UIColony.ResearchStation.latitude),4096)
	y = MulDivRound(y,cos(60*UIColony.ResearchStation.latitude),4096)
	spaceStation:SetAttachOffset(x,y,z)
    local StationMissionID = UIColony.ResearchStation.mission_id
    for _, item in ipairs(LH_SpaceStationComponents) do
		if IsKindOf(item, "ModItemAttachment") and item.name~="origin" then
			if StationMissionID == "ResearchStation_1" then
				AddAttachment(spaceStation, item)
			elseif StationMissionID == "ResearchStation_2" and item.comment == "phase1" then
				AddAttachment(spaceStation, item)
			elseif StationMissionID == "ResearchStation_3" and item.comment == "phase2" then
				AddAttachment(spaceStation, item)
			elseif StationMissionID == "ResearchStation_4" and (item.comment == "phase2" or item.comment == "phase3") then
				AddAttachment(spaceStation, item)
			elseif StationMissionID == "ResearchStation_5" and (item.comment == "phase2" or item.comment == "phase3" or item.comment == "phase4") then
				AddAttachment(spaceStation, item)
			elseif StationMissionID == "ResearchStation_6" or StationMissionID == "ResearchStation_7" then
				AddAttachment(spaceStation, item)
			end
		end
	end
	spaceStation:SetScale(10)
	if StationMissionID == "ResearchStation_1" then
		spaceStation:SetOpacity(30)
	end
end