function OnMsg.ClassesPostprocess()
	if(table.find(XTemplates.POIAdditionalContent,"ActionId","outpost_mission")) then
		return
	end
	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateAction", {
			"__condition", function(parent, context)
				return context.selected_spot and context.selected_spot.spot_type == "outpost"
			end,
			"ActionId", "outpost_mission",
			"ActionName", T{"SEND MISSION"},
			"ActionToolbar", "ActionBar",
			"ActionGamepad", "ButtonX",
			"ActionState",
			function(self, host)
				local spot = host.context.selected_spot
				return spot and (spot.spot_type == "outpost" and Presets.POI.OutpostMissions[spot.mission_id]:PrerequisiteToCreate()) and not spot.rocket and not spot.timeout or "disabled"
			end,
			"OnAction", function(self, host, source)
				if PlanetaryExpeditionPossible("use in orbit") then
					local context = host.context
					if context.expedition_rocket then
						SendExpeditionAction(context.expedition_rocket, context.selected_spot, host)
						return
					end
					host:SetMode("rockets")
				else
					PromptNoAvailableRockets()
				end
			end
		})
	)
	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateAction", {
			"__condition", function(parent, context)
				return context.selected_spot and context.selected_spot.spot_type == "outpost" and Presets.POI.Outposts[context.selected_spot.project_id].dismantle_id
			end,
			"ActionId", "dismantle_outpost",
			"ActionName", T{"DISMANTLE"},
			"ActionToolbar", "ActionBar",
			"ActionGamepad", "ButtonX",
			"ActionState",
			function(self, host)
				local spot = host.context.selected_spot
				return spot and spot.spot_type == "outpost" and not spot.rocket or "disabled"
			end,
			"OnAction", function(self, host, source)
				if PlanetaryExpeditionPossible("use in orbit") then
					local context = host.context
					if context.expedition_rocket then
						context.selected_spot.mission_id = Presets.POI.Outposts[context.selected_spot.project_id].dismantle_id
						SendExpeditionAction(context.expedition_rocket, context.selected_spot, host)
						return
					end
					context.selected_spot.mission_id = Presets.POI.Outposts[context.selected_spot.project_id].dismantle_id
					host:SetMode("rockets")
				else
					PromptNoAvailableRockets()
				end
			end
		})
	)
	PlaceObj("XTemplate", {
		__is_kind_of = "XWindow",
		group = "InGame",
		id = "OutpostViewResources",
		PlaceObj("XTemplateWindow", {
			"Margins", box(55, 20, 0, 0),
			"LayoutMethod", "HList",
			"LayoutHSpacing", 50
		}, {
			PlaceObj("XTemplateWindow", {
				"LayoutMethod", "VList"
			}, {
				PlaceObj("XTemplateWindow", {
					"comment", "drones",
					"__condition",
					function(parent, context)
						return context.selected_spot.num_drones and context.selected_spot.num_drones > 0
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T("Drones")
				}),
				PlaceObj("XTemplateWindow", {
					"comment", "crew",
					"__condition",
					function(parent, context)
						return context.selected_spot.num_crew and context.selected_spot.num_crew > 0
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T("Crew")
				}),
				PlaceObj("XTemplateWindow", {
					"comment", "specialization",
					"__condition",
					function(parent, context)
						return context.selected_spot.num_crew and context.selected_spot.num_crew > 0
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T("Crew Specialization")
				}),PlaceObj("XTemplateWindow", {
					"comment", "expedition timeout",
					"__condition",
					function(parent, context)
						return context.selected_spot.timeout
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T("Mission Timeout")
				})
			}),
			PlaceObj("XTemplateWindow", {
				"LayoutMethod", "VList"
			}, {								
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return context.selected_spot.num_drones and context.selected_spot.num_drones > 0
					end,
					"__class", "XText",
					"Id", "idLocalDrones",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return context.selected_spot.num_crew and context.selected_spot.num_crew > 0
					end,
					"__class", "XText",
					"Id", "idLocalCrew",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return context.selected_spot.num_crew and context.selected_spot.num_crew > 0
					end,
					"__class", "XText",
					"Id", "idLocalSpecialization",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return context.selected_spot.timeout
					end,
					"__class", "XText",
					"Id", "idExpeditionTimeout",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				})
			}),
			PlaceObj("XTemplateCode", {
				"run",
				function(self, parent, context)
					context:SetOutpostMissionParams()
				end
			})
		})
	})

	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateWindow", {
			"LayoutMethod", "VList",
			"FoldWhenHidden", true,
			"__condition", function(parent, context)
				return context.selected_spot and context.selected_spot.spot_type == "outpost"		  	
			end
		}, {
			PlaceObj("XTemplateWindow", {
				"__class", "XText",
				"__context", function(parent, context)
					return context.selected_spot
				end,
				"Padding", box(0, 0, 0, 0),
				"HandleMouse", false,
				"TextStyle", "PGChallengeDescription",
				"Translate", true,
				"Text", T{"<OutpostDescription>"}
			}),
			PlaceObj("XTemplateTemplate", {
				"__template",
				"OutpostViewResources",
				"Margins",
				box(55, 20, 0, 20)
			}),
			PlaceObj("XTemplateWindow", {	        
				"__condition", function(parent, context)
					return context.selected_spot and context.selected_spot.spot_type == "outpost"		  	
				end,
				"LayoutMethod",
				"VList"
			}, {
				PlaceObj("XTemplateTemplate", {
					"__template", "DialogTitleSmall",
					"__context", function(parent, context)
						return context.selected_spot
					end,
					"Margins", box(55, 0, 0, 0),
					"Title", T{"Mission: <MissionName>"}
				})
			})
		})
	)
	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateWindow", {
			"__class", "XText",
			"__context", function(parent, context)
				return context.selected_spot
			end,
			"__condition",
			function(parent, context)
				return context and context.spot_type == "outpost"
			end,
			"Padding", box(0, 0, 0, 0),
			"HandleMouse", false,
			"TextStyle", "PGChallengeDescription",
			"Translate", true,
			"Text", T{"<MissionDescription>"}
		})
	)

	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateWindow", {
			"__condition",
			function(parent, context)
				return context.selected_spot and context.selected_spot.spot_type == "outpost"
			end,
			"Margins", box(55, 20, 0, 0),
			"LayoutMethod", "HList",
			"LayoutHSpacing", 50
		}, {
			PlaceObj("XTemplateWindow", {
				"LayoutMethod", "VList"
			}, {
				PlaceObj("XTemplateWindow", {
					"comment", "crew",
					"__condition",
					function(parent, context)
						return Presets.POI.OutpostMissions[context.selected_spot.mission_id].num_crew
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T(12011, "Crew")
				}),
				PlaceObj("XTemplateWindow", {
					"comment", "specialization",
					"__condition",
					function(parent, context)
						return Presets.POI.OutpostMissions[context.selected_spot.mission_id].num_crew
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T(240, "Specialization")
				}),
				PlaceObj("XTemplateWindow", {
					"comment", "additional inventory",
					"__condition",
					function(parent, context)
						return #context.selected_spot:GetRocketResources() > 0
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T(387987656324, "Additional Inventory")
				}),
				PlaceObj("XTemplateWindow", {
					"comment", "expedition time",
					"__condition",
					function(parent, context)
						return true --not context.selected_spot.rocket
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T("Mission Time")
				}),
				PlaceObj("XTemplateWindow", {
					"comment", "outcome",
					"__condition",
					function(parent, context)
						return context.selected_spot:GetOutcomeText()
					end,
					"__class", "XText",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGLandingPosDetails",
					"Translate", true,
					"Text", T(130571839539, "Outcome")
				})
			}),
			PlaceObj("XTemplateWindow", {
				"LayoutMethod", "VList"
			}, {				
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return Presets.POI.OutpostMissions[context.selected_spot.mission_id].num_crew
					end,
					"__class", "XText",
					"Id", "idCrew",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return Presets.POI.OutpostMissions[context.selected_spot.mission_id].num_crew
					end,
					"__class", "XText",
					"Id", "idSpecialization",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return #context.selected_spot:GetRocketResources() > 0
					end,
					"__class", "XText",
					"Id", "idInventory",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return true
					end,
					"__class", "XText",
					"Id", "idExpeditionTime",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				}),
				PlaceObj("XTemplateWindow", {
					"__condition",
					function(parent, context)
						return context.selected_spot:GetOutcomeText()
					end,
					"__class", "XText",
					"Id", "idOutcome",
					"Padding", box(0, 0, 0, 0),
					"HandleMouse", false,
					"TextStyle", "PGChallengeDescription",
					"Translate", true
				})
			}),
			PlaceObj("XTemplateCode", {
				"run",
				function(self, parent, context)
					context:SetOutpostMissionParams()
				end
			})
		})
	)
end