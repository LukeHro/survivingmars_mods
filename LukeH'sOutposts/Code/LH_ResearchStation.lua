function OnMsg.ClassesPostprocess()
	if Presets.POI.Outposts and Presets.POI.Outposts.ResearchStation then
		return
	end
	local speedup = 1
	PlaceObj("POI", {
		description = T{"Orbital research station"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Research Station"},
		group = "Outposts",
		id = "ResearchStation",
		mission_id = "ResearchStation_1",
		dismantle_id = "ResearchStation_0",
		is_orbital = true,
		max_projects_of_type = 3,
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		description = T{"The station is being dismantled"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Dismantle station"},
		expedition_time = 720000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_0",		
		is_orbital = true,
		max_projects_of_type = 3,		
		recover_drones = true,
		recover_crew = true,
		dismantle = "ResearchStation",
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		description = T{"Send resources to build the skeleton"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Start construction"},
		expedition_time = 1440000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_1",
		next_id = "ResearchStation_2",
		is_orbital = true,
		max_projects_of_type = 3,
		num_drones = 5,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Metals",
				"amount", 100000/speedup
			}),
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		description = T{"Send resources to build the hull"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Build station hull"},
		expedition_time = 1440000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_2",
		next_id = "ResearchStation_3",
		is_orbital = true,
		max_projects_of_type = 3,
		num_drones = 5,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Polymers",
				"amount", 200000/speedup
			}),
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	local reactor = PlaceObj("POI", {
		description = T{"Send resources to build the reactor"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Build power reactor"},
		expedition_time = 1440000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_3",
		next_id = "ResearchStation_4",
		is_orbital = true,
		max_projects_of_type = 3,
		num_drones = 5,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Fuel",
				"amount", 40000/speedup
			}),
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})
	if table.find(ModsLoaded, "id", "LH_Resources") then
		reactor.rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Radioactive",
				"amount", 40000/speedup
			}),
		}
	end

	PlaceObj("POI", {
		description = T{"Send resources to build the support systems"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Build support systems"},
		expedition_time = 1440000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_4",
		next_id = "ResearchStation_5",
		is_orbital = true,
		max_projects_of_type = 3,
		num_drones = 5,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "MachineParts",
				"amount", 80000/speedup
			}),
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		description = T{"Send resources to finalize the research labs"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Equip research labs"},
		expedition_time = 1440000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_5",
		next_id = "ResearchStation_6",
		is_orbital = true,
		max_projects_of_type = 3,
		leave_drones = true,
		num_drones = 5,
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Electronics",
				"amount", 60000/speedup
			}),
		},
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})

	PlaceObj("POI", {
		description = T{"Send specialists to help towards research goals"},
		display_icon = CurrentModPath .. "UI/Icons/research_station.tga",
		display_name = T{"Research project"},
		expedition_time = 1800000/speedup,
		group = "OutpostMissions",
		id = "ResearchStation_6",
		next_id = "ResearchStation_6",
		is_orbital = false,
		max_projects_of_type = 3,
		num_crew = 5,
		crew_specialization = "scientist",
		rocket_required_resources = {
			PlaceObj("ResourceAmount", {
				"resource", "Food",
				"amount", 50000
			}),
		},
		reserch_reward = 5000,
		save_in = "armstrong",
		spawn_period = range(3, 12),
	})


	if(table.find(XTemplates.POIAdditionalContent,"ActionId","create_researchStation")) then
		return
	end
	table.insert(XTemplates.POIAdditionalContent, 
		PlaceObj("XTemplateAction", {
			"__condition",
			function(parent, context)
				local poi = Presets.POI.Outposts.ResearchStation
				return not UIColony.ResearchStation and not context.selected_spot and poi:PrerequisiteToCreate()
			end,
			"ActionId", "create_researchStation",
			"ActionName", T{"CREATE RESEARCH STATION"},
			"ActionToolbar", "ActionBar",
			"ActionGamepad", "LeftTrigger",
			"OnAction", function(self, host, source)  
				local project = Presets.POI.Outposts.ResearchStation	      
				local obj = MarsSpecialOutpost:new({
					id = project.id .. (g_SpecialProjectSpawnNextIdx[poi_id] or ""),
					project_id = project.id,
					longitude = host.context.long / 60,
					latitude = host.context.lat / 60,
					is_orbital = project.is_orbital,
					display_name = project.display_name,
					description = project.description,
					consume_rocket = project.consume_rocket,
					expedition_time = project.expedition_time,
					num_drones = project.num_drones,
					rover_type = project.rover_type,
					num_crew = project.num_crew,
					crew_specialization = project.crew_specialization,
					mission_id = project.mission_id,
				})
                UIColony.ResearchStation = obj
				SortedMarsScreenLandingSpots = nil		
				CloseDialog("PlanetaryView")
				OpenDialog("PlanetaryView")
			end
		})
	)	
end
