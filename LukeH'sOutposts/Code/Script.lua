DefineClass.MarsSpecialOutpost = {
	__parents = {
		"MarsSpecialProject"
	},
	spot_type = "outpost",
	rocket = false,
	project_id = false,
	funding = false,
	num_drones = false,
	rover_type = false,
	num_crew = false,
	crew_specialization = false,
	fuel = false,
	mission_id = false
}

function MarsSpecialOutpost:OnCompletion(city)
	local mission = Presets.POI.OutpostMissions[self.mission_id]
	for _, terraforming in ipairs(mission.terraforming_changes or {}) do
		ChangeTerraformParam(terraforming.param, terraforming.amount)
	end
	AddOnScreenNotification("CompletedSpecialProject", false, {
		project_id = self.project_id,
		popup_preset = "CompleteSpecialProject",
		GetPopupPreset = function()
			return "Completed" .. self.project_id
		end,
		override_text = T({12169, "Completed: <display_name>", mission})
	})
	if mission.reserch_reward then
		UIColony:AddResearchPoints(mission.reserch_reward)
		AddOnScreenNotification("PlanetaryAnomaly_GrantRP", nil, {points = mission.reserch_reward, resource = "Research"})
	end
	if mission.leave_drones then
		self.drones = self.rocket.expedition.drones
		self.num_drones = self.rocket.expedition.num_drones
		self.rocket.expedition.drones = false
		self.rocket.expedition.num_drones = false
		self.rocket.units = false
	elseif mission.recover_drones then
		self.rocket.expedition.drones = self.drones
		self.rocket.expedition.num_drones = self.num_drones
		self.drones = false
		self.num_drones = false
	end
	if mission.leave_crew then
		self.crew = self.crew or self.rocket.expedition.crew
		if self.num_crew and self.num_crew > 0 then
			for _,v in pairs(self.rocket.expedition.crew) do
				table.insert_unique(self.crew, v)
			end
		end	
		for _,v in pairs(self.crew) do
			v.outpost = self
		end
		self.num_crew = #self.crew
		self.crew_specialization = self.rocket.expedition.crew_specialization
		self.rocket.expedition.crew = false
		self.rocket.expedition.num_crew = false
		self.rocket.expedition.crew_specialization = false
		self.rocket.units = false
	elseif mission.recover_crew then
		self.rocket.expedition.crew = self.crew
		for _,v in pairs(self.crew or {}) do
			v.outpost = nil
		end
		for _,v in pairs(self.retired or {}) do
			v.outpost = nil
			table.insert_unique(self.rocket.expedition.crew, v)
		end
		self.rocket.expedition.num_crew = self.num_crew
		self.rocket.expedition.crew_specialization = self.specialization
		self.crew = false
		self.retired = false
		self.num_crew = false
		self.crew_specialization = false
	end
	self.rocket = false
	if self.mission_id == Presets.POI.Outposts[self.project_id].dismantle_id then
        UIColony[Presets.POI.OutpostMissions[self.mission_id].dismantle]=nil
		DoneObject(self)
		return
	end
	self.mission_id = mission.next_id
	if Presets.POI.OutpostMissions[self.mission_id].refill_id then
		local refill = Presets.POI.OutpostMissions[Presets.POI.OutpostMissions[self.mission_id].refill_id]
		if self.num_crew < refill.need_crew then
			self.mission_id = Presets.POI.OutpostMissions[self.mission_id].refill_id
			refill.num_crew = refill.need_crew - self.num_crew
			return
		end
	end
	if mission.expedition_timeout then
		self.timeout = mission.expedition_timeout
		local time = GameTime()
		local timeout = time + self.timeout
		local outpost = self		
		CreateGameTimeThread(function()
	    while GameTime() < timeout do	      
	      outpost.timeout = timeout - GameTime()
	      Sleep(5000)
	    end
	    outpost.timeout = false
	  end)
	end
end

function MarsSpecialOutpost:GetOutcomeText()
	local mission = Presets.POI.OutpostMissions[self.mission_id]
	local outcome = {}
	if mission.terraforming_changes and next(mission.terraforming_changes) then
		for _, terraforming in ipairs(mission.terraforming_changes) do
			outcome[#outcome + 1] = T({11999, "<resource(number, res)>", number = terraforming.amount, res = terraforming.param .. "TP"})
		end
	end
	return #outcome>0 and table.concat(outcome, ", ") or false
end

function MarsSpecialOutpost:ShowOutcomeText()
	local project = Presets.POI.Outposts[self.project_id]
	if project.terraforming_changes and next(project.terraforming_changes) then
		return true
	end
end

function MarsSpecialOutpost:PrerequisiteToStart()
	local project = Presets.POI.Outposts[self.project_id]
	if not project then
		return false
	end
	return project.PrerequisiteToStart(project, self, UIColony)
end

function MarsSpecialOutpost:GetRocketResources()
	local mission = Presets.POI.OutpostMissions[self.mission_id]
	return mission and mission.rocket_required_resources or empty_table
end

function MarsSpecialOutpost:GetOutpostName()
	local project = Presets.POI.Outposts[self.project_id]
		return project.display_name
end

function MarsSpecialOutpost:GetOutpostDescription()
	local project = Presets.POI.Outposts[self.project_id]
		return project.description
end

function MarsSpecialOutpost:GetMissionName()
	local mission = Presets.POI.OutpostMissions[self.mission_id]
		return mission.display_name
end

function MarsSpecialOutpost:GetMissionDescription()
	local mission = Presets.POI.OutpostMissions[self.mission_id]
		return mission.description
end

function LandingSiteObject:SetOutpostMissionParams()
	local spot = self.selected_spot
	if spot and spot.spot_type == "outpost" then
		local mission = Presets.POI.OutpostMissions[spot.mission_id]

		if spot.num_crew then
			local specialization = const.ColonistSpecialization[spot.crew_specialization]
			local name = specialization and specialization.display_name or T(3609, "Any")
			local crew = self.dialog:ResolveId("idLocalCrew")
			if crew then
				crew:SetText(T({11539, "<colonist(colonists)>", colonists = spot.num_crew}))
			end
			local spec = self.dialog:ResolveId("idLocalSpecialization")
			if spec then
				spec:SetText(name)
			end
		end

		if spot.num_drones then
			local drones = self.dialog:ResolveId("idLocalDrones")
			if drones then
				drones:SetText(T({11180, "<drone(num)>", num = spot.num_drones}))
			end
		end

		if mission.num_crew then
			local specialization = const.ColonistSpecialization[mission.crew_specialization]
			local name = specialization and specialization.display_name or T(3609, "Any")
			local crew = self.dialog:ResolveId("idCrew")
			if crew then
				crew:SetText(T({11539, "<colonist(colonists)>", colonists = mission.num_crew}))
			end
			local spec = self.dialog:ResolveId("idSpecialization")
			if spec then
				spec:SetText(name)
			end
		end

		local inventory = {}
		if mission.num_drones or mission.rover_type then
			if mission.num_drones then
				inventory[#inventory + 1] = T({11180, "<drone(num)>", num = mission.num_drones})
			end
			if mission.rover_type then
				inventory[#inventory + 1] = g_Classes[requirements.rover_type].display_name
			end
		end

		local requirements = mission.rocket_required_resources
		for _,resource in pairs(requirements or {}) do
			inventory[#inventory + 1] = T({"<resource(amount,type)>", amount = resource.amount, type = resource.resource})
		end

		if #inventory > 0 then
			inventory = table.concat(inventory, ", ")
			local inv = self.dialog:ResolveId("idInventory")
			if inv then
				inv:SetText(inventory)
			end
		end

		if not spot.rocket then
			local time = self.dialog:ResolveId("idExpeditionTime")
			if time then
				time:SetText(T({11604, "<time(time)>", time = Presets.POI.OutpostMissions[spot.mission_id].expedition_time * 2 / (IsGameRuleActive("FastRockets") and 10 or 1)}))
			end
		elseif spot.rocket then
			local time = self.dialog:ResolveId("idExpeditionTime")
			if time then
				time:SetText(T{"IN PROGRESS"})
			end
		end

		if spot.timeout then
			local time = self.dialog:ResolveId("idExpeditionTimeout")
			if time then
				time:SetText(T({11604, "<time(time)>", time = spot.timeout}))
			end
		end

		local txt = spot:GetOutcomeText()
		if txt and txt ~= "" then
			local outcome = self.dialog:ResolveId("idOutcome")
			if outcome then
				outcome:SetText(txt)
			end
		end
	end
end


local GetSpotImage_LandingSiteObject = LandingSiteObject.GetSpotImage
function LandingSiteObject:GetSpotImage(win, ...)
	if self.planetary_view and self.marker_id_to_spot_id then
		local spot_id = self.marker_id_to_spot_id[win.Id]
		local spot = MarsScreenLandingSpots[spot_id]
		if spot and spot.spot_type == "outpost" then    	
			local image = Presets.POI.OutpostMissions[spot.mission_id].display_icon
			local image_size = PlanetUISpotLogoSizeCache[image] or point(UIL.MeasureImage(image))
			PlanetUISpotLogoSizeCache[image] = image_size
			local x, y = image_size:xy() --decrease size?
			return image, x, y, 90        
		end
	end
	return GetSpotImage_LandingSiteObject(self, win, ...)
end

local SendExpeditionAction_Vanilla = SendExpeditionAction
function SendExpeditionAction(obj, spot, dialog, param, additional_params)
	local spot_type = spot and spot.spot_type
	if spot and spot_type == "outpost" then
		local mission = Presets.POI.OutpostMissions[spot.mission_id]
		if mission.consume_rocket then
			PromptRocketWillBeDestoyed(obj, spot, dialog, param, additional_params)
			return
		end
	else
		return SendExpeditionAction_Vanilla(obj, spot, dialog, param, additional_params)
	end
	SendRocketToMarsPoint(obj, spot, dialog, param, additional_params)
end

local SendRocketToMarsPoint_Vanilla = SendRocketToMarsPoint
function SendRocketToMarsPoint(obj, spot, dialog)
	if spot.spot_type == "outpost" then
		local rocket = PlaceBuilding("RocketExpedition", {
			city = UICity,
			ExpeditionTime = Presets.POI.OutpostMissions[spot.mission_id].expedition_time / (IsGameRuleActive("FastRockets") and 10 or 1) --or nil
		})
		rocket:SetCommand("BeginExpedition", obj, spot)
		spot.rocket = rocket
		dialog:Close()
		CreateRealTimeThread(function()
			WaitMsg("PlanetCameraSet")
			ViewAndSelectObject(IsValid(obj) and obj or rocket)
		end)
	else
		return SendRocketToMarsPoint_Vanilla(obj, spot, dialog)
	end
end

local BeginExpedition_RocketExpedition = RocketExpedition.BeginExpedition
function RocketExpedition:BeginExpedition(rocket, spot)
	if spot.spot_type == "outpost" then
		local mission = Presets.POI.OutpostMissions[spot.mission_id]
		local funding = mission.funding
		if funding and funding > 0 then
			rocket.city:ChangeFunding(-funding, "special project")
		end
		self:__InternalExpeditionBegin(rocket, {
			export = spot:GetRocketExpeditionResources(),
			num_drones = mission.num_drones,
			rover_type = mission.rover_type,
			num_crew = mission.num_crew,
			crew_specialization = mission.crew_specialization,
			project = spot
		})
	else
		BeginExpedition_RocketExpedition(self, rocket, spot)
	end
end

local SortMarsPointsOfInterest_vanilla = SortMarsPointsOfInterest
function SortMarsPointsOfInterest( ... )
	SortedMarsScreenLandingSpots = false
	SortMarsPointsOfInterest_vanilla(...)
	DelayedCall(0, function()
		if UIColony.ForestationOutpost then
			table.insert(SortedMarsScreenLandingSpots, 2, UIColony.ForestationOutpost)
		end
		if UIColony.ResearchStation then
			table.insert(SortedMarsScreenLandingSpots, 2, UIColony.ResearchStation)
		end
	end)
end
