function OnMsg.ClassesBuilt()
    table.insert_unique(BuildingTemplates.ForeignAidRocket.storable_resources, "PreciousMinerals")
    table.insert_unique(BuildingTemplates.ForeignTradeRocket.storable_resources, "PreciousMinerals")
end

local EnhanceBasicRow_LandingSiteObject = LandingSiteObject.EnhanceBasicRow
function LandingSiteObject:EnhanceBasicRow(row, rival)
    if rival then
        table.insert(row, 4, T({"<preciousminerals(minerals)>", minerals = Max(rival.resources.preciousminerals * const.ResourceScale, 0)}))
    end
    EnhanceBasicRow_LandingSiteObject(self, row, rival)
end

function OnMsg.ClassesPostprocess()
    table.insert(HelpOfferBasic,3,{ ai_res = "preciousminerals", game_res = "PreciousMinerals", min = 500, help = 10000 })
    table.insert(DistressCallBasic,3,{message = T("We need Precious Minerals"), ai_res = "preciousminerals", game_res = "PreciousMinerals", amount = 50})
    local cargo = Presets.Cargo["Other Resources"].PreciousMinerals
    if cargo then
        cargo.group = "Basic Resources"
        cargo.price = 24000000
    end
end

OnMsg.ModsReloaded = function ( ... )
    if (XTemplates.customTradePad) then
        table.insert_unique(TradePad.trade_resources, "PreciousMinerals")
        local export = XTemplates.customTradePad[3].OnContextUpdate
        XTemplates.customTradePad[3].OnContextUpdate = function(self, context)
            local resource = ResolveValue(context, "export_resource")
            if resource=="PreciousMinerals" then
                self:SetIcon(CurrentModPath.."UI/Icons/PreciousMinerals_6.tga")
                self:SetTitle(T({11503, "Offering: <resource(res)>", res = resource }))
            else
                export(self, context)
            end
            self:SetVisible(context:IsAvailable())
        end
        local import = XTemplates.customTradePad[4].OnContextUpdate
        XTemplates.customTradePad[4].OnContextUpdate = function(self, context)
            local resource = ResolveValue(context, "import_resource")
            if resource=="PreciousMinerals" then
                self:SetIcon(CurrentModPath.."UI/Icons/PreciousMinerals_5.tga")
                self:SetTitle(T({11505, "Requesting: <resource(res)>", res = resource }))
            else
                import(self, context)
            end
            self:SetVisible(context:IsAvailable())
        end
    end
end

local SpawnRivalAI_vanilla = SpawnRivalAI
function SpawnRivalAI(preset, ...)
    local obj = SpawnRivalAI_vanilla(preset, ...)
    print("Adding Precious Minerals to", preset.id)
    UpdateRivalAI(obj, preset.id, Presets.DumbAIDef.Modded.lh_preciousminerals)
    return obj
end

OnMsg.LoadGame = function()
    for k,v in pairs(RivalAIs) do
        UpdateRivalAI(v, k, Presets.DumbAIDef.Modded.lh_preciousminerals)
    end
end

function OnMsg.ClassesPostprocess()
    if not IsDlcAvailable("picard") then
        return
    end
    PlaceObj("DumbAIDef", {
        biases = {},
        group = "Modded",
        id = "lh_preciousminerals",
        initial_resources = {
            PlaceObj("AIResourceAmount", { "resource", "preciousminerals", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "preciousminerals_production", "amount", 0 })
        },
        production_rules = {
            PlaceObj("AIProductionRule", {
                "rule_id", "Preciousminerals Production and Consumption",
                "Run", function(self, resources, ai_player)
                    local res_references = {
                        { "preciousminerals", 10000, "PreciousMinerals", 20 }
                    }
                    local rival_stock, player_stock, change, diff
                    for _, res in pairs(res_references) do
                        local r1 = res[1]
                        rival_stock = Max(1000, resources[r1] * const.ResourceScale)
                        player_stock = Max(1000, GetCityResourceOverview(MainCity):GetAvailable(res[3])) + 1000
                        change = resources[res[1] .. "_production"]
                        diff = rival_stock - player_stock
                        diff = diff / player_stock
                        diff = diff / 100
                        if rival_stock > res[2] and diff > 20 then
                            change = change * (100 - Min(95, diff)) / 100
                        elseif diff < -20 then
                            change = change + change * -diff / 100
                        end
                        if rival_stock > res[2] and 0 < res[4] then
                            change = change - resources.buildings * res[4] / const.ResourceScale
                        end
                        resources[r1] = resources[r1] + change
                    end
                end
            }),
        },
        PlaceObj("AIAction", {
            "id", "increase Preciousminerals production",
            "log_entry", T("Constructed Precious Minerals Extractor"),
            "base_eval", 75000000,
            "delay", 360000,
            "tags", {"mining"},
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 15 }),
                PlaceObj("AIResourceAmount", { "resource", "machineparts", "amount", 4 }),
                PlaceObj("AIResourceAmount", { "resource", "power", "amount", 5 }),
                PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 12 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "deposits_underground", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "preciousminerals_production", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
            },
            "IsAllowed", function(self, ai_player)
                return true
            end,
            "Eval", function(self, ai_player)
                if ai_player.resources.preciousminerals < 1 then
                    return 5000 * const.Scale.mil
                end
                if ai_player.resources.preciousminerals < 10 then
                    return 150 * const.Scale.mil
                elseif ai_player.resources.preciousminerals > 250 then
                    return 10 * const.Scale.mil
                elseif ai_player.resources.preciousminerals > 500 then
                    return 1 * const.Scale.mil
                end
            end
        }),
        PlaceObj("AIAction", {
            "id", "construct Second Dome",
            "log_entry", T(915496776171, "Constructed a small Dome"),
            "base_eval", 150000000,
            "delay", 1440000,
            "tags", {"colonists"},
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 100 }),
                PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 25 }),
                PlaceObj("AIResourceAmount", { "resource", "polymers", "amount", 10 }),
                PlaceObj("AIResourceAmount", { "resource", "oxygen", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "power", "amount", 30 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "colonists_max", "amount", 50 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "IsAllowed", function(self, ai_player)
                return not IsRivalRecentlySabotaged(ai_player) and (ai_player.resources.colonists_workforce < 10 or ai_player.resources.food > 100)
            end,
            "Eval", function(self, ai_player)
                if ai_player.resources.colonists_max > 40 and ai_player.resources.colonists_max < 110 then
                    local player_colonists = GetCityResourceOverview(MainCity):GetColonistCount()
                    if ai_player.resources.colonists_max > player_colonists then
                        return 0
                    elseif ai_player.resources.colonists_max < player_colonists then
                        return 250 * const.Scale.mil
                    end
                end
            end,
            "Run", function(self, ai_player)
                AIContestMilestone(ai_player, "ConstructDome", 48, 49)
            end
        }),
        PlaceObj("AIAction", {
            "id", "construct Underground Dome",
            "log_entry", T{"Constructed an Underground Dome"},
            "base_eval", 150000000,
            "delay", 1440000,
            "tags", {"colonists"},
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 80 }),
                PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 25 }),
                PlaceObj("AIResourceAmount", { "resource", "preciousminerals", "amount", 80 }),
                PlaceObj("AIResourceAmount", { "resource", "oxygen", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "power", "amount", 30 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "colonists_max", "amount", 20 }),
                PlaceObj("AIResourceAmount", { "resource", "deposits_underground", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "IsAllowed",
            function(self, ai_player)
                return not IsRivalRecentlySabotaged(ai_player) and (ai_player.resources.colonists_workforce < 10 or ai_player.resources.food > 100)
            end,
            "Eval",
            function(self, ai_player)
                if ai_player.resources.colonists_max > 40 then
                    local player_colonists = GetCityResourceOverview(MainCity):GetColonistCount()
                    if ai_player.resources.colonists_max > player_colonists * 20 / 100 or ai_player.resources.colonists_max > 2000 then
                        return 0
                    elseif ai_player.resources.colonists_max < player_colonists * 20 / 100 then
                        return 250 * const.Scale.mil
                    end
                end
            end,
            "Run",
            function(self, ai_player)
                AIContestMilestone(ai_player, "ConstructDome", 72, 73)
            end
        }),
    })
end



local Initialize_MarsTradeRoute = MarsTradeRoute.Initialize
function MarsTradeRoute:Initialize(sponsor_id, ...)
    table.insert_unique(self.basic_import, "PreciousMinerals")
    table.insert_unique(self.export_resource, "PreciousMinerals")
    return Initialize_MarsTradeRoute(self, sponsor_id, ...)
end
