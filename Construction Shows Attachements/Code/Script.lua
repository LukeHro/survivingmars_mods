local ModItemAttaches

function LoadAttaches()
	ModItemAttaches = {}  
	for _,mod in pairs(ModsLoaded) do
		for i,item in ipairs(mod.items) do
			if item:IsKindOf("ModItemAttachment") then
				ModItemAttaches[item.Parent] = ModItemAttaches[item.Parent] or {}
				local index =#ModItemAttaches[item.Parent]+1
				ModItemAttaches[item.Parent][index] = {spot_idx = -1, show_at_placement = true}
				ModItemAttaches[item.Parent][index][2] =item.Child
				ModItemAttaches[item.Parent][index].offset = item.AttachOffset
				ModItemAttaches[item.Parent][index].scale = item.AttachScale
				ModItemAttaches[item.Parent][index].axis, ModItemAttaches[item.Parent][index].angle = quaternion(item.AttachRotate:xyz()):GetAxisAngle()
			end
		end
	end
end

function OnMsg.ModsReloaded()
	LoadAttaches()
end

function Placement_AutoAttachObjects(obj, context)
	if (not obj:IsKindOf("CursorBuilding")) then
		return
	end
	local selectable = obj:GetEnumFlags(const.efSelectable) ~= 0
	local attaches = ModItemAttaches[obj.template.template_name]
	for i = 1, #(attaches or "") do
		local attach = attaches[i]
		local spot_attaches = {}
		if PlaceCheck(obj, attach, context) then
			local classes = attach[2]
			local o = PlaceAtSpot(obj, attach.spot_idx, classes, context)
			o:ChangeEntity(attach[2])
			if o then
				if attach.mirrored then
					o:SetMirrored(true)
				end
				if attach.offset then
					o:SetAttachOffset(attach.offset)
				end
				if attach.scale then
					o:SetScale(attach.scale)
				end
				if attach.axis and attach.angle and attach.angle ~= 0 then
					o:SetAttachAxis(attach.axis)
					o:SetAttachAngle(attach.angle)
				end
				if selectable then
					o:SetEnumFlags(const.efSelectable)
				end
				spot_attaches[#spot_attaches + 1] = o
			end
		end
	end
end

function Building_AutoAttachObjects(obj, context)
	if (not obj:IsKindOf("ConstructionSite")) then
		return
	end
	local selectable = obj:GetEnumFlags(const.efSelectable) ~= 0
	local attaches = ModItemAttaches[obj.building_class]
	for i = 1, #(attaches or "") do
		local attach = attaches[i]
		local spot_attaches = {}
		if PlaceCheck(obj, attach, context) then
			local classes = attach[2]
			local o = PlaceAtSpot(obj, attach.spot_idx, classes, context)
			o:ChangeEntity(attach[2])
			if o then
				if attach.mirrored then
					o:SetMirrored(true)
				end
				if attach.offset then
					o:SetAttachOffset(attach.offset)
				end
				if attach.scale then
					o:SetScale(attach.scale)
				end
				if attach.axis and attach.angle and attach.angle ~= 0 then
					o:SetAttachAxis(attach.axis)
					o:SetAttachAngle(attach.angle)
				end
				if selectable then
					o:SetEnumFlags(const.efSelectable)
				end
				spot_attaches[#spot_attaches + 1] = o
			end
		end
	end
end

local Vanilla_AutoAttachObjectsToPlacementCursor = AutoAttachObjectsToPlacementCursor
function AutoAttachObjectsToPlacementCursor(obj, ...)
	Vanilla_AutoAttachObjectsToPlacementCursor(obj, ...)
	Placement_AutoAttachObjects(obj, "placementcursor")
end

local Vanilla_AutoAttachObjectsToShapeshifter = AutoAttachObjectsToShapeshifter
function AutoAttachObjectsToShapeshifter(obj, ...)
	Vanilla_AutoAttachObjectsToShapeshifter(obj, ...)
	Building_AutoAttachObjects(obj, "placementcursor")
end


