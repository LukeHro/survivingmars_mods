local translationIdBase = 1107050100

function OnMsg.ClassesBuilt()	
	PlaceObj('OnScreenNotificationPreset', {
		ImagePreview = "UI/Icons/Notifications/New/renegade.tga",
		SortKey = 1000310,
		close_on_read = true,
		expiration = 150000,
		group = "Colonists",
		id = "RenegadeArrested",
		image = "UI/Icons/Notifications/New/renegade.tga",
		text = T{translationIdBase+1, "Your Officers in <dome_name> arrested <renegade_name>!"},
		title = T{translationIdBase+2, "Renegade Arrested"},
	})
	PlaceObj('OnScreenNotificationPreset', {
		ImagePreview = "UI/Icons/Notifications/New/renegade.tga",
		SortKey = 1000320,
		close_on_read = true,
		expiration = 150000,
		group = "Colonists",
		id = "RenegadeReeducated",
		image = "UI/Icons/Notifications/New/renegade.tga",
		text = T{translationIdBase+3, "<renegade_name> served his sentence in <dome_name>!"},
		title = T{translationIdBase+4, "Renegade Reeducated"},
	})
end

local vanillaCanChangeCommand = Colonist.CanChangeCommand
function Colonist:CanChangeCommand()
	if self.traits.LH_Inmate then
		return false
	end
	return vanillaCanChangeCommand(self)
end

local vanillaRoam = Colonist.Roam
function Colonist:Roam(duration)
	if self.traits.LH_Inmate then
		local tEnd = GameTime() + duration
		self:PlayState("idle", -1000)
	else
		vanillaRoam(self, duration)
	end
end

--[[function Colonist:ExitBuilding(...)
	if self.traits.LH_Inmate then
		return false
	end
	Unit.ExitBuilding(self,...)
end

function Colonist:EnterBuilding(...)
	if self.traits.LH_Inmate then
		return false
	end
	Unit.EnterBuilding(self,...)
end]]

local vanillaCanService = Service.CanService
function Service:CanService(unit)
	if unit.traits.LH_Inmate then
		return unit.residence == self
	end
	return vanillaCanService(self, unit)
end

local vanillaDomeUpdate = Dome.BuildingUpdate
function Dome:BuildingUpdate()
	vanillaDomeUpdate(self)
	if IsGameRuleActive("LH_OftenCrime") then
		local now = GameTime()
		if (self.next_crime_events_check-now)/const.HourDuration > 100 then
			self.next_crime_events_check = now + (50 + self:Random(50))*const.HourDuration	
		end
		--print((self.next_crime_events_check - now)/const.HourDuration)
	end
end

local vanillaCheckCrimeEvents = Dome.CheckCrimeEvents
function Dome:CheckCrimeEvents()	
	vanillaCheckCrimeEvents(self)
	local prison = false
	local stations = UICity.labels.SecurityStation or empty_table
	for _, station in ipairs(stations) do
		if station.prison and AreDomesConnectedWithPassage(self, station.parent_dome) and station:GetFreeSpace()>0 then
			prison = station
		end
	end	
	if not prison then
		return
	end
	local renegades = self.labels.Renegade or empty_table
	for i, renegade in ipairs(renegades) do
		if prison:Arrest(renegade) then			
			break
		end	
	end
end

