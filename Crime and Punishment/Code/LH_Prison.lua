DefineClass.LH_Prison =
{
	__parents = { "ElectricityConsumer", "Residence", "SecurityStation", "ServiceWorkplace"},
	prison = true,
	--visitors = {}
}

LH_Prison.OnSelected = Residence.OnSelected
LH_Prison.CheatSpawnCat = Residence.CheatSpawnCat
LH_Prison.CheatSpawnDog = Residence.CheatSpawnDog
LH_Prison.SpawnAnimal = Residence.SpawnAnimal

function LH_Prison:GameInit()
	self.city:AddToLabel("SecurityStation", self)
end

function LH_Prison:Done()
	self.city:RemoveFromLabel("SecurityStation", self)
	self:OnDestroyed()
end

function LH_Prison:OnDestroyed()
	Service.OnDestroyed(self)
	Residence.OnDestroyed(self)
	Workplace.OnDestroyed(self)
end

function LH_Prison:ClosePositions(shift, idx)
	if idx then
		Workplace.ClosePositions(self, shift, idx)
	else
		Residence.ClosePositions(self, shift) --first param
	end
end

function LH_Prison:OpenPositions(shift,idx)
	if idx then
		Workplace.OpenPositions(self, shift, idx)
	else
		Residence.OpenPositions(self, shift) --first param
	end
end

function LH_Prison:CheckHomeForHomeless()
end

function LH_Prison:IsSuitable(colonist)
	return Workplace.IsSuitable(self, colonist)
end

function LH_Prison:SetDome(dome)
	if self.parent_dome then
		self.parent_dome:RemoveFromLabel("SecurityStation", self)
	end	
	Building.SetDome(self, dome)	
	if dome then
		self.parent_dome:AddToLabel("SecurityStation", self)
		self:CheckHomeForHomeless()
	end	
end

function LH_Prison:CanReserveResidence(unit)
	return unit.traits.LH_Inmate and unit.residence == self
end

function LH_Prison:ReserveResidence(unit)
	return false
end

function LH_Prison:ColonistCanInteract(col)
	return Workplace.ColonistCanInteract(self, col)
end

function LH_Prison:ColonistInteract(col)
	return Workplace.ColonistInteract(self, col)
end

function LH_Prison:Service(...)
	local vars = {...}
	if vars[3] then
		Service.Service(self, ...)
	else
		Residence.Service(self, ...)
	end
end

function LH_Prison:ConsumeOnVisit(unit, interest)
	if interest=="needFood" then		
		StatsChange.ConsumeOnVisit(self, unit, interest)
	end
end

function LH_Prison:RemoveResident(unit)
	self:Liberate(unit)
	Residence.RemoveResident(self, unit)
end

function LH_Prison:KickResident(unit, i)
	self:Liberate(unit)
	Residence.KickResident(self, unit, i)
end

function LH_Prison:SendToArrest(renegade)
	local dome = self.parent_dome
	if #self.workers[self.current_shift] > 0 then
		self.workers[self.current_shift][1]:SetCommand("InterceptRenegade", renegade)
		return
	end
	local workers = self.workers or empty_table
	for _, v in ipairs(workers) do
		if #v > 0 then
			v[1]:SetCommand("InterceptRenegade", renegade)
			return
		end
	end
end

function LH_Prison:Arrest(renegade)
	if self:Random(100) < (#self.colonists+1)*10 then
		return
	end
	if not renegade.traits.LH_Inmate and self:GetFreeSpace()>0 then
		renegade.ip_freeIcon = renegade.ip_specialization_icon
		renegade.pin_freeIcon = renegade.pin_specialization_icon
		renegade.ip_specialization_icon = CurrentModPath..string.format("UI/Icons/Colonists/IP/IP_%s_%s.tga", "Inmate", renegade.entity_gender)
		renegade.pin_specialization_icon = CurrentModPath..string.format("UI/Icons/Colonists/Pin/%s_%s.tga", "Inmate", renegade.entity_gender)
		renegade.serveTime = 0

		renegade:AddTrait("LH_Inmate")
		renegade:RemoveTrait("Renegade")
		renegade:AssignToService(false)
		renegade:SetResidence(self)
		renegade:SetCommand("Rest")
		RebuildInfopanel(self)
		if renegade.workplace then
			renegade.workplace:FireWorker(renegade)		
		end
		self:SendToArrest(renegade)
		AddOnScreenNotification("RenegadeArrested", false, {dome_name = self.parent_dome:GetDisplayName(), renegade_name = renegade:GetDisplayName()}, self, self:GetMapID())
		return true
	end
	return false
end

function LH_Prison:Liberate(renegade)
	if renegade.traits.LH_Inmate then		
		renegade.ip_specialization_icon = renegade.ip_freeIcon
		renegade.pin_specialization_icon = renegade.pin_freeIcon
		renegade:RemoveTrait("LH_Inmate")
		renegade:AssignToService(false)
		if renegade.serveTime < 2000 then
			renegade:AddTrait("Renegade")	
			return false
		end
		AddOnScreenNotification("RenegadeReeducated", false, {dome_name = self.parent_dome:GetDisplayName(), renegade_name = renegade:GetDisplayName()}, self, self:GetMapID())
		return true
	end
	return false
end

function LH_Prison:BuildingUpdate(dt, ...)
	if self.working then
		local points = MulDivRound(self.performance, 10, 100)
		local colonists = self.colonists or empty_table
		for _,colonist in ipairs(colonists) do
			colonist.serveTime = colonist.serveTime + points
			if colonist.serveTime >= 2000 then
				self:RemoveResident(colonist)
			end
		end
	end	
end

function LH_Prison:GetNegatedRenegades()
	if g_CurrentMissionParams.idCommanderProfile == "LH_SecurityOfficer" then
		return MulDivRound(self.performance, #self.colonists, 100)
	end
	return 0
end

function LH_Prison:CanService(unit)
	if unit.residence ~= self then
		return false
	end
	if self:DoesHaveConsumption() and self.consumption_type == g_ConsumptionType.Visit and self.consumption_stored_resources <= 0 then
		return false
	end
	return true
end
