function CanShootRenegade(unit, renegade, ignore_command)
	if unit:IsDying() or not ignore_command and unit.command == "InterceptRenegade" then
		return false
	end
	if GetDomeAtPoint(GetObjectHexGrid(unit), unit) ~= GetDomeAtPoint(GetObjectHexGrid(renegade), renegade) or unit:IsVisitingBuilding() or unit:GetVisualDist2D(renegade) > (20 * guim) then
		return false
	end
	return IsObjectVisibleFromPoint(renegade, unit:GetVisualPos() + point(0, 0, 180 * guic))
end

function Colonist:InterceptRenegadeMoveSleep(...)
	Unit.MoveSleep(self, ...)
	if not self.intercept_params then
		return
	end
	local renegade = self.intercept_params.renegade
	local dome = self.intercept_params.dome
	local dist = self.intercept_params.dist
	if not IsValid(renegade) or renegade:IsDead() or dome and dist < HexAxialDistance(renegade, dome) then
		self:SetCommand("Idle")
	end
	if CanShootRenegade(self, renegade, true) then
		if self:ShootRenegade(renegade) then
			self:SetCommand("Idle")
		end
	end
end

function Colonist:InterceptRenegade(renegade, dome, dist, sent_by_station)
	local moving = false
	self:PushDestructor(function(self)
		if sent_by_station and dome and IsValid(renegade) and renegade.rogue_intercepted then 
			renegade.rogue_intercepted[dome] = nil
		end
		self.intercept_params = nil
		self:SetMoveAnim("moveWalk")
		self.MoveSleep = nil
	end)
	self.intercept_params = {
		renegade = renegade,
		dome = dome,
		dist = dist
	}
	self.MoveSleep = self.InterceptRenegadeMoveSleep
	self:SetMoveAnim("moveRun")
	if sent_by_station and dome then
		renegade.rogue_intercepted = renegade.rogue_intercepted or {}
		renegade.rogue_intercepted[dome] = true
	end
	self:ExitHolder(renegade)
	self:Goto(renegade, 2 * guim, 0)
	self:PopAndCallDestructor()
end

function Colonist:ShootRenegade(renegade)
	self:Face(renegade)
	self:SetAnim(1, "attackStart")
	Sleep(self:TimeToAnimEnd())
	self:SetAnim(1, "attackIdle")
	PlayFX("Shoot", "start", self, renegade)
	Sleep(self:TimeToAnimEnd())
	local dead
	if not renegade:IsDead() then
		renegade:SetState("layDying")
		PlayFX("GetShot", "start", renegade)
		dead = true
	end
	self:SetAnim(1, "attackEnd")
	Sleep(self:TimeToAnimEnd())
	return dead
end
