local ImproveDemandRequest_Drone = Drone.ImproveDemandRequest
function Drone:ImproveDemandRequest(s_request, d_request, resource, amount, must_change)
    local new_req = ImproveDemandRequest_Drone(self, s_request, d_request, resource, amount, must_change) or d_request
    --print(resource, not not d_request, not not new_req, d_request == new_req)
    local destination = new_req and new_req:GetBuilding()
    if must_change or not destination or destination:GetPriorityForRequest(new_req) > 0
            or IsKindOf(destination, "CargoTransporter") or IsKindOf(destination, "SpaceElevator") --don't mess with those
            or not (IsKindOf(destination, "StorageDepot") or destination.parent and IsKindOf(destination.parent, "MechanizedDepot")) then
        return new_req
    end
    local delta = destination:GetStoredAmount(resource, true) - destination.desired_amount
    if delta < 0 then
        return new_req
    end
    local requesters = self.command_center and self.command_center.connected_task_requesters
    local filter = function(i, requester)
        if IsKindOf(requester, "CargoTransporter") or IsKindOf(requester, "SpaceElevator") then
            return false --don't mess with rockets and elevators
        end
        return (IsKindOf(requester, "StorageDepot") and requester.demand[resource])
                or (requester.parent and IsKindOf(requester.parent, "MechanizedDepot") and requester.parent:GetAttachedDemandRequest(resource))
    end
    local storages = table.ifilter(requesters or empty_table, filter)
    --print("found", #storages, "storages, old delta", delta)
    for _, storage in pairs(storages) do
        local new_delta = storage:GetStoredAmount(resource, true) - storage.desired_amount
        if new_delta > - amount and delta > new_delta then
            local is_storing = IsKindOf(destination, "StorageDepot") and storage:IsStoring(resource)
                    or IsKindOf(storage, "MechanizedDepot") and storage:GetMaxStorage(resource) > 0
            if is_storing and storage:GetStoredAmount(resource, true) + amount < storage:GetMaxStorage(resource) then
                delta = new_delta
                destination = storage.parent and IsKindOf(storage.parent, "MechanizedDepot") and storage.parent or storage
            end
        end
    end
    if destination ~= new_req:GetBuilding() then
        --print("found better", destination.entity, ", new delta", delta)
        local newer_req = IsKindOf(destination, "MechanizedDepot") and destination:GetAttachedDemandRequest(resource) or destination.demand[resource]
        if newer_req:AssignUnit(amount) then
            new_req:UnassignUnit(amount, false)
            return newer_req
        end
    end
    return new_req
end

function MechanizedDepot:GetAttachedDemandRequest(resource)
    --overwrite for a modded mechanized depot with more stockpiles maybe
    local demand = self.stockpiles[1].demand_request
    return demand:GetResource() == resource and demand
end