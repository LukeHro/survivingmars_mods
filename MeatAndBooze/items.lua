return {
PlaceObj('ModItemCode', {
	'name', "Meat",
	'FileName', "Code/Meat.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Booze",
	'FileName', "Code/Booze.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Pasture",
	'FileName', "Code/Pasture.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Diner",
	'FileName', "Code/Diner.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Habitat",
	'FileName', "Code/Habitat.lua",
}),
PlaceObj('ModItemCode', {
	'name', "BuildingTemplate",
	'FileName', "Code/BuildingTemplate.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Transportation",
	'FileName', "Code/Transportation.lua",
}),
PlaceObj('ModItemCode', {
	'name', "CCCandInfobar",
	'FileName', "Code/CCCandInfobar.lua",
}),
PlaceObj('ModItemCode', {
	'name', "PlanetUIandRivals",
	'FileName', "Code/PlanetUIandRivals.lua",
}),
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemAttachment', {
	'comment', "mid 1",
	'Parent', "LH_Distillery",
	'Child', "WaterTankLarge",
	'AttachOffset', point(0, 540, 1050),
	'AttachScale', 25,
}),
PlaceObj('ModItemAttachment', {
	'comment', "mid 1",
	'Parent', "LH_Distillery",
	'Child', "WaterTankLarge",
	'AttachOffset', point(0, 540, 535),
	'AttachScale', 25,
}),
PlaceObj('ModItemEntity', {
	'name', "LH_MeatDepo",
	'entity_name', "LH_MeatDepo",
	'import', "Entities/LH_MeatDepo.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_MeatDepo_A",
	'entity_name', "LH_MeatDepo_A",
	'import', "Entities/LH_MeatDepo_A.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_MeatContainer",
	'entity_name', "LH_MeatContainer",
	'import', "Entities/LH_MeatContainer.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_MeatContainer10",
	'entity_name', "LH_MeatContainer10",
	'import', "Entities/LH_MeatContainer10.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_BoozeDepo",
	'entity_name', "LH_BoozeDepo",
	'import', "Entities/LH_BoozeDepo.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_BoozeDepo_A",
	'entity_name', "LH_BoozeDepo_A",
	'import', "Entities/LH_BoozeDepo_A.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_BoozeContainer",
	'entity_name', "LH_BoozeContainer",
	'import', "Entities/LH_BoozeContainer.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_BoozeContainer10",
	'entity_name', "LH_BoozeContainer10",
	'import', "Entities/LH_BoozeContainer10.ent",
}),
}
