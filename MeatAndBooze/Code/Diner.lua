ColonistStatReasons.good_diner = T("<green>Enjoyed a good diner <amount></green>")
Diner.needMeat = false

function Diner:InitConsumptionRequest(...)
    HasConsumption.InitConsumptionRequest(self, ...)
    self.needMeat = self:AddAdditionalConsumption("Meat", 200, self.consumption_max_storage, self.consumption_type)
end

function Diner:CanConsume()
    return HasConsumption.CanConsume(self)
            or self.needMeat and self.needMeat.consumption_stored_resources and self.needMeat.consumption_stored_resources > 0
end

function Diner:CanService(unit)
    if ServiceBase.CanService(self, unit) then
        local insufficient_consumptions = self:DoesHaveConsumption() and self.consumption_type == g_ConsumptionType.Visit
                and self.consumption_stored_resources <= 0 and self.needMeat.consumption_stored_resources <= 0
        return not insufficient_consumptions
    end
    return false
end

function Diner:Consume_Visit(unit, interest)
    local needMeat = self.needMeat
    if self.consumption_resource_type == "Food" then
        if GameTime() - unit.last_meal >= const.DayDuration then
            if not self.working or unit.traits.Vegan or needMeat.consumption_stored_resources <= 0 then
                return HasConsumption.Consume_Visit(self, unit)
            end
            local max = self.consumption_stored_resources + needMeat.consumption_stored_resources
            if unit.assigned_to_service_with_amount then
                max = Min(max, unit.assigned_to_service_with_amount)
            end
            local use = unit:Eat(max)
            if use == 0 then
                return
            end
            local meat = Min(use / 2, needMeat.consumption_stored_resources)
            meat = Max(meat, use - self.consumption_stored_resources)
            meat = needMeat:Consume_Internal(meat)
            local vegs = self:Consume_Internal(use - meat)
            if vegs == use/2 then
                unit:ChangeComfort(g_Consts.OutsideFoodConsumptionComfort, "good_diner")
            end
            return meat + vegs
        end
    else
        return self:Consume_Internal(self.consumption_amount)
    end
end

if IsDlcAccessible("gagarin") then
    table.insert_unique(MegaMall.__parents, "Diner")

    function OnMsg.ClassesPostprocess()
        MegaMall.GetUIConsumptionTexts = HasConsumption.GetUIConsumptionTexts
    end

    function MegaMall:CanConsume()
        return Diner.CanConsume(self) or self.needBooze.consumption_stored_resources and self.needBooze.consumption_stored_resources > 0
    end

    function MegaMall:ConsumeOnVisit(unit, interest)
        if interest == "needFood" or interest == "interestDining" then
            self:Consume_Visit(unit, interest)
        elseif interest == "interestDrinking" then
            self.needBooze:Consume_Visit(unit, interest)
        end
    end

    function MegaMall:CanService(unit)
        if unit.daily_interest == "interestDining" and unit.daily_interest == "needFood" then
            return IsKindOf(self, "Diner") and Diner.CanService(self, unit) or Service.CanService(self, unit)
        elseif unit.daily_interest ~= "interestDrinking" and self.needBooze then
            return self.needBooze.consumption_stored_resources > 0
        else
            return true
        end
    end
end