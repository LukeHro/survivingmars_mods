Resources["Meat"] = {
    id = "Meat",
    name = "Meat",
    display_name = T { "Meat" },
    display_icon = CurrentModPath .. "UI/Icons/Buildings/res_meat.tga",
    unit_amount = const.ResourceScale,
    color = RGB(255, 128, 0),
    entity = "LH_MeatContainer",
    description = T { "Meat for consumption." }
}

table.insert_unique(ResourceDescription, Resources["Meat"])
table.insert_unique(StockpileResourceList, "Meat")
table.insert_unique(CargoShuttle.storable_resources, "Meat")
table.insert_unique(AllResourcesList, "Meat")
table.insert_unique(RCTransport.storable_resources, "Meat")

function OnMsg.ClassesPostprocess()
    table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotMeat" }))
    BuildingTechRequirements["LH_MechanizedDepotMeat"] = { { tech = "AutomatedStorage", hide = false } }
end

const.TagLookupTable["icon_Meat"] = "<image " .. CurrentModPath .. "UI/Icons/res_meat.tga 1300>"
const.TagLookupTable["icon_Meat_small"] = "<image " .. CurrentModPath .. "UI/Icons/res_meat.tga 800>"
const.TagLookupTable["icon_Meat_orig"] = "<image " .. CurrentModPath .. "UI/Icons/res_meat.tga>"

function StorageDepot:Getui_Meat()
    return T({ "Meat<right><meat(Stored_Meat,MaxAmount_Meat)>", self })
end

function InfobarObj:GetMeatText()
    local meat = GetCityResourceOverview(UICity):GetAvailableMeat() / const.ResourceScale
    return T({ "<meat><icon_Meat_orig>", meat = self:FmtRes(meat) })
end

function InfobarObj:GetMeatRollover()
    return GetCityResourceOverview(UICity):GetMeatRollover()
end

function ResourceOverview:GetMeatRollover()
    local ret = { self:GetBasicResourcesHeading(),
                  T(316, "<newline>"),
                  T({ "Meat production<right><meat(MeatProducedYesterday)>", self }),
                  T({ "Meat consumption<right><meat(MeatConsumedByConsumptionYesterday)>", self }),
                  T({ "Stored in service buildings<right><meat(MeatStoredInServiceBuildings)>", self })
    }
    return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetMeatProducedYesterday"] = function(self)
    return self:GetProducedYesterday("Meat")
end
ResourceOverview["GetMeatConsumedByConsumptionYesterday"] = function(self)
    return self:GetConsumedByConsumptionYesterday("Meat")
end
ResourceOverview["GetAvailableMeat"] = function(self)
    return self:GetAvailable("Meat")
end

function ResourceOverview:GetMeatStoredInServiceBuildings()
    return self.data.meat_in_service_buildings or 0
end

function OnMsg.ClassesBuilt()
    if IsDlcAvailable("shepard") and XTemplates.sectionPasture then
        local stored = lh.deep_find(XTemplates.sectionPasture, function(o)
            return string.match(TDevModeGetEnglishText(o.Text), "Food")
        end)
        if stored then
            stored.Text = T("Stored Meat<right><resource(StoredAmount, max_storage, 'Meat')>")
        end
    end
end

function OnMsg.ClassesPostprocess()
    table.insert_unique(FormattableResources, { text = "Meat", value = "meat" })
    TFormat.meat = function(context_obj, value, max)
        return FormatResourceValueMaxResource(context_obj, value, max, "Meat")
    end
end
