Resources["Booze"] = {
    id = "Booze",
    name = "Booze",
	display_name = T{"Booze"},
	display_icon = CurrentModPath.."UI/Icons/Buildings/res_booze.tga",
	unit_amount = const.ResourceScale, 
	color = RGB(255, 128, 0), 
	entity = "LH_BoozeContainer", 
	description = T{"Booze for consumption."}
}

table.insert_unique(ResourceDescription, Resources["Booze"])	
table.insert_unique(StockpileResourceList, "Booze")
table.insert_unique(CargoShuttle.storable_resources, "Booze")
table.insert_unique(AllResourcesList, "Booze")
table.insert_unique(RCTransport.storable_resources,"Booze")

function OnMsg.ClassesPostprocess()
    table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotBooze" }))
    BuildingTechRequirements["LH_MechanizedDepotBooze"] = {{ tech = "AutomatedStorage", hide = false }}
end

const.TagLookupTable["icon_Booze"] = "<image "..CurrentModPath.."UI/Icons/res_booze.tga 1300>"
const.TagLookupTable["icon_Booze_small"] = "<image "..CurrentModPath.."UI/Icons/res_booze.tga 800>"
const.TagLookupTable["icon_Booze_orig"] = "<image "..CurrentModPath.."UI/Icons/res_booze.tga>"

function StorageDepot:Getui_Booze()
    return T({"Booze<right><booze(Stored_Booze,MaxAmount_Booze)>", self})
end

function InfobarObj:GetBoozeText()
    local booze = GetCityResourceOverview(UICity):GetAvailableBooze() / const.ResourceScale
    return T({"<booze><icon_Booze_orig>", booze = self:FmtRes(booze)})
end

function InfobarObj:GetBoozeRollover()
    return GetCityResourceOverview(UICity):GetBoozeRollover()
end

function ResourceOverview:GetBoozeRollover()
    local ret = {self:GetBasicResourcesHeading(),
                 T(316, "<newline>"),
                 T({"Booze production<right><booze(BoozeProducedYesterday)>", self}),
                 T({"Booze consumption<right><booze(BoozeConsumedByConsumptionYesterday)>", self}),
                 T({"Stored in service buildings<right><booze(BoozeStoredInServiceBuildings)>", self})
    }
    return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetBoozeProducedYesterday"] = function(self)
    return self:GetProducedYesterday("Booze")
end
ResourceOverview["GetBoozeConsumedByConsumptionYesterday"] = function(self)
    return self:GetConsumedByConsumptionYesterday("Booze")
end
ResourceOverview["GetAvailableBooze"] = function(self)
    return self:GetAvailable("Booze")
end

function ResourceOverview:GetBoozeStoredInServiceBuildings()
    return self.data.booze_in_service_buildings or 0
end

function OnMsg.ClassesPostprocess()
    table.insert_unique(FormattableResources, {text = "Booze", value = "booze"})
    TFormat.booze = function(context_obj, value, max)
        return FormatResourceValueMaxResource(context_obj, value, max, "Booze")
    end
end

