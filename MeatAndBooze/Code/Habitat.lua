ColonistStatReasons["habitat food"] = T("<green>Had a meal <amount></green>")
ColonistStatReasons["habitat meat"] = T("<green>Had a feast <amount></green>")
ColonistStatReasons["habitat booze"] = T("<green>Had a party <amount></green>")

DefineClass.Habitat = {
    __parents = {
        "HasConsumption"
    }
}

Habitat.InitConsumptionRequest = Diner.InitConsumptionRequest

function Habitat:InitConsumptionRequest(...)
    HasConsumption.InitConsumptionRequest(self, ...)
    self.needMeat = self:AddAdditionalConsumption("Meat", 200, self.consumption_max_storage, self.consumption_type)
    self.needBooze = self:AddAdditionalConsumption("Booze", 200, self.consumption_max_storage, self.consumption_type)
end

function Habitat:GetService(need, unit, starving)
    if need == "needFood" then
        local amount_to_eat = unit:GetEatPerVisit()
        local needMeat = self.needMeat
        if self:DoesHaveConsumption() and amount_to_eat <= self.consumption_stored_resources + needMeat.consumption_stored_resources then
            unit:Eat(amount_to_eat)
            if self.consumption_resource_type == "Food" then
                if unit.traits.Vegan or needMeat.consumption_stored_resources <= 0 then
                    if self:Consume_Internal(amount_to_eat) == amount_to_eat then
                        unit:ChangeHealth(self.food_health_change, "habitat food")
                    end
                else
                    local meat = Min(amount_to_eat / 2, needMeat.consumption_stored_resources)
                    meat = Max(meat, amount_to_eat - self.consumption_stored_resources)
                    meat = needMeat:Consume_Internal(meat)
                    local vegs = self:Consume_Internal(amount_to_eat - meat)
                    if vegs >= amount_to_eat/2 then
                        unit:ChangeHealth(self.food_health_change, "habitat food")
                    end
                    if meat >= amount_to_eat/2 then
                        unit:ChangeComfort(self.food_comfort_change, "habitat meat")
                    end
                end
            end
            return false, false
        end
        return false, true
    end
    return false, false
end

if IsDlcAccessible("picard") then
    table.insert_unique(MicroGHabitat.__parents, "Habitat")

    function OnMsg.ClassesPostprocess()
        MicroGHabitat.GetService = Habitat.GetService
        MicroGHabitat.GetUIConsumptionTexts = HasConsumption.GetUIConsumptionTexts
    end
end