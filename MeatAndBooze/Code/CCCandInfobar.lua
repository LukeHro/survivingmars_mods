function OnMsg.LHResInfobarReady()
    local lhLifeResources = lh.deep_find(XTemplates.Infobar, "Id", "idLifeResources")
    if lhLifeResources then
        local _, food = lh.deep_search(lhLifeResources, "Id", "idFood")
        local idxFood = lh.deep_search(lhLifeResources, function(v) return v end, food)
        if not table.find(lhLifeResources[2], 'Id', "meatParent") then
            local meat = PlaceObj("XTemplateWindow", {
                "__class", "XButton",
                'Id', "meatParent",
                "RolloverTemplate", "Rollover",
                "RolloverAnchor", "bottom",
                "RolloverText", T("<MeatRollover>"),
                "RolloverTitle", T("Meat"),
                "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
                "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
                "HAlign", "center",
                "BorderColor", RGBA(0, 0, 0, 0),
                "Background", RGBA(0, 0, 0, 0),
                "MouseCursor", "UI/Cursors/Rollover.tga",
                "RelativeFocusOrder", "next-in-line",
                "FXMouseIn", "UIButtonMouseIn",
                "FXPress", "BuildMenuButtonClick",
                "FocusedBorderColor", RGBA(0, 0, 0, 0),
                "FocusedBackground", RGBA(0, 0, 0, 0),
                "DisabledBorderColor", RGBA(0, 0, 0, 0),
                "OnPress", function(self, gamepad)
                    self.context:CycleResources("Meat")
                end,
                "RolloverBackground", RGBA(0, 0, 0, 0),
                "PressedBackground", RGBA(0, 0, 0, 0)
            }, {
                PlaceObj("XTemplateWindow", {
                    "__class", "XImage",
                    "Id", "idRollover",
                    "Dock", "box",
                    "Visible", false,
                    "Image", "UI/Common/row_shine_2.tga",
                    "ImageFit", "stretch"
                }),
                PlaceObj("XTemplateWindow", {
                    "__class", "XText",
                    "Id", "idMeat",
                    "Padding", box(3, 4, 3, 4),
                    "TextStyle", "Infobar",
                    "Translate", true,
                    "Text", T("<MeatText>")
                })
            })
            table.insert(lhLifeResources[2], idxFood + 1, meat)
        end
        if not table.find(lhLifeResources[2], 'Id', "boozeParent") then
            local booze = PlaceObj("XTemplateWindow", {
                "__class", "XButton",
                'Id', "boozeParent",
                "RolloverTemplate", "Rollover",
                "RolloverAnchor", "bottom",
                "RolloverText", T("<BoozeRollover>"),
                "RolloverTitle", T("Booze"),
                "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
                "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
                "HAlign", "center",
                "BorderColor", RGBA(0, 0, 0, 0),
                "Background", RGBA(0, 0, 0, 0),
                "MouseCursor", "UI/Cursors/Rollover.tga",
                "RelativeFocusOrder", "next-in-line",
                "FXMouseIn", "UIButtonMouseIn",
                "FXPress", "BuildMenuButtonClick",
                "FocusedBorderColor", RGBA(0, 0, 0, 0),
                "FocusedBackground", RGBA(0, 0, 0, 0),
                "DisabledBorderColor", RGBA(0, 0, 0, 0),
                "OnPress", function(self, gamepad)
                    self.context:CycleResources("Booze")
                end,
                "RolloverBackground", RGBA(0, 0, 0, 0),
                "PressedBackground", RGBA(0, 0, 0, 0)
            }, {
                PlaceObj("XTemplateWindow", {
                    "__class", "XImage",
                    "Id", "idRollover",
                    "Dock", "box",
                    "Visible", false,
                    "Image", "UI/Common/row_shine_2.tga",
                    "ImageFit", "stretch"
                }),
                PlaceObj("XTemplateWindow", {
                    "__class", "XText",
                    "Id", "idBooze",
                    "Padding", box(3, 4, 3, 4),
                    "TextStyle", "Infobar",
                    "Translate", true,
                    "Text", T("<BoozeText>")
                })
            })
            table.insert(lhLifeResources[2], idxFood + 2, booze)
        end
    end
end

local CalcConsumptionProduction_ResourceOverview = ResourceOverview.CalcConsumptionProduction
function ResourceOverview:CalcConsumptionProduction()
    CalcConsumptionProduction_ResourceOverview(self)
    self.data.meat_in_service_buildings = 0
    self.data.booze_in_service_buildings = 0
    for _, foodBuilding in pairs(self.city.labels.needFood or empty_table) do
        local request = foodBuilding.consumption_resource_request
        if request and request:GetResource() == "Meat" then
            self.data.meat_in_service_buildings = self.data.meat_in_service_buildings + foodBuilding.consumption_stored_resources
            self.data.food_in_service_buildings = self.data.food_in_service_buildings - foodBuilding.consumption_stored_resources
        end
    end
    for _, diningBuilding in pairs(self.city.labels.interestDining or empty_table) do
        local needMeat = diningBuilding.needMeat
        local request = needMeat and needMeat.consumption_resource_request
        if request and request:GetResource() == "Meat" then
            self.data.meat_in_service_buildings = self.data.meat_in_service_buildings + needMeat.consumption_stored_resources
        end
    end
    for _, drinksBuilding in pairs(self.city.labels.interestDrinking or empty_table) do
        local request = drinksBuilding.consumption_resource_request
        if request and request:GetResource() == "Booze" then
            self.data.booze_in_service_buildings = self.data.booze_in_service_buildings + drinksBuilding.consumption_stored_resources
        end
    end
end

function OnMsg.LHResCCCReady()
    local lhLife = lh.deep_find(XTemplates.CommandCenterCategories, "Id", "idLifeResources")
    if lhLife then
        local idxFood, parent = lh.deep_search(lhLife, "comment", "food")
        if not table.find(parent, "comment", "meat") then
            table.insert(parent, idxFood + 1, PlaceObj('XTemplateTemplate', {
                'comment', "meat",
                '__template', "CommandCenterStatsRow",
                'Name', T("Meat"),
                'Value', T("<meat(AvailableMeat)>"),
            }))
        end
        if not table.find(parent, "comment", "booze") then
            table.insert(parent, idxFood + 2, PlaceObj('XTemplateTemplate', {
                'comment', "booze",
                '__template', "CommandCenterStatsRow",
                'Name', T("Booze"),
                'Value', T("<booze(AvailableBooze)>"),
            }))
        end
    end
end

local GetColonyStatsButtons_City = City.GetColonyStatsButtons
function City:GetColonyStatsButtons()
    local buttons = GetColonyStatsButtons_City(self)
    local meat = table.remove(buttons, table.ifind_if(buttons, function(v)
        return v.id == "Meat"
    end))
    local booze = table.remove(buttons, table.ifind_if(buttons, function(v)
        return v.id == "Booze"
    end))
    local foodId = table.ifind_if(buttons, function(v)
        return v.id == "Food"
    end)
    meat.on_icon = CurrentModPath .. meat.on_icon
    meat.off_icon = CurrentModPath .. meat.off_icon
    booze.off_icon = CurrentModPath .. booze.off_icon
    booze.on_icon = CurrentModPath .. booze.on_icon
    table.insert(buttons, foodId + 1, meat)
    table.insert(buttons, foodId + 2, booze)
    return buttons
end