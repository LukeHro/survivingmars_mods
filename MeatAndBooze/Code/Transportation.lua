function OnMsg.ClassesBuilt()
    lh.insert_after(SpaceElevator.storable_resources, "Booze", "Food")
    lh.insert_after(SpaceElevator.storable_resources, "Meat", "Food")
    lh.insert_after(RocketExpedition.storable_resources, "Booze", "Food")
    lh.insert_after(RocketExpedition.storable_resources, "Meat", "Food")
    lh.insert_after(SupplyRocket.storable_resources, "Booze", "Food")
    lh.insert_after(SupplyRocket.storable_resources, "Meat", "Food")
    lh.insert_after(BuildingTemplates.SpaceElevator.storable_resources, "Booze", "Food")
    lh.insert_after(BuildingTemplates.SpaceElevator.storable_resources, "Meat", "Food")
    lh.insert_after(BuildingTemplates.RocketExpedition.storable_resources, "Booze", "Food")
    lh.insert_after(BuildingTemplates.RocketExpedition.storable_resources, "Meat", "Food")
    lh.insert_after(BuildingTemplates.SupplyRocket.storable_resources, "Booze", "Food")
    lh.insert_after(BuildingTemplates.SupplyRocket.storable_resources, "Meat", "Food")
    if IsDlcAvailable("gagarin") then
        lh.insert_after(BuildingTemplates.ZeusRocket.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.ZeusRocket.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.DragonRocket.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.DragonRocket.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.ForeignAidRocket.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.ForeignAidRocket.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.ForeignTradeRocket.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.ForeignTradeRocket.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.TradeRocket.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.TradeRocket.storable_resources, "Meat", "Food")
    end
    if IsDlcAvailable("picard") then
        lh.insert_after(BuildingTemplates.LanderRocket.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.LanderRocket.storable_resources, "Meat", "Food")
    end
    if IsDlcAccessible("prunariu") then
        lh.insert_after(Train.storable_resources, "Booze", "Food")
        lh.insert_after(Train.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.StationSmall.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.StationSmall.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.StationBig.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.StationBig.storable_resources, "Meat", "Food")
        lh.insert_after(BuildingTemplates.DynamicStorageDepot.storable_resources, "Booze", "Food")
        lh.insert_after(BuildingTemplates.DynamicStorageDepot.storable_resources, "Meat", "Food")
    end
    local idxFood, parent = lh.deep_search(XTemplates.customRocketExpedition,"comment","food")
    if not table.find(parent,"comment", "meat") then
        table.insert(parent, idxFood + 1, PlaceObj("XTemplateTemplate", {
            "comment", "meat",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local export = context.expedition and context.expedition.export
                local amount = export and export.Meat or 0
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('Meat' )><right><meat(ExportAmount_Meat, ExportTarget_Meat)>")
        }))
    end
    if not table.find(parent,"comment", "booze") then
        table.insert(parent, idxFood + 2, PlaceObj("XTemplateTemplate", {
            "comment", "booze",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local export = context.expedition and context.expedition.export
                local amount = export and export.Booze or 0
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('Booze' )><right><booze(ExportAmount_Booze, ExportTarget_Booze)>")
        }))
    end
    local idxFood, parent = lh.deep_search(XTemplates.customForeignAidRocket, "comment", "food")
    if not table.find(parent,"comment", "meat") then
        table.insert(parent, idxFood + 1, PlaceObj("XTemplateTemplate", {
            "comment", "meat",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local amount = context:GetStoredAmount("Meat")
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('Meat' )><right><meat(Stored_Meat)>")
        }))
    end
    if not table.find(parent,"comment", "booze") then
        table.insert(parent, idxFood + 2, PlaceObj("XTemplateTemplate", {
            "comment", "booze",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local amount = context:GetStoredAmount("Booze")
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('Booze' )><right><booze(Stored_Booze)>")
        }))
    end
end

function OnMsg.ClassesPostprocess()
    PlaceObj('Cargo', {
        SortKey = 3003300,
        description = T("More nutritious food to keep the colonists in good shape."),
        group = "Basic Resources",
        id = "Meat",
        kg = 500,
        name = T("Meat"),
        price = 6000000,
    })
    PlaceObj('Cargo', {
        SortKey = 3003600,
        description = T("If can't be locally produced alcohol from Earth is good enough."),
        group = "Basic Resources",
        id = "Booze",
        kg = 500,
        name = T("Booze"),
        price = 12000000,
    })
end

function OnMsg.ClassesBuilt()
    local matchFood = function(o) return string.match(TDevModeGetEnglishText(o.Text), "<food") end
    local idxFood, parent = lh.deep_search(XTemplates.ipRover, matchFood)
    if not table.find(parent, "Id", "idMeat") then
        table.insert(parent, idxFood + 1,
                PlaceObj('XTemplateTemplate', {
                    '__condition', function(parent, context)
                        return context:HasMember("GetStored_Meat")
                    end,
                    'Id', "idMeat",
                    '__template', "InfopanelText",
                    'Text', T("Meat<right><meat(Stored_Meat)>"),
                })
        )
    end
    if not table.find(parent, "Id", "idBooze") then
        table.insert(parent, idxFood + 2,
                PlaceObj('XTemplateTemplate', {
                    '__condition', function(parent, context)
                        return context:HasMember("GetStored_Booze")
                    end,
                    'Id', "idBooze",
                    '__template', "InfopanelText",
                    'Text', T("Booze<right><booze(Stored_Booze)>"),
                })
        )
    end
end