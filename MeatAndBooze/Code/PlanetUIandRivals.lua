local EnhanceLifeRow_LandingSiteObject = LandingSiteObject.EnhanceLifeRow
function LandingSiteObject:EnhanceLifeRow(row, rival)
    if rival then
        table.insert(row, 2, T({"<meat(meat)>", meat = Max(rival.resources.meat * const.ResourceScale, 0)}))
        table.insert(row, 3, T({"<booze(booze)>", booze = Max(rival.resources.booze * const.ResourceScale, 0)}))
    else
        local resourcesOverview = GetCityResourceOverview(UICity)
        table.insert(row, 2, T({"<meat(meat)>", meat = resourcesOverview:GetAvailableMeat()}))
        table.insert(row, 3, T({"<booze(booze)>", booze = resourcesOverview:GetAvailableBooze()}))
    end
    EnhanceLifeRow_LandingSiteObject(self, row, rival)
end

function OnMsg.ClassesPostprocess()
    if not IsDlcAccessible("gagarin") then
        return
    end
    table.insert(HelpOfferLSRes,2,{ ai_res = "meat", game_res = "Meat", min = 500, help = 50000 })
    table.insert(HelpOfferLSRes,3,{ ai_res = "booze", game_res = "Booze", min = 500, help = 50000 })
    table.insert(DistressCallLS,2,{message = T("We need Meat"), ai_res = "meat", game_res = "Meat", amount = 50})
    table.insert(DistressCallLS,3,{message = T("We need Booze"), ai_res = "booze", game_res = "Booze", amount = 50})
end

OnMsg.ModsReloaded = function(...)
    if (XTemplates.customTradePad) then
        table.insert_unique(TradePad.trade_resources, "Meat")
        table.insert_unique(TradePad.trade_resources, "Booze")
        local exports = lh.deep_find(XTemplates.customTradePad, "comment", "export/offer")
        local export_f = exports.OnContextUpdate
        exports.OnContextUpdate = function(self, context)
            local resource = ResolveValue(context, "export_resource")
            if resource == "Meat" or resource == "Booze" then
                self:SetIcon(CurrentModPath .. "UI/Icons/Sections/" .. resource .. "_6.tga")
                self:SetTitle(T({ 11503, "Offering: <resource(res)>", res = resource }))
            else
                export_f(self, context)
            end
            self:SetVisible(context:IsAvailable())
        end
        local imports = lh.deep_find(XTemplates.customTradePad, "comment", "import/ask")
        local import_f = imports.OnContextUpdate
        imports.OnContextUpdate = function(self, context)
            local resource = ResolveValue(context, "import_resource")
            if resource == "Meat" or resource == "Booze" then
                self:SetIcon(CurrentModPath .. "UI/Icons/Sections/" .. resource .. "_5.tga")
                self:SetTitle(T({ 11505, "Requesting: <resource(res)>", res = resource }))
            else
                import_f(self, context)
            end
            self:SetVisible(context:IsAvailable())
        end
    end
end

local SpawnRivalAI_vanilla = SpawnRivalAI
function SpawnRivalAI(preset, ...)
    local obj = SpawnRivalAI_vanilla(preset,...)
    print("Adding Life Support resource to", preset.id)
    UpdateRivalAI(obj, preset.id, Presets.DumbAIDef.Modded.lh_meat_booze)
    return obj
end

OnMsg.LoadGame = function()
    for k,v in pairs(RivalAIs) do
        UpdateRivalAI(v, k, Presets.DumbAIDef.Modded.lh_meat_booze)
    end
end

function OnMsg.ClassesPostprocess()
    if not IsDlcAvailable("gagarin") then
        return
    end
    PlaceObj("DumbAIDef", {
        biases = {},
        group = "Modded",
        id = "lh_meat_booze",
        initial_resources = {
            PlaceObj("AIResourceAmount", { "resource", "meat", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "booze", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "meat_production", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "booze_production", "amount", 0 }),
        },
        production_rules = {
            PlaceObj("AIProductionRule", {
                "rule_id", "Resource Production and Consumption (lukeh_resources)",
                "Run", function(self, resources, ai_player)
                    local res_references = {
                        { "meat", 50000, "Meat", 0 },
                        { "booze", 50000, "Booze", 0 },
                    }
                    local rival_stock, player_stock, change, diff
                    for _, res in pairs(res_references) do
                        local r1 = res[1]
                        rival_stock = Max(1000, resources[r1] * const.ResourceScale)
                        player_stock = Max(1000, GetCityResourceOverview(MainCity):GetAvailable(res[3])) + 1000
                        change = resources[res[1] .. "_production"]
                        diff = rival_stock - player_stock
                        diff = diff / player_stock
                        diff = diff / 100
                        if rival_stock > res[2] and diff > 20 then
                            change = change * (100 - Min(95, diff)) / 100
                        elseif diff < -20 then
                            change = change + change * -diff / 100
                        end
                        if rival_stock > res[2] and 0 < res[4] then
                            change = change - resources.buildings * res[4] / const.ResourceScale
                        end
                        resources[r1] = resources[r1] + change
                    end
                end
            }),
            PlaceObj("AIProductionRule", {
                "from_resource", "colonists",
                "mul", -50,
                "produce_resource", "meat",
                "rule_id", "Meat Consumption"
            }),
            PlaceObj("AIProductionRule", {
                "from_resource", "colonists",
                "mul", -50,
                "produce_resource", "booze",
                "rule_id", "Booze Consumption"
            }),
        },
        PlaceObj("AIAction", {
            "id", "increase Meat production",
            "log_entry", T("Constructed ranch"),
            "base_eval", 75000000,
            "delay", 90000,
            "tags", { "colonists" },
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 8 }),
                PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 6 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "meat_production", "amount", 3 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
            },
            "Eval", function(self, ai_player)
                local rations = ai_player.resources.colonists / 5
                if ai_player.resources.meat < 100 then
                    return 250 * const.Scale.mil
                elseif ai_player.resources.meat > 250 then
                    return 10 * const.Scale.mil
                elseif ai_player.resources.meat > 1000 then
                    return 1 * const.Scale.mil
                end
            end,
            "Run", function(self, ai_player)
                if not IsRivalRecentlySabotaged(ai_player) then
                    AIContestMilestone(ai_player, "ProduceFood", 72, 96)
                end
            end
        }),
        PlaceObj("AIAction", {
            "id", "increase Booze production",
            "log_entry", T("Constructed distillery"),
            "base_eval", 75000000,
            "delay", 90000,
            "tags", { "colonists" },
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 8 }),
                PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 6 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "booze_production", "amount", 2 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
            },
            "Eval", function(self, ai_player)
                local rations = ai_player.resources.colonists / 5
                if ai_player.resources.booze < 100 then
                    return 250 * const.Scale.mil
                elseif ai_player.resources.booze > 250 then
                    return 10 * const.Scale.mil
                elseif ai_player.resources.booze > 1000 then
                    return 1 * const.Scale.mil
                end
            end
        }),
        PlaceObj("AIAction", {
            "id", "call supply Rocket (Meat)",
            "log_entry", T("Imported Meat"),
            "base_eval", 150000000,
            "delay", 240000,
            "tags", { "stockpiling" },
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 250 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "meat", "amount", 40 })
            },
            "IsAllowed", function(self, ai_player)
                return ai_player.resources.meat < 0 and ai_player.resources.colonists > 0 and g_Consts.SupplyMissionsEnabled == 1
            end
        }),
        PlaceObj("AIAction", {
            "id", "call supply Rocket (Booze)",
            "log_entry", T("Imported Booze"),
            "base_eval", 150000000,
            "delay", 240000,
            "tags", { "stockpiling" },
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 250 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "booze", "amount", 40 })
            },
            "IsAllowed", function(self, ai_player)
                return ai_player.resources.booze < 0 and ai_player.resources.colonists > 0 and g_Consts.SupplyMissionsEnabled == 1
            end
        }),
        PlaceObj("AIAction", {
            "id", "construct Second Dome", --this is needed earlier cause AI now needs to produce more resources
            "log_entry", T (915496776171, "Constructed a small Dome"),
            "base_eval", 150000000,
            "delay", 1440000,
            "tags", { "colonists" },
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 100 }),
                PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 25 }),
                PlaceObj("AIResourceAmount", { "resource", "polymers", "amount", 10 }),
                PlaceObj("AIResourceAmount", { "resource", "oxygen", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "water", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "power", "amount", 30 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "colonists_max", "amount", 50 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "IsAllowed", function(self, ai_player)
                return not IsRivalRecentlySabotaged(ai_player) and (ai_player.resources.colonists_workforce < 10 or ai_player.resources.food > 100)
            end,
            "Eval", function(self, ai_player)
                if ai_player.resources.colonists_max > 40 and ai_player.resources.colonists_max < 110 then
                    local player_colonists = GetCityResourceOverview(MainCity):GetColonistCount()
                    if ai_player.resources.colonists_max > player_colonists then
                        return 0
                    elseif ai_player.resources.colonists_max < player_colonists then
                        return 250 * const.Scale.mil
                    end
                end
            end,
            "Run", function(self, ai_player)
                AIContestMilestone(ai_player, "ConstructDome", 48, 49)
            end
        }),
    })
end

local Initialize_MarsTradeRoute = MarsTradeRoute.Initialize
function MarsTradeRoute:Initialize(sponsor_id, ...)
    table.insert_unique(self.basic_import, "Meat")
    table.insert_unique(self.basic_import, "Booze")
    table.insert_unique(self.export_resource, "Meat")
    table.insert_unique(self.export_resource, "Booze")
    return Initialize_MarsTradeRoute(self, sponsor_id, ...)
end