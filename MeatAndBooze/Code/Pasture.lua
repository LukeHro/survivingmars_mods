Pasture.hadFood = false

function OnMsg.ClassesPostprocess()
    BuildingTemplates.OpenPasture.resource_produced1 = "Meat"
    BuildingTemplates.InsidePasture.resource_produced1 = "Meat"
    BuildingTemplates.OpenPasture.description = T("Produces Meat. Consumes Food, Water and Oxygen depending on animal type.")
    BuildingTemplates.InsidePasture.description = T("Produces Meat. Consumes Seeds, Water and Oxygen depending on animal type.")
end

function _ENV:UIBreedUpdate(pasture, index)
    local herd = pasture:GetHarvestTypeInfo(index)
    local active
    if pasture.current_herd then
        active = herd and pasture.current_harvest_idx == index and true
    else
        active = herd and pasture:GetNextHarvestIndex() == index
    end
    self.idActive:SetVisible(active)
    self.idProgress:SetVisible(active)
    local name = herd and herd.display_name
    self:SetTitle(active and "" or name or T(12423, "Select new breed"))
    self.idCropProduction:SetVisible(herd)
    if herd then
        local context = self.idCropProduction.context
        local production = pasture:CalcExpectedProduction(index, herd)
        context.prod = production / 1000
        context.frac = production / 100 % 10
        context.warn = active and (production == 0 and TLookupTag("<red>") or TLookupTag("<em>")) or ""
        context.icon = const.TagLookupTable.icon_Meat
        ObjModified(context)
    end
    function self:OnActivate(context)
        OpenInfopanelItems(pasture, self, context.crop_index)
    end
    self:SetIcon(herd and herd.display_icon or "UI/Icons/Buildings/crops_empty.tga")
    self.idIcon:SetColumns(2)
    if active then
    else
    end
    self:SetRolloverTitle(T({
        7603,
        "<name>  <percent(p)>",
        name = name,
        p = pasture:GetGrowthProgress()
    }) or "")
    function self:GetRolloverText()
        local texts = {}
        if herd then
            texts[#texts + 1] = T({
                248,
                "Water consumption<right><water(number)>",
                number = herd.water_consumption
            })
            texts[#texts + 1] = T({
                12586,
                "Oxygen consumption<right><air(number)>",
                number = herd.air_consumption
            })
            texts[#texts + 1] = T({
                "Seeds consumption per sol<right><seeds(number)>",
                number = pasture:GetHerdConsumption(herd)
            })
            texts[#texts + 1] = T({
                "Optimal meat output<right><meat(number)>",
                number = herd.food * herd.herd_size
            })
            texts[#texts + 1] = T({
                12588,
                "Growth time<right><time(lifetime)>",
                lifetime = herd.lifetime
            })
        else
            texts[#texts + 1] = T(12589, "Animals vary in growth time, Water and Oxygen consumption, and produce different amounts of Meat.")
        end
        texts[#texts + 1] = T(316, "<newline>")
        if not pasture.working then
            if pasture.current_herd then
                texts[#texts + 1] = T(12590, "<em>The ranch is not working. The animals are dying</em>")
            else
                texts[#texts + 1] = T(12591, "<em>The ranch is not working. New animals won't be bred</em>")
            end
        end
        return table.concat(texts, [[

<left>]])
    end
end

local oldGetInfopanelSelectorItems = GetInfopanelSelectorItems
function GetInfopanelSelectorItems(dataset, list)
    local object = dataset.object
    if object:IsKindOf("Pasture") then
        local items = {}
        local align = "bottom"
        for _, animal in ipairs(object:GetAnimalsInfo()) do
            local desctiption_texts = {
                animal.description
            }
            desctiption_texts[#desctiption_texts + 1] = T(316, "<newline>")
            desctiption_texts[#desctiption_texts + 1] = T({
                248,
                "Water consumption<right><water(number)>",
                number = animal.water_consumption
            })
            desctiption_texts[#desctiption_texts + 1] = T({
                12586,
                "Oxygen consumption<right><air(number)>",
                number = animal.air_consumption
            })
            desctiption_texts[#desctiption_texts + 1] = T({
                "Seeds consumption per sol<right><seeds(number)>",
                number = object:GetHerdConsumption(animal)
            })
            desctiption_texts[#desctiption_texts + 1] = T({
                "Optimal meat output<right><meat(number)>",
                number = animal.food * animal.herd_size
            })
            desctiption_texts[#desctiption_texts + 1] = T({
                12588,
                "Growth time<right><time(lifetime)>",
                lifetime = animal.lifetime
            })
            table.insert(items, HexButtonInfopanel:new({
                ButtonAlign = align,
                name = animal.display_name,
                icon = animal.display_icon,
                display_name = animal.display_name,
                description = table.concat(desctiption_texts, [[

<left>]]),
                hint = object:UIGetHint(),
                enabled = true,
                lock_image = "UI/Icons/bmc_lock.tga",
                gamepad_hint = object:UIGetHintGamepad(),
                action = function(dataset, delta, controller)
                    local pasture = dataset.object
                    if not IsValid(pasture) then
                        return
                    end
                    local broadcast = controller and delta < 0 or not controller and IsMassUIModifierPressed()
                    pasture:SetHarvestType(animal.id, dataset.idx, broadcast)
                end
            }, list))
            align = align == "top" and "bottom" or "top"
        end
        table.insert(items, HexButtonInfopanel:new({
            ButtonAlign = align,
            name = "",
            display_name = T(6864, "None"),
            description = T(6865, "None"),
            icon = "UI/Icons/Buildings/crops_empty.tga",
            action = function(dataset, delta, controller)
                local pasture = dataset.object
                if not IsValid(pasture) then
                    return
                end
                local broadcast = controller and delta < 0 or not controller and IsMassUIModifierPressed()
                pasture:SetHarvestType("None", dataset.idx, broadcast)
            end
        }, list))
        return items
    else
        return oldGetInfopanelSelectorItems(dataset, list)
    end
end

function Pasture:InitConsumptionRequest(...)
    self.consumption_resource_type = "Seeds"
    self.consumption_max_storage = 20000
    self.consumption_amount = 1
    self.consumption_type = g_ConsumptionType.Workshop
    HasConsumption.InitConsumptionRequest(self, ...)
    CreateGameTimeThread(function()
        Sleep(100)
        if IsKindOf(self, "InsidePasture") then
            self:GetAttaches("ConsumptionResourceStockpile")[1]:SetAttachOffset(1000, 10, 0)
        else
            self:GetAttaches("ConsumptionResourceStockpile")[1]:SetAttachOffset(-1000, 3700, 0)
        end
    end)
end

local BuildingUpdate_Pasture = Pasture.BuildingUpdate
function Pasture:BuildingUpdate(time, ...)
    local herd = self:GetCurrentHarvestType()
    if herd and self:IsValidHerd() and self:DoesHaveConsumption() and self.consumption_type == g_ConsumptionType.Workshop then
        if self.consumption_stored_resources == 0 and not (self:HasAir() or self:HasWater()) then
            self.current_herd_health = Max(self.current_herd_health - 1, 1) --kill them faster
        else
            local amount_to_consume = Min(self:GetHerdConsumption(herd, time), self.consumption_stored_resources)
            self.consumption_stored_resources = self.consumption_stored_resources - amount_to_consume
            self.consumption_resource_request:AddAmount(amount_to_consume)
            self.city:OnConsumptionResourceConsumed(self.consumption_resource_type, amount_to_consume)
            self:UpdateVisualStockpile()
            self:UpdateRequestConnectivity()
        end
        local hasFood = self:CanConsume()
        if hasFood ~= self.hadFood then
            self:AttachSign(not hasFood, "SignNoConsumptionResource")
            self.hadFood = hasFood;
        end
        if SelectedObj == self then
            RebuildInfopanel(self)
        end
    end
    BuildingUpdate_Pasture(self, time, ...)
end

function Pasture:GetBuildMenuProductionText(production_props)
    return T("Base production: <icon_Meat> based on breed")
end

function Pasture:GetHerdConsumption(herd, dt)
    local day_consumption = MulDivRound(herd.food, herd.herd_size, DivRound(herd.lifetime, const.DayDuration) * 3)
    local scaled_consumption = dt and MulDivRound(day_consumption, #(self.current_herd or empty_table), herd.herd_size) or day_consumption
    return dt and MulDivRound(scaled_consumption, dt, const.DayDuration) or scaled_consumption
end