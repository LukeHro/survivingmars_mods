local GetResourceProducedIcon_SingleResourceProducer = SingleResourceProducer.GetResourceProducedIcon
function SingleResourceProducer:GetResourceProducedIcon()
    local icon = GetResourceProducedIcon_SingleResourceProducer(self)
    if self.resource_produced == "Meat" or self.resource_produced == "Booze" then
        icon = CurrentModPath .. icon
    end
    return icon
end

local GetResourceProducedIcon_Mine = Mine.GetResourceProducedIcon
function Mine:GetResourceProducedIcon(idx)
    local icon = GetResourceProducedIcon_Mine(self, idx)
    if self.exploitation_resource == "Booze" or self.exploitation_resource == "Meat" then
        icon = CurrentModPath .. icon
    end
    return icon
end

local InitConsumptionRequest_HasConsumption = HasConsumption.InitConsumptionRequest
function HasConsumption:InitConsumptionRequest( ... )
    if self:IsKindOf("Service") and self:IsOneOfInterests("interestDrinking") then
        if (not self:DoesHaveConsumption()) then
            self.consumption_resource_type = "Booze"
            self.consumption_max_storage = 10000
            self.consumption_amount = 100
            self.consumption_type = 2
        else
            self.needBooze = self:AddAdditionalConsumption("Booze", 200, self.consumption_max_storage, self.consumption_type)
        end
    end
    InitConsumptionRequest_HasConsumption(self, ...)
end

local ConsumeVisit_HasConsumption = HasConsumption.Consume_Visit
function HasConsumption:Consume_Visit(unit, interest)
    if self.consumption_resource_type == "Meat" then
        if GameTime() - unit.last_meal >= const.DayDuration then
            local max = self.consumption_stored_resources
            if unit.assigned_to_service_with_amount then
                max = Min(max, unit.assigned_to_service_with_amount)
            end
            local use = unit:Eat(max)
            return self:Consume_Internal(use)
        end
    else
        return ConsumeVisit_HasConsumption(self, unit, interest)
    end
end

local GetUIConsumptionTexts_Building = Building.GetUIConsumptionTexts
function Building:GetUIConsumptionTexts(short)
    local text = GetUIConsumptionTexts_Building(self, short)
    if self.needMeat then
        text.resource = text.resource .. (short and " " or "<newline><left>") .. self:GetAdditionalConsumptionText(short, self.needMeat)
    end
    if self.needBooze then
        text.resource = text.resource .. (short and " " or "<newline><left>") .. self:GetAdditionalConsumptionText(short, self.needBooze)
    end
    return text
end

function OnMsg.ClassesPostprocess()
    local GetTemplateConsumptionDescription_res = GetTemplateConsumptionDescription
    function GetTemplateConsumptionDescription(template, base)
        base = GetTemplateConsumptionDescription_res(template, base)
        local needsMeat, needsBooze
        local i = 1
        while true do
            local interest = template["interest"..i]
            if not interest then break end
            if interest == "interestDining" then
                needsMeat = true
            end
            if interest == "interestDrinking" then
                needsBooze = true
            end
            i = i + 1
        end
        if needsMeat and needsBooze then -- mega mall
            return base .. T("/<icon_Meat>") .. T("/<icon_Booze>")
        end
        if needsMeat then -- diners
            return base .. T("/<icon_Meat>")
        end
        if needsBooze and template.consumption_resource_type ~= "Booze" then
            return base .. Untranslated(" ") .. FormatResource(empty_table, 1000, "Booze")
        end
        if template.template_class == "MicroGHabitat" then
            return base .. T("/<icon_Meat>") .. T("/<icon_Booze>")
        end
        return base
    end
end

function OnMsg.LHResStorageReady()
    function UniversalStorageDepot:DoesAcceptLifeSupportResources()
        return ResourceStockpileBase.DoesAcceptLifeSupportResources(self)
                or self:DoesAcceptResource("Meat") or self:DoesAcceptResource("Booze")
    end

    local idxFood, parent = lh.deep_search(XTemplates.sectionStorage, "Id", "Food")
    if not table.find(parent, "Id", "Booze") then
        local booze = PlaceObj('XTemplateTemplate', {
            'Id', "Booze",
            '__context', function (parent, context) return SubContext(context, {res = "Booze"}) end,
            '__template', "sectionStorageRow",
            'Title', T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
            'Icon', "UI/Icons/Sections/workshifts_active.tga",
            'TitleHAlign', "stretch",
        })
        table.insert(parent, idxFood + 1, booze)
    end
    if not table.find(parent, "Id", "Meat") then
        local meat = PlaceObj('XTemplateTemplate', {
            'Id', "Meat",
            '__context', function (parent, context) return SubContext(context, {res = "Meat"}) end,
            '__template', "sectionStorageRow",
            'Title', T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
            'Icon', "UI/Icons/Sections/workshifts_active.tga",
            'TitleHAlign', "stretch",
        })
        table.insert(parent, idxFood + 1, meat)
    end
end

function OnMsg.ClassesPostprocess()
    --table.remove_value(BuildingTemplates.UniversalStorageDepot.storable_resources,"Fuel")
    table.insert_unique(BuildingTemplates.UniversalStorageDepot.storable_resources,"Meat")
    table.insert_unique(BuildingTemplates.UniversalStorageDepot.storable_resources,"Booze")
end