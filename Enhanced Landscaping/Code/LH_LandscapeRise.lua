GlobalVar("CityLandscapeRise", {})
function GetLandscapeRiseController()
    local obj = CityLandscapeRise[UICity]
    if not obj then
        obj = LandscapeRiseController:new({
            city = UICity
        })
        CityLandscapeRise[UICity] = obj
    end
    return obj
end

DefineClass.LandscapeRiseDialog = {
    __parents = {
        "LandscapeConstructionDialog"
    },
    mode_name = "landscape_rise"
}

DefineClass.LandscapeRiseController = {
    __parents = {
        "LandscapeConstructionController"
    },
    brush_height = 5000,
    brush_height_max = 20000,
    brush_height_min = 1000,
    brush_height_step = 1000,
    height_sign = 1,
}
function LandscapeRiseController:Mark(test)
    LandscapeMarkCancel()
    LandscapeMarkRise(self:GetMapID(), self.last_pos, self.last_undo_pos, self.brush_radius, self.brush_height, test)
    local success = self:ValidateMark(true)
    local ready = success and self.last_undo_pos and not IsPlacingMultipleConstructions()
    return success, ready
end

function LandscapeMarkRise(map_id, pt1, pt0, radius, height, test)
    local landscape = Landscapes[LandscapeMark]
    if not landscape then
        return
    end
    if not pt0 then
        test = true
        pt0 = pt1
    end
    local game_map = GameMaps[map_id]
    local h0 = landscape.height + height
    local primes, bbox = Landscape_MarkLine(map_id, LandscapeMark, h0, pt0, h0, pt1, radius, game_map.landscape_grid, game_map.object_hex_grid.grid, test)
    if not primes then
        return
    end
    landscape.bbox = Extend(landscape.bbox, bbox)
    landscape.primes = landscape.primes + primes
    return true
end
