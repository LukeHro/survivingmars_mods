local GetConstructionController_vanilla = GetConstructionController
function GetConstructionController(mode)
    return mode == "landscape_rise" and GetLandscapeRiseController() or mode == "landscape_lower" and GetLandscapeLowerController() or GetConstructionController_vanilla(mode)
end

function OnMsg.LandscapeCompleted(ls)
    local game_map = GameMaps[ls:GetMapID()]
    game_map:RefreshBuildableGrid()
end

local OnKbdKeyDown_LandscapeConstructionDialog = LandscapeConstructionDialog.OnKbdKeyDown
function LandscapeConstructionDialog:OnKbdKeyDown(virtual_key, repeated)
    --why doesn't work without this hack?
    if (self.mode_name == "landscape_rise" or self.mode_name == "landscape_lower") then
        if virtual_key == 82 then
            if terminal.IsKeyPressed(const.vkControl) then
                GetConstructionController(self.mode_name):HeightAndResize(-1)
            else
                GetConstructionController(self.mode_name):HeightAndResize(-1, "radius")
            end
        elseif virtual_key == 84 then
            if terminal.IsKeyPressed(const.vkControl) then
                GetConstructionController(self.mode_name):HeightAndResize(1)
            else
                GetConstructionController(self.mode_name):HeightAndResize(1, "radius")
            end
        end
    end
    return OnKbdKeyDown_LandscapeConstructionDialog(self, virtual_key, repeated)
end

local OnShortcut_LandscapeConstructionDialog = LandscapeConstructionDialog.OnShortcut
function LandscapeConstructionDialog:OnShortcut(shortcut, source)
    local left = table.find(XTemplates.GameShortcuts[4], "ActionId", "actionRotBuildingLeft")
    left = left and XTemplates.GameShortcuts[4][left]
    local right = table.find(XTemplates.GameShortcuts[4], "ActionId", "actionRotBuildingRight")
    right = right and XTemplates.GameShortcuts[4][right]
    --ActionShortcut
    --ActionShortcut2
    --ActionGamepad

    if (self.mode_name == "landscape_rise" or self.mode_name == "landscape_lower") then
        if shortcut == "+RightTrigger" then
            self.radius = false
        elseif shortcut == "-RightTrigger" then
            self.radius = "radius"
        end

        if shortcut == "LeftShoulder" then
            GetConstructionController(self.mode_name):HeightAndResize(-1, self.radius)
            return "break"
        elseif shortcut == "RightShoulder" then
            GetConstructionController(self.mode_name):HeightAndResize(1, self.radius)
            return "break"
        end
    end
    if OnShortcut_LandscapeConstructionDialog then
        return OnShortcut_LandscapeConstructionDialog(self, shortcut, source)
    else
        return "break"
    end
end

function LandscapeConstructionController:HeightAndResize(step, mode)
    if mode == "radius" then
        self.brush_radius = Clamp(self.brush_radius + step * self.brush_radius_step, self.brush_radius_min, self.brush_radius_max)
    else
        self.last_brush_radius = self.brush_radius_min --hack to refresh
        self.brush_height = Clamp(self.brush_height + step * self.brush_height_step, self.brush_height_min, self.brush_height_max)
    end
    self:UpdateCursor(self.last_pos)
end

function OnMsg.ClassesBuilt()
    if not table.find(XTemplates.ipConstruction[1][7][2], "Id", "changeHeight") then
        local size = #XTemplates.ipConstruction[1][7][2]
        XTemplates.ipConstruction[1][7][2][size + 1] = XTemplates.ipConstruction[1][7][2][size]
        XTemplates.ipConstruction[1][7][2][size] = PlaceObj("XTemplateTemplate", {
            "Id", "changeHeight",
            "__condition", function(parent, context)
                return IsKindOf(context, "LandscapeRiseController") or IsKindOf(context, "LandscapeLowerController")
            end,
            "__template", "InfopanelText",
            "Text", T { "<em>Hold<RT></em>+<LB> Change Height <em>Hold<RT></em>+<RB>" }
        })
    end
    if not table.find(XTemplates.ipConstruction[1][7][1], "Id", "changeHeight") then
        local size = #XTemplates.ipConstruction[1][7][1]
        XTemplates.ipConstruction[1][7][1][size + 1] = XTemplates.ipConstruction[1][7][1][size]
        XTemplates.ipConstruction[1][7][1][size] = XTemplates.ipConstruction[1][7][1][size - 1]
        XTemplates.ipConstruction[1][7][1][size - 1] = PlaceObj("XTemplateTemplate", {
            "Id", "changeHeight",
            "__condition", function(parent, context)
                return IsKindOf(context, "LandscapeRiseController") or IsKindOf(context, "LandscapeLowerController")
            end,
            "__template", "InfopanelText",
            "Text", T { "<em>Ctrl + <ShortcutName('actionRotBuildingLeft')>, Ctrl + <ShortcutName('actionRotBuildingRight')></em> Change Height" }
        })
    end
end

--If not doing this can't build on raised/lowered terrain
local Done_LandscapeConstructionSiteBase = LandscapeConstructionSiteBase.Done
function LandscapeConstructionSiteBase:Done()
    Done_LandscapeConstructionSiteBase(self)
    if (self.building_class=="LH_Lower" or self.building_class=="LH_Rise") then
        GameMaps[self:GetMapID()]:RefreshBuildableGrid()
    end
end