local hour_duration = const.HourDuration
local suspend_reason = const.DustStormSuspendReason
local suspend_labels = const.DustStormSuspendBuildings

function OnMsg.ClassesPreprocess()
    SensorTower.LightningRodRange = 10
end

local GameInit_SensorTower = SensorTower.GameInit
function SensorTower:GameInit(...)
    GameInit_SensorTower(self, ...)
    self.electricity.charge = 0
    self.electricity.discharge = 20000
    self.electricity.current_storage = 0
    self.electricity.storage_capacity = 200000
    self.electricity.max_charge = 0
    self.electricity.max_discharge = 20000
    self.electricity.charge_efficiency = 100
    self.electricity.storage_mode = "empty"
    self.electricity.discharge_factor = SupplyGridElement.discharge_factor
    self.capacity = 200000
    self.max_electricity_discharge = 20000

end

function SensorTower:GetLightningRodRange(...)
    return SensorTower.LightningRodRange
end

local GameInit_CursorBuilding = CursorBuilding.GameInit
function CursorBuilding:GameInit(...)
    GameInit_CursorBuilding(self, ...)
    if IsKindOf(self.template, "SensorTower") then
        self.GetLightningRodRange = SensorTower.LightningRodRange
        ShowHexRanges(UICity, false, self, "GetLightningRodRange")
        ShowHexRanges(UICity, "SensorTower", false, "GetLightningRodRange")
    end
end

local Done_CursorBuilding = CursorBuilding.Done
function CursorBuilding:Done(...)
    Done_CursorBuilding(self, ...)
    if IsKindOf(self.template, "SensorTower") then
        HideHexRanges(UICity, "SensorTower", false, "GetLightningRodRange")
    end
end

function OnMsg.SelectionChange()
    if SelectedObj then
        if SelectedObj:IsKindOf("SensorTower") then
            ShowHexRanges(UICity, false, SelectedObj, "LightningRodRange")
        end
        if SelectedObj:IsKindOf("ConstructionSite") and SelectedObj.building_class == "SensorTower" then
            SelectedObj.LightningRodRange = SensorTower.LightningRodRange
            ShowHexRanges(UICity, false, SelectedObj, "LightningRodRange")
        end
    end
end

local apply_dust = function(label, dust, batch, batches)
    local IsObjInDome = IsObjInDome
    local count = #(label or "")
    local start_i, end_i = 1, count
    if batches < count then
        start_i = 1 + count * (batch - 1) / batches
        end_i = count * batch / batches
    elseif 1 < batch then
        return
    end
    for i = start_i, end_i do
        local obj = label[i]
        if not IsObjInDome(obj) then
            obj:AddDust(dust)
        end
    end
end
local apply_dust_elements = function(grid, dust, batch, batches)
    local IsKindOf = IsKindOf
    for i = 1, #(grid or "") do
        local elements = grid[i].elements
        local count = #elements
        local start_i, end_i = 1, count
        if batches < count then
            start_i = 1 + count * (batch - 1) / batches
            end_i = count * batch / batches
        elseif 1 < batch then
            end_i = 0
        end
        for i = start_i, end_i do
            local obj = elements[i].building
            if IsKindOf(obj, "DustGridElement") then
                obj:AddDust(dust)
            end
        end
    end
end

function StartDustStorm(storm_type, dust_storm)
    local city = MainCity
    local map_id = city.map_id
    g_DustStormDuration = SessionRandom:Random(dust_storm.min_duration, dust_storm.max_duration)
    g_DustStorm = {
        type = storm_type,
        descr = dust_storm,
        start_time = GameTime(),
        duration = g_DustStormDuration
    }
    Msg("DustStorm", map_id)
    RemoveDisasterNotifications(map_id)
    local preset = storm_type .. "DustStormDuration"
    g_DustStormStart = GameTime()
    g_DustStormEnd = g_DustStormStart + g_DustStormDuration
    local id = AddDisasterNotification(preset, {
        start_time = g_DustStormStart,
        expiration = g_DustStormDuration
    }, nil, map_id)
    ShowDisasterDescription("DustStorm", map_id)
    local target_dust = g_DustStorm.type == "great" and 2 * dust_storm.target_dust or dust_storm.target_dust
    local time = 0
    local next_strike = GameTime() + dust_storm.strike_interval + SessionRandom:Random(dust_storm.strike_random)
    if g_DustStorm.type == "electrostatic" then
        PlayFX({
            actionFXClass = "ElectrostaticStorm",
            actionFXMoment = "start",
            action_map_id = map_id
        })
    elseif g_DustStorm.type == "great" then
        PlayFX({
            actionFXClass = "GreatStorm",
            actionFXMoment = "start",
            action_map_id = map_id
        })
    else
        PlayFX({
            actionFXClass = "DustStorm",
            actionFXMoment = "start",
            action_map_id = map_id
        })
    end
    g_DustStormStopped = false
    local interval = 100
    local dust_labels = {
        "Building",
        "Drone",
        "Rover",
        "Dome"
    }
    local dust_grids = {
        "water",
        "electricity"
    }
    local max_count = 0
    for _, label_name in ipairs(dust_labels) do
        max_count = Max(#(city.labels[label_name] or ""), max_count)
    end
    for _, grid_name in ipairs(dust_grids) do
        for _, grid in ipairs(city[grid_name] or empty_table) do
            max_count = Max(#grid.elements, max_count)
        end
    end
    local period = Max(max_count / 2, 1000)
    local period_dust = target_dust * period / 1000
    local batches = period / interval
    local batch = 1
    local realm = GetRealm(city)
    while not g_DustStormStopped and g_DustStormDuration > 0 do
        for _, label_name in ipairs(dust_labels) do
            apply_dust(city.labels[label_name], period_dust, batch, batches)
        end
        for _, grid_name in ipairs(dust_grids) do
            apply_dust_elements(city[grid_name], period_dust, batch, batches)
        end
        if g_DustStorm.type == "electrostatic" and next_strike < GameTime() then
            next_strike = GameTime() + dust_storm.strike_interval + SessionRandom:Random(dust_storm.strike_random)
            local strike_pos = GetRandomPassable(city)
            local strike_radius = dust_storm.strike_radius
            local lightningRod = false
            realm:MapForEach(strike_pos, SensorTower.LightningRodRange * guim * 10 + GetEntityMaxSurfacesRadius(),
                    "SensorTower",
                    function(obj)
                        print("rod")
                        lightningRod = obj
                    end)
            if lightningRod then
                PlayFX({
                    actionFXClass = "ElectrostaticStorm",
                    actionFXMoment = "hit-moment" .. tostring(1 + SessionRandom:Random(4)),
                    action_pos = lightningRod:GetPos(),
                    action_map_id = map_id
                })
            else
                PlayFX({
                    actionFXClass = "ElectrostaticStormArea",
                    actionFXMoment = "start",
                    action_pos = strike_pos,
                    action_map_id = map_id
                })
                PlayFX({
                    actionFXClass = "ElectrostaticStorm",
                    actionFXMoment = "hit-moment" .. tostring(1 + SessionRandom:Random(4)),
                    action_pos = strike_pos,
                    action_map_id = map_id
                })
                local fuel_explosions
                local IsObjInDome = IsObjInDome
                local IsKindOf = IsKindOf
                local IsCloser2D = IsCloser2D
                local FuelExplosion = FuelExplosion
                realm:MapForEach(strike_pos, strike_radius + GetEntityMaxSurfacesRadius(), "Colonist", "Building", "Drone", "RCRover", "ResourceStockpileBase", function(obj)
                    if not IsCloser2D(obj, strike_pos, strike_radius) or IsObjInDome(obj) then
                        return
                    end
                    PlayFX({
                        actionFXClass = "ElectrostaticStormObject",
                        actionFXMoment = "start",
                        target = obj,
                        action_pos = strike_pos
                    })
                    if IsKindOf(obj, "Drone") then
                        obj:UseBattery(obj.battery)
                    elseif IsKindOf(obj, "RCRover") then
                        obj:SetCommand("Malfunction")
                    elseif IsKindOf(obj, "UniversalStorageDepot") then
                        if not IsKindOf(obj, "RocketBase") and obj:GetStoredAmount("Fuel") > 0 then
                            obj:CheatEmpty()
                            fuel_explosions = fuel_explosions or {}
                            fuel_explosions[#fuel_explosions + 1] = obj
                        end
                    elseif IsKindOf(obj, "ResourceStockpileBase") then
                        local amount = obj:GetStoredAmount()
                        if obj.resource == "Fuel" and 0 < amount then
                            obj:AddResourceAmount(-amount, true)
                            fuel_explosions = fuel_explosions or {}
                            fuel_explosions[#fuel_explosions + 1] = obj
                        end
                    elseif IsKindOf(obj, "Building") then
                        obj:SetSuspended(true, "Suspended", dust_storm.strike_suspend)
                        if IsKindOf(obj, "ElectricityStorage") then
                            obj.electricity.current_storage = Max(0, obj.electricity.current_storage - dust_storm.strike_discharge)
                        end
                    elseif IsKindOf(obj, "Colonist") and not obj:IsDying() then
                        obj:SetCommand("Die", "lighting strike")
                    end
                end)
                for _, obj in ipairs(fuel_explosions or empty_table) do
                    if IsValid(obj) then
                        FuelExplosion(obj)
                    end
                end
            end
        end
        if batch == 1 and time > hour_duration then
            for _, label in ipairs(g_SuspendLabels or suspend_labels) do
                for _, bld in ipairs(city.labels[label] or empty_table) do
                    if not bld.suspended and not IsObjInDome(bld) then
                        bld:SetSuspended(true, suspend_reason)
                    end
                end
            end
        end
        local delta = Min(g_DustStormDuration, interval)
        Sleep(delta)
        time = time + delta
        g_DustStormDuration = g_DustStormDuration - delta
        batch = batches > batch and batch + 1 or 1
    end
    local actionFXClass = "DustStorm"
    if g_DustStorm.type == "electrostatic" then
        actionFXClass = "ElectrostaticStorm"
    elseif g_DustStorm.type == "great" then
        actionFXClass = "GreatStorm"
    end
    PlayFX({
        actionFXClass = actionFXClass,
        actionFXMoment = "end",
        action_map_id = map_id
    })
    g_DustStorm = false
    g_DustStormStart = false
    g_DustStormEnd = false
    if id then
        --I've seen it throw an exception at last while triggering storms from ECM...the storm was active but no notification
        --just in case it happens cause otherwise when an exception is thrown there's no Msg and buildings remain not working
        RemoveOnScreenNotification(id, map_id)
    end
    Msg("DustStormEnded", map_id)
end

