function TransportBuildingDepot:GetResourcesToTransport(target)
    local to_transport = {}
    local res_lot = const.ResourceScale
    for _, resource in ipairs(target.resource) do
        local demand_request = target.demand[resource]
        if demand_request and demand_request:GetTargetAmount() > 0 then
            local transport_policy = self.transport_policy[resource] or "default"
            if not target.transport_policy or target.transport_policy[resource] ~= "send" then
                if transport_policy == "send" then
                    to_transport[resource] = self:GetStoredAmount(resource, false)
                elseif transport_policy ~= "accept" then
                    local excess = self:GetStoredAmount(resource, false) - target:GetStoredAmount(resource, true)
                    if 0 < excess then
                        to_transport[resource] = DivRound(excess / 2, res_lot) * res_lot
                    end
                end
            end
        end
    end
    return to_transport
end