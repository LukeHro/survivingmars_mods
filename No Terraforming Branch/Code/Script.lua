local InitResearch_City = City.InitResearch

function OnMsg.LoadGame()
	if #Presets.TechPreset.Terraforming == 0 or Presets.TechPreset.Terraforming.MartianVegetation.group~="Terraforming" then
		Presets.TechFieldPreset.Default.Terraforming = nil
		local idx = table.find(Presets.TechFieldPreset.Default, "id", "Terraforming")
		if idx then
			table.remove(Presets.TechFieldPreset.Default, idx)
		end
	end
end

function City:InitResearch( ... )

	if not Presets.TechFieldPreset.Default.Terraforming then
		return InitResearch_City(self, ...)
	end

	Presets.TechPreset.Terraforming.MartianVegetation.group = "Biotech"
	Presets.TechPreset.Terraforming.MartianVegetation.SortKey = #Presets.TechPreset.Biotech+1
	Presets.TechPreset.Biotech[#Presets.TechPreset.Biotech+1] = Presets.TechPreset.Terraforming.MartianVegetation
	Presets.TechPreset.Biotech.MartianVegetation = Presets.TechPreset.Terraforming.MartianVegetation

	Presets.TechPreset.Terraforming.GrowthStimulators.group = "Biotech"
	Presets.TechPreset.Terraforming.GrowthStimulators.SortKey = #Presets.TechPreset.Biotech+1
	Presets.TechPreset.Biotech[#Presets.TechPreset.Biotech+1] = Presets.TechPreset.Terraforming.GrowthStimulators
	Presets.TechPreset.Biotech.GrowthStimulators = Presets.TechPreset.Terraforming.GrowthStimulators

	Presets.TechPreset.Terraforming.AdaptedVegetation.group = "Biotech"
	Presets.TechPreset.Terraforming.AdaptedVegetation.SortKey = #Presets.TechPreset.Biotech+1
	Presets.TechPreset.Biotech[#Presets.TechPreset.Biotech+1] = Presets.TechPreset.Terraforming.AdaptedVegetation
	Presets.TechPreset.Biotech.AdaptedVegetation = Presets.TechPreset.Terraforming.AdaptedVegetation

	Presets.TechPreset.Terraforming.PlanetaryProjects.group = "Biotech"
	Presets.TechPreset.Terraforming.PlanetaryProjects.SortKey = #Presets.TechPreset.Biotech+1
	Presets.TechPreset.Biotech[#Presets.TechPreset.Biotech+1] = Presets.TechPreset.Terraforming.PlanetaryProjects
	Presets.TechPreset.Biotech.PlanetaryProjects = Presets.TechPreset.Terraforming.PlanetaryProjects

	Presets.TechPreset.Terraforming.TerraformingRover.group = "Robotics"
	Presets.TechPreset.Terraforming.TerraformingRover.SortKey = #Presets.TechPreset.Robotics+1
	Presets.TechPreset.Robotics[#Presets.TechPreset.Robotics+1] = Presets.TechPreset.Terraforming.TerraformingRover
	Presets.TechPreset.Robotics.TerraformingRover = Presets.TechPreset.Terraforming.TerraformingRover
	
	Presets.TechPreset.Terraforming.DomelessFarming.group = "Robotics"
	Presets.TechPreset.Terraforming.DomelessFarming.SortKey = #Presets.TechPreset.Robotics+1
	Presets.TechPreset.Robotics[#Presets.TechPreset.Robotics+1] = Presets.TechPreset.Terraforming.DomelessFarming
	Presets.TechPreset.Robotics.DomelessFarming = Presets.TechPreset.Terraforming.DomelessFarming

	Presets.TechPreset.Terraforming.TopologyAI.group = "Robotics"
	Presets.TechPreset.Terraforming.TopologyAI.SortKey = #Presets.TechPreset.Robotics+1
	Presets.TechPreset.Robotics[#Presets.TechPreset.Robotics+1] = Presets.TechPreset.Terraforming.TopologyAI
	Presets.TechPreset.Robotics.TopologyAI = Presets.TechPreset.Terraforming.TopologyAI

	Presets.TechPreset.Terraforming.ConservationLandscaping.group = "Robotics"
	Presets.TechPreset.Terraforming.ConservationLandscaping.SortKey = #Presets.TechPreset.Robotics+1
	Presets.TechPreset.Robotics[#Presets.TechPreset.Robotics+1] = Presets.TechPreset.Terraforming.ConservationLandscaping
	Presets.TechPreset.Robotics.ConservationLandscaping = Presets.TechPreset.Terraforming.ConservationLandscaping

	Presets.TechPreset.Terraforming.GreenhouseMars.group = "Engineering"
	Presets.TechPreset.Terraforming.GreenhouseMars.SortKey = #Presets.TechPreset.Engineering+1
	Presets.TechPreset.Engineering[#Presets.TechPreset.Engineering+1] = Presets.TechPreset.Terraforming.GreenhouseMars
	Presets.TechPreset.Engineering.GreenhouseMars = Presets.TechPreset.Terraforming.GreenhouseMars

	Presets.TechPreset.Terraforming.LakeCrafting.group = "Engineering"
	Presets.TechPreset.Terraforming.LakeCrafting.SortKey = #Presets.TechPreset.Engineering+1
	Presets.TechPreset.Engineering[#Presets.TechPreset.Engineering+1] = Presets.TechPreset.Terraforming.LakeCrafting
	Presets.TechPreset.Engineering.LakeCrafting = Presets.TechPreset.Terraforming.LakeCrafting

	Presets.TechPreset.Terraforming.CarbonateProcessor.group = "Engineering"
	Presets.TechPreset.Terraforming.CarbonateProcessor.SortKey = #Presets.TechPreset.Engineering+1
	Presets.TechPreset.Engineering[#Presets.TechPreset.Engineering+1] = Presets.TechPreset.Terraforming.CarbonateProcessor
	Presets.TechPreset.Engineering.CarbonateProcessor = Presets.TechPreset.Terraforming.CarbonateProcessor

	Presets.TechPreset.Terraforming.MegaSatellites.group = "Engineering"
	Presets.TechPreset.Terraforming.MegaSatellites.SortKey = #Presets.TechPreset.Engineering+1
	Presets.TechPreset.Engineering[#Presets.TechPreset.Engineering+1] = Presets.TechPreset.Terraforming.MegaSatellites
	Presets.TechPreset.Engineering.MegaSatellites = Presets.TechPreset.Terraforming.MegaSatellites

	Presets.TechPreset.Terraforming.PlanetarySurvey.group = "Social"
	Presets.TechPreset.Terraforming.PlanetarySurvey.SortKey = #Presets.TechPreset.Social+1
	Presets.TechPreset.Social[#Presets.TechPreset.Social+1] = Presets.TechPreset.Terraforming.PlanetarySurvey
	Presets.TechPreset.Social.PlanetarySurvey = Presets.TechPreset.Terraforming.PlanetarySurvey

	Presets.TechPreset.Terraforming.TeraTourism.group = "Social"
	Presets.TechPreset.Terraforming.TeraTourism.SortKey = #Presets.TechPreset.Social+1
	Presets.TechPreset.Social[#Presets.TechPreset.Social+1] = Presets.TechPreset.Terraforming.TeraTourism
	Presets.TechPreset.Social.TeraTourism = Presets.TechPreset.Terraforming.TeraTourism

	Presets.TechPreset.Terraforming.InterplanetaryProjects.group = "Social"
	Presets.TechPreset.Terraforming.InterplanetaryProjects.SortKey = #Presets.TechPreset.Social+1
	Presets.TechPreset.Social[#Presets.TechPreset.Social+1] = Presets.TechPreset.Terraforming.InterplanetaryProjects
	Presets.TechPreset.Social.InterplanetaryProjects = Presets.TechPreset.Terraforming.InterplanetaryProjects

	Presets.TechPreset.Terraforming.TerraformingSubsidies.group = "Social"
	Presets.TechPreset.Terraforming.TerraformingSubsidies.SortKey = #Presets.TechPreset.Social+1
	Presets.TechPreset.Social[#Presets.TechPreset.Social+1] = Presets.TechPreset.Terraforming.TerraformingSubsidies
	Presets.TechPreset.Social.TerraformingSubsidies = Presets.TechPreset.Terraforming.TerraformingSubsidies

	Presets.TechPreset.Terraforming.MagneticFieldGenerator.group = "Physics"
	Presets.TechPreset.Terraforming.MagneticFieldGenerator.SortKey = #Presets.TechPreset.Physics+1
	Presets.TechPreset.Physics[#Presets.TechPreset.Physics+1] = Presets.TechPreset.Terraforming.MagneticFieldGenerator
	Presets.TechPreset.Physics.MagneticFieldGenerator = Presets.TechPreset.Terraforming.MagneticFieldGenerator

	Presets.TechPreset.Terraforming.CoreHeatConvertor.group = "Physics"
	Presets.TechPreset.Terraforming.CoreHeatConvertor.SortKey = #Presets.TechPreset.Physics+1
	Presets.TechPreset.Physics[#Presets.TechPreset.Physics+1] = Presets.TechPreset.Terraforming.CoreHeatConvertor
	Presets.TechPreset.Physics.CoreHeatConvertor = Presets.TechPreset.Terraforming.CoreHeatConvertor

	Presets.TechPreset.Terraforming.NuclearTerraforming.group = "Physics"
	Presets.TechPreset.Terraforming.NuclearTerraforming.SortKey = #Presets.TechPreset.Physics+1
	Presets.TechPreset.Physics[#Presets.TechPreset.Physics+1] = Presets.TechPreset.Terraforming.NuclearTerraforming
	Presets.TechPreset.Physics.NuclearTerraforming = Presets.TechPreset.Terraforming.NuclearTerraforming

	Presets.TechPreset.Terraforming.TerraformingAmplification.group = "Physics"
	Presets.TechPreset.Terraforming.TerraformingAmplification.SortKey = #Presets.TechPreset.Physics+1
	Presets.TechPreset.Physics[#Presets.TechPreset.Physics+1] = Presets.TechPreset.Terraforming.TerraformingAmplification
	Presets.TechPreset.Physics.TerraformingAmplification = Presets.TechPreset.Terraforming.TerraformingAmplification

	table.clear(Presets.TechPreset.Terraforming)

	Presets.TechFieldPreset.Default.Terraforming = nil
	local idx = table.find(Presets.TechFieldPreset.Default, "id", "Terraforming")
	if idx then
		table.remove(Presets.TechFieldPreset.Default, idx)
	end

	return InitResearch_City(self, ...)
end