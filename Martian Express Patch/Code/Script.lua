function OnMsg.ModsReloaded()
    Presets.BuildingTemplate.Infrastructure.StationSmall.desire_slider_max = 36
    Presets.BuildingTemplate.Infrastructure.StationBig.desire_slider_max = 72
    ClassTemplates.Building.StationSmall.desire_slider_max = 36
    ClassTemplates.Building.StationBig.desire_slider_max = 72
end
function DynamicStorageDepot:GetMaxStorage(resource)
    return self.max_storage_per_resource
end

DynamicStorageDepot.GetMaxStorageForAnyOneResource = DynamicStorageDepot.GetMaxStorage

function TransportBuildingDepot:GetEmptyStorage(resource, include_inbound)
    return self:GetMaxStorage(resource) - self:GetStoredAmount(resource, include_inbound)
end

function Train:Start()
    if self.is_stopping then
        local destination = self.current_station
        if not IsGameRuleActive("EasyTrains") then
            if IsValid(destination) then
                destination.available_trains = destination.available_trains + 1
            else
                PlaceResourceStockpile_Delayed(self:GetPos(), self:GetMapID(),
                        Station.GetConstructResourceMetal(), g_Consts.TrainMetalCost / 2, self:GetAngle(), true)
                PlaceResourceStockpile_Delayed(self:GetPos(), self:GetMapID(),
                        Station.GetConstructResourceMP(), g_Consts.TrainMPCost / 2, self:GetAngle(), true)
            end
        end
        self:DestroySilent()
    else
        self:SetCommand("LoadTrain")
    end
end

local SetCommand_Colonist = Colonist.SetCommand
function Colonist:SetCommand(command, ...)
    if command == "Work" and self.leave_early_for_work and self.leave_early_for_work > 0 then
        --damn, I messed this somehow in ME
        return SetCommand_Colonist(self, command, self.workplace, true)
    end
    return SetCommand_Colonist(self, command, ...)
end

