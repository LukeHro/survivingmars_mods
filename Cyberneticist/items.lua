return {
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemCommanderProfilePreset', {
	SortKey = 100,
	challenge_mod = 30,
	display_name = T(711011, --[[ModItemCommanderProfilePreset Default LH_Cyberneticist display_name]] "Cyberneticist"),
	effect = T(711012, --[[ModItemCommanderProfilePreset Default LH_Cyberneticist effect]] "- Drone Assembler can work without people and are available as prefabs\n- Bonus Tech: <color 250 236 208>The Positronic Brain</color>"),
	group = "Default",
	id = "LH_Cyberneticist",
	name = "Cyberneticist",
	tech1 = "ThePositronicBrain",
}),
PlaceObj('ModItemGameRules', {
	SortKey = 101650,
	challenge_mod = 100,
	description = T(711013, --[[ModItemGameRules LH_NoArk description]] "Can not call a Passenger Rocket ever."),
	display_name = T(711014, --[[ModItemGameRules LH_NoArk display_name]] "There's No Ark"),
	exclusionlist = "TheLastArk, LH_BiorobotTechs",
	flavor = T(711015, --[[ModItemGameRules LH_NoArk flavor]] '<color  158 158 158>"Be alone, that is the secret of invention; be alone, that is when ideas are born."\n<right>Nikola Tesla</color><left>'),
	group = "Default",
	id = "LH_NoArk",
	PlaceObj('Effect_Code', {
		OnApplyEffect = function (self, city, parent)
if IsGameRuleActive("LH_NoArk") then
	g_ColonyNotViableUntil = -4
end
end,
		OnInitEffect = function (self, city, parent)
if IsGameRuleActive("LH_NoArk") then
	g_ColonyNotViableUntil = -4
end
end,
	}),
}),
PlaceObj('ModItemGameRules', {
	SortKey = 101660,
	challenge_mod = -20,
	description = T(711016, --[[ModItemGameRules LH_BiorobotTechs description]] "Thechs that help to increase the number of colonists also add some effect to speadup biorobot production."),
	display_name = T(711017, --[[ModItemGameRules LH_BiorobotTechs display_name]] "Enhanced Biorobotics"),
	exclusionlist = "LH_NoArk",
	flavor = T(711018, --[[ModItemGameRules LH_BiorobotTechs flavor]] '<color  158 158 158>"...I make friends. They\'re toys. My friends are toys. I make them. It\'s a hobby. I\'m a genetic designer."\n<right>J.F. Sebastian</color><left>'),
	group = "Default",
	id = "LH_BiorobotTechs",
}),
}
