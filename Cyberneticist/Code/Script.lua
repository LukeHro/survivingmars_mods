local function hasEnhancedBiorobotics()
    return IsGameRuleActive("LH_NoArk") or IsGameRuleActive("LH_BiorobotTechs")
end

DefineClass.Biorobot = {
    __parents = { "Vehicle" },
    display_name = T({ 7303, --[[TraitPreset Android display_name]] "Biorobot" }),
    description = T({ 7110108, "Biorobot built on earth and all accommodations needed to survive the transport." }),
    display_icon = "UI/Icons/IPButtons/biorobot.tga",
}

local LH_AndroidElectronicsCost = 5000

function OnMsg.ClassesBuilt()
    local orig_TrainingBuilding_CanTrain = TrainingBuilding.CanTrain
    function TrainingBuilding:CanTrain(unit)
        -- overwrites so biorobots can train in schools
        if (hasEnhancedBiorobotics()) and
                UIColony:IsTechResearched("ForeverYoung") and unit.traits.Android and IsKindOf(self, "School") and unit.traits.Youth and
                ((not unit.training_points) or (not unit.training_points[self.training_type]) or unit.training_points[self.training_type] < 200) then
            return true
        end
        return orig_TrainingBuilding_CanTrain(self, unit)
    end
    local orig_DroneFactory_UpdateDroneConstruction = DroneFactory.UpdateDroneConstruction
    function DroneFactory:UpdateDroneConstruction(delta)
        -- overwrites so biorobots can be built faster
        if not self.working then
            return
        end
        if hasEnhancedBiorobotics() and self.construction_queue[1] == "Android" and self:HasUpgrade("DroneFactory_HolographicScanner") and self:IsUpgradeOn("DroneFactory_HolographicScanner") then
            orig_DroneFactory_UpdateDroneConstruction(self, delta + delta / 3)
            return
        end
        orig_DroneFactory_UpdateDroneConstruction(self, delta)
    end
    PlaceObj('Cargo', {
        SortKey = 5000000,
        description = T({ 7110108, "Biorobot built on earth and and all accomodations needed to survive the transport." }),
        group = "Rovers",
        icon = "UI/Icons/IPButtons/biorobot.tga",
        id = "Biorobot",
        locked = true,
        kg = 5000,
        name = T { 7110110, "Biorobot" },
        pack = 1,
    })
end

local InitResearch_City = City.InitResearch

function City:InitResearch(...)
    if hasEnhancedBiorobotics() then
        Presets.TechPreset.Social.LiveFromMars.position = range(12, 15)
    else
        Presets.TechPreset.Social.LiveFromMars.position = range(1, 5)
    end
    InitResearch_City(self, ...)
end

local function AddAssemblerCargo()
    local comm = g_CurrentMissionParams and g_CurrentMissionParams.idCommanderProfile
    local assembler_cargo = RocketPayload_GetMeta("DroneFactory")
    if assembler_cargo then
        assembler_cargo.locked = (comm ~= "LH_Cyberneticist")
        assembler_cargo.group = (comm ~= "LH_Cyberneticist") and "Prefabs" or "Refab"
        assembler_cargo.kg = 10000
        assembler_cargo.price = 400000000
    end
end

local function AddBiorobotCargo(add)
    local robo_cargo = RocketPayload_GetMeta("Biorobot")
    if robo_cargo then
        robo_cargo.locked = (not add)
    end
end

local replacedPassengerTechs = 0
function InitTechs()
    if hasEnhancedBiorobotics() then
        replacedPassengerTechs = 1
        local tech = table.find_value(Presets.TechPreset.Engineering, 'id', "CompactPassengerModule")
        tech.display_name = T({ 7110101, "Compact Biorobot Design" })
        tech.description = T({ 7110102, "<em>Biorobots</em> cost less resources.\n\n" ..
                "<grey>Better design patterns and lighter, tougher materials have all contributed to a more advanced and compact Biorobot design reducing their production cost.</grey>" })
        tech = table.find_value(Presets.TechPreset.Biotech, 'id', "StemReconstruction")
        tech.description = T({ 7110105, "<em>Retirement age</em> and <em>death</em> from old age happen later in the Colonists' lifespan.\n\n" ..
                "<grey>Also thanks to stem manipulation we are able to alter the <em>Biorobots</em> when we build them to specialize in the needs of the colony.</grey>" })
        tech = table.find_value(Presets.TechPreset.Biotech, 'id', "HolographicScanning")
        tech.description = T({ 7110106, "<em>Medical Center</em> Upgrade (Holographic Scanner) - Increases birth rate in the Dome.\n\n" ..
                "<grey>Holographic technologies can significantly improve medical diagnostics and monitoring procedures.\n" ..
                "<grey>Used in <em>Drone Assemblers</em> to monitor <em>Biorobots</em> during construction speeding up the whole process but increases energy consumption.</grey>" })
        if not rawget(_G, "g_AT_modEnabled") then
            tech = table.find_value(Presets.TechPreset.Social, 'id', "LiveFromMars")
            tech.description = T({ 7110107, "More <em>applicants</em> will start to appear on Earth.\n\n" ..
                    "<grey>PR research aimed at exposing the benefits and positive aspects of life as a Colonist on Mars.\n" ..
                    "<em>Tourists</em> will also buy a ticket on cargo rockets from time to time and visit your colony.</grey>" })
        end
        tech = table.find_value(Presets.TechPreset.Breakthroughs, 'id', "ForeverYoung")
        --TODO something about Project Morpheus which adds perks only to adults
        tech.description = T({ 7110108, "<em>Seniors</em> can work and have children.\n\n" ..
                "<grey>Every man and woman on Mars has now the ability to remain within their most productive and fertile age forever.\n" ..
                "Applied in <em>Biorobot</em> constructions allows us to make create them in their youth and trainable in our schools</grey>" })
        tech = table.find_value(Presets.TechPreset.Breakthroughs, 'id', "CryoSleep")
        tech.description = T({ 7110109, "<em>Rockets</em> carry <param1> more Colonists.\n\n" ..
                "<grey>Putting our Colonists in a state of suspended animation for the duration of the flight to Mars greatly reduces the space needed for a single Colonist on the rocket.\n" ..
                "Cryo-tubes will allow us to transport more Colonists on a single rocket and installed on Cargo Rockets can transport <em>Biorobots</em> from Earth" })
    elseif replacedPassengerTechs then
        replacedPassengerTechs = 0
        local tech = table.find_value(Presets.TechPreset.Engineering, 'id', "CompactPassengerModule")
        tech.display_name = T({ 6371, "default" })
        tech.description = T({ 6372, "default" })
        tech = table.find_value(Presets.TechPreset.Biotech, 'id', "StemReconstruction")
        tech.description = T({ 6354, "default" })
        tech = table.find_value(Presets.TechPreset.Biotech, 'id', "HolographicScanning")
        tech.description = T({ 6347, "default" })
        tech = table.find_value(Presets.TechPreset.Social, 'id', "LiveFromMars")
        tech.description = T({ 6483, "default" })
        tech = table.find_value(Presets.TechPreset.Breakthroughs, 'id', "ForeverYoung")
        tech.description = T({ 6600, "default" })
        tech = table.find_value(Presets.TechPreset.Breakthroughs, 'id', "CryoSleep")
        tech.description = T({ 6604, "default" })
    end
end

function OnMsg.NewMapLoaded()
    CreateRealTimeThread(function()
        InitTechs()
        while (not GameState.gameplay) do
            AddAssemblerCargo()
            Sleep(500)
        end
    end)
end

local function AddDroneFactoryUpgrade()
    local group = Presets.BuildingTemplate.Production
    if hasEnhancedBiorobotics() and group then
        local i = table.find(group, 'id', "DroneFactory")
        group[i].upgrade1_id = "DroneFactory_HolographicScanner"
        group[i].upgrade1_display_name = T { 5140, "Holographic Scanner" }
        group[i].upgrade1_description = T { 7110111, "Reduces Biorobot construction time but adds +<power(5000)> consumption." }
        group[i].upgrade1_icon = "UI/Icons/Upgrades/holographic_scanner_01.tga"
        group[i].upgrade1_upgrade_cost_Polymers = 10000
        group[i].upgrade1_upgrade_cost_Electronics = 5000
        group[i].upgrade1_mod_prop_id_1 = "electricity_consumption"
        group[i].upgrade1_add_value_1 = 5000
    end
    for _, building in ipairs(UICity.labels.DroneFactory or empty_table) do
        building.upgrade1_id = "DroneFactory_HolographicScanner"
        building.upgrade1_display_name = T { 5140, "Holographic Scanner" }
        building.upgrade1_description = T { 7110111, "Reduces Biorobot construction time but adds +<power(5000)> consumption." }
        building.upgrade1_icon = "UI/Icons/Upgrades/holographic_scanner_01.tga"
        building.upgrade1_upgrade_cost_Polymers = 10000
        building.upgrade1_upgrade_cost_Electronics = 5000
        building.upgrade1_mod_prop_id_1 = "electricity_consumption"
        building.upgrade1_add_value_1 = 5000
        building:SetUIWorking(true)
    end
end

function OnMsg.LoadGame()
    InitTechs()
    AddAssemblerCargo()
    AddDroneFactoryUpgrade()
    AddBiorobotCargo(hasEnhancedBiorobotics() and UIColony:IsTechResearched("CryoSleep"))
    if (hasEnhancedBiorobotics() and UIColony:IsTechResearched("HolographicScanning")) then
        PlaceObj('Effect_UnlockUpgrade', {
            Upgrade = "DroneFactory_HolographicScanner",
        })
        UIColony:UnlockUpgrade("DroneFactory_HolographicScanner")
    end
    g_Consts.AndroidElectronicsCost = LH_AndroidElectronicsCost -- vanilla value
    if hasEnhancedBiorobotics() and UIColony:IsTechResearched("CompactPassengerModule") then
        g_Consts.AndroidElectronicsCost = g_Consts.AndroidElectronicsCost - LH_AndroidElectronicsCost / 5
    end
end

function OnMsg.ConstructionComplete(building)
    if building and IsKindOf(building, "DroneFactory") then
        local comm = g_CurrentMissionParams and g_CurrentMissionParams.idCommanderProfile
        if comm == "LH_Cyberneticist" or hasEnhancedBiorobotics() then
            if (comm == "LH_Cyberneticist") then
                building.automation = 3
                building.auto_performance = 60
            end
            building.upgrade1_id = "DroneFactory_HolographicScanner"
            building.upgrade1_display_name = T { 5140, "Holographic Scanner" }
            building.upgrade1_description = T { 7110111, "Reduces Biorobot construction time but adds +<power(5000)> consumption." }
            building.upgrade1_icon = "UI/Icons/Upgrades/holographic_scanner_01.tga"
            building.upgrade1_upgrade_cost_Polymers = 10000
            building.upgrade1_upgrade_cost_Electronics = 5000
            building.upgrade1_mod_prop_id_1 = "electricity_consumption"
            building.upgrade1_add_value_1 = 5000
            building:SetUIWorking(true)
        end
    end
end

function OnMsg.TechResearched(tech_id, city, first_time)
    if hasEnhancedBiorobotics() then
        if (tech_id == "CompactPassengerModule" or tech_id == "AdvancedPassengerModule") then
            g_Consts.AndroidElectronicsCost = g_Consts.AndroidElectronicsCost - LH_AndroidElectronicsCost / 5
        end
        if (tech_id == "CryoSleep") then
            AddBiorobotCargo(true)
        end
        if (tech_id == "HolographicScanning") then
            AddDroneFactoryUpgrade()
            PlaceObj('Effect_UnlockUpgrade', {
                Upgrade = "DroneFactory_HolographicScanner",
            })
            UIColony:UnlockUpgrade("DroneFactory_HolographicScanner")
        end
    end
end

function GetNeededSpecialists(city)
    --copied from MartianUniversity.lua
    local needed_specialist = {}
    for _, building in ipairs(city.labels.Workplace or empty_table) do
        local spec = building.specialist
        if spec ~= "none" and not building.destroyed and not building.demolishing and not building.bulldozed then
            local count = 0
            if building.active_shift > 0 then
                count = building.max_workers - (building.closed_workplaces[building.active_shift] or 0)
            else
                local max = building.max_workers
                for i = 1, building.max_shifts do
                    count = count + Max(0, max - (building.closed_workplaces[i] or 0))
                end
            end
            needed_specialist[spec] = (needed_specialist[spec] or 0) + count
        end
    end
    for _, spec in ipairs(ColonistSpecializationList) do
        local specs_count = 0
        local specs = city.labels[spec] or empty_table
        for _, colonist in ipairs(specs) do
            if colonist:CanWork() then
                specs_count = specs_count + 1
            end
        end
        needed_specialist[spec] = Max(0, (needed_specialist[spec] or 0) - specs_count)
    end
    return needed_specialist
end

local function GetMostNeededSpecialist()
    --copied from MartianUniversity.lua
    local best_v, best_s = 0
    local needed = GetNeededSpecialists(UICity) or empty_table
    for spec, count in pairs(needed) do
        if count > best_v then
            best_s = spec
            best_v = count
        end
    end
    return best_s
end

function OnMsg.ColonistBorn(colonist, event)
    --to allow completion of vanilla milestones and triger gameplay events
    if IsGameRuleActive("LH_NoArk") then
        Msg("ColonistArrived", colonist)
        Msg("ColonyApprovalPassed")
    end
    if (hasEnhancedBiorobotics() and event == "android" and UIColony:IsTechResearched("StemReconstruction")) then
        local most = GetMostNeededSpecialist()
        local specialization = most or table.interaction_rand(ColonistSpecializationList, "specialization")
        colonist:SetSpecialization(specialization)
    end
    if (hasEnhancedBiorobotics() and event == "android" and UIColony:IsTechResearched("ForeverYoung")) then
        colonist.age_trait = "Youth"
        colonist:AddTrait("Youth")
    end
end

function OnMsg.RocketLanded(rocket)
    if (hasEnhancedBiorobotics() and UIColony:IsTechResearched("LiveFromMars")and not rawget(_G, "g_AT_modEnabled")) then
        local colonist_table = GenerateColonistData(UICity, "Adult")
        local domes, safety_dome = GetDomesInWalkableDistance(UICity, rocket:GetPos())
        local dome = ChooseDome(colonist_table.traits, domes, safety_dome)
        colonist_table.dome = dome
        colonist_table.traits.Tourist = true
        colonist_table.specialist = "none"
        local colonist = Colonist:new(colonist_table)
        rocket:OnEnterUnit(colonist)
    end
    if (hasEnhancedBiorobotics() and UIColony:IsTechResearched("CryoSleep")) then
        for _, cargo in pairs(rocket.cargo or empty_table) do
            if (type(cargo) == "table" and cargo.amount and cargo.class == "Biorobot") then
                for _ = 1, cargo.amount do
                    rocket:SpawnAndroid()
                end
                cargo.amount = 0
            end
        end
    end
end

function SupplyRocket:SpawnAndroid()
    local colonist_table = GenerateColonistData(self.city, "Adult", "Martianborn", { no_specialization = true })
    if IsValid(self.parent_dome) then
        colonist_table.dome = self.parent_dome
    else
        local domes, safety_dome = GetDomesInWalkableDistance(self.city, self:GetPos())
        local dome = ChooseDome(colonist_table.traits, domes, safety_dome)
        colonist_table.dome = dome
    end
    colonist_table.traits["Android"] = true
    colonist_table.specialist = "none"
    local colonist = Colonist:new(colonist_table)
    self:OnEnterUnit(colonist)
    Msg("ColonistBorn", colonist, "android")
end

function School:BuildingUpdate(time, day, hour)
    --overwrites function in TrainingBuilding.lua in order to custom finish Biorobot training
    local visitors = self.visitors[self.current_shift] or empty_table
    local martianborn_adaptability = UIColony:IsTechResearched("MartianbornAdaptability") and TechDef.MartianbornAdaptability.param1
    for j = #visitors, 1, -1 do
        local unit = visitors[j]
        self:GainPoints(unit, time / 2, martianborn_adaptability)
        if (unit.training_points and unit.training_points[self.training_type] or 0) >= 100 then
            self:OnTrainingCompleted(unit)
            self.life_time_trained = self.life_time_trained + 1
            TrainingBuilding.FireWorker(self, unit)
            unit.training_points = unit.training_points or {}
            unit.training_points[self.training_type] = 200
        end
    end
end 