function Colonist:OnCommandStart()
    if self.command == "WorkCycle" then
        self.should_wait = true
        self:QueueCommand("WorkWait", 1)
        self:QueueCommand("WorkWait", 2)
        self:QueueCommand("WorkWait", 3)
    end
end

function Colonist:SearchFurtherForTransport(command)
    return command ~= "Work" and command ~= "WorkWait"
end

local StopWorkCycle_Workplace = Workplace.StopWorkCycle
function Workplace:StopWorkCycle(unit)
    return StopWorkCycle_Workplace(self, unit)
end

local StopWorkCycle_TrainingBuilding = TrainingBuilding.StopWorkCycle
function TrainingBuilding:StopWorkCycle(unit)
    return StopWorkCycle_TrainingBuilding(self, unit)
end

function Colonist:WorkWait(times)
    local workplace = self.workplace
    if not IsValid(workplace) or not self.dome or not self.should_wait then
        return
    end
    self.should_wait = false
    if self.work_route and self.work_route[1] == workplace then
        if self.work_route[3]:CanAcceptColonists() then
            return
        end
        if times > 1 and IsInWalkingDist(self, self.dome) then
            return
        end
        if times > 2 and self:GetTransportRoute(self.dome, "WorkWait") then
            return
        end
    else
        return
    end
    self.should_wait = true
    self:PushDestructor(function(self)
        local workplace = self.workplace
        if IsValid(workplace) then
            if self.dome ~= workplace then
                self:ExitBuilding(workplace)
            end
        end
    end)
    self:EnterBuilding(workplace)
    self:PlayPrg(GetWorkPrg(workplace), const.HourDuration / Min(2, times) + self:Random(2 * const.MinuteDuration), workplace)
    self:PopAndCallDestructor()
end

function ServiceBase:BuildingUpdate()
    for _, visitor in pairs(self.visitors or empty_table) do
        if visitor.workplace and visitor:ShouldLeaveForWorkAlready() then
            self:Unassign(visitor)
            visitor:SetCommand("Idle")
        end
    end
end

local CanBeUsedBy_ServiceBase = ServiceBase.CanBeUsedBy
function ServiceBase:CanBeUsedBy(colonist)
    if colonist.workplace and colonist:ShouldLeaveForWorkAlready() then
        return false, "should go to work"
    end
    return CanBeUsedBy_ServiceBase(self, colonist)
end

local CanService_ServiceBase = ServiceBase.CanService
function ServiceBase:CanService(unit)
    if unit.workplace and unit:ShouldLeaveForWorkAlready() then
        return false
    end
    return CanService_ServiceBase(self, unit)
end

local SetCommand_Colonist = Colonist.SetCommand
function Colonist:SetCommand(command, ...)
    if command == "Work" and self.leave_early_for_work and self.leave_early_for_work > 0 then
        --damn, I messed this somehow in ME
        return SetCommand_Colonist(self, command, self.workplace, true)
    end
    return SetCommand_Colonist(self, command, ...)
end

function Colonist:ShouldLeaveForWorkAlready()
    if self:ShouldLeaveForWork() then
        return true
    elseif self.workplace and self.leave_early_for_work then
        local hour = UIColony.hour
        local workshift_start = const.DefaultWorkshifts[self.workplace_shift][1]
        return hour >= workshift_start - 3 and hour <= workshift_start
    else
        return false
    end
end