Station.allow_transfer = true

function Station:HasTransferOption()
    return self.template_name ~= "StationSmall"
end

function Station:GetReachableStations(colonist, no_hops)
    local direct_connections = table.filter(self.linked_stations, function(track, station)
        if IsValid(track) and IsValid(station) then
            return track:CanTransportColonists() and station:CanColonistsFromDifferentDomesWorkServiceTrainHere()
        else
            return false
        end
    end)
    if no_hops then
        return direct_connections
    end
    local with_hops = {}
    for _, s in pairs(direct_connections) do
        table.insert_unique(with_hops, s)
    end
    for _, s in pairs(direct_connections) do
        if s.allow_transfer then
            for t, rs in pairs(s.linked_stations) do
                if rs ~= self and IsValid(t) and IsValid(rs)
                        and t:CanTransportColonists() and rs:CanColonistsFromDifferentDomesWorkServiceTrainHere() then
                    table.insert_unique(with_hops, rs)
                end
            end
        end
    end
    return with_hops
end

function Station:HasConnectedTransport(destination)
    for t, rs in pairs(self.linked_stations) do
        if rs == destination and IsValid(t) and IsValid(rs)
                and t:CanTransportColonists() and rs:CanColonistsFromDifferentDomesWorkServiceTrainHere() then
            return true
        end
    end
end

function Colonist:ShouldBoardTrain(target_station)
    return (self.transport_ticket.dst_station == target_station or target_station:HasConnectedTransport(self.transport_ticket.dst_station))
            and  #target_station.waiting_for_train + #target_station.colonists_inbound < target_station.max_colonists_to_transport
end

local GoToStation_Colonist = Colonist.GoToStation
function Colonist:GoToStation(station)
    if station then
        local ticket = self.transport_ticket
        ticket.stage = "Inbound"
        ticket.origin = ticket.src_station
        ticket.src_station = station
        local vehicle = ticket.vehicle
        local travel_time = GameTime() - ticket.start_wait
        ticket.start_wait = GameTime()
        vehicle:AddSpentTime(travel_time)
        vehicle.track:AddSpentTime(travel_time)
        RebuildInfopanel(vehicle.track)
        self:SetHolder(station)
    end
    GoToStation_Colonist(self)
end

local BoardVehicle_Colonist = Colonist.BoardVehicle
function Colonist:BoardVehicle(vehicle)
    if vehicle:GetNextStation() ~= self.transport_ticket.dst_station then
        table.insert_unique(vehicle:GetNextStation().colonists_inbound, self)
    end
    if self.transport_ticket.origin then
        TrainsLogging.info(self, "boarding connection")
        table.remove_entry(self.transport_ticket.src_station.waiting_for_train, self)
        self.transport_ticket.src_station = self.transport_ticket.origin
        self.transport_ticket.origin = false
    end
    BoardVehicle_Colonist(self, vehicle)
end

function OnMsg.ClassesBuilt()
    local old_idx = table.find(XTemplates.customStation[1], "Id", "colonists_transfer")
    if not old_idx then
        table.insert_unique(XTemplates.customStation[1],PlaceObj("XTemplateTemplate", {
            "Id", "colonists_transfer",
            "comment", "allow/forbid transfer",
            "__template", "InfopanelButton",
            "__condition", function(parent, context)
                return context:HasTransferOption()
            end,
            "RolloverHint", T(11255, "<left_click> Toggle <newline><em>Ctrl + <left_click></em> Toggle for all <display_name_pl>"),
            "RolloverHintGamepad", T(454042608125, "<ButtonA> Toggle <newline><ButtonX> Toggle for all <display_name_pl>"),
            "OnContextUpdate", function(self, context)
                if context.allow_transfer then
                    self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/transfer_allow.tga")
                    self:SetRolloverTitle(T("Allow transfer"))
                    self:SetRolloverText(T("Allow colonists to change trains here.\n\nColonists can change one train per trip."))
                else
                    self:SetIcon(CurrentModPath.."UI/Icons/IPButtons/transfer_block.tga")
                    self:SetRolloverTitle(T("Block transfer"))
                    self:SetRolloverText(T("Don't allow colonists to change trains here.\n\nColonists can change one train per trip."))
                end
            end,
            "OnPress", function(self, gamepad)
                self.context:ToggleTransfer(not gamepad and IsMassUIModifierPressed())
            end,
            "AltPress",
            true,
            "OnAltPress", function(self, gamepad)
                if gamepad then
                    self.context:ToggleTransfer(true)
                end
            end,
        }))
    end
end

function Station:ToggleTransfer(broadcast)
    self.allow_transfer = not self.allow_transfer
    ObjModified(self)
    if broadcast then
        for _, station in ipairs(self.city.labels.Station or empty_table) do
            if station:HasTransferOption() then
                station.allow_transfer = self.allow_transfer
            else
                station.allow_transfer = false
            end
        end
    end
end