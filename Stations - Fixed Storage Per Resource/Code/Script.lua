function OnMsg.ModsReloaded()
    Presets.BuildingTemplate.Infrastructure.StationSmall.desire_slider_max = 30
    Presets.BuildingTemplate.Infrastructure.StationBig.desire_slider_max = 60
    ClassTemplates.Building.StationSmall.desire_slider_max = 30
    ClassTemplates.Building.StationBig.desire_slider_max = 60
    Presets.BuildingTemplate.Infrastructure.StationSmall.max_storage_per_resource = 30 * const.ResourceScale
    Presets.BuildingTemplate.Infrastructure.StationBig.max_storage_per_resource = 60 * const.ResourceScale
    ClassTemplates.Building.StationSmall.max_storage_per_resource = 30 * const.ResourceScale
    ClassTemplates.Building.StationBig.max_storage_per_resource = 60 * const.ResourceScale
end

function TransportBuildingDepot:GetMaxStorage(resource)
    if not resource then
        local storable_resources = table.copy(self.storable_resources)
        if not IsDlcAvailable("armstrong") then
            table.remove_value(storable_resources, "Seeds")
        end
        if not IsDlcAvailable("picard") then
            table.remove_value(storable_resources, "PreciousMinerals")
        end
        if BuildMenuPrerequisiteOverrides.StorageMysteryResource then
            table.remove_value(storable_resources, "MysteryResource")
        end
        if not IsTechResearched("BlackCubesDisposal") then
            table.remove_value(storable_resources, "BlackCube")
        end
        return #storable_resources * self.max_storage_per_resource
    end
    return self.max_storage_per_resource
end

function TransportBuildingDepot:GetMaxStorageForAnyOneResource()
    return self.max_storage_per_resource
end

function TransportBuildingDepot:RefreshDemand()
    --not needed
end

function TransportBuildingDepot:IsFull(resource, include_inbound)
    return self:GetStoredAmount(resource, include_inbound) >= self:GetMaxStorage(resource)
end

function TransportBuildingDepot:CheatFill()
    local storable_resources = self.storable_resources
    local resource_count = #storable_resources
    self:InterruptDrones(nil, function(drone)
        local r = drone.d_request
        if r and self.demand[r:GetResource()] == r then
            return drone
        end
    end)
    for i = 1, resource_count do
        local resource_name = storable_resources[i]
        if self.supply[resource_name] then
            self:AddResource(self.max_storage_per_resource - self:GetStoredAmount(resource_name, true), resource_name)
        end
    end
end
TransportBuildingDepot.Fill = TransportBuildingDepot.CheatFill

function SavegameFixups.PerResourceStations()
    for _, city in ipairs(Cities) do
        for _, station in ipairs(city.labels.Station or empty_table) do
            station.max_storage_per_resource = ClassTemplates.Building[station.template_name].max_storage_per_resource
            station.desire_slider_max = ClassTemplates.Building[station.template_name].desire_slider_max
            for _, resource in pairs(station.storable_resources) do
                station.demand[resource]:SetAmount(station:IsFull(resource) and 0
                        or (station:GetMaxStorage(resource) - station:GetStoredAmount(resource)))
            end
        end
    end
end