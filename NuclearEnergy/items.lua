return {
PlaceObj('ModItemCode', {
	'name', "Radioactive",
	'FileName', "Code/Radioactive.lua",
}),
PlaceObj('ModItemCode', {
	'name', "NuclearFuel",
	'FileName', "Code/NuclearFuel.lua",
}),
PlaceObj('ModItemCode', {
	'name', "BuildingTemplate",
	'FileName', "Code/BuildingTemplate.lua",
}),
PlaceObj('ModItemCode', {
	'name', "CCCandInfobar",
	'FileName', "Code/CCCandInfobar.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Transportation",
	'FileName', "Code/Transportation.lua",
}),
PlaceObj('ModItemCode', {
	'name', "PlanetUIandRivals",
	'FileName', "Code/PlanetUIandRivals.lua",
}),
PlaceObj('ModItemCode', {
	'name', "Buildings",
	'FileName', "Code/Buildings.lua",
}),
PlaceObj('ModItemCode', {
	'name', "LanderRocket",
	'FileName', "Code/LanderRocket.lua",
}),
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_NuclearFuelContainer",
	'entity_name', "LH_NuclearFuelContainer",
	'import', "Entities/LH_NuclearFuelContainer.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_NuclearFuelContainer10",
	'entity_name', "LH_NuclearFuelContainer10",
	'import', "Entities/LH_NuclearFuelContainer10.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_NuclearFuelDepo",
	'entity_name', "LH_NuclearFuelDepo",
	'import', "Entities/LH_NuclearFuelDepo.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_NuclearFuelDepo_A",
	'entity_name', "LH_NuclearFuelDepo_A",
	'import', "Entities/LH_NuclearFuelDepo_A.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_RadioactiveContainer",
	'entity_name', "LH_RadioactiveContainer",
	'import', "Entities/LH_RadioactiveContainer.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_RadioactiveContainer10",
	'entity_name', "LH_RadioactiveContainer10",
	'import', "Entities/LH_RadioactiveContainer10.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_RadioactiveDepo",
	'entity_name', "LH_RadioactiveDepo",
	'import', "Entities/LH_RadioactiveDepo.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_RadioactiveDepo_A",
	'entity_name', "LH_RadioactiveDepo_A",
	'import', "Entities/LH_RadioactiveDepo_A.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_RadioactiveDeposit",
	'entity_name', "LH_RadioactiveDeposit",
	'import', "Entities/LH_RadioactiveDeposit.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_NuclearLab",
	'entity_name', "LH_NuclearLab",
	'import', "Entities/LH_NuclearLab.ent",
}),
PlaceObj('ModItemEntity', {
	'name', "LH_RadioactiveDisabledDeposit",
	'entity_name', "LH_RadioactiveDisabledDeposit",
	'import', "Entities/LH_RadioactiveDisabledDeposit.ent",
}),
}
