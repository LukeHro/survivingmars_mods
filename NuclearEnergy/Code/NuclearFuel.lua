Resources["NuclearFuel"] = {
    id = "NuclearFuel",
    name = "NuclearFuel",
    display_name = T("Nuclear Fuel"),
    display_icon = CurrentModPath.."UI/Icons/Buildings/res_nuclearfuel.tga",
    unit_amount = const.ResourceScale,
    color = RGB(255, 128, 0),
    entity = "LH_NuclearFuelContainer",
    description = T("Nuclear Fuel.")
}

table.insert_unique(ResourceDescription, Resources["NuclearFuel"])
table.insert_unique(StockpileResourceList, "NuclearFuel")
table.insert_unique(CargoShuttle.storable_resources, "NuclearFuel")
table.insert_unique(AllResourcesList, "NuclearFuel")
table.insert_unique(RCTransport.storable_resources,"NuclearFuel")
table.insert_unique(ConstructionResourceList,"NuclearFuel")
table.insert_unique(SubSurfaceDeposits, "NuclearFuel")

function OnMsg.ClassesPostprocess()
    table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotNuclearFuel" }))
    BuildingTechRequirements["LH_MechanizedDepotNuclearFuel"] = {{ tech = "AutomatedStorage", hide = false }}
end

const.TagLookupTable["icon_NuclearFuel"] = "<image "..CurrentModPath.."UI/Icons/res_nuclearfuel.tga 1300>"
const.TagLookupTable["icon_NuclearFuel_small"] = "<image "..CurrentModPath.."UI/Icons/res_nuclearfuel.tga 800>"
const.TagLookupTable["icon_NuclearFuel_orig"] = "<image "..CurrentModPath.."UI/Icons/res_nuclearfuel.tga>"

function StorageDepot:Getui_NuclearFuel()
    return T({"NuclearFuel<right><nuclearfuel(Stored_NuclearFuel,MaxAmount_NuclearFuel)>", self})
end


function InfobarObj:GetNuclearFuelText()
    local nuclearfuel = GetCityResourceOverview(UICity):GetAvailableNuclearFuel() / const.ResourceScale
    return T({"<nuclearfuel><icon_NuclearFuel_orig>", nuclearfuel = self:FmtRes(nuclearfuel)})
end

function InfobarObj:GetNuclearFuelRollover()
    return GetCityResourceOverview(UICity):GetNuclearFuelRollover()
end

function ResourceOverview:GetNuclearFuelRollover()
    local ret = {self:GetBasicResourcesHeading(),
                 T(316, "<newline>"),
                 T({"Nuclear Fuel production<right><nuclearfuel(NuclearFuelProducedYesterday)>", self}),
                 T({"Nuclear Fuel consumption<right><nuclearfuel(NuclearFuelConsumedByConsumptionYesterday)>", self}),
                 T({"Nuclear Fuel maintenance<right><nuclearfuel(NuclearFuelConsumedByMaintenanceYesterday)>", self}),
                 T({"Upgrade construction<right><nuclearfuel(NuclearFuelUpgradeConstructionActual, NuclearFuelUpgradeConstructionTotal)>", self})
    }
    return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetNuclearFuelProducedYesterday"] = function(self)
    return self:GetProducedYesterday("NuclearFuel")
end
ResourceOverview["GetNuclearFuelGatheredYesterday"] = function(self)
    return self:GetGatheredYesterday("NuclearFuel")
end
ResourceOverview["GetNuclearFuelConsumedByConsumptionYesterday"] = function(self)
    return self:GetConsumedByConsumptionYesterday("NuclearFuel")
end
ResourceOverview["GetNuclearFuelConsumedByMaintenanceYesterday"] = function(self)
    return self:GetEstimatedDailyMaintenance("NuclearFuel")
end
ResourceOverview["GetAvailableNuclearFuel"] = function(self)
    return self:GetAvailable("NuclearFuel")
end
ResourceOverview["GetNuclearFuelInConstructionSitesActual"] = function(self)
    return self:GetInConstructionSites("NuclearFuel", "actual")
end
ResourceOverview["GetNuclearFuelInConstructionSitesTotal"] = function(self)
    return self:GetInConstructionSites("NuclearFuel", "total")
end
ResourceOverview["GetNuclearFuelUpgradeConstructionActual"] = function(self)
    return self:GetUpgradeConstruction("NuclearFuel", "actual")
end
ResourceOverview["GetNuclearFuelUpgradeConstructionTotal"] = function(self)
    return self:GetUpgradeConstruction("NuclearFuel", "total")
end

function OnMsg.ClassesPostprocess()
    table.insert_unique(FormattableResources, {text = "Nuclear Fuel", value = "nuclearfuel"})
    TFormat.nuclearfuel = function(context_obj, value, max)
        return FormatResourceValueMaxResource(context_obj, value, max, "NuclearFuel")
    end
end