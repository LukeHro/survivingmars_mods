local Init_Exploration = Exploration.Init
function Exploration:Init(...)
    Init_Exploration(self, ...)
    local city = Cities[self.map_id]
    --if city == MainCity then
        for i = 1, 10 do
            for j = 1, 10 do
                local sector = city.MapSectors[i][j]
                for _, deposit in ipairs(sector.markers.subsurface or "") do
                    if deposit.resource == "PreciousMetals" then
                        LH_Res_AddDeposit(city, "Radioactive", 1000, 1)
                    end
                end
                for _, deposit in ipairs(sector.markers.deep or "") do
                    if deposit.resource == "PreciousMetals" then
                        LH_Res_AddDeposit(city, "Radioactive", 2000, 2)
                    end
                end
            end
        end
    --end
end

DefineClass("SubsurfaceDepositRadioactive",
        { __parents = { "SubsurfaceDeposit" },
          resource = "Radioactive",
          entity = "LH_RadioactiveDeposit",
          disabled_entity = "LH_RadioactiveDisabledDeposit",
          display_icon = "UI/Icons/Buildings/deposit_polymers.tga",
            --name and description is reassigned when setting a new 'depth_layer' (see SubsurfaceDeposit:Setdepth_layer)
          display_name = T("Underground Radioactive Materials"),
          description = T("An underground deposit of Radioactive Materials."),
          display_name_deep = T("Deep Radioactive"),
          description_deep = T("Deep underground deposit of Radioactive Materials."),
          fx_actor_class = "SubsurfaceAnomaly"
        })

DefineConst({
    group = "Gameplay",
    help = T("Deep Radioactive Materials deposits are exploitable when this is not 0"),
    id = "IsDeepRadioactiveExploitable",
    name = T("Exploitable Radioactive Materials"),
    value = 0,
})

SubSurfaceDeposits["Radioactive"] = true

DepositGradeToWasteRockMultipliers.Radioactive = {
    Depleted = 1200,
    ["Very Low"] = 1000,
    Low = 900,
    Average = 700,
    High = 600,
    ["Very High"] = 400
}

function OnMsg.LoadGame( ... )
    if not g_Consts["IsDeepRadioactiveExploitable"] == 1 and UICity:IsTechResearched("DeepMetalExtraction") then
        g_Consts["IsDeepRadioactiveExploitable"] = 1
        MapForEach("map", "SubsurfaceDepositRadioactive", function(obj)
            obj:UpdateEntity()
        end)
    end
end

function OnMsg.TechResearched(tech_id, city)
    if tech_id == "DeepMetalExtraction" then
        g_Consts["IsDeepRadioactiveExploitable"] = 1
        MapForEach("map", "SubsurfaceDepositRadioactive", function(obj)
            obj:UpdateEntity()
        end)
    end
end

local vanillaIsDeepExploitable = IsDeepExploitable
function IsDeepExploitable(res)
    if res == "Radioactive" then
        return g_Consts["IsDeepRadioactiveExploitable"] ~= 0
    else
        return vanillaIsDeepExploitable(res)
    end
end

local GetResourceProducedIcon_SingleResourceProducer = SingleResourceProducer.GetResourceProducedIcon
function SingleResourceProducer:GetResourceProducedIcon()
    local icon = GetResourceProducedIcon_SingleResourceProducer(self)
    if self.resource_produced == "Radioactive" or self.resource_produced == "NuclearFuel" then
        icon = CurrentModPath .. icon
    end
    return icon
end

local GetResourceProducedIcon_Mine = Mine.GetResourceProducedIcon
function Mine:GetResourceProducedIcon(idx)
    local icon = GetResourceProducedIcon_Mine(self, idx)
    if self.exploitation_resource == "Radioactive" or self.exploitation_resource == "NuclearFuel" then
        icon = CurrentModPath .. icon
    end
    return icon
end

function OnMsg.ClassesPostprocess()
    local GetTemplateConsumptionDescription_res = GetTemplateConsumptionDescription
    function GetTemplateConsumptionDescription(template, base)
        base = GetTemplateConsumptionDescription_res(template, base)
        if template.template_class == "RadioactiveDisposal" then
            return base .. Untranslated(" ") .. FormatResource(empty_table, 1000, "Concrete")
        end
        return base
    end
end

function ToxicPool:CreateResourceRequests()
    self.clean_request = self:AddSupplyRequest("Radioactive", self.clean_amount_per_stage * self.stage, const.rfCanExecuteAlone, 10)
end

function OnMsg.ClassesPostprocess()
    Presets.POI.Default.CaptureMeteors.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
        "resource", "NuclearFuel",
        "amount", 60000
    })
    Presets.POI.Default.HeighSpeedComSatellite.rocket_required_resources[2] = PlaceObj("ResourceAmount", { --sic
        "resource", "NuclearFuel",
        "amount", 50000
    })
    Presets.POI.Default.SETISatellite.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
        "resource", "NuclearFuel",
        "amount", 50000
    })
    Presets.POI.Default.StoryBit_ContractExplorationAccess.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
        "resource", "NuclearFuel",
        "amount", 20000
    })
    if IsDlcAvailable("armstrong") then
        Presets.POI.Default.MeltThePolarCaps.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
            "resource", "Radioactive",
            "amount", 100000
        })
        Presets.POI.Default.CaptureIceAsteroids.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
            "resource", "NuclearFuel",
            "amount", 60000
        })
        Presets.POI.Default.LaunchSpaceSunshade.rocket_required_resources[2] = PlaceObj("ResourceAmount", {
            "resource", "NuclearFuel",
            "amount", 30000
        })
        Presets.POI.Default.LaunchSpaceMirror.rocket_required_resources[2] = PlaceObj("ResourceAmount", {
            "resource", "NuclearFuel",
            "amount", 30000
        })
        Presets.POI.Default.CloudSeeding.rocket_required_resources[1] = PlaceObj("ResourceAmount", {
            "resource", "Radioactive",
            "amount", 20000
        })
        for _, outcome in pairs(StoryBits.IceAsteroid or empty_table) do
            if IsKindOf(outcome, "StoryBitOutcome") then
                for _, effect in pairs(outcome.Effects or empty_table) do
                    if IsKindOf(effect, "CreatePlanetaryAnomaly") then
                        for _, resource in pairs(effect.required_resources or empty_table) do
                            if resource.resource == "Fuel" then
                                resource.resource = "NuclearFuel"
                            end
                        end
                    end
                end
            end
        end
        for _, outcome in pairs(StoryBits.BabyVolcano or empty_table) do
            if IsKindOf(outcome, "StoryBitOutcome") then
                for _, effect in pairs(outcome.Effects or empty_table) do
                    if IsKindOf(effect, "CreatePlanetaryAnomaly") then
                        for _, resource in pairs(effect.required_resources or empty_table) do
                            if resource.resource == "Fuel" then
                                resource.resource = "Radioactive"
                            end
                        end
                    end
                end
            end
        end
    end

    Presets.TechPreset.Physics.DeepMetalExtraction.display_name = T("Deep Mining")
    Presets.TechPreset.Physics.DeepMetalExtraction.description = T("Can mine deep deposits.\n\n<grey>Digging deep can be dangerous but it is necessary for sustained production.</grey>")

    local ExtractorAI = Presets.TechPreset.Breakthroughs.ExtractorAI
    ExtractorAI.description = T("<em>Extractors</em> can work without crews at <param1> performance.\n\n<grey>Extracting algorithms have been successfully implemented by our new Extractor AI, allowing for a certain degree of autonomy in mining operations.</grey>")
    table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 1, Label = "LH_RadioactiveExtractor", Prop = "automation",}))
    table.insert_unique(ExtractorAI, PlaceObj('Effect_ModifyLabel', {Amount = 50, Label = "LH_RadioactiveExtractor", Prop = "auto_performance",}))
end

function OnMsg.LHResStorageReady()
    function UniversalStorageDepot:DoesAcceptOtherResources()
        return ResourceStockpileBase.DoesAcceptOtherResources(self)
                or self:DoesAcceptResource("Radioactive") or self:DoesAcceptResource("NuclearFuel")
    end

    local idxFood, parent = lh.deep_search(XTemplates.sectionStorage, "Id", "Fuel")
    if not table.find(parent, "Id", "Radioactive") then
        local radioactive = PlaceObj('XTemplateTemplate', {
            'Id', "Radioactive",
            '__context', function (parent, context) return SubContext(context, {res = "Radioactive"}) end,
            '__template', "sectionStorageRow",
            'Title', T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
            'Icon', "UI/Icons/Sections/workshifts_active.tga",
            'TitleHAlign', "stretch",
        })
        table.insert(parent, idxFood + 1, radioactive)
    end
    if not table.find(parent, "Id", "NuclearFuel") then
        local nuclearfuel = PlaceObj('XTemplateTemplate', {
            'Id', "NuclearFuel",
            '__context', function (parent, context) return SubContext(context, {res = "NuclearFuel"}) end,
            '__template', "sectionStorageRow",
            'Title', T(615073837286, "<resource(res)><right><resource(GetStoredAmount(res),GetMaxStorage(res),res)>"),
            'Icon', "UI/Icons/Sections/workshifts_active.tga",
            'TitleHAlign', "stretch",
        })
        table.insert(parent, idxFood + 1, nuclearfuel)
    end
end
