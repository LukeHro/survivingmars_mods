DefineClass.RadioactiveExtractor = {
    __parents = {"MetalExtractorWorkplace"},
    subsurface_deposit_class = "SubsurfaceDepositRadioactive",
    exploitation_resource = "Radioactive",
}

RadioactiveExtractor.OnCalcProduction_Radioactive = BaseMetalsExtractor.OnCalcProduction_Metals
RadioactiveExtractor.OnProduce_Radioactive = BaseMetalsExtractor.OnProduce_Metals
RadioactiveExtractor.GetPredictedProduction_Radioactive = BaseMetalsExtractor.GetPredictedProduction_Metals
RadioactiveExtractor.GetPredictedDailyProduction_Radioactive = BaseMetalsExtractor.GetPredictedDailyProduction_Metals

DefineClass.NuclearGenerator = {
    __parents = {"StirlingGenerator"},
    accumulate_dust = true,
    accumulate_maintenance_points = true,
}

function NuclearGenerator:CanBeOpened()
    return false
end

function NuclearGenerator:OnChangeState()
    self:SetBase("electricity_production", self.working and self:GetClassValue("electricity_production") or 0)
end

function NuclearGenerator:BuildingUpdate(dt, ...)
    local wasWorking = self.working
    self:SetWorking(self.ui_working and self.consumption_stored_resources and self.consumption_stored_resources > 0)
    if wasWorking ~= self.working then
        self:OnChangeState()
    end
    self:ConsumeResourcePerDay(self, dt)
end

FusionReactor.OnChangeState = NuclearGenerator.OnChangeState
FusionReactor.BuildingUpdate = NuclearGenerator.BuildingUpdate

ArtificialSun.OnChangeState = NuclearGenerator.OnChangeState
function ArtificialSun:CanConsume()
    return self.work_state ~= "produce" or self.consumption_stored_resources and self.consumption_stored_resources > 0
end
function ArtificialSun:BuildingUpdate(dt, ...)
    if self.work_state == "produce" then
        NuclearGenerator.BuildingUpdate(self, dt, ...)
    end
end

local SetWorkState_ArtificialSun = ArtificialSun.SetWorkState
--"fix" demanding water again when stopped or running out of NuclearFuel
function ArtificialSun:SetWorkState(state, ...)
    local stored = self.stored_water
    SetWorkState_ArtificialSun(self, state, ...)
    self.stored_water = Min(stored, self.water_capacity)
    if state == "produce" and not (self.consumption_stored_resources and self.consumption_stored_resources > 0) then
        SetWorkState_ArtificialSun(self, "disabled", ...)
    end
end

if IsDlcAvailable("armstrong") then
    MagneticFieldGenerator.OnChangeState = NuclearGenerator.OnChangeState
    local BuildingUpdate_MagneticFieldGenerator = MagneticFieldGenerator.BuildingUpdate
    function MagneticFieldGenerator:BuildingUpdate(dt, ...)
        NuclearGenerator.BuildingUpdate(self, dt, ...)
        BuildingUpdate_MagneticFieldGenerator(self, dt, ...)
    end
end

DefineClass.NuclearLab = {
    __parents = {"Factory"},
    resource_produced1 = "NuclearFuel"
}
function NuclearLab.OnCalcProduction_NuclearFuel(producer, amount_to_produce)
    return MulDivRound(producer.parent.performance, amount_to_produce, 100)
end

DefineClass.RadioactiveDisposal = {
    __parents = {
        "OutsideBuildingWithShifts",
        "ElectricityConsumer",
        "DustGenerator",
    },
    force_extend_bb_during_placement_checks = 120 * guim,
    anim_obj = false,
    AltVisualClasses = {}
}
RadioactiveDisposal.CreateAnimObj = RegolithExtractor.CreateAnimObj
RadioactiveDisposal.DeleteAnimObj = RegolithExtractor.DeleteAnimObj --apparently unused

function RadioactiveDisposal:GameInit()
    self:CreateAnimObj()
    self:AddAdditionalConsumption("Concrete", self.consumption_amount, self.consumption_max_storage)
end

function RadioactiveDisposal:BuildingUpdate(dt, ...)
    self:SetWorking(self.ui_working and self:CanConsume())
    if IsValid(self.anim_obj) and self.anim_obj.command ~= self.working and "Work" or "Idle" then
        self.anim_obj:SetCommand(self.working and "Work" or "Idle")
    end
    if self:CanConsume() then
        local storedRadioactive = self:ConsumeResourcePerDay(self, dt)
        if storedRadioactive then
            self:ConsumeResourcePerDay(self.additionalConsumption.Concrete, dt)
            --beats the price for buying Radioactive but not if you count the concrete, power and transport fuel costs
            local foundPerUnit = MulDivRound(Presets.Cargo["Basic Resources"].Radioactive.price, 125, 100)
            local amount = MulDivRound(foundPerUnit, storedRadioactive, const.ResourceScale)
            UIColony.funds:ChangeFunding(amount, "Building")
        end
    end
end

function RadioactiveDisposal:CanConsume()
    local needConcrete = self.additionalConsumption.Concrete
    return self.consumption_stored_resources and self.consumption_stored_resources > 0 and needConcrete
            and needConcrete.consumption_stored_resources and needConcrete.consumption_stored_resources > 0
end

function RadioactiveDisposal:GetUIConsumptionTexts(short)
    local text = Building.GetUIConsumptionTexts(self, short)
    if self.additionalConsumption.Concrete then
        text.resource = text.resource .. (short and " " or "<newline><left>") .. self:GetAdditionalConsumptionText(short, self.additionalConsumption.Concrete)
    end
    return text
end