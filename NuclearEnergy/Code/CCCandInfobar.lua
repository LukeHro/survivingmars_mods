function OnMsg.LHResInfobarReady()
    local idOtherResources = lh.deep_find(XTemplates.Infobar, "Id", "idOtherResources")
    if idOtherResources then
        local _, fuel = lh.deep_search(idOtherResources, "Id", "idFuel")
        local idxFuel = lh.deep_search(idOtherResources, function(v) return v end, fuel)
        if not table.find(idOtherResources[2], 'Id', "radioactiveParent") then
            local radioactive = PlaceObj("XTemplateWindow", {
                "__class", "XButton",
                'Id', "radioactiveParent",
                "RolloverTemplate", "Rollover",
                "RolloverAnchor", "bottom",
                "RolloverText", T("<RadioactiveRollover>"),
                "RolloverTitle", T("Radioactive Materials"),
                "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
                "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
                "HAlign", "center",
                "BorderColor", RGBA(0, 0, 0, 0),
                "Background", RGBA(0, 0, 0, 0),
                "MouseCursor", "UI/Cursors/Rollover.tga",
                "RelativeFocusOrder", "next-in-line",
                "FXMouseIn", "UIButtonMouseIn",
                "FXPress", "BuildMenuButtonClick",
                "FocusedBorderColor", RGBA(0, 0, 0, 0),
                "FocusedBackground", RGBA(0, 0, 0, 0),
                "DisabledBorderColor", RGBA(0, 0, 0, 0),
                "OnPress", function(self, gamepad)
                    self.context:CycleResources("Radioactive")
                end,
                "RolloverBackground", RGBA(0, 0, 0, 0),
                "PressedBackground", RGBA(0, 0, 0, 0)
            }, {
                PlaceObj("XTemplateWindow", {
                    "__class", "XImage",
                    "Id", "idRollover",
                    "Dock", "box",
                    "Visible", false,
                    "Image", "UI/Common/row_shine_2.tga",
                    "ImageFit", "stretch"
                }),
                PlaceObj("XTemplateWindow", {
                    "__class", "XText",
                    "Id", "idRadioactive",
                    "Padding", box(3, 4, 3, 4),
                    "TextStyle", "Infobar",
                    "Translate", true,
                    "Text", T("<RadioactiveText>")
                })
            })
            table.insert(idOtherResources[2], idxFuel + 1, radioactive)
        end
        if not table.find(idOtherResources[2], 'Id', "nuclearParent") then
            local nuclearFuel = PlaceObj("XTemplateWindow", {
                "__class", "XButton",
                'Id', "nuclearParent",
                "RolloverTemplate", "Rollover",
                "RolloverAnchor", "bottom",
                "RolloverText", T("<NuclearFuelRollover>"),
                "RolloverTitle", T("Nuclear Fuel"),
                "RolloverHint", T(185759925809, "<left_click> Cycle stored resources"),
                "RolloverHintGamepad", T(868548677390, "<ButtonA> Cycle stored resources<newline><DPad> Navigate <DPadDown> Close"),
                "HAlign", "center",
                "BorderColor", RGBA(0, 0, 0, 0),
                "Background", RGBA(0, 0, 0, 0),
                "MouseCursor", "UI/Cursors/Rollover.tga",
                "RelativeFocusOrder", "next-in-line",
                "FXMouseIn", "UIButtonMouseIn",
                "FXPress", "BuildMenuButtonClick",
                "FocusedBorderColor", RGBA(0, 0, 0, 0),
                "FocusedBackground", RGBA(0, 0, 0, 0),
                "DisabledBorderColor", RGBA(0, 0, 0, 0),
                "OnPress", function(self, gamepad)
                    self.context:CycleResources("NuclearFuel")
                end,
                "RolloverBackground", RGBA(0, 0, 0, 0),
                "PressedBackground", RGBA(0, 0, 0, 0)
            }, {
                PlaceObj("XTemplateWindow", {
                    "__class", "XImage",
                    "Id", "idRollover",
                    "Dock", "box",
                    "Visible", false,
                    "Image", "UI/Common/row_shine_2.tga",
                    "ImageFit", "stretch"
                }),
                PlaceObj("XTemplateWindow", {
                    "__class", "XText",
                    "Id", "idNuclearFuel",
                    "Padding", box(3, 4, 3, 4),
                    "TextStyle", "Infobar",
                    "Translate", true,
                    "Text", T("<NuclearFuelText>")
                })
            })
            table.insert(idOtherResources[2], idxFuel + 1, nuclearFuel)
        end
    end
end

function OnMsg.LHResCCCReady()
    local idOtherResources = lh.deep_find(XTemplates.CommandCenterCategories, "Id", "idOtherResources")
    if idOtherResources then
        local idxFuel, parent = lh.deep_search(idOtherResources, "comment", "fuel")
        if not table.find(parent, "comment", "radioactive") then
            table.insert(parent, idxFuel + 1, PlaceObj('XTemplateTemplate', {
                'comment', "radioactive",
                '__template', "CommandCenterStatsRow",
                'Name', T("Radioactive Materials"),
                'Value', T("<radioactive(AvailableRadioactive)>"),
            }))
        end
        if not table.find(parent, "comment", "nuclearfuel") then
            table.insert(parent, idxFuel + 1, PlaceObj('XTemplateTemplate', {
                'comment', "nuclearfuel",
                '__template', "CommandCenterStatsRow",
                'Name', T("Nuclear Fuel"),
                'Value', T("<nuclearfuel(AvailableNuclearFuel)>"),
            }))
        end
    end
end

local GetColonyStatsButtons_City = City.GetColonyStatsButtons
function City:GetColonyStatsButtons()
    local buttons = GetColonyStatsButtons_City(self)
    local fuelId = table.ifind_if(buttons, function(v)
        return v.id == "Fuel"
    end)
    local radioactive = table.remove(buttons, table.ifind_if(buttons, function(v)
        return v.id == "Radioactive"
    end))
    radioactive.on_icon = CurrentModPath .. radioactive.on_icon
    radioactive.off_icon = CurrentModPath .. radioactive.off_icon
    table.insert(buttons, fuelId + 1, radioactive)
    local nuclearfuel = table.remove(buttons, table.ifind_if(buttons, function(v)
        return v.id == "NuclearFuel"
    end))
    nuclearfuel.on_icon = CurrentModPath .. nuclearfuel.on_icon
    nuclearfuel.off_icon = CurrentModPath .. nuclearfuel.off_icon
    table.insert(buttons, fuelId + 1, nuclearfuel)
    return buttons
end