function OnMsg.ClassesPostprocess()
    PlaceObj('BuildingTemplate', {
        'Group', "Depots",
        'Id', "LH_StorageNuclearFuel",
        'template_class', "UniversalStorageDepot",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'pin_summary1', T("<nuclearfuel(Stored_NuclearFuel)>"),
        'build_points', 0,
        'instant_build', true,
        'dome_forbidden', true,
        'display_name', T("Nuclear Fuel Depot"),
        'display_name_pl', T("Nuclear Fuel Depots"),
        'description', T("Stores <nuclearfuel(max_storage_per_resource)>. Some resources will be transported to other depots within Drone range."),
        'build_category', "Depots",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_nuclearfuel_storage.tga",
        'build_pos', 5,
        'entity', "LH_NuclearFuelDepo",
        'on_off_button', false,
        'prio_button', false,
        'count_as_building', false,
        'disabled_in_environment1', "",
        'disabled_in_environment2', "",
        'max_storage_per_resource', 180000,
        'max_x', 12,
        'storable_resources', {"NuclearFuel"}
    })
    PlaceObj('BuildingTemplate', {
        'Group', "MechanizedDepots",
        'Id', "LH_MechanizedDepotNuclearFuel",
        'template_class', "MechanizedDepot",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'pin_summary1', T("<nuclearfuel(Stored_NuclearFuel)>"),
        'construction_cost_Metals', 12000,
        'construction_cost_MachineParts', 3000,
        'build_points', 500,
        'is_tall', true,
        'dome_forbidden', true,
        'use_demolished_state', false,
        'maintenance_resource_type', "MachineParts",
        'display_name', T("Nuclear Fuel Storage"),
        'display_name_pl', T("Nuclear Fuel Storages"),
        'description', T("Stores <nuclearfuel(UIMaxStorageAmount)>. Some resources will be transported to other depots within Drone range."),
        'build_category', "MechanizedDepots",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_nuclearfuel_mechanized_depot.tga",
        'build_pos', 5,
        'entity', "LH_NuclearFuelDepo_A",
        'label1', "OutsideBuildings",
        'demolish_sinking', range(0, 0),
        'disabled_in_environment1', "",
        'disabled_in_environment2', "",
        'storable_resources', {"NuclearFuel"},
        'max_storage_per_resource', 3950000
    })
    PlaceObj('BuildingTemplate', {
        'Group', "Depots",
        'Id', "LH_StorageRadioactive",
        'template_class', "UniversalStorageDepot",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'pin_summary1', T("<radioactive(Stored_Radioactive)>"),
        'build_points', 0,
        'instant_build', true,
        'dome_forbidden', true,
        'display_name', T("Radioactive Materials Depot"),
        'display_name_pl', T("Radioactive Materials Depots"),
        'description', T("Stores <radioactive(max_storage_per_resource)>. Some resources will be transported to other depots within Drone range."),
        'build_category', "Depots",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_radioactive_storage.tga",
        'build_pos', 5,
        'entity', "LH_RadioactiveDepo",
        'on_off_button', false,
        'prio_button', false,
        'count_as_building', false,
        'disabled_in_environment1', "",
        'disabled_in_environment2', "",
        'max_storage_per_resource', 180000,
        'max_x', 12,
        'storable_resources', {"Radioactive"}
    })
    PlaceObj('BuildingTemplate', {
        'Group', "MechanizedDepots",
        'Id', "LH_MechanizedDepotRadioactive",
        'template_class', "MechanizedDepot",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'pin_summary1', T("<radioactive(Stored_Radioactive)>"),
        'construction_cost_Metals', 12000,
        'construction_cost_MachineParts', 3000,
        'build_points', 500,
        'is_tall', true,
        'dome_forbidden', true,
        'use_demolished_state', false,
        'maintenance_resource_type', "MachineParts",
        'display_name', T("Radioactive Materials Storage"),
        'display_name_pl', T("Radioactive Materials Storages"),
        'description', T("Stores <radioactive(UIMaxStorageAmount)>. Some resources will be transported to other depots within Drone range."),
        'build_category', "MechanizedDepots",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_radioactive_mechanized_depot.tga",
        'build_pos', 5,
        'entity', "LH_RadioactiveDepo_A",
        'label1', "OutsideBuildings",
        'demolish_sinking', range(0, 0),
        'disabled_in_environment1', "",
        'disabled_in_environment2', "",
        'storable_resources', {"Radioactive"},
        'max_storage_per_resource', 3950000
    })
    PlaceObj('BuildingTemplate', {
        'Group', "Production",
        'Id', "LH_RadioactiveExtractor",
        'template_class', "RadioactiveExtractor",
        'resource_produced1', "Radioactive",
        'production_per_day1', 5000,
        'resource_produced2', "WasteRock",
        'stockpile_class2', "WasteRockStockpile",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'construction_cost_Metals', 20000,
        'construction_cost_MachineParts', 5000,
        'build_points', 5000,
        'is_tall', true,
        'dome_forbidden', true,
        'upgrade1_id', "PreciousMetalsExtractor_FueledExtractor",
        'upgrade1_display_name', T(5026, "Fueled Extractor"),
        'upgrade1_description', T(5027, "+<upgrade1_mul_value_1>% Production as long as the building is supplied with Fuel. If not supplied with Fuel, the building will continue to operate but will lose the bonus."),
        'upgrade1_icon', "UI/Icons/Upgrades/fueled_extractor_01.tga",
        'upgrade1_upgrade_cost_MachineParts', 5000,
        'upgrade1_mod_label_1', "PreciousMetalsExtractor",
        'upgrade1_mod_prop_id_1', "production_per_day1",
        'upgrade1_mul_value_1', 30,
        'upgrade1_consumption_resource_type', "Fuel",
        'upgrade1_consumption_amount', 800,
        'upgrade1_consumption_resource_stockpile_spot_name', "",
        'upgrade2_id', "PreciousMetalsExtractor_Amplify",
        'upgrade2_display_name', T(5028, "Amplify"),
        'upgrade2_description', T(5234, "+<upgrade2_mul_value_2>% Production; +<power(upgrade2_add_value_1)> Consumption."),
        'upgrade2_icon', "UI/Icons/Upgrades/amplify_01.tga",
        'upgrade2_mod_label_1', "PreciousMetalsExtractor",
        'upgrade2_mod_prop_id_1', "electricity_consumption",
        'upgrade2_add_value_1', 10000,
        'upgrade2_mod_label_2', "PreciousMetalsExtractor",
        'upgrade2_mod_prop_id_2', "production_per_day1",
        'upgrade2_mul_value_2', 25,
        'upgrade2_upgrade_cost_Polymers', 2000,
        'upgrade3_id', "PreciousMetalsExtractor_MagneticExtraction",
        'upgrade3_display_name', T(5030, "Magnetic Extraction"),
        'upgrade3_description', T(5031, "+<upgrade3_mul_value_1>% Production."),
        'upgrade3_icon', "UI/Icons/Upgrades/magnetic_extraction_01.tga",
        'upgrade3_upgrade_cost_Electronics', 5000,
        'upgrade3_upgrade_cost_MachineParts', 5000,
        'upgrade3_mod_label_1', "PreciousMetalsExtractor",
        'upgrade3_mod_prop_id_1', "production_per_day1",
        'upgrade3_mul_value_1', 50,
        'maintenance_resource_type', "MachineParts",
        'maintenance_resource_amount', 2000,
        'display_name', T("Radioactive Materials Extractor"),
        'display_name_pl', T("Radioactive Materials Extractors"),
        'description', T("Extracts Radioactive Materials from underground deposits.  All extractors raise dust resulting in more frequent maintenance for buildings in the grey area."),
        'build_category', "Production",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_radioactive_extractor.tga",
        'build_pos', 6,
        'entity', "RareMetalExtractorCP3",
        'label1', "OutsideBuildings",
        'label2', "OutsideBuildingsTargets",
        'color_modifier', RGBA(169, 97, 97, 255),
        'palette_color1', "mining_accent_1",
        'palette_color2', "mining_base",
        'palette_color3', "outside_dark",
        'demolish_sinking', range(5, 10),
        'demolish_debris', 68,
        'palette2_color1', "mining_accent_1",
        'palette2_color2', "outside_base",
        'palette2_color3', "mining_base",
        'palette2_color4', "outside_dark",
        'exploitation_resource', "Radioactive",
        'electricity_consumption', 5000,
        'max_workers', 4,
        'specialist', "geologist",
    })
    PlaceObj('BuildingTemplate', {
        'Group', "Power",
        'Id', "LH_NuclearGenerator",
        'template_class', "NuclearGenerator",
        'pin_rollover_context', "electricity",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'construction_cost_Polymers', 5000,
        'construction_cost_Electronics', 5000,
        'build_points', 1000,
        'require_prefab', true,
        'dome_forbidden', true,
        'maintenance_resource_type', "Electronics",
        'display_name', T("Nuclear Generator"),
        'display_name_pl', T("Nuclear Generators"),
        'description', T("Generates Power using Nuclear Fuel."),
        'build_category', "Power",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_nuclear_generator.tga",
        'build_pos', 5,
        'entity', "StirlingGeneratorCP3",
        'label1', "OutsideBuildings",
        'label2', "OutsideBuildingsTargets",
        'palette_color1', "electro_accent_1",
        'palette_color2', "electro_base",
        'palette_color3', "electro_accent_2",
        'demolish_sinking', range(5, 10),
        'demolish_debris', 80,
        'disabled_in_environment1', "",
        'disabled_in_environment2', "",
        'palette2_color1', "electro_base",
        'palette2_color2', "electro_accent_1",
        'palette2_color3', "electro_accent_2",
        'electricity_production', 20000,
        'consumption_resource_type', "NuclearFuel",
        'consumption_amount', 1000,
    })
    PlaceObj("BuildingTemplate", {
        'Group', "Production",
        'Id', "LH_NuclearEnrichmentPlant",
        'template_class', "NuclearLab",
        'resource_produced1', "NuclearFuel",
        'production_per_day1', 2000,
        'construction_cost_Concrete', 10000,
        'construction_cost_Polymers', 5000,
        'construction_cost_Electronics', 15000,
        'build_points', 7000,
        'require_prefab', true,
        'is_tall', true,
        'dome_forbidden', true,
        'upgrade1_id', "ElectronicsFactory_FactoryAI",
        'upgrade1_display_name', T(5054, "Factory AI"),
        'upgrade1_description', T(7667, "+<upgrade1_mul_value_1>% Production."),
        'upgrade1_icon', "UI/Icons/Upgrades/factory_ai_01.tga",
        'upgrade1_mod_label_1', "ElectronicsFactory",
        'upgrade1_mod_prop_id_1', "production_per_day1",
        'upgrade1_mul_value_1', 20,
        'upgrade1_upgrade_cost_Electronics', 5000,
        'upgrade1_upgrade_cost_MachineParts', 2000,
        'upgrade2_id', "ElectronicsFactory_Automation",
        'upgrade2_display_name', T(5056, "Automation"),
        'upgrade2_description', T(5057, "Decreases Workplaces by <abs(upgrade2_add_value_1)>."),
        'upgrade2_icon', "UI/Icons/Upgrades/automation_01.tga",
        'upgrade2_mod_label_1', "ElectronicsFactory",
        'upgrade2_mod_prop_id_1', "max_workers",
        'upgrade2_add_value_1', -2,
        'upgrade2_upgrade_cost_Polymers', 5000,
        'upgrade2_upgrade_cost_Electronics', 5000,
        'upgrade3_id', "ElectronicsFactory_Amplify",
        'upgrade3_display_name', T(5028, "Amplify"),
        'upgrade3_description', T(5058, "+<upgrade3_mul_value_1>% Production; +<power(upgrade3_add_value_2)> Consumption."),
        'upgrade3_icon', "UI/Icons/Upgrades/amplify_01.tga",
        'upgrade3_mod_label_1', "ElectronicsFactory",
        'upgrade3_mod_prop_id_1', "production_per_day1",
        'upgrade3_mul_value_1', 25,
        'upgrade3_mod_label_2', "ElectronicsFactory",
        'upgrade3_mod_prop_id_2', "electricity_consumption",
        'upgrade3_add_value_2', 20000,
        'upgrade3_upgrade_cost_Polymers', 5000,
        'maintenance_resource_type', "Electronics",
        'maintenance_resource_amount', 2000,
        'consumption_resource_type', "Radioactive",
        'consumption_amount', 1000,
        'display_name', T("Nuclear Lab"),
        'display_name_pl', T("Nuclear Labs"),
        'description', T("Creates Nuclear Fuel from Radioactive Materials."),
        'build_category', "Production",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_nuclear_lab.tga",
        'build_pos', 11,
        'entity', "LH_NuclearLab",
        'label1', "OutsideBuildings",
        'label2', "OutsideBuildingsTargets",
        'label3', "FactoryBuildings",
        'palette_color1', "life_accent_1",
        'palette_color2', "outside_base",
        'palette_color3', "outside_accent_factory",
        'demolish_sinking', range(5, 10),
        'demolish_tilt_angle', range(720, 1020),
        'demolish_debris', 80,
        'disabled_in_environment1', "",
        'electricity_consumption', 20000,
        'enabled_shift_2', false,
        'enabled_shift_3', false,
        'max_workers', 3,
        'specialist', "scientist"
    })
    PlaceObj('BuildingTemplate', {
        'Group', "Storages",
        'Id', "LH_RadioactiveDisposal",
        'template_class', "RadioactiveDisposal",
        'resource_produced1', "WasteRock",
        'production_per_day1', 5000,
        'stockpile_class1', "WasteRockStockpile",
        'pin_rollover_hint', T("<image UI/Infopanel/left_click.tga 1400> Select"),
        'pin_rollover_hint_xbox', T("<image UI/PS4/Cross.tga> View"),
        'construction_cost_Metals', 20000,
        'construction_cost_MachineParts', 5000,
        'build_points', 5000,
        'is_tall', true,
        'dome_forbidden', true,
        'maintenance_resource_type', "MachineParts",
        'maintenance_resource_amount', 2000,
        'display_name', T("Radioactive Disposal"),
        'display_name_pl', T("Radioactive Disposals"),
        'description', T("Permanently stores Radioactive Materials underground using Concrete eliminating the radioactivity hazard generated from those. It generates Funding for doing it."),
        'build_category', "Storages",
        'display_icon', CurrentModPath.."UI/Icons/Buildings/lh_radioactive_disposal.tga",
        'build_pos', 10,
        'entity', "ConcreteExtractorCP3Main",
        'label1', "OutsideBuildings",
        'label2', "OutsideBuildingsTargets",
        'color_modifier', RGBA(169, 97, 97, 255),
        'palette_color1', "mining_accent_1",
        'palette_color2', "mining_base",
        'palette_color3', "outside_dark",
        'demolish_sinking', range(5, 10),
        'demolish_debris', 68,
        'palette2_color1', "mining_accent_1",
        'palette2_color2', "outside_base",
        'palette2_color3', "mining_base",
        'palette2_color4', "outside_dark",
        'electricity_consumption', 5000,
        'exploitation_resource', false,
        'consumption_resource_type', "Radioactive",
        'consumption_max_storage', 10000,
        'consumption_amount', 2000,
    })
end

local function UpdateUpgrades(templates)
    templates = templates or BuildingTemplates
    templates.FusionReactor.upgrade2_upgrade_cost_NuclearFuel = 20000 --Eternal Fusion
    templates.FusionReactor.upgrade2_upgrade_cost_Polymers = nil

    templates.StirlingGenerator.upgrade1_upgrade_cost_NuclearFuel = 2000 --Plutonium Core
    templates.StirlingGenerator.upgrade1_upgrade_cost_Polymers = nil
    --todo power producers don't have upgrade consumption implemented :(
    --templates.StirlingGenerator.upgrade1_consumption_resource_type="NuclearFuel"
    --templates.StirlingGenerator.upgrade1_consumption_amount=100
    --templates.StirlingGenerator.upgrade1_consumption_resource_stockpile_spot_name=""

    if templates.AdvancedStirlingGenerator then
        templates.AdvancedStirlingGenerator.upgrade1_upgrade_cost_NuclearFuel = 2000 --Plutonium Core
        templates.AdvancedStirlingGenerator.upgrade1_upgrade_cost_Polymers = nil
        --templates.AdvancedStirlingGenerator.upgrade1_consumption_resource_type="NuclearFuel"
        --templates.AdvancedStirlingGenerator.upgrade1_consumption_amount=100
        --templates.AdvancedStirlingGenerator.upgrade1_consumption_resource_stockpile_spot_name=""
    end

    if IsDlcAvailable("picard") then
        templates.LanderRocket.upgrade1_description = T("+<upgrade1_add_value_1>kg Cargo Capacity.")
        templates.LanderRocket.upgrade1_mod_label_2 = nil
        templates.LanderRocket.upgrade1_mod_prop_id_2 = nil
        templates.LanderRocket.upgrade1_add_value_2 = nil
    end
end

function OnMsg.ClassesPostprocess()
    BuildingTemplates.AtomicBattery.construction_cost_NuclearFuel = 10000
    BuildingTemplates.AtomicBattery.maintenance_resource_type = "NuclearFuel"
    BuildingTemplates.AtomicBattery.maintenance_resource_amount = 2000

    BuildingTemplates.FusionReactor.consumption_resource_type = "NuclearFuel"
    BuildingTemplates.FusionReactor.consumption_max_storage = 20000
    BuildingTemplates.FusionReactor.consumption_amount = 8000

    BuildingTemplates.ArtificialSun.consumption_resource_type = "NuclearFuel"
    BuildingTemplates.ArtificialSun.consumption_max_storage = 20000
    BuildingTemplates.ArtificialSun.consumption_amount = 10000

    if BuildingTemplates.MagneticFieldGenerator then
        BuildingTemplates.MagneticFieldGenerator.consumption_resource_type = "Radioactive"
        BuildingTemplates.MagneticFieldGenerator.consumption_max_storage = 20000
        BuildingTemplates.MagneticFieldGenerator.consumption_amount = 4000
    end

    UpdateUpgrades()
end

function OnMsg.CityStart(...)
    UpdateUpgrades(ClassTemplates.Building)
end

function OnMsg.LoadGame(...)
    UpdateUpgrades(ClassTemplates.Building)
end

function OnMsg.ModsReloaded()
    BuildingTemplates.PreciousMetalsExtractor.entity2 = nil
    BuildingTemplates.StirlingGenerator.entity2 = nil
    BuildingTemplates.RegolithExtractor.entity2 = nil
    BuildingTemplates.ResearchLab.entity2 = nil
end