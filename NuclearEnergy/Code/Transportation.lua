function OnMsg.ClassesBuilt()
    lh.insert_after(SpaceElevator.storable_resources, "Radioactive", "Fuel")
    lh.insert_after(SpaceElevator.storable_resources, "NuclearFuel", "Fuel")
    lh.insert_after(RocketExpedition.storable_resources, "Radioactive", "Fuel")
    lh.insert_after(RocketExpedition.storable_resources, "NuclearFuel", "Fuel")
    lh.insert_after(SupplyRocket.storable_resources, "Radioactive", "Fuel")
    lh.insert_after(SupplyRocket.storable_resources, "NuclearFuel", "Fuel")
    lh.insert_after(BuildingTemplates.SpaceElevator.storable_resources, "Radioactive", "Fuel")
    lh.insert_after(BuildingTemplates.SpaceElevator.storable_resources, "NuclearFuel", "Fuel")
    lh.insert_after(BuildingTemplates.RocketExpedition.storable_resources, "Radioactive", "Fuel")
    lh.insert_after(BuildingTemplates.RocketExpedition.storable_resources, "NuclearFuel", "Fuel")
    lh.insert_after(BuildingTemplates.SupplyRocket.storable_resources, "Radioactive", "Fuel")
    lh.insert_after(BuildingTemplates.SupplyRocket.storable_resources, "NuclearFuel", "Fuel")
    if IsDlcAvailable("gagarin") then
        lh.insert_after(BuildingTemplates.ZeusRocket.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.ZeusRocket.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.DragonRocket.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.DragonRocket.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.ForeignAidRocket.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.ForeignAidRocket.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.ForeignTradeRocket.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.ForeignTradeRocket.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.TradeRocket.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.TradeRocket.storable_resources, "NuclearFuel", "Fuel")
    end
    if IsDlcAvailable("picard") then
        lh.insert_after(BuildingTemplates.LanderRocket.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.LanderRocket.storable_resources, "NuclearFuel", "Fuel")
        BuildingTemplates.LanderRocket.launch_fuel = 20000
        ClassTemplates.Building.LanderRocket.launch_fuel = 20000
        Presets.TechPreset.ReconAndExpansion.ExtendedCargoModules.description =
        T("Asteroid Lander Upgrade (<em>Extended Cargo Module</em>) - Cargo capacity increases by <param1> kg.\n\n<grey>\226\128\156Storage experts are hoarders.\"   \n<right>Marie Kondo</grey><left>")
    end
    if IsDlcAccessible("prunariu") then
        lh.insert_after(Train.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(Train.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.StationSmall.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.StationSmall.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.StationBig.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.StationBig.storable_resources, "NuclearFuel", "Fuel")
        lh.insert_after(BuildingTemplates.DynamicStorageDepot.storable_resources, "Radioactive", "Fuel")
        lh.insert_after(BuildingTemplates.DynamicStorageDepot.storable_resources, "NuclearFuel", "Fuel")
    end
    local idxFuel, parent = lh.deep_search(XTemplates.customRocketExpedition,"comment", "fuel")
    if not table.find(parent,"comment", "radioactive") then
        table.insert(parent, idxFuel + 1, PlaceObj("XTemplateTemplate", {
            "comment", "radioactive",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local export = context.expedition and context.expedition.export
                local amount = export and export.Radioactive or 0
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('Radioactive' )><right><radioactive(ExportAmount_Radioactive, ExportTarget_Radioactive)>")
        }))
    end
    if not table.find(parent,"comment", "nuclearfuel") then
        table.insert(parent, idxFuel + 1, PlaceObj("XTemplateTemplate", {
            "comment", "nuclearfuel",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local export = context.expedition and context.expedition.export
                local amount = export and export.NuclearFuel or 0
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('NuclearFuel' )><right><radioactive(ExportAmount_NuclearFuel, ExportTarget_NuclearFuel)>")
        }))
    end
    local idxPolymers, parent = lh.deep_search(XTemplates.customForeignAidRocket, "comment", "polymers")
    if not table.find(parent,"comment", "radioactive") then
        table.insert(parent, idxPolymers + 1, PlaceObj("XTemplateTemplate", {
            "comment", "radioactive",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local amount = context:GetStoredAmount("Radioactive")
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('Radioactive' )><right><radioactive(Stored_Radioactive)>")
        }))
    end
    if not table.find(parent,"comment", "nuclearfuel") then
        table.insert(parent, idxPolymers + 1, PlaceObj("XTemplateTemplate", {
            "comment", "nuclearfuel",
            "__template", "InfopanelText",
            "FoldWhenHidden", true,
            "ContextUpdateOnOpen", true,
            "OnContextUpdate", function(self, context)
                local amount = context:GetStoredAmount("NuclearFuel")
                self:SetVisible(amount > 0)
                self:SetText(self.Text)
            end,
            "Text", T("<resource('NuclearFuel' )><right><nuclearfuel(Stored_NuclearFuel)>")
        }))
    end
end

function OnMsg.ClassesPostprocess()
    PlaceObj("Cargo", {
        SortKey = 4003600,
        description = T("Radioactive Materials that can be used to produce Nuclear Fuel."),
        group = "Basic Resources",
        id = "Radioactive",
        kg = 400,
        name = T("Radioactive Materials"),
        price = 8000000
    })
    PlaceObj("Cargo", {
        SortKey = 4003700,
        hidden = true,
        description = T("Nuclear Fuel that can be used to produce energy."),
        group = "Other Resources",
        id = "NuclearFuel",
        kg = 400,
        name = T("Nuclear Fuel"),
        price = 12000000
    })
end

function OnMsg.ClassesBuilt()
    local matchPrecious = function(o) return string.match(TDevModeGetEnglishText(o.Text), "<fuel") end
    local idxPrecious, parent = lh.deep_search(XTemplates.ipRover, matchPrecious)
    if not table.find(parent, "Id", "idRadioactive") then
        table.insert(parent, idxPrecious + 1,
                PlaceObj('XTemplateTemplate', {
                    '__condition', function(parent, context)
                        return context:HasMember("GetStored_Radioactive")
                    end,
                    'Id', "idRadioactive",
                    '__template', "InfopanelText",
                    'Text', T("Radioactive Materials<right><radioactive(Stored_Radioactive)>"),
                })
        )
    end
    if not table.find(parent, "Id", "idNuclearFuel") then
        table.insert(parent, idxPrecious + 1,
                PlaceObj('XTemplateTemplate', {
                    '__condition', function(parent, context)
                        return context:HasMember("GetStored_Booze")
                    end,
                    'Id', "idNuclearFuel",
                    '__template', "InfopanelText",
                    'Text', T("Nuclear Fuel<right><nuclearfuel(Stored_NuclearFuel)>"),
                })
        )
    end
end