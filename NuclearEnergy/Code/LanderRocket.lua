if LanderRocketCargoPreset then
    table.insert_unique(LanderRocketCargoPreset, {class = "NuclearFuel", amount = 20})
    table.remove_entry(LanderRocketCargoPreset, "class", "Fuel")
end

if IsDlcAvailable("picard") then
    function LanderRocketBase:GetLaunchFuel()
        return 0
    end

    function LanderRocketBase:GetNuclearFuel()
        return self.launch_fuel
    end

    function LanderRocketBase:BuildingUpdate(...)
        --remove auto refuel for IMM sponsor
    end

    function LanderRocketBase:UpdateRefuelRequests(...)
        --let's not complicate with changing fuel needed by techs/upgrade
        --TODO change upgrade/tech description
        self.launch_fuel = self.base_launch_fuel
    end

    function LanderRocketBase:OnDemolish()
        if self.refuel_request then
            local amount = self:GetNuclearFuel() - self.refuel_request:GetActualAmount()
            if 0 < amount then
                PlaceResourceStockpile_Delayed(self:GetVisualPos(), self:GetMapID(), "NuclearFuel", amount, self:GetAngle(), true)
            end
            self.refuel_request:ResetAmount(0)
        end
        RocketBase.OnDemolish(self)
    end

    local CreateResourceRequests_LanderRocketBase = LanderRocketBase.CreateResourceRequests
    function LanderRocketBase:CreateResourceRequests()
        CreateResourceRequests_LanderRocketBase(self)
        self:UpdateRefuelRequests()
        --const.rfRestrictorRocket whatever it means seems to work for "Fuel" only
        self.refuel_request = self:AddDemandRequest("NuclearFuel", self:GetNuclearFuel())
    end

    function LanderRocketBase:ResetFuelDemandRequests()
        RocketBase.ResetFuelDemandRequests(self)
        self.refuel_request:ResetAmount(self:GetNuclearFuel())
    end

    function LanderRocketBase:GetRefuelProgress()
        local extra = self.unload_fuel_request and self.unload_fuel_request:GetActualAmount() or 0
        return self:GetNuclearFuel() - self.refuel_request:GetActualAmount() + extra
    end

    GetUIRocketStatus_LanderRocketBase = LanderRocketBase.GetUIRocketStatus
    function LanderRocketBase:GetUIRocketStatus()
        local status = GetUIRocketStatus_LanderRocketBase(self)
        if self:GetNuclearFuel() ~= 0 then
            local refuel = T({
                "Refueling<right><current>/<nuclearfuel(launch_fuel)>",
                current = self:GetRefuelProgress() / const.ResourceScale
            })
            return status .. "<newline><left>" .. refuel
        end
        return status
    end

    function LanderRocketBase:CheckCargoFuel()
        local refuel_amount = self.refuel_request:GetActualAmount()
        if 0 < refuel_amount and self.stockpiled_amount.NuclearFuel and 0 < self.stockpiled_amount.NuclearFuel then
            local cargo_amount = self.stockpiled_amount.NuclearFuel
            local amount = Min(cargo_amount, refuel_amount)
            self.refuel_request:SetAmount(refuel_amount - amount)
            self.supply.NuclearFuel:SetAmount(cargo_amount - amount)
            self.stockpiled_amount.NuclearFuel = cargo_amount - amount
            self.cargo.NuclearFuel.amount = (cargo_amount - amount) / const.ResourceScale
            if self:HasEnoughFuelToLaunch() then
                Msg("RocketRefueled", self)
            end
        end
    end
end