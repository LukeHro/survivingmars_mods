local EnhanceOthersRow_LandingSiteObject = LandingSiteObject.EnhanceOthersRow
function LandingSiteObject:EnhanceOthersRow(row, rival)
    if rival then
        table.insert(row, 1, T({"<nuclearfuel(nuclearfuel)>", nuclearfuel = Max(rival.resources.nuclearfuel * const.ResourceScale, 0)}))
        table.insert(row, 2, T({"<radioactive(radioactive)>", radioactive = Max(rival.resources.radioactive * const.ResourceScale, 0)}))
    else
        local resourcesOverview = GetCityResourceOverview(UICity)
        table.insert(row, 1, T({"<nuclearfuel(nuclearfuel)>", nuclearfuel = resourcesOverview:GetAvailableNuclearFuel()}))
        table.insert(row, 2, T({"<radioactive(radioactive)>", radioactive = resourcesOverview:GetAvailableRadioactive()}))
    end
    EnhanceOthersRow_LandingSiteObject(self, row, rival)
end

function OnMsg.ClassesPostprocess()
    if not IsDlcAccessible("gagarin") then
        return
    end
    table.insert(HelpOfferOtherRes,1,{ ai_res = "radioactive", game_res = "Radioactive", min = 500, help = 50000 })
    table.insert(DistressCallOtherRes,1,{message = T("We need Radioactive materials"), ai_res = "radioactive", game_res = "Radioactive", amount = 50})
end

OnMsg.ModsReloaded = function(...)
    if (XTemplates.customTradePad) then
        table.insert_unique(TradePad.trade_resources, "Radioactive")
        local exports = lh.deep_find(XTemplates.customTradePad, "comment", "export/offer")
        local export_f = exports.OnContextUpdate
        exports.OnContextUpdate = function(self, context)
            local resource = ResolveValue(context, "export_resource")
            if resource == "Radioactive" then
                self:SetIcon(CurrentModPath .. "UI/Icons/Sections/" .. resource .. "_6.tga")
                self:SetTitle(T({ 11503, "Offering: <resource(res)>", res = resource }))
            else
                export_f(self, context)
            end
            self:SetVisible(context:IsAvailable())
        end
        local imports = lh.deep_find(XTemplates.customTradePad, "comment", "import/ask")
        local import_f = imports.OnContextUpdate
        imports.OnContextUpdate = function(self, context)
            local resource = ResolveValue(context, "import_resource")
            if resource == "Radioactive" then
                self:SetIcon(CurrentModPath .. "UI/Icons/Sections/" .. resource .. "_5.tga")
                self:SetTitle(T({ 11505, "Requesting: <resource(res)>", res = resource }))
            else
                import_f(self, context)
            end
            self:SetVisible(context:IsAvailable())
        end
    end
end

local SpawnRivalAI_vanilla = SpawnRivalAI
function SpawnRivalAI(preset, ...)
    local obj = SpawnRivalAI_vanilla(preset,...)
    print("Adding Radioactive resource to", preset.id)
    UpdateRivalAI(obj, preset.id, Presets.DumbAIDef.Modded.lh_radioactive)
    return obj
end

OnMsg.LoadGame = function()
    for k,v in pairs(RivalAIs) do
        UpdateRivalAI(v, k, Presets.DumbAIDef.Modded.lh_radioactive)
    end
end

function OnMsg.ClassesPostprocess()
    if not IsDlcAvailable("gagarin") then
        return
    end
    PlaceObj("DumbAIDef", {
        biases = {},
        group = "Modded",
        id = "lh_radioactive",
        initial_resources = {
            PlaceObj("AIResourceAmount", { "resource", "radioactive", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "radioactive_production", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "nuclearfuel", "amount", 0 }),
            PlaceObj("AIResourceAmount", { "resource", "nuclearfuel_production", "amount", 0 }),
        },
        production_rules = {
            PlaceObj("AIProductionRule", {
                "rule_id", "Resource Production and Consumption (radioactive)",
                "Run", function(self, resources, ai_player)
                    local res_references = {
                        { "radioactive", 10000, "Radioactive", 5 },
                        { "nuclearfuel", 10000, "NuclearFuel", 10 }
                    }
                    local rival_stock, player_stock, change, diff
                    for _, res in pairs(res_references) do
                        local r1 = res[1]
                        rival_stock = Max(1000, resources[r1] * const.ResourceScale)
                        player_stock = Max(1000, GetCityResourceOverview(MainCity):GetAvailable(res[3])) + 1000
                        change = resources[res[1] .. "_production"]
                        diff = rival_stock - player_stock
                        diff = diff / player_stock
                        diff = diff / 100
                        if rival_stock > res[2] and diff > 20 then
                            change = change * (100 - Min(95, diff)) / 100
                        elseif diff < -20 then
                            change = change + change * -diff / 100
                        end
                        if rival_stock > res[2] and 0 < res[4] then
                            change = change - resources.buildings * res[4] / const.ResourceScale
                        end
                        resources[r1] = resources[r1] + change
                    end
                end
            }),
            PlaceObj("AIProductionRule", {
                "rule_id",
                "dispose of Radioactive",
                "Run",
                function(self, resources, ai_player)
                    if resources.radioactive > 50 and resources.concrete > 100 then
                        resources.radioactive = resources.radioactive - 2
                        resources.concrete = resources.concrete - 2
                        local foundPerUnit = MulDivRound(Presets.Cargo["Basic Resources"].Radioactive.price, 125, 100)
                        resources.funding = resources.funding + MulDivRound(foundPerUnit, 2, 1000000)
                    end
                end
            }),
            PlaceObj("AIProductionRule", {
                "rule_id",
                "increase nuclear Power",
                "Run",
                function(self, resources, ai_player)
                    if resources.radioactive > 50 and resources.power < 50 then
                        resources.radioactive = resources.radioactive - 2
                        resources.power = resources.power + 10
                    end
                end
            }),
        },
        PlaceObj("AIAction", {
            "id", "increase Radioactive production",
            "log_entry", T("Constructed Radioactive Extractor"),
            "base_eval", 75000000,
            "delay", 360000,
            "tags", {"mining"},
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "metals", "amount", 20 }),
                PlaceObj("AIResourceAmount", { "resource", "machineparts", "amount", 5 }),
                PlaceObj("AIResourceAmount", { "resource", "power", "amount", 5 }),
                PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 12 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "deposits_underground", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "radioactive_production", "amount", 2 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
            },
            "IsAllowed", function(self, ai_player)
                return true
            end,
            "Eval", function(self, ai_player)
                if ai_player.resources.radioactive < 1 then
                    return 5000 * const.Scale.mil
                end
                if ai_player.resources.radioactive < 10 then
                    return 150 * const.Scale.mil
                elseif ai_player.resources.radioactive > 250 then
                    return 10 * const.Scale.mil
                elseif ai_player.resources.radioactive > 500 then
                    return 1 * const.Scale.mil
                end
            end
        }),
        PlaceObj("AIAction", {
            "id", "increase NuclearFuel production",
            "log_entry", T("Constructed Nuclear Enrichment Plant"),
            "base_eval", 75000000,
            "delay", 360000,
            "tags", {"mining"},
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "concrete", "amount", 10 }),
                PlaceObj("AIResourceAmount", { "resource", "polymers", "amount", 5 }),
                PlaceObj("AIResourceAmount", { "resource", "electronics", "amount", 15 }),
                PlaceObj("AIResourceAmount", { "resource", "radioactive_production", "amount", 4 }),
                PlaceObj("AIResourceAmount", { "resource", "power", "amount", 20 }),
                PlaceObj("AIResourceAmount", { "resource", "colonists_workforce", "amount", 8 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "nuclearfuel_production", "amount", 3 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
            },
            "IsAllowed", function(self, ai_player)
                return true
            end,
            "Eval", function(self, ai_player)
                if ai_player.resources.nuclearfuel_production < 1 or ai_player.resources.nuclearfuel < 10 then
                    return 250 * const.Scale.mil
                end
                if ai_player.resources.nuclearfuel < 25 then
                    return 150 * const.Scale.mil
                elseif ai_player.resources.nuclearfuel > 100 then
                    return 10 * const.Scale.mil
                elseif ai_player.resources.nuclearfuel > 500 then
                    return 1 * const.Scale.mil
                end
            end
        }),
        PlaceObj("AIAction", {
            "id", "increase nuclear Power production",
            "log_entry", T("Constructed Nuclear Power Plant"),
            "base_eval", 80000000,
            "delay", 90000,
            "tags", {"mining", "production"},
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "nuclearfuel_production", "amount", 5 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "power_production", "amount", 8 }),
                PlaceObj("AIResourceAmount", { "resource", "construction", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "buildings", "amount", 1 })
            },
            "Eval", function(self, ai_player)
                if ai_player.resources.power > 30 and ai_player.resources.buildings < 50
                        or ai_player.resources.radioactive < 25 and ai_player.resources.power > 10 then
                    return 1
                end
                if ai_player.resources.power < 50 then
                    return 250 * const.Scale.mil
                elseif ai_player.resources.power > 100 then
                    return 20 * const.Scale.mil
                elseif ai_player.resources.power > 1000 then
                    return 1 * const.Scale.mil
                end
            end
        }),
        PlaceObj("AIAction", {
            "id", "call supply Rocket (Radioactive)",
            "log_entry", T("Imported Radioactive Materials"),
            "base_eval", 150000000,
            "delay", 240000,
            "tags", { "stockpiling" },
            "required_resources", {
                PlaceObj("AIResourceAmount", { "resource", "rockets", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "funding", "amount", 240 })
            },
            "resulting_resources", {
                PlaceObj("AIResourceAmount", { "resource", "rockets_landed", "amount", 1 }),
                PlaceObj("AIResourceAmount", { "resource", "radioactive", "amount", 30 })
            },
            "IsAllowed", function(self, ai_player)
                return ai_player.resources.radioactive < 10 and g_Consts.SupplyMissionsEnabled == 1
            end
        }),
    })
end

local Initialize_MarsTradeRoute = MarsTradeRoute.Initialize
function MarsTradeRoute:Initialize(sponsor_id, ...)
    table.insert_unique(self.basic_import, "Radioactive")
    table.insert_unique(self.basic_import, "NuclearFuel")
    table.insert_unique(self.export_resource, "Radioactive")
    table.insert_unique(self.export_resource, "NuclearFuel")
    return Initialize_MarsTradeRoute(self, sponsor_id, ...)
end