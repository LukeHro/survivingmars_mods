Resources["Radioactive"] = {
    id = "Radioactive",
    name = "Radioactive",
    display_name = T("Radioactive Materials"),
    display_icon = CurrentModPath.."UI/Icons/Buildings/res_radioactive.tga",
    unit_amount = const.ResourceScale,
    color = RGB(255, 128, 0),
    entity = "LH_RadioactiveContainer",
    description = T("Underground Radioactive Materials.")
}

table.insert_unique(ResourceDescription, Resources["Radioactive"])
table.insert_unique(StockpileResourceList, "Radioactive")
table.insert_unique(CargoShuttle.storable_resources, "Radioactive")
table.insert_unique(AllResourcesList, "Radioactive")
table.insert_unique(RCTransport.storable_resources,"Radioactive")
table.insert_unique(ConstructionResourceList,"Radioactive")
table.insert_unique(SubSurfaceDeposits, "Radioactive")

function OnMsg.ClassesPostprocess()
    table.insert_unique(Presets.TechPreset["Robotics"].AutomatedStorage, PlaceObj("Effect_TechUnlockBuilding", { Building = "LH_MechanizedDepotRadioactive" }))
    BuildingTechRequirements["LH_MechanizedDepotRadioactive"] = {{ tech = "AutomatedStorage", hide = false }}
end

const.TagLookupTable["icon_Radioactive"] = "<image "..CurrentModPath.."UI/Icons/res_radioactive.tga 1300>"
const.TagLookupTable["icon_Radioactive_small"] = "<image "..CurrentModPath.."UI/Icons/res_radioactive.tga 800>"
const.TagLookupTable["icon_Radioactive_orig"] = "<image "..CurrentModPath.."UI/Icons/res_radioactive.tga>"

function StorageDepot:Getui_Radioactive()
    return T({"Radioactive<right><radioactive(Stored_Radioactive,MaxAmount_Radioactive)>", self})
end


function InfobarObj:GetRadioactiveText()
    local radioactive = GetCityResourceOverview(UICity):GetAvailableRadioactive() / const.ResourceScale
    return T({"<radioactive><icon_Radioactive_orig>", radioactive = self:FmtRes(radioactive)})
end

function InfobarObj:GetRadioactiveRollover()
    return GetCityResourceOverview(UICity):GetRadioactiveRollover()
end

function ResourceOverview:GetRadioactiveRollover()
    local ret = {self:GetBasicResourcesHeading(),
                 T(316, "<newline>"),
                 T({"Radioactive production<right><radioactive(RadioactiveProducedYesterday)>", self}),
                 T({"Radioactive consumption<right><radioactive(RadioactiveConsumedByConsumptionYesterday)>", self}),
                 T({"Radioactive maintenance<right><radioactive(RadioactiveConsumedByMaintenanceYesterday)>", self}),
                 T({"Upgrade construction<right><radioactive(RadioactiveUpgradeConstructionActual, RadioactiveUpgradeConstructionTotal)>", self})
    }
    return table.concat(ret, "<newline><left>")
end

ResourceOverview["GetRadioactiveProducedYesterday"] = function(self)
    return self:GetProducedYesterday("Radioactive")
end
ResourceOverview["GetRadioactiveGatheredYesterday"] = function(self)
    return self:GetGatheredYesterday("Radioactive")
end
ResourceOverview["GetRadioactiveConsumedByConsumptionYesterday"] = function(self)
    return self:GetConsumedByConsumptionYesterday("Radioactive")
end
ResourceOverview["GetRadioactiveConsumedByMaintenanceYesterday"] = function(self)
    return self:GetEstimatedDailyMaintenance("Radioactive")
end
ResourceOverview["GetAvailableRadioactive"] = function(self)
    return self:GetAvailable("Radioactive")
end
ResourceOverview["GetRadioactiveInConstructionSitesActual"] = function(self)
    return self:GetInConstructionSites("Radioactive", "actual")
end
ResourceOverview["GetRadioactiveInConstructionSitesTotal"] = function(self)
    return self:GetInConstructionSites("Radioactive", "total")
end
ResourceOverview["GetRadioactiveUpgradeConstructionActual"] = function(self)
    return self:GetUpgradeConstruction("Radioactive", "actual")
end
ResourceOverview["GetRadioactiveUpgradeConstructionTotal"] = function(self)
    return self:GetUpgradeConstruction("Radioactive", "total")
end

function OnMsg.ClassesPostprocess()
    table.insert_unique(FormattableResources, {text = "Radioactive Materials", value = "radioactive"})
    TFormat.radioactive = function(context_obj, value, max)
        return FormatResourceValueMaxResource(context_obj, value, max, "Radioactive")
    end
end