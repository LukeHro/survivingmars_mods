return PlaceObj('ModDef', {
	'title', "Covert Ops Enhanced",
	'description', '*** UPDATED FOR PICARD ***\n\nAdds additional options to the list of covert ops available against rival colonies:\n"Steal Resources" if successful, it takes and brings you 10% of each resource from the rival colonies up to a maximum of 50 units of each.\n"Steal unique RC vehicle" available only against rivals that have unique RC vehicle\n"Steal unique building prefab" gets back with one of the rival\'s unique prefab that you can use once, obviously, per successful mission',
	'image', "LH_CovertOps.png",
	'last_changes', "Updated for Picard",
	'id', "LH_CovertOps",
	'steam_id', "1672111646",
	'pops_desktop_uuid', "f47e90c1-fcf3-44fb-8093-87fd557d7c46",
	'pops_any_uuid', "733536ae-8fb1-41b0-9116-12f9abef9681",
	'author', "lukeh_ro",
	'version_major', 2,
	'version', 2,
	'lua_revision', 1007000,
	'saved_with_revision', 1007874,
	'code', {
		"Code/LHStealResources.lua",
		"Code/LHStealRCVehicle.lua",
		"Code/LHStealPrefab.lua",
	},
	'saved', 1632237754,
	'screenshot1', "LH_CovertOps.png",
	'TagGameplay', true,
	'TagInterface', true,
	'TagOther', true,
})