local translationIdBase = 1107040200

DefineClass.CovertOps_StealRCVehicle = {
  __parents = {"Effect"},
  properties = {
    {
      id = "standing_change",
      name = "Standing Change",
      editor = "number",
      default = 0
    },
    {
      id = "min_team_size",
      name = "Min Team Size",
      editor = "number",
      default = 1,
      min = 1
    },
    {
      id = "max_team_size",
      name = "Max Team Size",
      editor = "number",
      default = 5,
      min = 1
    },
    {
      id = "fail_chance",
      name = "Fail Chance",
      editor = "number",
      default = 0,
      min = 0,
      max = 100
    },
    {
      id = "fail_rocket_chance",
      name = "Chance to lose Rocket",
      editor = "number",
      default = 0,
      min = 0,
      max = 100
    }
  },
  Description = T({translationIdBase+1, "send <max_team_size> Officers to steal unique RC vehicle"}),
  RequiredObjClasses = false,
  ForbiddenObjClasses = false
}
function CovertOps_StealRCVehicle:Execute(map_id, obj, context)
  local op = MarsCovertOp:new({
    effect = "vehicle",
    num_crew = SessionRandom:Random(self.min_team_size, self.max_team_size),
    fail_chance = self.fail_chance,
    standing_change = self.standing_change,
    fail_rocket_chance = self.fail_rocket_chance,
    target_player = context.object,
    description = T({translationIdBase+2, "Covert operation Rocket, tasked to steal unique RC vehicle from the Colony of <rival>.", rival = context.object.display_name })
  })
  local dlg = GetDialog("PlanetaryView")
  if dlg then
    dlg:SetMode("rockets", op)
  end
end

local orig_CountRivalResource_GetNegotiationsDisabledText = CountRivalResource.GetNegotiationsDisabledText
function CountRivalResource:GetNegotiationsDisabledText(...)
  if self.Resource == "lh_rovers" then
    return T(translationIdBase+9, "No unique vehicle available") --why this not working?
  else
    return orig_CountRivalResource_GetNegotiationsDisabledText(self,...)
  end
end


local function EnableStealVehicle()		
	for _,v in pairs(RivalAIs) do
		v.resources.lh_rovers = 0
	end
	for k,v in pairs(BuildingTemplates) do
		if v.sponsor_name1 and #v.sponsor_name1>0 and v.sponsor_status1~="disabled" and v.group=="Rovers" then
			if RivalAIs[v.sponsor_name1] then
				RivalAIs[v.sponsor_name1].resources.lh_rovers = 1
			end
		end
	end
end

OnMsg.CityStart = EnableStealVehicle
OnMsg.LoadGame = EnableStealVehicle

function OnMsg.ClassesBuilt()
	local initialCovertOps = Presets.Negotiation["Covert Ops Start"]["CovertOps_Initial"]
	if initialCovertOps.lh_steal_vehicle then
		return
	end
	initialCovertOps.lh_steal_vehicle = true
	PlaceObj("OnScreenNotificationPreset", {
		group = "Rival Colonies",
		id = "CovertOpSuccess_Vehicle",
		save_in = "gagarin",
		text = T({translationIdBase+3, "Stolen RC vehicle: <vehicle>" }),
		title = T({translationIdBase+4, "Successful Covert Op" })
	})

	PlaceObj("EffectDef", {
		id = "CovertOps_StealRCVehicle",
		save_in = "gagarin",
		PlaceObj("ClassConstDef", {
		    "name", "Description",
		    "type", "translate",
		    "value", T({translationIdBase+5, "send <max_team_size> Officers to steal RC vehicle"})
		}),
  		PlaceObj("ClassConstDef", {
		    "name", "RequiredObjClasses",
		    "type", "string_list"
  		}),
		PlaceObj("ClassConstDef", {
		    "name", "ForbiddenObjClasses",
		    "type", "string_list"
		}),
  		PlaceObj("PropertyDefNumber", {
		    "id", "standing_change",
		    "name", T({ "Standing Change"}),
		    "default", 0
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "min_team_size",
		    "name", T({ "Min Team Size"}),
		    "default", 3,
		    "min", 1
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "max_team_size",
		    "name", T({ "Max Team Size" }),
		    "default", 5,
		    "min", 1
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "fail_chance",
		    "name", T({ "Fail Chance" }),
		    "default", 0,
		    "min", 0,
		    "max", 100
		}),
		PlaceObj("PropertyDefNumber", {
		    "id",
		    "fail_rocket_chance",
		    "name",
		    T({ 588185991237, "Chance to lose Rocket" }),
		    "default", 0,
		    "min", 0,
		    "max", 100
		}),
  		PlaceObj("ClassMethodDef", {
		    "name", "Execute",
		    "params", "obj, context",
		    "code", function(self, obj, context)
				local op = MarsCovertOp:new({
			        effect = "vehicle",
			        num_crew = SessionRandom:Random(self.min_team_size, self.max_team_size),
			        fail_chance = self.fail_chance,
			        standing_change = self.standing_change,
			        fail_rocket_chance = self.fail_rocket_chance,
			        target_player = context.object,
			        description = T({translationIdBase+6, "Covert operation Rocket, tasked to steal RC vehicle from the Colony of <rival>.", rival = context.object.display_name })
				})
				local dlg = GetDialog("PlanetaryView")
				if dlg then
		        	dlg:SetMode("rockets", op)
				end
			end
		})
	})		
	for i= #initialCovertOps,1,-1 do
		initialCovertOps[i+4] = initialCovertOps[i]
	end
	initialCovertOps[1] = PlaceObj("NegotiationReply", {
	    "Text", T({ 11070401, "Steal unique RC vehicle<RiskTextRCvehicle>" }),
	    "CustomOutcomeText", T({translationIdBase+7, "steal unique RC vehicle" }),
	    "CustomDisabledText", T({translationIdBase+8, "Not yet available" }),
	    "Prerequisites", { 
	    	PlaceObj("RivalHasLandedRocket", nil),
	    	PlaceObj("CountRivalResource", {
				"Resource", "lh_rovers",
				"Amount", 1
			})
		 },
	    "Action", "Steal Vehicle",
	    "Cooldown", 7200000
	})
	initialCovertOps[2] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding2", {
	        "Standing1", "hostile",
	        "Standing2", "bad",
	        "Negate", true
			})
	    },
	    "Action", "Steal Vehicle",
	    "Effects", { PlaceObj("CovertOps_StealRCVehicle", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 15
			})
		}
	})
	initialCovertOps[3] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding", {"Standing", "bad"}) },
	    "Action", "Steal Vehicle",
	    "Effects", { PlaceObj("CovertOps_StealRCvehicle", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 30
			})
		}
	})
	initialCovertOps[4] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding", {"Standing", "hostile"}) },
	    "Action", "Steal Vehicle",
	    "Effects", { PlaceObj("CovertOps_StealRCvehicle", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 50,
	        "fail_rocket_chance", 10
			})
	    }
	})
end

function DumbAIPlayer:GetRiskTextRCvehicle()
  local standing = self.resources.standing
  return IsRivalStanding(standing, "bad") and risk_failure or IsRivalStanding(standing, "hostile") and T({
    11656,
    ", very dangerous"
  }) or ""
end

local vanillaMarsCovertOpExecute = MarsCovertOp.Execute
function MarsCovertOp:Execute(rocket)
	if self.effect == "vehicle" then
		rocket.lh_mission = "vehicle"
		local roll = rocket.city:Random(100)
		if roll < self.fail_rocket_chance then
			AddOnScreenNotification("CovertOpFailedCritical", nil, {
				rival = self.target_player.display_name
			}, nil, rocket.city.map_id)
			DoneObject(rocket)
			DoneObject(self)
			return
		elseif roll < self.fail_rocket_chance + self.fail_chance then
			self:Fail(rocket.expedition.crew)
			DoneObject(self)
			return
		end	
		if (Presets.MissionSponsorPreset[1][self.target_player.sponsor].lock_name1) then
			rocket.lh_cargo = {} --TODO get rid of this and use rocket.cargo directly
			rocket.lh_cargo[#rocket.lh_cargo+1] = {
				class = (Presets.MissionSponsorPreset[1][self.target_player.sponsor].lock_name1),
				amount = 1
			}
			AddOnScreenNotification("CovertOpSuccess_Vehicle", nil, { vehicle = BuildingTemplates[Presets.MissionSponsorPreset[1][self.target_player.sponsor].lock_name1.."Building"].display_name}, nil, rocket.city.map_id)
		end
	    return
	end
	vanillaMarsCovertOpExecute(self, rocket)
end

local vanillaRocketExpeditionUnload = RocketExpedition.Unload
function RocketExpedition:Unload(cancel_expedition)
	if not cancel_expedition and self.lh_mission and self.lh_mission == "vehicle" then
        self:OpenDoor()
        self.cargo = self.lh_cargo
        local rovers = self.rovers or self:SpawnRovers()
        self.rovers = nil
        self.placement = {}
        local out = self:GetSpotPos(self:GetSpotBeginIndex("Roverout"))
        CargoTransporter.UnloadRovers(self, rovers, out)
        self.cargo = nil
        self.lh_cargo = nil
    end
    vanillaRocketExpeditionUnload(self,cancel_expedition)
end
