local translationIdBase = 1107040300

DefineClass.CovertOps_StealPrefab = {
  __parents = {"Effect"},
  properties = {
    {
      id = "standing_change",
      name = "Standing Change",
      editor = "number",
      default = 0
    },
    {
      id = "min_team_size",
      name = "Min Team Size",
      editor = "number",
      default = 1,
      min = 1
    },
    {
      id = "max_team_size",
      name = "Max Team Size",
      editor = "number",
      default = 5,
      min = 1
    },
    {
      id = "fail_chance",
      name = "Fail Chance",
      editor = "number",
      default = 0,
      min = 0,
      max = 100
    },
    {
      id = "fail_rocket_chance",
      name = "Chance to lose Rocket",
      editor = "number",
      default = 0,
      min = 0,
      max = 100
    }
  },
  Description = T({translationIdBase+1, "send <max_team_size> Officers to steal unique building prefab"}),
  RequiredObjClasses = false,
  ForbiddenObjClasses = false
}
function CovertOps_StealPrefab:Execute(map_id, obj, context)
  local op = MarsCovertOp:new({
    effect = "prefab",
    num_crew = SessionRandom:Random(self.min_team_size, self.max_team_size),
    fail_chance = self.fail_chance,
    standing_change = self.standing_change,
    fail_rocket_chance = self.fail_rocket_chance,
    target_player = context.object,
    description = T({translationIdBase+2, "Covert operation Rocket, tasked to steal unique building prefab from the Colony of <rival>.", rival = context.object.display_name })
  })
  local dlg = GetDialog("PlanetaryView")
  if dlg then
    dlg:SetMode("rockets", op)
  end
end

function OnMsg.GetAdditionalBuildingLocks(template, locks)
	if (UICity.available_prefabs[template.id] and UICity.available_prefabs[template.id] > 0) then
		locks.sponsor_required = false
	end
end

local unique_buildings = {}
function OnMsg.ModsReloaded()		
	for k,v in pairs(BuildingTemplates) do
		if v.sponsor_name1 and #v.sponsor_name1>0 and v.sponsor_status1~="disabled" and v.group~="Rovers" and v.group~="Hidden" then
			unique_buildings[v.sponsor_name1] = k
		end
	end
end

function OnMsg.ClassesBuilt()
	local initialCovertOps = Presets.Negotiation["Covert Ops Start"]["CovertOps_Initial"]
	if initialCovertOps.lh_steal_prefab then
		return
	end
	initialCovertOps.lh_steal_prefab = true
	PlaceObj("OnScreenNotificationPreset", {
		group = "Rival Colonies",
		id = "CovertOpSuccess_Prefab",
		save_in = "gagarin",
		text = T({translationIdBase+3, "Stolen prefab: <prefab>" }),
		title = T({translationIdBase+4, "Successful Covert Op" })
	})

	PlaceObj("EffectDef", {
		id = "CovertOps_StealPrefab",
		save_in = "gagarin",
		PlaceObj("ClassConstDef", {
		    "name", "Description",
		    "type", "translate",
		    "value", T({translationIdBase+5, "send <max_team_size> Officers to steal unique building prefab"})
		}),
  		PlaceObj("ClassConstDef", {
		    "name", "RequiredObjClasses",
		    "type", "string_list"
  		}),
		PlaceObj("ClassConstDef", {
		    "name", "ForbiddenObjClasses",
		    "type", "string_list"
		}),
  		PlaceObj("PropertyDefNumber", {
		    "id", "standing_change",
		    "name", T({ "Standing Change"}),
		    "default", 0
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "min_team_size",
		    "name", T({ "Min Team Size"}),
		    "default", 3,
		    "min", 1
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "max_team_size",
		    "name", T({ "Max Team Size" }),
		    "default", 5,
		    "min", 1
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "fail_chance",
		    "name", T({ "Fail Chance" }),
		    "default", 0,
		    "min", 0,
		    "max", 100
		}),
		PlaceObj("PropertyDefNumber", {
		    "id",
		    "fail_rocket_chance",
		    "name",
		    T({ 588185991237, "Chance to lose Rocket" }),
		    "default", 0,
		    "min", 0,
		    "max", 100
		}),
  		PlaceObj("ClassMethodDef", {
		    "name", "Execute",
		    "params", "obj, context",
		    "code", function(self, obj, context)
				local op = MarsCovertOp:new({
			        effect = "prefab",
			        num_crew = SessionRandom:Random(self.min_team_size, self.max_team_size),
			        fail_chance = self.fail_chance,
			        standing_change = self.standing_change,
			        fail_rocket_chance = self.fail_rocket_chance,
			        target_player = context.object,
			        description = T({translationIdBase+6, "Covert operation Rocket, tasked to steal unique building prefab from the Colony of <rival>.", rival = context.object.display_name })
				})
				local dlg = GetDialog("PlanetaryView")
				if dlg then
		        	dlg:SetMode("rockets", op)
				end
			end
		})
	})		
	for i= #initialCovertOps,1,-1 do
		initialCovertOps[i+4] = initialCovertOps[i]
	end
	initialCovertOps[1] = PlaceObj("NegotiationReply", {
	    "Text", T({ 11070401, "Steal unique building prefab<RiskTextPrefab>" }),
	    "CustomOutcomeText", T({translationIdBase+7, "steal unique building prefab" }),
	    "CustomDisabledText", T({translationIdBase+8, "Not yet available" }),
	    "Prerequisites", { PlaceObj("RivalHasLandedRocket", nil) },
	    "Action", "Steal Prefab",
	    "Cooldown", 7200000
	})
	initialCovertOps[2] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding2", {
	        "Standing1", "hostile",
	        "Standing2", "bad",
	        "Negate", true
			})
	    },
	    "Action", "Steal Prefab",
	    "Effects", { PlaceObj("CovertOps_StealPrefab", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 20
			})
		}
	})
	initialCovertOps[3] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding", {"Standing", "bad"}) },
	    "Action", "Steal Prefab",
	    "Effects", { PlaceObj("CovertOps_StealPrefab", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 35
			})
		}
	})
	initialCovertOps[4] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding", {"Standing", "hostile"}) },
	    "Action", "Steal Prefab",
	    "Effects", { PlaceObj("CovertOps_StealPrefab", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 50,
	        "fail_rocket_chance", 15
			})
	    }
	})
end

function DumbAIPlayer:GetRiskTextPrefab()
  local standing = self.resources.standing
  return IsRivalStanding(standing, "bad") and risk_failure or IsRivalStanding(standing, "hostile") and T({
    11656,
    ", very dangerous"
  }) or ""
end


local vanillaMarsCovertOpExecute = MarsCovertOp.Execute
function MarsCovertOp:Execute(rocket)
	if self.effect == "prefab" then
		rocket.lh_mission = "prefab"
		local roll = rocket.city:Random(100)
		if roll < self.fail_rocket_chance then
			AddOnScreenNotification("CovertOpFailedCritical", nil, {
				rival = self.target_player.display_name
			}, nil, rocket.city.map_id)
			DoneObject(rocket)
			DoneObject(self)
			return
		elseif roll < self.fail_rocket_chance + self.fail_chance then
			self:Fail(rocket.expedition.crew)
			DoneObject(self)
			return
		end
		if (unique_buildings[self.target_player.sponsor]) then
			rocket.lh_cargo = {} --TODO get rid of this and use rocket.cargo directly
			rocket.lh_cargo[#rocket.lh_cargo+1] = {
				class = unique_buildings[self.target_player.sponsor],
				amount = 1
			}
			AddOnScreenNotification("CovertOpSuccess_Prefab", nil, { prefab = BuildingTemplates[unique_buildings[self.target_player.sponsor]].display_name}, nil, rocket.city.map_id)
		end
	    return
	end
	vanillaMarsCovertOpExecute(self, rocket)
end

local vanillaRocketExpeditionUnload = RocketExpedition.Unload
function RocketExpedition:Unload(cancel_expedition)	
	if not cancel_expedition and self.lh_mission and self.lh_mission == "prefab" then
		self.cargo = self.lh_cargo
		local refreshBM = false
		if self.cargo and #self.cargo > 0 then
			for i = 1, #self.cargo do
				local item = self.cargo[i]
				local classdef = g_Classes[item.class]
				if BuildingTemplates[item.class] then
					refreshBM  = true
					self.city:AddPrefabs(item.class, item.amount, false)
				else
					printf("unexpected cargo type %s, ignored", item.class)
				end
			end
		end
		if refreshBM then
			RefreshXBuildMenu()
		end
		self.cargo = nil
		self.lh_cargo = nil
	end
	vanillaRocketExpeditionUnload(self,cancel_expedition)
end