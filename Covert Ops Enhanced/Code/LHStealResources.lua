local translationIdBase = 1107040100

DefineClass.CovertOps_StealResources = {
  __parents = {"Effect"},
  properties = {
    {
      id = "standing_change",
      name = "Standing Change",
      editor = "number",
      default = 0
    },
    {
      id = "min_team_size",
      name = "Min Team Size",
      editor = "number",
      default = 1,
      min = 1
    },
    {
      id = "max_team_size",
      name = "Max Team Size",
      editor = "number",
      default = 5,
      min = 1
    },
    {
      id = "fail_chance",
      name = "Fail Chance",
      editor = "number",
      default = 0,
      min = 0,
      max = 100
    },
    {
      id = "fail_rocket_chance",
      name = "Chance to lose Rocket",
      editor = "number",
      default = 0,
      min = 0,
      max = 100
    }
  },
  Description = T({translationIdBase+1, "send <max_team_size> Officers to steal resources"}),
  RequiredObjClasses = false,
  ForbiddenObjClasses = false
}
function CovertOps_StealResources:Execute(map_id, obj, context)
  local op = MarsCovertOp:new({
    effect = "resources",
    num_crew = SessionRandom:Random(self.min_team_size, self.max_team_size),
    fail_chance = self.fail_chance,
    standing_change = self.standing_change,
    fail_rocket_chance = self.fail_rocket_chance,
    target_player = context.object,
    description = T({translationIdBase+2, "Covert operation Rocket, tasked to steal resources from the Colony of <rival>.", rival = context.object.display_name })
  })
  local dlg = GetDialog("PlanetaryView")
  if dlg then
    dlg:SetMode("rockets", op)
  end
end

function OnMsg.ClassesBuilt()
	local initialCovertOps = Presets.Negotiation["Covert Ops Start"]["CovertOps_Initial"]
	if initialCovertOps.lh_steal_resources then
		return
	end
	initialCovertOps.lh_steal_resources = true
	PlaceObj("OnScreenNotificationPreset", {
		group = "Rival Colonies",
		id = "CovertOpSuccess_Resources",
		save_in = "gagarin",
		text = T({translationIdBase+3, "Stolen resources total: <number>" }),
		title = T({translationIdBase+4, "Successful Covert Op" })
	})

	PlaceObj("EffectDef", {
		id = "CovertOps_StealResources",
		save_in = "gagarin",
		PlaceObj("ClassConstDef", {
		    "name", "Description",
		    "type", "translate",
		    "value", T({translationIdBase+5, "send <max_team_size> Officers to steal resources"})
		}),
  		PlaceObj("ClassConstDef", {
		    "name", "RequiredObjClasses",
		    "type", "string_list"
  		}),
		PlaceObj("ClassConstDef", {
		    "name", "ForbiddenObjClasses",
		    "type", "string_list"
		}),
  		PlaceObj("PropertyDefNumber", {
		    "id", "standing_change",
		    "name", T({ "Standing Change"}),
		    "default", 0
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "min_team_size",
		    "name", T({ "Min Team Size"}),
		    "default", 3,
		    "min", 1
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "max_team_size",
		    "name", T({ "Max Team Size" }),
		    "default", 5,
		    "min", 1
		}),
		PlaceObj("PropertyDefNumber", {
		    "id", "fail_chance",
		    "name", T({ "Fail Chance" }),
		    "default", 0,
		    "min", 0,
		    "max", 100
		}),
		PlaceObj("PropertyDefNumber", {
		    "id",
		    "fail_rocket_chance",
		    "name",
		    T({ 588185991237, "Chance to lose Rocket" }),
		    "default", 0,
		    "min", 0,
		    "max", 100
		}),
  		PlaceObj("ClassMethodDef", {
		    "name", "Execute",
		    "params", "obj, context",
		    "code", function(self, obj, context)
				local op = MarsCovertOp:new({
			        effect = "resources",
			        num_crew = SessionRandom:Random(self.min_team_size, self.max_team_size),
			        fail_chance = self.fail_chance,
			        standing_change = self.standing_change,
			        fail_rocket_chance = self.fail_rocket_chance,
			        target_player = context.object,
			        description = T({translationIdBase+6, "Covert operation Rocket, tasked to steal resources from the Colony of <rival>.", rival = context.object.display_name })
				})
				local dlg = GetDialog("PlanetaryView")
				if dlg then
		        	dlg:SetMode("rockets", op)
				end
			end
		})
	})		
	for i= #initialCovertOps,1,-1 do
		initialCovertOps[i+4] = initialCovertOps[i]
	end
	initialCovertOps[1] = PlaceObj("NegotiationReply", {
	    "Text", T({ 11070401, "Steal resources<RiskTextResources>" }),
	    "CustomOutcomeText", T({translationIdBase+7, "steal resources" }),
	    "CustomDisabledText", T({translationIdBase+8, "Not yet available" }),
	    "Prerequisites", { PlaceObj("RivalHasLandedRocket", nil) },
	    "Action", "Steal Resources",
	    "Cooldown", 7200000
	})
	initialCovertOps[2] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding2", {
	        "Standing1", "hostile",
	        "Standing2", "bad",
	        "Negate", true
			})
	    },
	    "Action", "Steal Resources",
	    "Effects", { PlaceObj("CovertOps_StealResources", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 10
			})
		}
	})
	initialCovertOps[3] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding", {"Standing", "bad"}) },
	    "Action", "Steal Resources",
	    "Effects", { PlaceObj("CovertOps_StealResources", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 25
			})
		}
	})
	initialCovertOps[4] = PlaceObj("NegotiationOutcome", {
	    "Prerequisites", { PlaceObj("RivalIsStanding", {"Standing", "hostile"}) },
	    "Action", "Steal Resources",
	    "Effects", { PlaceObj("CovertOps_StealResources", {
	        "standing_change", -20,
	        "max_team_size", 3,
	        "fail_chance", 50,
	        "fail_rocket_chance", 5
			})
	    }
	})
end

function DumbAIPlayer:GetRiskTextResources()
  local standing = self.resources.standing
  return IsRivalStanding(standing, "bad") and risk_failure or IsRivalStanding(standing, "hostile") and T({
    11656,
    ", very dangerous"
  }) or ""
end


local resourcesToSteal = {"concrete", "food", "metals", "raremetals", "polymers", "machineparts", "electronics", "crystals", "meat", "booze", "radioactive"}
local resourcesToTransport = {"Concrete", "Food", "Metals", "PreciousMetals", "Polymers", "MachineParts", "Electronics", "Crystals", "Meat", "Booze", "Radioactive"}
local vanillaMarsCovertOpExecute = MarsCovertOp.Execute
function MarsCovertOp:Execute(rocket)
	if self.effect == "resources" then
		local roll = rocket.city:Random(100)
		if roll < self.fail_rocket_chance then
			AddOnScreenNotification("CovertOpFailedCritical", nil, {
				rival = self.target_player.display_name
			}, nil, rocket.city.map_id)
			DoneObject(rocket)
			DoneObject(self)
			return
		elseif roll < self.fail_rocket_chance + self.fail_chance then
			self:Fail(rocket.expedition.crew)
			DoneObject(self)
			return
		end
		local cargo = {}
		local total = 0
		for i, res in ipairs(resourcesToSteal) do
			local available = self.target_player.resources[res] or 0
			local stolen = MulDivRound(available, 10, 100)
			if (stolen >= 1) then
				if (stolen > 50) then stolen = 50 end
				self.target_player.resources[res] = available - stolen
				cargo[#cargo+1] = {
					class = res,
					amount = stolen
				}
				rocket:AddResource(stolen * const.ResourceScale, resourcesToTransport[i])
				total = total + stolen
			end
		end
		rocket.cargo = cargo
		AddOnScreenNotification("CovertOpSuccess_Resources", nil, { number = total }, nil, rocket.city.map_id)
	    return
	end
	vanillaMarsCovertOpExecute(self, rocket)
end
