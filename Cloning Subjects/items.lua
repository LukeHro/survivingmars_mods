return {
PlaceObj('ModItemCode', {
	'FileName', "Code/Script.lua",
}),
PlaceObj('ModItemCommanderProfilePreset', {
	SortKey = 5000,
	challenge_mod = 10,
	display_name = T(660632086534, --[[ModItemCommanderProfilePreset Default LH_cloningspecialist display_name]] "Cloning Specialist"),
	effect = T(135290754126, --[[ModItemCommanderProfilePreset Default LH_cloningspecialist effect]] "- Can clone individually selected colonists with good results\n- Bonus Tech: <color em>Cloning</color> (unlocks a Cloning Vats spire)"),
	group = "Default",
	id = "LH_cloningspecialist",
	param1 = 10,
	PlaceObj('Effect_GrantTech', {
		Field = "Breakthroughs",
		Research = "Cloning",
	}),
}),
}
