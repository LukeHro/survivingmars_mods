local translationIdBase = 1107030100

local function GetCloningVats(dome)
	for _, spire in ipairs(dome.labels.Spire or empty_table) do
		if not spire.bulldozed and not spire.destroyed and spire.allowClone then			
			return spire
		end
	end
	for connected, _ in pairs(dome.connected_domes or empty_table) do
		for _, spire in ipairs(connected.labels.Spire or empty_table) do
			if not spire.bulldozed and not spire.destroyed and spire.allowClone then			
				return spire
			end
		end
	end
	return false
end

function OnMsg.ClassesBuilt()
	local content_path = CurrentModPath
	local idx = table.find(XTemplates.ipColonist[1],"lh_cloneButton", true)
	if idx then
		table.remove(XTemplates.ipColonist[1],idx)
	end 
		XTemplates.ipColonist[1][#XTemplates.ipColonist[1]+1] = PlaceObj("XTemplateTemplate", {
			'lh_cloneButton', true,
			'Id',"lh_cloneButton",
			'__template', "InfopanelButton",
			'__condition', function(parent, context) 
				return not context.traits.Android and not context.traits.Clone and not context.traits.Child
			end,
			'RolloverText', T{translationIdBase+1, "Queue for cloning."},
			'RolloverDisabledText', T(translationIdBase+2, "Queue full."),
			'RolloverTitle', T{translationIdBase+3, "Clone"},
			'RolloverHint', T{translationIdBase+4, "<left_click> Queue <newline>"},
			'RolloverHintGamepad', T{translationIdBase+5, "<ButtonA> Queue <newline>"},
			'OnContextUpdate', function(self, context, ...)
				local cloner = context.dome and GetCloningVats(context.dome)
				self:SetVisible(cloner)
				self:SetEnabled(cloner and cloner.max_visitors > #cloner.visitors)
			end,
			'OnPress', function(self, gamepad)
				local cloner = GetCloningVats(self.context.dome)
				cloner.visitors[#cloner.visitors+1] = self.context
				RebuildInfopanel(self.context)				
				XContextWindow.OnContextUpdate(self, self.context)
			end,
			'Icon', content_path.."UI/Icons/IPButtons/lh_clone.tga"
		})
	--end
	XTemplates.customCloningVats = PlaceObj("XTemplateTemplate", {
		'customCloningVats', true,
		'Id',"customCloningVats",
		'__context_of_kind', "CloningVats",
		'__template', "InfopanelSection",
		'RolloverText', T(translationIdBase+6, "Cloning subjects."),
		'Title', T(translationIdBase+7, "Subjects"),
		'Icon', "UI/Icons/Sections/colonist.tga",
	}, {
			PlaceObj('XTemplateTemplate', {
				'__template', "sectionVisitorsList",
			}),
			PlaceObj('XTemplateTemplate', {
				'__template', "InfopanelProgress",
				'BindTo', "CloningProgress",
			})
	})	
end

function CloningVats:Init()
	self.progress = 0
	self.visitors = {}
	self.allowClone = true
	self.max_visitors = 8
end

function CloningVats:Unassign(subject, pos)
	for i=pos,#self.visitors-1 do
		self.visitors[i] = self.visitors[i+1]
	end	
	table.remove(self.visitors, #self.visitors)	
end

function CloningVats:BuildingUpdate(dt, ...)
	if self.working then
		local points = MulDivRound(self.performance, self.cloning_speed, 100)
		self.progress = self.progress + points
		if self.progress >= 1000 then
			if #self.visitors == 0 then
				local colonist_table = GenerateColonistData(self.city, "Child")
				colonist_table.dome = self.parent_dome
				colonist_table.traits["Clone"] = true
				if UICity.mystery and UICity.mystery:IsKindOf("DreamMystery") then
					colonist_table.traits["Dreamer"] = true
				end
				local colonist = Colonist:new(colonist_table)
				colonist:OnEnterDome(self.parent_dome)
				self:OnEnterUnit(colonist)
				Msg("ColonistBorn", colonist, "cloned")
				self.progress = 0
				self.parent_dome.clones_created = self.parent_dome.clones_created + 1
			else
				local chance = GetCommanderProfile("LH_cloningspecialist").param1
				local comm = g_CurrentMissionParams and g_CurrentMissionParams.idCommanderProfile
    			if comm == "LH_cloningspecialist" then 
    				chance = 100 - chance
    			end
				local subject = self.visitors[1]
				local heir = GenerateColonistData(subject.city, "Youth", "martianborn",{gender = subject.gender, entity_gender = subject.entity_gender, no_traits = "no_traits",no_specialization = true})		
				for trait_id, _ in pairs(subject.traits) do
					local trait = TraitPresets[trait_id]
					local category = trait and trait.group or false
					if category and category~="Age Group" and category~="Gender" and category~="Specialization" then
						heir.traits[trait_id] = true
					end
				end
				heir.traits["Clone"] = true
				heir.dome = subject.dome
				heir.progenitor_name = subject.progenitor_name			
				if heir.progenitor_name == "" then
					heir.progenitor_name = subject.name
				end
				subject.clone_id = subject.clone_id and (subject.clone_id + 1) or 1
				heir.clone_id = subject.clone_id
				heir.name = T{4301, "<name> <serial>", name = heir.progenitor_name, serial = heir.clone_id}
				heir.race = subject.race
				local colonist = Colonist:new(heir)											
				for i=1,#self.visitors-1 do
					self.visitors[i] = self.visitors[i+1]
				end		
				table.remove(self.visitors, #self.visitors)	
				self.progress = 0
				self.parent_dome.clones_created = self.parent_dome.clones_created + 1		
				subject.dome:RandPlaceColonist(colonist)
				Msg("ColonistBorn", colonist, "cloned")
				CreateRealTimeThread(function()
					while (not colonist or GameInitThreads[colonist]) do      
				    	Sleep(100) --unless waiting for GameInitThreads to end they are messing things being called after what I do bellow
				    end 
					if self:Random(100) < chance then
						colonist:AddTrait(subject.specialist)
					end
					if subject.specialist ~= colonist.specialist then
						colonist.inner_entity = subject.inner_entity:gsub(ColonistClasses[subject.specialist],"Colonist",1)
						colonist.infopanel_icon = subject.infopanel_icon:gsub(ColonistClasses[subject.specialist],"Colonist",1)
						colonist.pin_icon = subject.pin_icon:gsub(ColonistClasses[subject.specialist],"Colonist",1)
						colonist.ip_specialization_icon = subject.ip_specialization_icon:gsub(ColonistClasses[subject.specialist],"Colonist",1)
						colonist.pin_specialization_icon = subject.pin_specialization_icon:gsub(ColonistClasses[subject.specialist],"Colonist",1)
					else
						colonist.inner_entity = subject.inner_entity
						colonist.infopanel_icon = subject.infopanel_icon
						colonist.pin_icon = subject.pin_icon	
						colonist.ip_specialization_icon = subject.ip_specialization_icon
						colonist.pin_specialization_icon = subject.pin_specialization_icon
					end									
				end)
			end	
		end
	end	
end

